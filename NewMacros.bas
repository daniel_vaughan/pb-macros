Attribute VB_Name = "NewMacros"

Sub DocAlyse()
' Version 18.11.14
' Analyse various aspects of a document

listFile = "C:\Documents and Settings\Paul\My Documents\" _
     & "DocAlyseList.doc"

showWild = True


' prompts to count number of tests
cc = 172

myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

' Use main file for italic 'et al' count...
myTot = ActiveDocument.Range.End
Set rng = ActiveDocument.Content

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "<et al>"
  .Font.Italic = True
  .Replacement.Text = "^&!"
  .Wrap = wdFindContinue
  .MatchWildcards = True
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute Replace:=wdReplaceAll
End With
italEtAls = ActiveDocument.Range.End - myTot
If italEtAls > 0 Then WordBasic.EditUndo

' ...and superscript degree count
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[oO0]"
  .Font.Superscript = True
  .Replacement.Text = "^&!"
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With
funnyDegrees = ActiveDocument.Range.End - myTot
If funnyDegrees > 0 Then WordBasic.EditUndo


' ...and text in coloured font
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ""
  .Font.Color = wdColorAutomatic
  .Replacement.Text = "^&!"
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With
colouredText = ActiveDocument.Range.End - myTot
If colouredText > 0 Then WordBasic.EditUndo
totParas = ActiveDocument.Paragraphs.Count
If colouredText > totParas Then
  colouredText = colouredText - totParas
  colourOverflow = True
Else
  colourOverflow = False
End If

' ...and line breaks
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^11"
  .Replacement.Text = "^&!"
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With
lineBreaks = ActiveDocument.Range.End - myTot
If lineBreaks > 0 Then WordBasic.EditUndo

' ...and page breaks
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^12"
  .Replacement.Text = "^&!"
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With
pageBreaks = ActiveDocument.Range.End - myTot
If pageBreaks > 0 Then WordBasic.EditUndo

ActiveDocument.TrackRevisions = myTrack

myPrompt = "Copying text into new file - for large files, this may take some time."
Debug.Print myPrompt
StatusBar = myPrompt

Selection.WholeStory
Selection.Copy
Selection.HomeKey Unit:=wdStory
Documents.Add
' Selection.PasteAndFormat (wdFormatPlainText)
Selection.PasteSpecial DataType:=wdPasteText
' Selection.PasteAndFormat (wdPasteDefault)

Selection.HomeKey Unit:=wdStory
Set copyFile = ActiveDocument
myRslt = ""
Set rng = ActiveDocument.Content
myTot = ActiveDocument.Range.End
CR = vbCrLf: CR2 = CR & CR
TR = Chr(9) & "0zczc" & CR: sp = "     "
Selection.HomeKey Unit:=wdStory



' Ten or 10
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "<ten>"
  .Replacement.Text = "^&!"
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
myRslt = myRslt

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = " <10>[!,]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
If i + g > 0 Then myRslt = myRslt & "ten" & vbTab & Trim(Str(i)) & CR _
     & "10" & vbTab & Trim(Str(g)) & CR2

' spelt-out lower-case numbers over nine
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<[efnst][efghinorvwx]{2,4}ty"
  .Execute Replace:=wdReplaceAll
End With
a = ActiveDocument.Range.End - myTot
If a > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<ten>"
  .Execute Replace:=wdReplaceAll
End With
b = ActiveDocument.Range.End - myTot
If b > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<eleven>"
  .Execute Replace:=wdReplaceAll
End With
C = ActiveDocument.Range.End - myTot
If C > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<twelve>"
  .Execute Replace:=wdReplaceAll
End With
d = ActiveDocument.Range.End - myTot
If d > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<[efnst][efghinuorvwx]{2,4}teen>"
  .Execute Replace:=wdReplaceAll
End With
E = ActiveDocument.Range.End - myTot
If E > 0 Then WordBasic.EditUndo
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<hundred>"
  .Execute Replace:=wdReplaceAll
End With
f = ActiveDocument.Range.End - myTot

If f > 0 Then WordBasic.EditUndo
If a + b + C + d + E + f > 0 Then myRslt = myRslt & _
     "spelt-out numbers (11-999)" & vbTab & _
     Trim(Str(a + b + C + d + E + f)) & CR2

' Four-digit numbers
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[!.]<[0-9]{4}>[!,]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

' take off 20xx dates
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[!.]<20[0-9]{2}>[!,]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

' take off 13xx to 19xx dates
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[!.]<1[3-9][0-9]{2}>[!,]"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo
i = i - g - k
myRslt = myRslt

' Four figs with comma
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[!.]<[0-9],[0-9]{3}>[!,]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
myRslt = myRslt

' Four figs with hard or ordinary space
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[!.]<[0-9][^0160^32][0-9]{3}>[!,]"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo
If i + g + k > 0 Then
  myRslt = myRslt & "nnnn" & vbTab & Trim(Str(i)) & CR _
       & "n,nnn" & vbTab & Trim(Str(g)) & CR _
       & "n nnn" & vbTab & Trim(Str(k)) & CR
  If showWild = True Then myRslt = myRslt & _
       "<[0-9]{4}>" & sp _
       & "<[0-9],[0-9]{3}>" & sp _
       & "<[0-9][^0160^32][0-9]{3}>" & TR
  myRslt = myRslt & CR
End If



' Dates with 'mid' in front
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "mid [0-9]{4}"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "mid-[0-9]{4}"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "mid[0-9]{4}"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo

If i + g + k > 0 Then
  myRslt = myRslt & "mid 1900(s)" & vbTab _
       & Trim(Str(i)) & CR & "mid-1900(s)" & vbTab & Trim(Str(g)) & CR _
       & "mid1900(s)" & vbTab & Trim(Str(k)) & CR
  If showWild = True Then myRslt = myRslt & _
       "               mid[0-9]{4}" & TR
  myRslt = myRslt & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "mid [0-9]{2}[!0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "mid-[0-9]{2}[!0-9]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "mid[0-9]{2}[!0-9]"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo

If i + g + k > 0 Then
  myRslt = myRslt & "mid 90(s)" & vbTab _
       & Trim(Str(i)) & CR & "mid-90(s)" & vbTab & Trim(Str(g)) & CR _
       & "mid90(s)" & vbTab & Trim(Str(k)) & CR
  If showWild = True Then myRslt = myRslt & _
       "                   mid[0-9]{2}[!0-9]" & TR
  myRslt = myRslt & CR
End If



' Serial comma/not serial comma
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[a-zA-Z\-]@, [a-zA-Z\-]@, and "
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
myRslt = myRslt & "serial comma" & vbTab & Trim(Str(i)) & CR

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[a-zA-Z\-]@, [a-zA-Z\-]@ and "
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
myRslt = myRslt & "no serial comma" & vbTab & Trim(Str(i)) & CR
If showWild = True Then myRslt = myRslt & _
     "[a-zA-Z\-]@, [a-zA-Z\-]@, and " & sp & _
     "[a-zA-Z\-]@, [a-zA-Z\-]@ and " & TR
myRslt = myRslt & CR




' spaced en dash
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = " ^= "
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
g = i
If i > 0 Then WordBasic.EditUndo
aBit = "spaced en" & vbTab & Trim(Str(i)) & CR

' spaced hyphen
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = " - "
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
aBit = aBit & "spaced hyphen" & vbTab & Trim(Str(i)) & CR
g = g + i

' unspaced em dash
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[! ]^+[! ]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
aBit = aBit & "unspaced em" & vbTab & Trim(Str(i)) & CR
g = g + i

' spaced em dash
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = " ^+ "
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
aBit = aBit & "spaced em" & vbTab & Trim(Str(i)) & CR
g = g + i

' unspaced en dash
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[! 0-9]^=[! 0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
aBit = aBit & "unspaced en (eg east-west?)" & vbTab & Trim(Str(i)) & CR
g = g + i

' unspaced hyphen
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[0-9]-[0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
aBit = aBit & "unspaced hyphen (eg 45-67)" & vbTab & Trim(Str(i)) & CR

If i + g > 0 Then myRslt = myRslt & aBit & CR




' hard spaces
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "^s"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
g = i
If i > 0 Then
  WordBasic.EditUndo
  myRslt = myRslt & "hard spaces" & vbTab & Trim(Str(i)) & CR
End If

' hard hyphens
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "^~"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
g = g + i
If i > 0 Then
  WordBasic.EditUndo
  myRslt = myRslt & "hard hyphens" & vbTab & Trim(Str(i)) & CR
End If



' line breaks
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "^l"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
g = g + i
If i > 0 Then
  WordBasic.EditUndo
  myRslt = myRslt & "line breaks" & vbTab & Trim(Str(i)) & CR
End If

' page breaks
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "^m"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  myRslt = myRslt & "page breaks" & vbTab & Trim(Str(i)) & CR
End If
If g + i > 0 Then myRslt = myRslt & CR




' Single/double quotes
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = ChrW(8216)
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
myRslt = myRslt & "curly open single quote" & vbTab & Trim(Str(i)) & CR

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = ChrW(8220)
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
myRslt = myRslt & "curly open double quote" & vbTab & Trim(Str(i)) & CR

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = Chr(39)
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
myRslt = myRslt & "straight single quote" & vbTab & Trim(Str(i)) & CR

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = Chr(34)
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
myRslt = myRslt & "straight double quote" & vbTab & Trim(Str(i)) & CR2




' etc(.)
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<etc[!.]"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<etc."
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<etc. [A-Z]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<etc.^13"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo

If h + i + g + k > 0 Then myRslt = myRslt & "etc" & vbTab & Trim(Str(h)) & CR _
     & "etc." & vbTab & Trim(Str(i - g - k)) & CR2




' et al(.)
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<et al[!.]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
aBit = "et al (no dot)" & vbTab & Trim(Str(i)) & CR

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<et al."
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
aBit = aBit & "et al." & vbTab & Trim(Str(g)) & CR
aBit = aBit & "et al (italic, total)" & vbTab & Trim(Str(italEtAls)) & CR
If i + g + italEtAls > 0 Then myRslt = myRslt & aBit & CR




' i.e./ie
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "i.e."
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
myRslt = myRslt

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<ie>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
If i + g > 0 Then myRslt = myRslt & "ie" & vbTab & Trim(Str(g)) & CR _
     & "i.e." & vbTab & Trim(Str(i)) & CR2




' e.g./eg
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "e.g."
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
myRslt = myRslt

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<eg>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
If i + g > 0 Then myRslt = myRslt & "eg" & vbTab & Trim(Str(g)) & CR _
      & "e.g." & vbTab & Trim(Str(i)) & CR2




' Initials with surnames
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<[A-Z]. [A-Z]. [A-Z][a-z]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<[A-Z][a-z]{2,}, [A-Z]. [A-Z]. "
  .Execute Replace:=wdReplaceAll
End With
i2 = ActiveDocument.Range.End - myTot
If i2 > 0 Then WordBasic.EditUndo
aBit = "J. L. B. Matekoni" & vbTab & Trim(Str(i + i2)) & CR
g = i + i2

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<[A-Z].[A-Z]. [A-Z][a-z]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<[A-Z][a-z]{2,}, [A-Z].[A-Z]."
  .Execute Replace:=wdReplaceAll
End With
i2 = ActiveDocument.Range.End - myTot
If i2 > 0 Then WordBasic.EditUndo
aBit = aBit & "J.L.B. Matekoni" & vbTab & Trim(Str(i + i2)) & CR
g = g + i + i2

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<[A-Z] [A-Z] [A-Z][a-z]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<[A-Z][a-z]{2,}, [A-Z] [A-Z] "
  .Execute Replace:=wdReplaceAll
End With
i2 = ActiveDocument.Range.End - myTot
If i2 > 0 Then WordBasic.EditUndo
aBit = aBit & "J L B Matekoni" & vbTab & Trim(Str(i + i2)) & CR
g = g + i + i2

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<[A-Z]{2}> [A-Z][a-z]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<[A-Z][a-z]{2,}, [A-Z]{2}"
  .Execute Replace:=wdReplaceAll
End With
i2 = ActiveDocument.Range.End - myTot
If i2 > 0 Then WordBasic.EditUndo
aBit = aBit & "JLB Matekoni" & vbTab & Trim(Str(i + i2)) & _
     "   (Beware! This can be inflated by, e.g. BBC Enterprises.)" & CR
If showWild = True Then aBit = aBit & "<[A-Z]. [A-Z]. [A-Z][a-z]" _
     & sp & "<[A-Z].[A-Z]. [A-Z][a-z]" & sp & "<[A-Z] [A-Z] [A-Z][a-z]" _
     & sp & "<[A-Z]{2}> [A-Z][a-z]" & TR
If showWild = True Then aBit = aBit & "<[A-Z][a-z]{2,}, [A-Z]. [A-Z]. " _
     & sp & "<[A-Z][a-z]{2,}, [A-Z].[A-Z]." & sp & "<[A-Z][a-z]{2,}, [A-Z] [A-Z] " _
     & sp & "<[A-Z][a-z]{2,}, [A-Z]{2}" & TR
  aBit = aBit & CR
  If g + i + i2 > 0 Then myRslt = myRslt & aBit




' Convention for page numbers
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<p. [1-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<pp. [1-9]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

k = i + g
aBit = "p/pp. 123" & vbTab & Trim(Str(k)) & CR

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<p.[1-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<pp.[1-9]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

aBit = aBit & "p/pp.123" & vbTab & Trim(Str(i + g)) & CR
k = k + i + g

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<p [1-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<pp [1-9]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

aBit = aBit & "p/pp 123" & vbTab & Trim(Str(i + g)) & CR
k = k + i + g

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<p[1-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<pp[1-9]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

aBit = aBit & "p/pp123" & vbTab & Trim(Str(i + g)) & CR
If showWild = True Then aBit = aBit & _
     "                 <pp[1-9]" & TR
aBit = aBit & CR
If k + i + g > 0 Then myRslt = myRslt & aBit




' Convention for am/pm
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[1-9][ap]m"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
aBit = "2pm" & vbTab & Trim(Str(i)) & CR

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[1-9][ap].m."
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
aBit = aBit & "2p.m." & vbTab & Trim(Str(g)) & CR

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[1-9] [ap]m"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo
aBit = aBit & "2 pm" & vbTab & Trim(Str(k)) & CR

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[1-9] [ap].m."
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo
aBit = aBit & "2 p.m." & vbTab & Trim(Str(h)) & CR2

If k + i + g + h > 0 Then myRslt = myRslt & aBit




' US/UK spelling
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[bpiv]our[ ,.s]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "elling>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "elled>"
  .Execute Replace:=wdReplaceAll
End With

k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo
myRslt = myRslt & "UK spelling (appx)" & vbTab & _
     Trim(Str(i + g + k)) & CR


cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[bpiv]or[ ,.s]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "rior[ ,.s]"
  .Execute Replace:=wdReplaceAll
End With
q = ActiveDocument.Range.End - myTot
If q > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "eling>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "eled>"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo
myRslt = myRslt & "US spelling (appx)" & vbTab & _
     Trim(Str(i - q + g + k)) & CR & _
     "(For a more accurate count, please use UKUScount.)" & CR2



' US/UK punctuation
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[a-zA-Z]" & Chr(34) & "[,.]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[a-zA-Z]" & Chr(39) & "[,.]"
  .Execute Replace:=wdReplaceAll
End With
j = ActiveDocument.Range.End - myTot
If j > 0 Then
  WordBasic.EditUndo
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[a-zA-Z]" & "[,.]" & Chr(34)
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then
  WordBasic.EditUndo
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[a-zA-Z]" & "[,.]" & Chr(39)
  .Execute Replace:=wdReplaceAll
End With
m = ActiveDocument.Range.End - myTot
If m > 0 Then
  WordBasic.EditUndo
End If

If i + j + k + m > 0 Then myRslt = myRslt & "UK punctuation (appx)" & vbTab & _
     Trim(Str(i + j)) & CR & "US punctuation (appx)" & vbTab & Trim(Str(k + m)) & CR2



' is/iz
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "ise>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "ise[sd]>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "ising>"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "isation"
  .Execute Replace:=wdReplaceAll
End With
L = ActiveDocument.Range.End - myTot
If L > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[armvt]ising"
  .Execute Replace:=wdReplaceAll
End With
p = ActiveDocument.Range.End - myTot
If p > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[arvtw]ise"
  .Execute Replace:=wdReplaceAll
End With
q = ActiveDocument.Range.End - myTot
If q > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "ex[eo]rcis[ei]"
  .Execute Replace:=wdReplaceAll
End With
r = ActiveDocument.Range.End - myTot
If r > 0 Then WordBasic.EditUndo
myRslt = myRslt & "-is- (very appx)" & vbTab & _
     Trim(Str(i + g + k + L - p - q - r)) & CR



cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "ize>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "ize[sd]>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "izing>"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "ization"
  .Execute Replace:=wdReplaceAll
End With
L = ActiveDocument.Range.End - myTot
If L > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[Pp]riz[ie]"
  .Execute Replace:=wdReplaceAll
End With
p = ActiveDocument.Range.End - myTot
If p > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[Sse]@iz[ie]"
  .Execute Replace:=wdReplaceAll
End With
q = ActiveDocument.Range.End - myTot
If q > 0 Then WordBasic.EditUndo

myRslt = myRslt & "-iz- (very appx)" & vbTab & Trim(Str(i + g + k _
     + L - p - q)) & CR & "(For a more accurate count, please use IZIScount.)" & CR2




' data singular/plural
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<data is>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<data has>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<data was>"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<[Tt]his data>"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo
myRslt = myRslt
L = i + g + h + k

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<data are>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<data have>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<data were>"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<[Tt]hese data>"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo
If L + i + h + g + k > 0 Then myRslt = myRslt & "data singular" & _
     vbTab & Trim(Str(L)) & CR & "data plural" & _
     vbTab & Trim(Str(i + g + h + k)) & CR2



' past participle -rnt -elt
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "sp[oi]@lt>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "lea[np]t>"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[l ][be][ua]rnt>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[ds][wpm]elt>"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "sp[oi]@[l]@ed>"
  .Execute Replace:=wdReplaceAll
End With
p = ActiveDocument.Range.End - myTot
If p > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "lea[np]ed>"
  .Execute Replace:=wdReplaceAll
End With
q = ActiveDocument.Range.End - myTot
If q > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[l ][be][ua]rned>"
  .Execute Replace:=wdReplaceAll
End With
r = ActiveDocument.Range.End - myTot
If r > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[ds][wpm]elled>"
  .Execute Replace:=wdReplaceAll
End With
S = ActiveDocument.Range.End - myTot
If g + h + i + k + p + q + r + S > 0 Then myRslt = myRslt & _
     "-rnt -elt" & vbTab & Trim(Str(g + h + i + k)) & CR & _
     "-rned -elled" & vbTab & Trim(Str(p + q + r + S)) & CR2




' amid(st), among(st), while(st)
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<[Aa]mid>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<[Aa]mong>"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo: g = g + h

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<[Ww]hile>"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo: g = g + h


cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<[Aa]midst>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<[Aa]mongst>"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo: i = i + h

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<[Ww]hilst>"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo: i = i + h

If i + g > 0 Then
  myRslt = myRslt & "amid/among/while" & vbTab & Trim(Str(g)) & CR
  myRslt = myRslt & "amidst/amongst/whilst" & vbTab & Trim(Str(i)) & CR
  If showWild = True Then myRslt = myRslt & "[dgl]st>" & TR
  myRslt = myRslt & CR
End If




' fig/figure
aBit = ""
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<fig>[!.]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "fig" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<Fig>[!.]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "Fig" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<fig."
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "fig." & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<Fig."
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "Fig." & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<figs>[!.]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "figs" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<Figs>[!.]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "Figs" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<figs."
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "figs." & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "figure [0-9\(]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "figure" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[!.] Figure [0-9\(]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "Figure" & vbTab & Trim(Str(i)) & CR
End If
If aBit > "" Then myRslt = myRslt & aBit & CR




' Chapter/chapter
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[!.] Chapter [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "chapter [0-9]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
If i + g > 0 Then
  myRslt = myRslt & "Chapter (number)" & vbTab & Trim(Str(i)) & CR _
       & "chapter (number)" & vbTab & Trim(Str(g)) & CR
  If showWild = True Then myRslt = myRslt & "chapter [0-9]" & TR
  myRslt = myRslt & CR
End If


' Section/section
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[!.] Section [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "section [0-9]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
If i + g > 0 Then
  myRslt = myRslt & "Section (number)" & vbTab & Trim(Str(i)) & CR _
     & "section (number)" & vbTab & Trim(Str(g)) & CR
  If showWild = True Then myRslt = myRslt & "section [0-9]" & TR
  myRslt = myRslt & CR
End If


' equations
aBit = ""
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<eq [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "eq" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<eq. [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "eq." & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<eqn [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "eqn" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<Eqn [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "Eqn" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "eqns [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "eqns" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "eqs [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "eqs" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<eq \("
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "eq (n.n)" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<eq. \("
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "eq. (n.n)" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<eqn \("
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "eqn (n.n)" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "<Eqn \("
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "Eqn (n.n)" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "eqns \("
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "eqns (n.n)" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "eqs \("
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "eqs" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "equation \("
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "equation (n.n)" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[!.] Equation \("
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "Equation (n.n)" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "equations \("
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "equations (n.n)" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[!.] Equations \("
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "Equations (n.n)" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "equation [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "equation" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[!.] Equation [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "Equation" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "equations [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "equations" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[!.] Equations [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "Equations" & vbTab & Trim(Str(i)) & CR
End If
If aBit > "" Then myRslt = myRslt & aBit & CR



' units
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[0-9][^32^160][kKcmM][NgAVm]>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[0-9][^32^160][NgAVm]>"
  .Execute Replace:=wdReplaceAll
End With
j = ActiveDocument.Range.End - myTot
If j > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[0-9][kKcmM][NgAVm]>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[0-9][NgAVm]>"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo
If i + j + g + h > 0 Then
  myRslt = myRslt & "spaced units (3 mm)" & vbTab & Trim(Str(i + j)) _
     & CR & "unspaced units (3mm)" & vbTab & Trim(Str(g + h)) & CR
  If showWild = True Then myRslt = myRslt & _
       "[0-9][^32^160][kKcmM][NgAVm]>" & sp & _
       "[0-9][kKcmM][NgAVm]>" & sp & _
       "[0-9][NgAVm]>" & sp & _
       "[0-9][kKcmM][NgAVmg]>" & TR
  myRslt = myRslt & CR
End If




' Types of ellipsis
aBit = ""
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "..."
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "triple dots" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = ". . ."
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "spaced triple dots" & vbTab & Trim(Str(i)) & CR
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = ChrW(8230)
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "proper ellipsis" & vbTab & Trim(Str(i)) & CR
End If




' focus(s)
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[Ff]ocus[ei]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[Ff]ocuss[ei]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
If i + g > 0 Then myRslt = myRslt & "focus..." & vbTab & Trim(Str(i)) & CR _
     & "focuss..." & vbTab & Trim(Str(g)) & CR2



' benefit(t)
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[Bb]enefit[ei]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[Bb]enefitt[ei]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
If i + g > 0 Then myRslt = myRslt & "benefit..." & vbTab & Trim(Str(i)) & CR _
     & "benefitt..." & vbTab & Trim(Str(g)) & CR2



' co(-)oper...
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[Cc]o-op[ei]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[Cc]oop[ei]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
If i + g > 0 Then myRslt = myRslt & "co-oper..." & vbTab & Trim(Str(i)) & CR _
     & "cooper..." & vbTab & Trim(Str(g)) & CR2



' Co-ordin
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[Cc]o-ord[ei]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[Cc]oord[ei]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
If i + g > 0 Then myRslt = myRslt & "co-ord..." & vbTab & Trim(Str(i)) & CR _
     & "coord..." & vbTab & Trim(Str(g)) & CR2



' Can't, cannot can not
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[Cc]an[!a-z]t>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[Cc]annot"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[Cc]an not"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo

If i + h + g > 0 Then myRslt = myRslt & "can't" & vbTab & Trim(Str(i)) & CR _
     & "cannot" & vbTab & Trim(Str(g)) & CR _
     & "can not" & vbTab & Trim(Str(h)) & CR2



' Wasn't, isn't, hasn't etc
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[owh ][aie]sn[!a-z]t>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[owh ][aie]s not"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo

If i + h > 0 Then myRslt = myRslt & "wasn't, isn't, hasn't etc" _
     & vbTab & Trim(Str(i)) & CR _
     & "was not, is not, has not etc" & vbTab & Trim(Str(h)) & CR2



' Funny characters
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "_"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  myRslt = myRslt & "funny squared character: (_)" & vbTab & Trim(Str(i)) & CR2
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[��ˉ劀̋������������떄����Ϳ�������__��]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  myRslt = myRslt & "diacritics" & vbTab & Trim(Str(i)) & CR2
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "[����" & ChrW(171) & ChrW(187) & "]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  myRslt = myRslt & "Continental punctuation" & vbTab & Trim(Str(i)) & CR2
End If

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With rng.Find
  .Text = "�"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  myRslt = myRslt & "funny degree symbols" _
      & vbTab & Trim(Str(i + funnyDegrees)) & CR2
End If



appx = ""
If colouredText > 0 Then
  If colourOverflow = True Then appx = " (I think)"
  myRslt = myRslt & "text in coloured font" _
      & appx & vbTab & Trim(Str(colouredText - 1)) & CR2
End If



If lineBreaks > 0 Then
  myRslt = myRslt & "line breaks" _
      & vbTab & Trim(Str(i + lineBreaks)) & CR2
End If

If pageBreaks > 0 Then
  myRslt = myRslt & "page breaks" _
      & vbTab & Trim(Str(i + pageBreaks)) & CR2
End If


' Medical bits go in here





myRslt = myRslt & CR

Selection.HomeKey Unit:=wdStory
Selection.TypeText Text:=CR & myRslt & CR2
Selection.Start = 0
Selection.Font.Bold = True
Selection.ParagraphFormat.TabStops(CentimetersToPoints(5#)).Position = _
    CentimetersToPoints(5#)

' Grey out the zero lines
cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
' For Word 2004 use:
'  .Text = "[\^13]([!\^13]@)^t0"
  .Text = "^13([!^13]@)^t0"
  .Replacement.Text = "^p\1^t^="
  .Replacement.Font.Bold = False
  .Replacement.Font.Color = wdColorGray25
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

cc = cc - 1
Debug.Print "Test number " & cc
StatusBar = "Test number " & cc
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^t^=zczc"
  .Wrap = wdFindContinue
  .Replacement.Text = ""
  .Execute Replace:=wdReplaceAll
  .MatchWildcards = False
End With

Set rng = ActiveDocument.Content
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ""
  .Replacement.Text = ""
  .Execute
  .MatchWildcards = False
End With

Selection.HomeKey Unit:=wdStory
Selection.TypeText Text:="Docalyse" & vbCrLf
Selection.HomeKey Unit:=wdStory, Extend:=wdExtend
Selection.Style = ActiveDocument.Styles("Heading 1")
Selection.HomeKey Unit:=wdStory
Beep
End Sub




Sub DocAlyseForMac()
' Version 18.11.14
' Analyse various aspects of a document


useFRedit = False
showWild = True

myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

' Use main file for italic 'et al' count...
myTot = ActiveDocument.Range.End
Set rng = ActiveDocument.Content

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "<et al>"
  .Font.Italic = True
  .Replacement.Text = "^&!"
  .MatchWildcards = True
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute Replace:=wdReplaceAll
End With
italEtAls = ActiveDocument.Range.End - myTot
If italEtAls > 0 Then WordBasic.EditUndo

' ...and superscript degree count
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[oO0]"
  .Font.Superscript = True
  .Replacement.Text = "^&!"
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With
funnyDegrees = ActiveDocument.Range.End - myTot
If funnyDegrees > 0 Then WordBasic.EditUndo


' ...and text in coloured font
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ""
  .Font.Color = wdColorAutomatic
  .Replacement.Text = "^&!"
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With
colouredText = ActiveDocument.Range.End - myTot
If colouredText > 0 Then WordBasic.EditUndo
totParas = ActiveDocument.Paragraphs.Count
If colouredText > totParas Then
  colouredText = colouredText - totParas
  colourOverflow = True
Else
  colourOverflow = False
End If

' ...and line breaks
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^11"
  .Replacement.Text = "^&!"
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With
lineBreaks = ActiveDocument.Range.End - myTot
If lineBreaks > 0 Then WordBasic.EditUndo

' ...and page breaks
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^12"
  .Replacement.Text = "^&!"
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With
pageBreaks = ActiveDocument.Range.End - myTot
If pageBreaks > 0 Then WordBasic.EditUndo

ActiveDocument.TrackRevisions = myTrack

Selection.WholeStory
Selection.Copy
Selection.HomeKey Unit:=wdStory
Documents.Add
' Selection.PasteAndFormat (wdFormatPlainText)
Selection.PasteSpecial DataType:=wdPasteText
MsgBox ("Wait for Word to catch up!")
Selection.HomeKey Unit:=wdStory
Set copyFile = ActiveDocument
myRslt = ""
Set rng = ActiveDocument.Content
myTot = ActiveDocument.Range.End
CR = vbCrLf: CR2 = CR & CR
TR = Chr(9) & "0zczc" & CR: sp = "     "
Selection.HomeKey Unit:=wdStory



' Ten or 10
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "<ten>"
  .Replacement.Text = "^&!"
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
myRslt = myRslt

With rng.Find
  .Text = " <10>[!,]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
If i + g > 0 Then myRslt = myRslt & "ten" & vbTab & Trim(Str(i)) & CR _
     & "10" & vbTab & Trim(Str(g)) & CR2

' spelt-out lower-case numbers over nine
With rng.Find
  .Text = "<[efnst][efghinorvwx]{2,4}ty"
  .Execute Replace:=wdReplaceAll
End With
a = ActiveDocument.Range.End - myTot
If a > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<ten>"
  .Execute Replace:=wdReplaceAll
End With
b = ActiveDocument.Range.End - myTot
If b > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<eleven>"
  .Execute Replace:=wdReplaceAll
End With
C = ActiveDocument.Range.End - myTot
If C > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<twelve>"
  .Execute Replace:=wdReplaceAll
End With
d = ActiveDocument.Range.End - myTot
If d > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<[efnst][efghinuorvwx]{2,4}teen>"
  .Execute Replace:=wdReplaceAll
End With
E = ActiveDocument.Range.End - myTot
If E > 0 Then WordBasic.EditUndo
With rng.Find
  .Text = "<hundred>"
  .Execute Replace:=wdReplaceAll
End With
f = ActiveDocument.Range.End - myTot

If f > 0 Then WordBasic.EditUndo
If a + b + C + d + E + f > 0 Then myRslt = myRslt & _
     "spelt-out numbers (11-999)" & vbTab & _
     Trim(Str(a + b + C + d + E + f)) & CR2

' Four-digit numbers
With rng.Find
  .Text = "[!.]<[0-9]{4}>[!,]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

' take off 20xx dates
With rng.Find
  .Text = "[!.]<20[0-9]{2}>[!,]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

' take off 13xx to 19xx dates
With rng.Find
  .Text = "[!.]<1[3-9][0-9]{2}>[!,]"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo
i = i - g - k
myRslt = myRslt

' Four figs with comma
With rng.Find
  .Text = "[!.]<[0-9],[0-9]{3}>[!,]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
myRslt = myRslt

' Four figs with hard or ordinary space
With rng.Find
  .Text = "[!.]<[0-9][^0160^32][0-9]{3}>[!,]"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo
If i + g + k > 0 Then
  myRslt = myRslt & "nnnn" & vbTab & Trim(Str(i)) & CR _
       & "n,nnn" & vbTab & Trim(Str(g)) & CR _
       & "n nnn" & vbTab & Trim(Str(k)) & CR
  If showWild = True Then myRslt = myRslt & _
       "<[0-9]{4}>" & sp _
       & "<[0-9],[0-9]{3}>" & sp _
       & "<[0-9][^0160^32][0-9]{3}>" & TR
  myRslt = myRslt & CR
End If



' Dates with 'mid' in front
With rng.Find
  .Text = "mid [0-9]{4}"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "mid-[0-9]{4}"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "mid[0-9]{4}"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo

If i + g + k > 0 Then
  myRslt = myRslt & "mid 1900(s)" & vbTab _
       & Trim(Str(i)) & CR & "mid-1900(s)" & vbTab & Trim(Str(g)) & CR _
       & "mid1900(s)" & vbTab & Trim(Str(k)) & CR
  If showWild = True Then myRslt = myRslt & _
       "               mid[0-9]{4}" & TR
  myRslt = myRslt & CR
End If

With rng.Find
  .Text = "mid [0-9]{2}[!0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "mid-[0-9]{2}[!0-9]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "mid[0-9]{2}[!0-9]"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo

If i + g + k > 0 Then
  myRslt = myRslt & "mid 90(s)" & vbTab _
       & Trim(Str(i)) & CR & "mid-90(s)" & vbTab & Trim(Str(g)) & CR _
       & "mid90(s)" & vbTab & Trim(Str(k)) & CR
  If showWild = True Then myRslt = myRslt & _
       "                   mid[0-9]{2}[!0-9]" & TR
  myRslt = myRslt & CR
End If



' Serial comma/not serial comma
With rng.Find
  .Text = "[a-zA-Z\-]@, [a-zA-Z\-]@, and "
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
myRslt = myRslt & "serial comma" & vbTab & Trim(Str(i)) & CR

With rng.Find
  .Text = "[a-zA-Z\-]@, [a-zA-Z\-]@ and "
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
myRslt = myRslt & "no serial comma" & vbTab & Trim(Str(i)) & CR
If showWild = True Then myRslt = myRslt & _
     "[a-zA-Z\-]@, [a-zA-Z\-]@, and " & sp & _
     "[a-zA-Z\-]@, [a-zA-Z\-]@ and " & TR
myRslt = myRslt & CR




' spaced en dash
With rng.Find
  .Text = " ^= "
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
g = i
If i > 0 Then WordBasic.EditUndo
aBit = "spaced en" & vbTab & Trim(Str(i)) & CR

' spaced hyphen
With rng.Find
  .Text = " - "
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
aBit = aBit & "spaced hyphen" & vbTab & Trim(Str(i)) & CR
g = g + i

' unspaced em dash
With rng.Find
  .Text = "[! ]^+[! ]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
aBit = aBit & "unspaced em" & vbTab & Trim(Str(i)) & CR
g = g + i

' spaced em dash
With rng.Find
  .Text = " ^+ "
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
aBit = aBit & "spaced em" & vbTab & Trim(Str(i)) & CR
g = g + i

' unspaced en dash
With rng.Find
  .Text = "[! 0-9]^=[! 0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
aBit = aBit & "unspaced en (eg east�west?)" & vbTab & Trim(Str(i)) & CR
g = g + i

' unspaced hyphen
With rng.Find
  .Text = "[0-9]-[0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
aBit = aBit & "unspaced hyphen (eg 45-67)" & vbTab & Trim(Str(i)) & CR

If i + g > 0 Then myRslt = myRslt & aBit & CR




' hard spaces
With rng.Find
  .Text = "^s"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
g = i
If i > 0 Then
  WordBasic.EditUndo
  myRslt = myRslt & "hard spaces" & vbTab & Trim(Str(i)) & CR
End If

' hard hyphens
With rng.Find
  .Text = "^~"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
g = g + i
If i > 0 Then
  WordBasic.EditUndo
  myRslt = myRslt & "hard hyphens" & vbTab & Trim(Str(i)) & CR
End If



' line breaks
With rng.Find
  .Text = "^l"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
g = g + i
If i > 0 Then
  WordBasic.EditUndo
  myRslt = myRslt & "line breaks" & vbTab & Trim(Str(i)) & CR
End If

' page breaks
With rng.Find
  .Text = "^m"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  myRslt = myRslt & "page breaks" & vbTab & Trim(Str(i)) & CR
End If
If g + i > 0 Then myRslt = myRslt & CR




' Single/double quotes
With rng.Find
  .Text = ChrW(8216)
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
myRslt = myRslt & "curly open single quote" & vbTab & Trim(Str(i)) & CR

With rng.Find
  .Text = ChrW(8220)
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
myRslt = myRslt & "curly open double quote" & vbTab & Trim(Str(i)) & CR

With rng.Find
  .Text = Chr(39)
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
myRslt = myRslt & "straight single quote" & vbTab & Trim(Str(i)) & CR

With rng.Find
  .Text = Chr(34)
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
myRslt = myRslt & "straight double quote" & vbTab & Trim(Str(i)) & CR2




' etc(.)
With rng.Find
  .Text = "<etc[!.]"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<etc."
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<etc. [A-Z]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<etc.^13"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo

If h + i + g + k > 0 Then myRslt = myRslt & "etc" & vbTab & Trim(Str(h)) & CR _
     & "etc." & vbTab & Trim(Str(i - g - k)) & CR2




' et al(.)
With rng.Find
  .Text = "<et al[!.]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
aBit = "et al (no dot)" & vbTab & Trim(Str(i)) & CR

With rng.Find
  .Text = "<et al."
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
aBit = aBit & "et al." & vbTab & Trim(Str(g)) & CR
aBit = aBit & "et al (italic, total)" & vbTab & Trim(Str(italEtAls)) & CR
If i + g + italEtAls > 0 Then myRslt = myRslt & aBit & CR




' i.e./ie
With rng.Find
  .Text = "i.e."
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
myRslt = myRslt

With rng.Find
  .Text = "<ie>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
If i + g > 0 Then myRslt = myRslt & "ie" & vbTab & Trim(Str(g)) & CR _
     & "i.e." & vbTab & Trim(Str(i)) & CR2




' e.g./eg
With rng.Find
  .Text = "e.g."
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo
myRslt = myRslt

With rng.Find
  .Text = "<eg>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
If i + g > 0 Then myRslt = myRslt & "eg" & vbTab & Trim(Str(g)) & CR _
      & "e.g." & vbTab & Trim(Str(i)) & CR2




' Initials with surnames
With rng.Find
  .Text = "<[A-Z]. [A-Z]. [A-Z][a-z]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<[A-Z][a-z]{2,}, [A-Z]. [A-Z]. "
  .Execute Replace:=wdReplaceAll
End With
i2 = ActiveDocument.Range.End - myTot
If i2 > 0 Then WordBasic.EditUndo
aBit = "J. L. B. Matekoni" & vbTab & Trim(Str(i + i2)) & CR
g = i + i2

With rng.Find
  .Text = "<[A-Z].[A-Z]. [A-Z][a-z]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<[A-Z][a-z]{2,}, [A-Z].[A-Z]."
  .Execute Replace:=wdReplaceAll
End With
i2 = ActiveDocument.Range.End - myTot
If i2 > 0 Then WordBasic.EditUndo
aBit = aBit & "J.L.B. Matekoni" & vbTab & Trim(Str(i + i2)) & CR
g = g + i + i2

With rng.Find
  .Text = "<[A-Z] [A-Z] [A-Z][a-z]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<[A-Z][a-z]{2,}, [A-Z] [A-Z] "
  .Execute Replace:=wdReplaceAll
End With
i2 = ActiveDocument.Range.End - myTot
If i2 > 0 Then WordBasic.EditUndo
aBit = aBit & "J L B Matekoni" & vbTab & Trim(Str(i + i2)) & CR
g = g + i + i2

With rng.Find
  .Text = "<[A-Z]{2}> [A-Z][a-z]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<[A-Z][a-z]{2,}, [A-Z]{2}"
  .Execute Replace:=wdReplaceAll
End With
i2 = ActiveDocument.Range.End - myTot
If i2 > 0 Then WordBasic.EditUndo
aBit = aBit & "JLB Matekoni" & vbTab & Trim(Str(i + i2)) & _
     "   (Beware! This can be inflated by, e.g. BBC Enterprises.)" & CR
If showWild = True Then aBit = aBit & "<[A-Z]. [A-Z]. [A-Z][a-z]" _
     & sp & "<[A-Z].[A-Z]. [A-Z][a-z]" & sp & "<[A-Z] [A-Z] [A-Z][a-z]" _
     & sp & "<[A-Z]{2}> [A-Z][a-z]" & TR
If showWild = True Then aBit = aBit & "<[A-Z][a-z]{2,}, [A-Z]. [A-Z]. " _
     & sp & "<[A-Z][a-z]{2,}, [A-Z].[A-Z]." & sp & "<[A-Z][a-z]{2,}, [A-Z] [A-Z] " _
     & sp & "<[A-Z][a-z]{2,}, [A-Z]{2}" & TR
  aBit = aBit & CR
  If g + i + i2 > 0 Then myRslt = myRslt & aBit


' Convention for page numbers
With rng.Find
  .Text = "<p. [1-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<pp. [1-9]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

k = i + g
aBit = "p/pp. 123" & vbTab & Trim(Str(k)) & CR

With rng.Find
  .Text = "<p.[1-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<pp.[1-9]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

aBit = aBit & "p/pp.123" & vbTab & Trim(Str(i + g)) & CR
k = k + i + g

With rng.Find
  .Text = "<p [1-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<pp [1-9]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

aBit = aBit & "p/pp 123" & vbTab & Trim(Str(i + g)) & CR
k = k + i + g

With rng.Find
  .Text = "<p[1-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<pp[1-9]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

aBit = aBit & "p/pp123" & vbTab & Trim(Str(i + g)) & CR
If showWild = True Then aBit = aBit & _
     "                 <pp[1-9]" & TR
aBit = aBit & CR
If k + i + g > 0 Then myRslt = myRslt & aBit




' US/UK spelling
With rng.Find
  .Text = "[bpiv]our[ ,.s]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "elling>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "elled>"
  .Execute Replace:=wdReplaceAll
End With

k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo
myRslt = myRslt & "UK spelling (appx)" & vbTab & _
     Trim(Str(i + g + k)) & CR


With rng.Find
  .Text = "[bpiv]or[ ,.s]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "rior[ ,.s]"
  .Execute Replace:=wdReplaceAll
End With
q = ActiveDocument.Range.End - myTot
If q > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "eling>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "eled>"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo
myRslt = myRslt & "US spelling (appx)" & vbTab & _
     Trim(Str(i - q + g + k)) & CR & _
     "(For a more accurate count, please use UKUScount.)" & CR2



' US/UK punctuation
With rng.Find
  .Text = "[a-zA-Z]" & Chr(34) & "[,.]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
End If

With rng.Find
  .Text = "[a-zA-Z]" & Chr(39) & "[,.]"
  .Execute Replace:=wdReplaceAll
End With
j = ActiveDocument.Range.End - myTot
If j > 0 Then
  WordBasic.EditUndo
End If

With rng.Find
  .Text = "[a-zA-Z]" & "[,.]" & Chr(34)
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then
  WordBasic.EditUndo
End If

With rng.Find
  .Text = "[a-zA-Z]" & "[,.]" & Chr(39)
  .Execute Replace:=wdReplaceAll
End With
m = ActiveDocument.Range.End - myTot
If m > 0 Then
  WordBasic.EditUndo
End If

If i + j + k + m > 0 Then myRslt = myRslt & "UK punctuation (appx)" & vbTab & _
     Trim(Str(i + j)) & CR & "US punctuation (appx)" & vbTab & Trim(Str(k + m)) & CR2



' is/iz
With rng.Find
  .Text = "ise>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "ise[sd]>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "ising>"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "isation"
  .Execute Replace:=wdReplaceAll
End With
L = ActiveDocument.Range.End - myTot
If L > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[armvt]ising"
  .Execute Replace:=wdReplaceAll
End With
p = ActiveDocument.Range.End - myTot
If p > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[arvtw]ise"
  .Execute Replace:=wdReplaceAll
End With
q = ActiveDocument.Range.End - myTot
If q > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "ex[eo]rcis[ei]"
  .Execute Replace:=wdReplaceAll
End With
r = ActiveDocument.Range.End - myTot
If r > 0 Then WordBasic.EditUndo
myRslt = myRslt & "-is- (very appx)" & vbTab & _
     Trim(Str(i + g + k + L - p - q - r)) & CR



With rng.Find
  .Text = "ize>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "ize[sd]>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "izing>"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "ization"
  .Execute Replace:=wdReplaceAll
End With
L = ActiveDocument.Range.End - myTot
If L > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[Pp]riz[ie]"
  .Execute Replace:=wdReplaceAll
End With
p = ActiveDocument.Range.End - myTot
If p > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[Sse]@iz[ie]"
  .Execute Replace:=wdReplaceAll
End With
q = ActiveDocument.Range.End - myTot
If q > 0 Then WordBasic.EditUndo

myRslt = myRslt & "-iz- (very appx)" & vbTab & Trim(Str(i + g + k _
     + L - p - q)) & CR & "(For a more accurate count, please use IZIScount.)" & CR2




' data singular/plural
With rng.Find
  .Text = "<data is>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<data has>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<data was>"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<[Tt]his data>"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo
myRslt = myRslt
L = i + g + h + k

With rng.Find
  .Text = "<data are>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<data have>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<data were>"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<[Tt]hese data>"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo
If L + i + h + g + k > 0 Then myRslt = myRslt & "data singular" & _
     vbTab & Trim(Str(L)) & CR & "data plural" & _
     vbTab & Trim(Str(i + g + h + k)) & CR2



' past participle -rnt -elt
With rng.Find
  .Text = "sp[oi]@lt>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "lea[np]t>"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[l ][be][ua]rnt>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[ds][wpm]elt>"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "sp[oi]@[l]@ed>"
  .Execute Replace:=wdReplaceAll
End With
p = ActiveDocument.Range.End - myTot
If p > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "lea[np]ed>"
  .Execute Replace:=wdReplaceAll
End With
q = ActiveDocument.Range.End - myTot
If q > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[l ][be][ua]rned>"
  .Execute Replace:=wdReplaceAll
End With
r = ActiveDocument.Range.End - myTot
If r > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[ds][wpm]elled>"
  .Execute Replace:=wdReplaceAll
End With
S = ActiveDocument.Range.End - myTot
If g + h + i + k + p + q + r + S > 0 Then myRslt = myRslt & _
     "-rnt -elt" & vbTab & Trim(Str(g + h + i + k)) & CR & _
     "-rned -elled" & vbTab & Trim(Str(p + q + r + S)) & CR2




' amid(st), among(st), while(st)
With rng.Find
  .Text = "<[Aa]mid>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<[Aa]mong>"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo: g = g + h

With rng.Find
  .Text = "<[Ww]hile>"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo: g = g + h


With rng.Find
  .Text = "<[Aa]midst>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<[Aa]mongst>"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo: i = i + h

With rng.Find
  .Text = "<[Ww]hilst>"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo: i = i + h

If i + g > 0 Then
  myRslt = myRslt & "amid/among/while" & vbTab & Trim(Str(g)) & CR
  myRslt = myRslt & "amidst/amongst/whilst" & vbTab & Trim(Str(i)) & CR
  If showWild = True Then myRslt = myRslt & "[dgl]st>" & TR
  myRslt = myRslt & CR
End If




' fig/figure
aBit = ""
With rng.Find
  .Text = "<fig>[!.]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "fig" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "<Fig>[!.]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "Fig" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "<fig."
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "fig." & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "<Fig."
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "Fig." & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "<figs>[!.]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "figs" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "<Figs>[!.]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "Figs" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "<figs."
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "figs." & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "figure [0-9\(]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "figure" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "[!.] Figure [0-9\(]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "Figure" & vbTab & Trim(Str(i)) & CR
End If
If aBit > "" Then myRslt = myRslt & aBit & CR




' Chapter/chapter
With rng.Find
  .Text = "[!.] Chapter [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "chapter [0-9]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
If i + g > 0 Then
  myRslt = myRslt & "Chapter (number)" & vbTab & Trim(Str(i)) & CR _
       & "chapter (number)" & vbTab & Trim(Str(g)) & CR
  If showWild = True Then myRslt = myRslt & "chapter [0-9]" & TR
  myRslt = myRslt & CR
End If


' Section/section
With rng.Find
  .Text = "[!.] Section [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "section [0-9]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
If i + g > 0 Then
  myRslt = myRslt & "Section (number)" & vbTab & Trim(Str(i)) & CR _
     & "section (number)" & vbTab & Trim(Str(g)) & CR
  If showWild = True Then myRslt = myRslt & "section [0-9]" & TR
  myRslt = myRslt & CR
End If


' equations
aBit = ""
With rng.Find
  .Text = "<eq [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "eq" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "<eq. [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "eq." & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "<eqn [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "eqn" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "<Eqn [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "Eqn" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "eqns [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "eqns" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "eqs [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "eqs" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "<eq \("
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "eq (n.n)" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "<eq. \("
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "eq. (n.n)" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "<eqn \("
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "eqn (n.n)" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "<Eqn \("
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "Eqn (n.n)" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "eqns \("
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "eqns (n.n)" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "eqs \("
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "eqs" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "equation \("
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "equation (n.n)" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "[!.] Equation \("
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "Equation (n.n)" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "equations \("
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "equations (n.n)" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "[!.] Equations \("
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "Equations (n.n)" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "equation [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "equation" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "[!.] Equation [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "Equation" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "equations [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "equations" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = "[!.] Equations [0-9]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "Equations" & vbTab & Trim(Str(i)) & CR
End If
If aBit > "" Then myRslt = myRslt & aBit & CR



' units
With rng.Find
  .Text = "[0-9][^32^160][kKcmM][NgAVm]>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[0-9][^32^160][NgAVm]>"
  .Execute Replace:=wdReplaceAll
End With
j = ActiveDocument.Range.End - myTot
If j > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[0-9][kKcmM][NgAVm]>"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[0-9][NgAVm]>"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo
If i + j + g + h > 0 Then
  myRslt = myRslt & "spaced units (3 mm)" & vbTab & Trim(Str(i + j)) _
     & CR & "unspaced units (3mm)" & vbTab & Trim(Str(g + h)) & CR
  If showWild = True Then myRslt = myRslt & _
       "[0-9][^32^160][kKcmM][NgAVm]>" & sp & _
       "[0-9][kKcmM][NgAVm]>" & sp & _
       "[0-9][NgAVm]>" & sp & _
       "[0-9][kKcmM][NgAVmg]>" & TR
  myRslt = myRslt & CR
End If




' Types of ellipsis
aBit = ""
With rng.Find
  .Text = "..."
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "triple dots" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = ". . ."
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "spaced triple dots" & vbTab & Trim(Str(i)) & CR
End If

With rng.Find
  .Text = ChrW(8230)
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  aBit = aBit & "proper ellipsis" & vbTab & Trim(Str(i)) & CR
End If




' focus(s)
With rng.Find
  .Text = "[Ff]ocus[ei]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[Ff]ocuss[ei]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
If i + g > 0 Then myRslt = myRslt & "focus..." & vbTab & Trim(Str(i)) & CR _
     & "focuss..." & vbTab & Trim(Str(g)) & CR2



' benefit(t)
With rng.Find
  .Text = "[Bb]enefit[ei]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[Bb]enefitt[ei]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
If i + g > 0 Then myRslt = myRslt & "benefit..." & vbTab & Trim(Str(i)) & CR _
     & "benefitt..." & vbTab & Trim(Str(g)) & CR2



' co(-)oper...
With rng.Find
  .Text = "[Cc]o-op[ei]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[Cc]oop[ei]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
If i + g > 0 Then myRslt = myRslt & "co-oper..." & vbTab & Trim(Str(i)) & CR _
     & "cooper..." & vbTab & Trim(Str(g)) & CR2



' Co-ordin
With rng.Find
  .Text = "[Cc]o-ord[ei]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[Cc]oord[ei]"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo
If i + g > 0 Then myRslt = myRslt & "co-ord..." & vbTab & Trim(Str(i)) & CR _
     & "coord..." & vbTab & Trim(Str(g)) & CR2



' Can't, cannot can not
With rng.Find
  .Text = "[Cc]an[!a-z]t>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[Cc]annot"
  .Execute Replace:=wdReplaceAll
End With
g = ActiveDocument.Range.End - myTot
If g > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[Cc]an not"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo

If i + h + g > 0 Then myRslt = myRslt & "can't" & vbTab & Trim(Str(i)) & CR _
     & "cannot" & vbTab & Trim(Str(g)) & CR _
     & "can not" & vbTab & Trim(Str(h)) & CR2



' Wasn't, isn't, hasn't etc
With rng.Find
  .Text = "[owh ][aie]sn[!a-z]t>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[owh ][aie]s not"
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo

If i + h > 0 Then myRslt = myRslt & "wasn't, isn't, hasn't etc" _
     & vbTab & Trim(Str(i)) & CR _
     & "was not, is not, has not etc" & vbTab & Trim(Str(h)) & CR2



' Funny characters
With rng.Find
  .Text = "_"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  myRslt = myRslt & "funny squared character: (_)" & vbTab & Trim(Str(i)) & CR2
End If

With rng.Find
  .Text = "[��������������������������_��]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  myRslt = myRslt & "diacritics" & vbTab & Trim(Str(i)) & CR2
End If

With rng.Find
  .Text = "[����" & ChrW(171) & ChrW(187) & "]"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  myRslt = myRslt & "Continental punctuation" & vbTab & Trim(Str(i)) & CR2
End If

With rng.Find
  .Text = "�"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then
  WordBasic.EditUndo
  myRslt = myRslt & "funny degree symbols" _
      & vbTab & Trim(Str(i + funnyDegrees)) & CR2
End If



appx = ""
If colouredText > 0 Then
  If colourOverflow = True Then appx = " (I think)"
  myRslt = myRslt & "text in coloured font" _
      & appx & vbTab & Trim(Str(colouredText - 1)) & CR2
End If



If lineBreaks > 0 Then
  myRslt = myRslt & "line breaks" _
      & vbTab & Trim(Str(i + lineBreaks)) & CR2
End If

If pageBreaks > 0 Then
  myRslt = myRslt & "page breaks" _
      & vbTab & Trim(Str(i + pageBreaks)) & CR2
End If


myRslt = myRslt & CR

Selection.HomeKey Unit:=wdStory
Selection.TypeText Text:=CR & myRslt & CR2
Selection.Start = 0
Selection.Font.Bold = True
Selection.ParagraphFormat.TabStops(CentimetersToPoints(5#)).Position = _
    CentimetersToPoints(5#)

' Grey out the zero lines
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
' For Word 2004 use:
'  .Text = "[\^13]([!\^13]@)^t0"
  .Text = "^13([!^13]@)^t0"
  .Replacement.Text = "^p\1^t^="
  .Replacement.Font.Bold = False
  .Replacement.Font.Color = wdColorGray25
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^t^=zczc"
  .Replacement.Text = ""
  .Execute Replace:=wdReplaceAll
  .MatchWildcards = False
End With

Set rng = ActiveDocument.Content
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ""
  .Replacement.Text = ""
  .Execute
  .MatchWildcards = False
End With

Selection.HomeKey Unit:=wdStory
Selection.TypeText Text:="Docalyse" & vbCrLf
Selection.HomeKey Unit:=wdStory, Extend:=wdExtend
Selection.Style = ActiveDocument.Styles("Heading 1")
Selection.HomeKey Unit:=wdStory
Beep
End Sub



Sub DocAlyseThiersBits()
Version 28.11.11
' Thiers Halliwell�s medical bits:

' bd, bds, bid b.i.d
With rng.Find
  .Text = "[Bb][Dd]>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[Bb][Dd][Ss]>"
  .Execute Replace:=wdReplaceAll
End With
j = ActiveDocument.Range.End - myTot
If j > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[Bb][Ii][Dd]>"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[Bb].[Ii].[Dd]>"
  .Execute Replace:=wdReplaceAll
End With
L = ActiveDocument.Range.End - myTot
If L > 0 Then WordBasic.EditUndo

If i + j + k + L > 0 Then myRslt = myRslt & "bd" & _
     vbTab & Trim(Str(i)) & CR & "bds" & vbTab & _
     Trim(Str(j)) & CR & "bid (?word or abbr.)" & _
     vbTab & Trim(Str(k)) & CR & "b.i.d" & vbTab _
     & Trim(Str(L)) & CR2


'tds, tid, t.i.d
With rng.Find
  .Text = "[Tt][Dd][Ss]>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[Tt][Ii][Dd]>"
  .Execute Replace:=wdReplaceAll
End With
j = ActiveDocument.Range.End - myTot
If j > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[Tt].[Ii].[Dd]>"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo

If i + j + k > 0 Then myRslt = myRslt & "tds" & vbTab _
     & Trim(Str(i)) & CR & "tid" & vbTab & Trim(Str(j)) _
     & CR & "t.i.d" & vbTab & Trim(Str(k)) & CR2

'qds, qid, q.i.d
With rng.Find
  .Text = "[Qq][Dd][Ss]>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[Qq][Ii][Dd]>"
  .Execute Replace:=wdReplaceAll
End With
j = ActiveDocument.Range.End - myTot
If j > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "[Qq].[Ii].[Dd]>"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo

If i + j + k > 0 Then myRslt = myRslt & "qds" & vbTab _
     & Trim(Str(i)) & CR & "qid" & vbTab & Trim(Str(j)) _
     & CR & "q.i.d" & vbTab & Trim(Str(k)) & CR2

'#hrly
With rng.Find
  .Text = "[0-9]hrly>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

'#[ -]hrly
With rng.Find
  .Text = "[0-9][ -]hrly>"
  .Execute Replace:=wdReplaceAll
End With
j = ActiveDocument.Range.End - myTot
If j > 0 Then WordBasic.EditUndo

'q#h
With rng.Find
  .Text = "[Qq][0-9][Hh]>"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo

'qqh
With rng.Find
  .Text = "[Qq][Qq][Hh]>"
  .Execute Replace:=wdReplaceAll
End With
L = ActiveDocument.Range.End - myTot
If L > 0 Then WordBasic.EditUndo

If i + j + k + L > 0 Then myRslt = myRslt & "#hrly" _
     & vbTab & Trim(Str(i)) & CR & "# hrly" & vbTab _
     & Trim(Str(j)) & CR & "q#h" & vbTab & Trim(Str(k)) & CR _
     & "qqh" & vbTab & Trim(Str(L)) & CR2

'prn
With rng.Find
  .Text = "[Pp][Rr][Nn]>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

'p.r.n
With rng.Find
  .Text = "[Pp].[Rr].[Nn]>"
  .Execute Replace:=wdReplaceAll
End With
j = ActiveDocument.Range.End - myTot
If j > 0 Then WordBasic.EditUndo

'sos
With rng.Find
  .Text = "[Ss][Oo][Ss]>"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo

's.o.s
With rng.Find
  .Text = "[Ss].[Oo].[Ss]>"
  .Execute Replace:=wdReplaceAll
End With
L = ActiveDocument.Range.End - myTot
If L > 0 Then WordBasic.EditUndo

If i + j + k + L > 0 Then myRslt = myRslt & "prn" & vbTab _
     & Trim(Str(i)) & CR & "p.r.n" & vbTab & Trim(Str(j)) _
     & CR & "sos" & vbTab & Trim(Str(k)) & CR _
     & "s.o.s" & vbTab & Trim(Str(L)) & CR2

'IV / i.v.
With rng.Find
  .Text = "<iv>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<i.v>"
  .Execute Replace:=wdReplaceAll
End With
j = ActiveDocument.Range.End - myTot
If j > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<IV>"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<I.V>"
  .Execute Replace:=wdReplaceAll
End With
L = ActiveDocument.Range.End - myTot
If L > 0 Then WordBasic.EditUndo

If i + j + k + L > 0 Then myRslt = myRslt & "iv" & vbTab _
     & Trim(Str(i)) & CR & "i.v." & vbTab & Trim(Str(j)) _
     & CR & "IV" & vbTab & Trim(Str(k)) & CR _
     & "I.V." & vbTab & Trim(Str(L)) & CR2

'IM / i.m.
With rng.Find
  .Text = "<im>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<i.m>"
  .Execute Replace:=wdReplaceAll
End With
j = ActiveDocument.Range.End - myTot
If j > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<IM>"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<I.M>"
  .Execute Replace:=wdReplaceAll
End With
L = ActiveDocument.Range.End - myTot
If L > 0 Then WordBasic.EditUndo

If i + j + k + L > 0 Then myRslt = myRslt & "im" & vbTab _
     & Trim(Str(i)) & CR & "i.m." & vbTab & Trim(Str(j)) _
     & CR & "IM" & vbTab & Trim(Str(k)) & CR _
     & "I.M." & vbTab & Trim(Str(L)) & CR2

'SC / s.c.
With rng.Find
  .Text = "<sc>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<s.c>"
  .Execute Replace:=wdReplaceAll
End With
j = ActiveDocument.Range.End - myTot
If j > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<SC>"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "<S.C>"
  .Execute Replace:=wdReplaceAll
End With
L = ActiveDocument.Range.End - myTot
If L > 0 Then WordBasic.EditUndo

If i + j + k + L > 0 Then myRslt = myRslt & "sc" & vbTab _
     & Trim(Str(i)) & CR & "s.c." & vbTab & Trim(Str(j)) _
     & CR & "SC" & vbTab & Trim(Str(k)) & CR _
     & "S.C." & vbTab & Trim(Str(L)) & CR2

'# �
With rng.Find
  .Text = "[0-9]" & Chr(181)
  .Execute Replace:=wdReplaceAll
End With
h = ActiveDocument.Range.End - myTot
If h > 0 Then WordBasic.EditUndo

'# �
With rng.Find
  .Text = "[0-9] " & Chr(181)
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

'#micro
With rng.Find
  .Text = "[0-9]micro"
  .Execute Replace:=wdReplaceAll
End With
j = ActiveDocument.Range.End - myTot
If j > 0 Then WordBasic.EditUndo

'# micro
With rng.Find
  .Text = "[0-9] micro"
  .Execute Replace:=wdReplaceAll
End With
k = ActiveDocument.Range.End - myTot
If k > 0 Then WordBasic.EditUndo

If h + i + j + k > 0 Then myRslt = myRslt & "# " _
     & Chr(181) & vbTab & Trim(Str(h + i)) & CR _
     & "# " & "micro" & vbTab & Trim(Str(j + k)) & CR2


'count/minute
With rng.Find
  .Text = "cpm>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "c.p.m>"
  .Execute Replace:=wdReplaceAll
End With
j = ActiveDocument.Range.End - myTot
If j > 0 Then WordBasic.EditUndo

If i + j > 0 Then myRslt = myRslt & "cpm" & vbTab & Trim(Str(i)) _
     & CR & "c.p.m." & vbTab & Trim(Str(j)) & CR2

'beats/minute
With rng.Find
  .Text = "bpm>"
  .Execute Replace:=wdReplaceAll
End With
i = ActiveDocument.Range.End - myTot
If i > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = "b.p.m>"
  .Execute Replace:=wdReplaceAll
End With
j = ActiveDocument.Range.End - myTot
If j > 0 Then WordBasic.EditUndo

If i + j > 0 Then myRslt = myRslt & "bpm" & vbTab & Trim(Str(i)) & CR _
     & "b.p.m." & vbTab & Trim(Str(j)) & CR2

End Sub




Sub PunctAlyse()
' Version 18.10.11
' Produces usage statistics of various punctuation

Dim wasLen As Long
CR = vbCrLf: CR2 = CR & CR

Selection.WholeStory
Selection.Copy
Selection.HomeKey Unit:=wdStory
Documents.Add
Selection.PasteAndFormat (wdFormatPlainText)
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Wrap = wdFindContinue
  .Text = "[,.][0-9]"
  .Replacement.Text = " 0"
  .Replacement.Highlight = False
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With


myResults = "Punctuation Use" & CR2

wasLen = ActiveDocument.Range.End
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "."
  .Replacement.Text = "^&!"
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With
pnts = ActiveDocument.Range.End - wasLen
If pnts > 0 Then WordBasic.EditUndo
myResults = myResults & "Full points" & vbTab & Trim(Str(pnts)) & CR

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ","
  .Replacement.Text = "^&!"
  .Execute Replace:=wdReplaceAll
End With
coms = ActiveDocument.Range.End - wasLen
If coms > 0 Then WordBasic.EditUndo
myResults = myResults & "Commas" & vbTab & Trim(Str(coms)) & CR

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ";"
  .Replacement.Text = "^&!"
  .Execute Replace:=wdReplaceAll
End With
semis = ActiveDocument.Range.End - wasLen
If semis > 0 Then WordBasic.EditUndo
myResults = myResults & "Semicolons" & vbTab & Trim(Str(semis)) & CR

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ":"
  .Replacement.Text = "^&!"
  .Execute Replace:=wdReplaceAll
End With
colons = ActiveDocument.Range.End - wasLen
If colons > 0 Then WordBasic.EditUndo
myResults = myResults & "Colons" & vbTab & Trim(Str(colons)) & CR

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "?"
  .Replacement.Text = "^&!"
  .Execute Replace:=wdReplaceAll
End With
qns = ActiveDocument.Range.End - wasLen
If qns > 0 Then WordBasic.EditUndo
myResults = myResults & "Question marks" & vbTab & Trim(Str(qns)) & CR

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "!"
  .Replacement.Text = "^&!"
  .Execute Replace:=wdReplaceAll
End With
exclams = ActiveDocument.Range.End - wasLen
If exclams > 0 Then WordBasic.EditUndo
myResults = myResults & "Exclamations" & vbTab & Trim(Str(exclams)) & CR2

sentCount = pnts + qns + exclams
commaFactor = Int(100 * coms / sentCount) / 10
colonFactor = Int(1000 * colons / sentCount) / 10
semiFactor = Int(1000 * semis / sentCount) / 10
qnFactor = Int(1000 * qns / sentCount) / 10
exclamFactor = Int(1000 * exclams / sentCount) / 10

printMe = Trim(Str(commaFactor))
If Left(printMe, 1) = "." Then printMe = "0" & printMe
If InStr(printMe, ".") = 0 Then printMe = printMe & ".0"
myResults = myResults & "Comma Factor" & vbTab & printMe & CR

printMe = Trim(Str(semiFactor))
If Left(printMe, 1) = "." Then printMe = "0" & printMe
If InStr(printMe, ".") = 0 Then printMe = printMe & ".0"
myResults = myResults & "Semicolon Factor" & vbTab & printMe & CR

printMe = Trim(Str(colonFactor))
If Left(printMe, 1) = "." Then printMe = "0" & printMe
If InStr(printMe, ".") = 0 Then printMe = printMe & ".0"
myResults = myResults & "Colon Factor" & vbTab & printMe & CR

printMe = Trim(Str(qnFactor))
If Left(printMe, 1) = "." Then printMe = "0" & printMe
If InStr(printMe, ".") = 0 Then printMe = printMe & ".0"
myResults = myResults & "Question Factor" & vbTab & printMe & CR

printMe = Trim(Str(exclamFactor))
If Left(printMe, 1) = "." Then printMe = "0" & printMe
If InStr(printMe, ".") = 0 Then printMe = printMe & ".0"
myResults = myResults & "Exclamation Factor" & vbTab & printMe & CR2

Selection.WholeStory

Selection.TypeText Text:=myResults
Selection.WholeStory
Selection.ParagraphFormat.TabStops.Add Position:=CentimetersToPoints(4)
Selection.HomeKey Unit:=wdStory
End Sub





Sub SpellAlyse()
' Version 19.02.14
' Spellcheck system

opposite = 9999
checkForeign1 = False
' foreignLanguage1 = wdEnglishUS
foreignLanguage1 = opposite

checkForeign2 = False
foreignLanguage2 = wdFrench

makeFReditList = False

' General options
myFile = "zzSwitchList"

minLengthProper = 5
minLengthSpell = 3

properNounColour = wdNoHighlight
properNounColour = wdGray25

mainColour = wdBrightGreen

foreign1Colour = wdRed
foreign2Colour = wdPink

' Check file for alternative variable settings
Set mainDoc = ActiveDocument
For Each myWnd In ActiveDocument.Windows
  If InStr(myWnd.Document.Name, myFile) Then
    myWnd.Activate
    allText = ActiveDocument.Content

    myVariable = "checkForeign1 = "
    myPos = InStr(allText, myVariable)
    If myPos > 0 Then
      If Mid(allText, myPos + Len(myVariable), 1) = "T" Then
        checkForeign1 = True
      Else
        checkForeign1 = False
      End If
    End If

    myVariable = "foreignLanguage1 = "
    myPos = InStr(allText, myVariable)
    If myPos > 0 Then
      myData = Mid(allText, myPos + Len(myVariable))
      endPos = InStr(myData, Chr(13)) - 1
      If endPos >= 0 Then myData = Left(myData, endPos)
     
      Select Case myData
        Case "wdEnglishUS": foreignLanguage1 = wdEnglishUS
        Case "wdEnglishUK": foreignLanguage1 = wdEnglishUK
        Case "opposite": foreignLanguage1 = opposite
        Case "wdFrench": foreignLanguage1 = wdFrench
        Case "wdGerman": foreignLanguage1 = wdGerman
        Case Else: MsgBox "Unknown language. Please contact Paul Bev."
      End Select

    End If

    myVariable = "checkForeign2 = "
    myPos = InStr(allText, myVariable)
    If myPos > 0 Then
      If Mid(allText, myPos + Len(myVariable), 1) = "T" Then
        checkForeign2 = True
      Else
        checkForeign2 = False
      End If
    End If

    myVariable = "foreignLanguage2 = "
    myPos = InStr(allText, myVariable)
    If myPos > 0 Then
      myData = Mid(allText, myPos + Len(myVariable))
      endPos = InStr(myData, Chr(13)) - 1
      If endPos >= 0 Then myData = Left(myData, endPos)
      Select Case myData
        Case "wdEnglishUS": foreignLanguage2 = wdEnglishUS
        Case "wdEnglishUK": foreignLanguage2 = wdEnglishUK
        Case "opposite": foreignLanguage2 = opposite
        Case "wdFrench": foreignLanguage2 = wdFrench
        Case "wdGerman": foreignLanguage2 = wdGerman
        Case Else: MsgBox "Unknown language. Please contact Paul Bev."
                   Exit Sub
      End Select
    End If

    myVariable = "makeFReditList = "
    myPos = InStr(allText, myVariable)
    If myPos > 0 Then
      If Mid(allText, myPos + Len(myVariable), 1) = "T" Then
        makeFReditList = True
      Else
        makeFReditList = False
      End If
    End If
  End If
Next myWnd

' Start of main program
mainDoc.Activate
If Selection.LanguageID = wdEnglishUK Then
  mainLanguage = wdEnglishUK: myLang = "UK"
  If foreignLanguage1 = opposite Then foreignLanguage1 = wdEnglishUS
  If foreignLanguage2 = opposite Then foreignLanguage2 = wdEnglishUS
Else
  mainLanguage = wdEnglishUS: myLang = "US"
  If foreignLanguage1 = opposite Then foreignLanguage1 = wdEnglishUK
  If foreignLanguage2 = opposite Then foreignLanguage2 = wdEnglishUK
End If

CR2 = vbCrLf & vbCrLf
myResponse = MsgBox("Main language = " & myLang, _
       vbQuestion + vbYesNoCancel, "SpellAlyse")
If myResponse <> vbYes Then Exit Sub

' To measure the time taken
timeStart = Timer

' Check that tracking is off!
nowTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
allPropNouns = vbCrLf & vbCrLf

' Blank off all apostrophe-s
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ChrW(8217) & "s"
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .MatchCase = True
  .Replacement.Text = " zczc"
  .Execute Replace:=wdReplaceAll
End With

' Spellcheck the endnotes
myJump = 100
If ActiveDocument.Endnotes.Count > 0 Then
  Set rng = ActiveDocument.StoryRanges(wdEndnotesStory)
  allWords = rng.Words.Count
  Set wdrng = ActiveDocument.StoryRanges(wdEndnotesStory)
  countWds = 0
  For Each wd In rng.Words
    If countWds Mod myJump = 1 Then StatusBar = "Checking words in endnotes: " _
         & Str(100 - Int(countWds * 100 / allWords)) & "%"
    countWds = countWds + 1
    If Len(wd) >= minLengthSpell And Trim(wd) <> "zczc" And Application.CheckSpelling(wd, _
         MainDictionary:=Languages(mainLanguage).NameLocal) = False Then
      wd.HighlightColorIndex = mainColour
      If checkForeign2 = True And Application.CheckSpelling(wd, _
           MainDictionary:=Languages(foreignLanguage2).NameLocal) _
            = True Then wd.HighlightColorIndex = foreign2Colour
      If checkForeign1 = True And Application.CheckSpelling(wd, _
           MainDictionary:=Languages(foreignLanguage1).NameLocal) _
           = True Then wd.HighlightColorIndex = foreign1Colour
    ' But might it be a proper noun?
      maybeProper = False
    ' If the first letter is a cap, it may be a PN
      If Asc(wd) > 64 And Asc(wd) < 91 Then
        maybeProper = True
      ' But if the second is also uppercase, it's probably not
        If Asc(Mid(wd, 2)) < 91 Then maybeProper = False
      ' Check the previous characters
        wdrng.Start = wd.Start - 4
        wdrng.End = wd.End
        myContext = wdrng
        minusTwo = Mid(myContext, 3, 1)
        minusThree = Mid(myContext, 2, 1)
        minusFour = Mid(myContext, 1, 1)
      ' If it's the start of a new line or after a tab, it may not be a PN
        If Asc(myContext) = 13 Or Asc(myContext) = 9 Then maybeProper = False
      ' If minusTwo is not a letter
        If LCase(minusTwo) = UCase(minusTwo) Then
          maybeProper = False
          If minusTwo = "," Or minusTwo = ";" Then maybeProper = True
        ' Check for, e.g. P. Funnyname
          If minusTwo = "." And UCase(minusThree) = minusThree _
               And minusFour = " " Then maybeProper = True
        ' Check for, e.g. P Funnyname
          If UCase(minusTwo) = minusTwo And minusThree = " " Then _
               maybeProper = True
        End If
        If maybeProper = True And Len(wd) >= minLengthProper Then
          wd.HighlightColorIndex = properNounColour
          allPropNouns = allPropNouns & wd & vbCrLf
        End If
      End If
    End If
  Next wd
End If

' Spellcheck the footnotes
myJump = 100
If ActiveDocument.Footnotes.Count > 0 Then
  Set rng = ActiveDocument.StoryRanges(wdFootnotesStory)
  allWords = rng.Words.Count
  Set wdrng = ActiveDocument.StoryRanges(wdFootnotesStory)
  countWds = 0
  For Each wd In rng.Words
    If countWds Mod myJump = 1 Then StatusBar = "Checking words in footnotes: " _
         & Str(100 - Int(countWds * 100 / allWords)) & "%"
    countWds = countWds + 1
    If Len(wd) >= minLengthSpell And Trim(wd) <> "zczc" And Application.CheckSpelling(wd, _
         MainDictionary:=Languages(mainLanguage).NameLocal) = False Then
      wd.HighlightColorIndex = mainColour
      If checkForeign2 = True And Application.CheckSpelling(wd, _
           MainDictionary:=Languages(foreignLanguage2).NameLocal) _
            = True Then wd.HighlightColorIndex = foreign2Colour
      If checkForeign1 = True And Application.CheckSpelling(wd, _
           MainDictionary:=Languages(foreignLanguage1).NameLocal) = True _
           Then wd.HighlightColorIndex = foreign1Colour
    ' But might it be a proper noun?
      maybeProper = False
    ' If the first letter is a cap, it may be a PN
      If Asc(wd) > 64 And Asc(wd) < 91 Then
        maybeProper = True
      ' But if the second is also uppercase, it's probably not
        If Asc(Mid(wd, 2)) < 91 Then maybeProper = False
      ' Check the previous characters
        wdrng.Start = wd.Start - 4
        wdrng.End = wd.End
        myContext = wdrng
        minusTwo = Mid(myContext, 3, 1)
        minusThree = Mid(myContext, 2, 1)
        minusFour = Mid(myContext, 1, 1)
      ' If it's the start of a new line or after a tab, it may not be a PN
        If Asc(myContext) = 13 Or Asc(myContext) = 9 Then maybeProper = False
      ' If minusTwo is not a letter
        If LCase(minusTwo) = UCase(minusTwo) Then
          maybeProper = False
          If minusTwo = "," Or minusTwo = ";" Then maybeProper = True
        ' Check for, e.g. P. Funnyname
          If minusTwo = "." And UCase(minusThree) = minusThree _
               And minusFour = " " Then maybeProper = True
        ' Check for, e.g. P Funnyname
          If UCase(minusTwo) = minusTwo And minusThree = " " Then _
               maybeProper = True
        End If
        If maybeProper = True And Len(wd) >= minLengthProper Then
          wd.HighlightColorIndex = properNounColour
          allPropNouns = allPropNouns & wd & vbCrLf
        End If
      End If
    End If
  Next wd
End If

' Spellcheck the main text
i = ActiveDocument.Words.Count
allWords = i
Set wdrng = ActiveDocument.Content
For Each wd In ActiveDocument.Words
  If Len(wd) >= minLengthSpell And Trim(wd) <> "zczc" And Application.CheckSpelling(wd, _
       MainDictionary:=Languages(mainLanguage).NameLocal) = False Then
    wd.HighlightColorIndex = mainColour
    If checkForeign2 = True And Application.CheckSpelling(wd, _
         MainDictionary:=Languages(foreignLanguage2).NameLocal) = True _
         Then wd.HighlightColorIndex = foreign2Colour
    If checkForeign1 = True And Application.CheckSpelling(wd, _
         MainDictionary:=Languages(foreignLanguage1).NameLocal) = True _
         Then wd.HighlightColorIndex = foreign1Colour
  ' But might it be a proper noun?
    maybeProper = False
  ' If the first letter is a cap, it may be a PN
    If Asc(wd) > 64 And Asc(wd) < 91 Then
      maybeProper = True
    ' But if the second is also uppercase, it's probably not
      If Asc(Mid(wd, 2)) < 91 Then maybeProper = False
    ' Check the previous characters
      wdrng.Start = wd.Start - 4
      wdrng.End = wd.End
      myContext = wdrng
      minusTwo = Mid(myContext, 3, 1)
      minusThree = Mid(myContext, 2, 1)
      minusFour = Mid(myContext, 1, 1)
    ' If it's the start of a new line or after a tab, it may not be a PN
      If Asc(myContext) = 13 Or Asc(myContext) = 9 Then maybeProper = False
    ' If minusTwo is not a letter
      If LCase(minusTwo) = UCase(minusTwo) Then
        maybeProper = False
        If minusTwo = "," Or minusTwo = ";" Then maybeProper = True
      ' Check for, e.g. P. Funnyname
        If minusTwo = "." And UCase(minusThree) = minusThree _
             And minusFour = " " Then maybeProper = True
      ' Check for, e.g. P Funnyname
        If UCase(minusTwo) = minusTwo And minusThree = " " Then _
             maybeProper = True
      End If
      If maybeProper = True And Len(wd) >= minLengthProper Then
        wd.HighlightColorIndex = properNounColour
        allPropNouns = allPropNouns & wd & vbCrLf
      End If
    End If
  End If
  i = i - 1
  If i Mod 100 = 0 Then StatusBar = "Spellchecking. To go: " & Str(Int(i * 100 / allWords)) & "%"
Next wd

Selection.HomeKey Unit:=wdStory

If makeFReditList = True Then
  allList = ""
  ' List words in endnotes
  If ActiveDocument.Endnotes.Count > 0 Then
    Set rng = ActiveDocument.StoryRanges(wdEndnotesStory)
    i = ActiveDocument.StoryRanges(wdEndnotesStory).Words.Count
    For Each wd In ActiveDocument.StoryRanges(wdEndnotesStory).Words
      rng.Start = wd.Start
      rng.End = wd.Start + 1
      If rng.HighlightColorIndex = mainColour Then
        theWord = Trim(wd)
        If Right(theWord, 1) = ChrW(8217) Then theWord = Left(theWord, Len(theWord) - 1)
        allList = allList & "m" & theWord & vbCrLf
      End If
      If rng.HighlightColorIndex = foreign1Colour Then
        theWord = Trim(wd)
        If Right(theWord, 1) = ChrW(8217) Then theWord = Left(theWord, Len(theWord) - 1)
        allList = allList & "y" & theWord & vbCrLf
      End If
      If rng.HighlightColorIndex = foreign2Colour Then
        theWord = Trim(wd)
        If Right(theWord, 1) = ChrW(8217) Then theWord = Left(theWord, Len(theWord) - 1)
        allList = allList & "z" & theWord & vbCrLf
      End If
      i = i - 1
      If i Mod 10 = 0 Then StatusBar = "Endnote word list. To go: " & Str(i)
    Next wd
  End If

  ' List words in footnotes
  If ActiveDocument.Footnotes.Count > 0 Then
    Set rng = ActiveDocument.StoryRanges(wdFootnotesStory)
    i = ActiveDocument.StoryRanges(wdFootnotesStory).Words.Count
    For Each wd In ActiveDocument.StoryRanges(wdFootnotesStory).Words
      rng.Start = wd.Start
      rng.End = wd.Start + 1
      If rng.HighlightColorIndex = mainColour Then
        theWord = Trim(wd)
        If Right(theWord, 1) = ChrW(8217) Then theWord = Left(theWord, Len(theWord) - 1)
        allList = allList & "m" & theWord & vbCrLf
      End If
      If rng.HighlightColorIndex = foreign1Colour Then
        theWord = Trim(wd)
        If Right(theWord, 1) = ChrW(8217) Then theWord = Left(theWord, Len(theWord) - 1)
        allList = allList & "y" & theWord & vbCrLf
      End If
      If rng.HighlightColorIndex = foreign2Colour Then
        theWord = Trim(wd)
        If Right(theWord, 1) = ChrW(8217) Then theWord = Left(theWord, Len(theWord) - 1)
        allList = allList & "z" & theWord & vbCrLf
      End If
      i = i - 1
      If i Mod 10 = 0 Then StatusBar = "Footnote word list. To go: " & Str(i)
    Next wd
  End If

  ' List words in main text
  i = ActiveDocument.Words.Count
  Set rng = ActiveDocument.Range
  For Each wd In ActiveDocument.Words
    rng.Start = wd.Start
    rng.End = wd.Start + 1
    If rng.HighlightColorIndex = mainColour Then
      theWord = Trim(wd)
      If Right(theWord, 1) = ChrW(8217) Then theWord = Left(theWord, Len(theWord) - 1)
      allList = allList & "m" & theWord & vbCrLf
    End If
    If rng.HighlightColorIndex = foreign1Colour Then
      theWord = Trim(wd)
      If Right(theWord, 1) = ChrW(8217) Then theWord = Left(theWord, Len(theWord) - 1)
      allList = allList & "y" & theWord & vbCrLf
    End If
    If rng.HighlightColorIndex = foreign2Colour Then
      theWord = Trim(wd)
      If Right(theWord, 1) = ChrW(8217) Then theWord = Left(theWord, Len(theWord) - 1)
      allList = allList & "z" & theWord & vbCrLf
    End If
    i = i - 1
    If i Mod 100 = 0 Then StatusBar = "Constructing FRedit list. To go: " & Str(i)
  Next wd

  Documents.Add
  Selection.TypeText Text:=allList

  Selection.WholeStory
  ' Sort the list and remove duplicates
  Selection.Sort ExcludeHeader:=False, CaseSensitive:=True, FieldNumber:="Paragraphs"
  For i = ActiveDocument.Paragraphs.Count To 2 Step -1
    Set rng1 = ActiveDocument.Paragraphs(i).Range
    Set rng2 = ActiveDocument.Paragraphs(i - 1).Range
    If rng1 = rng2 Then rng1.Delete
  Next i

  Selection.HomeKey Unit:=wdStory
  For Each myPara In ActiveDocument.Paragraphs
    If Asc(myPara) = Asc("m") Then myPara.Range.HighlightColorIndex = mainColour
    If Asc(myPara) = Asc("y") Then myPara.Range.HighlightColorIndex = foreign1Colour
    If Asc(myPara) = Asc("z") Then myPara.Range.HighlightColorIndex = foreign2Colour
  Next myPara
  Set rng = ActiveDocument.Range
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "^p^$"
    .Replacement.Text = "^p"
    .Replacement.Highlight = False
    .Execute Replace:=wdReplaceAll
  End With
End If


' restore all apostrophe-s
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = " zczc"
  .Replacement.Text = ChrW(8217) & "s"
  .Replacement.Highlight = False
  .MatchCase = True
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

Selection.End = backHere
Selection.Start = backHere

MsgBox ((Int(10 * (Timer - timeStart) / 60) / 10) & "  minutes")
ActiveDocument.TrackRevisions = nowTrack
StatusBar = ""

End Sub



Sub HighlightOffWord()
' Version 28.01.12
' Remove highlight from all occurrences of this word
' Alt-H

doNotes = True

' Select current word
Selection.Words(1).Select
Selection.MoveEndWhile cset:=ChrW(8217) & "' ", Count:=wdBackward
theWord = Trim(Replace(Selection, Chr(160), " "))
theWord = Replace(theWord, ChrW(8217) & "s", "")
Selection.Collapse wdCollapseEnd

' Now unhighlight all occurrences of the word ...
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "<" & theWord & ">??"
  .MatchWildcards = True
  .Replacement.Text = ""
  .Replacement.Highlight = False
  .Forward = True
  .Execute Replace:=wdReplaceAll
End With

' ... and all occurrences of the word with a single quote
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "<" & theWord & ChrW(8217) & "??"
  .MatchWildcards = True
  .Replacement.Text = ""
  .Replacement.Highlight = False
  .Execute Replace:=wdReplaceAll
End With

If doNotes = True And ActiveDocument.Endnotes.Count > 0 Then
  Set rng = ActiveDocument.StoryRanges(wdEndnotesStory)
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "<" & theWord & ">??"
    .MatchWildcards = True
    .Replacement.Text = ""
    .Replacement.Highlight = False
    .Forward = True
    .Execute Replace:=wdReplaceAll
  End With
 
  ' ... and all occurrences of the word with a single quote
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "<" & theWord & ChrW(8217) & "??"
    .MatchWildcards = True
    .Replacement.Text = ""
    .Replacement.Highlight = False
    .Execute Replace:=wdReplaceAll
  End With
End If

If doNotes = True And ActiveDocument.Footnotes.Count > 0 Then
  Set rng = ActiveDocument.StoryRanges(wdFootnotesStory)
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "<" & theWord & ">??"
    .MatchWildcards = True
    .Replacement.Text = ""
    .Replacement.Highlight = False
    .Forward = True
    .Execute Replace:=wdReplaceAll
  End With
 
  ' ... and all occurrences of the word with a single quote
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "<" & theWord & ChrW(8217) & "??"
    .MatchWildcards = True
    .Replacement.Text = ""
    .Replacement.Highlight = False
    .Execute Replace:=wdReplaceAll
  End With
End If

End Sub



Sub SpellingToolkit()
' Version 14.03.14
' Complete Spelling Toolkit

listInitialCapsSeparately = True

myHighlightColour = wdYellow

myChangeColour = wdTurquoise

' If track changes is on, don't bother highlighting
If ActiveDocument.TrackRevisions = True Then myChangeColour = 0

exListName = "zzExceptions"


oldColour = Options.DefaultHighlightColorIndex
myTrack = ActiveDocument.TrackRevisions
CR = vbCrLf
CR2 = CR & CR
CR3 = CR2 & CR
menuText = "1 - List possible spelling errors" & CR2 & _
           "2 - Remove exceptions from the error list" & CR2 & _
           "3 - Only highlight the errors" & CR2 & _
           "4 - Create suggested change list" & CR2 & _
           "5 - Make changes to text" & CR

Do
  myJob = Val(InputBox(menuText, "Spelling Toolkit"))
Loop Until myJob < 6
If myJob = 0 Then Exit Sub

If myJob <> 2 Then
  Set rng = ActiveDocument.Content
  rng.End = 40
  topText = Replace(rng.Text, Chr(13), " ")
  If InStr(topText, exListName) > 0 Then
       myResponse = MsgBox("Please place the cursor in the file" & _
       CR & "to be edited and run the macro again.", vbOK, "Spelling Toolkit")
    Exit Sub
  End If
End If

' Look for the start of the spelling error list
Set rng = ActiveDocument.Content
endOfDoc = rng.End
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "|Spelling"
  .Wrap = wdFindContinue
  .Replacement.Text = ""
  .Forward = True
  .MatchCase = False
  .MatchWildcards = False
  .Execute
End With

gottaList = rng.Find.Found
If gottaList = True Then
  rng.Expand wdParagraph
  listStart = rng.End
Else
  listStart = 1
End If
listEnd = rng.End

Select Case myJob
  Case 1: GoTo errorLister
  Case 2: GoTo removeExceptions
  Case 3: highlightOnly = True: GoTo correctSpellings
  Case 4: GoTo suggestCorrections
  Case 5: highlightOnly = False: GoTo correctSpellings
  Case Else: MsgBox "Please tell Paul there's an error.": Exit Sub
End Select


' List possible spelling errors
errorLister:
thisLanguage = Selection.LanguageID
langName = Languages(thisLanguage).NameLocal
myLang = "Unknown language. OK?"
If thisLanguage = wdEnglishUK Then myLang = "UK spelling. OK?"
If thisLanguage = wdEnglishUS Then myLang = "US spelling. OK?"
myResponse = MsgBox(myLang, vbQuestion + vbYesNoCancel, "Spelling Toolkit")
If myResponse <> vbYes Then Exit Sub

timeStart = Timer
myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False


' Check if spelling error list is wanted
generateList = False

If gottaList = True Then
' If we've found a spelling error list, check whether the user
' is happy to delete it.
  myResponse = MsgBox("Delete the existing error list?", vbQuestion _
          + vbYesNoCancel, "Spelling Toolkit")
  If myResponse = vbYes Then
    rng.End = ActiveDocument.Range.End
    rng.Delete
  Else
    Exit Sub
  End If
End If

' Find if there's an exceptions list open
Set thisDoc = ActiveDocument
gotExceptFile = False
For Each myWnd In ActiveDocument.Windows
  thisName = myWnd.Document.Name
  myWnd.Document.Activate
  Set rng = ActiveDocument.Content
  rng.End = 40
  topText = Replace(rng.Text, Chr(13), " ")
  If InStr(topText, exListName) > 0 Or InStr(thisName, exListName) = 0 Then
    allExceps = rng.Text
    allExceps = " " & Replace(allExceps, Chr(13), " ") & " "
    gotExceptFile = True
    Exit For
  End If
Next myWnd

thisDoc.Activate

Set rng = ActiveDocument.Content
myEnd = rng.End
allErrors = CR2 & "!!!!!!!!" & CR2
numCorrects = 0
numErrors = 0
' Create spelling error list
erList1 = CR
erList2 = CR
For i = 1 To 3
  If myResponse = vbNo Then i = 3
  If i = 1 And ActiveDocument.Footnotes.Count = 0 Then i = 2
  If i = 1 Then Set rng = ActiveDocument.StoryRanges(wdFootnotesStory)
  If i = 2 And ActiveDocument.Endnotes.Count = 0 Then i = 3
  If i = 2 Then Set rng = ActiveDocument.StoryRanges(wdEndnotesStory)
  If i = 3 Then Set rng = ActiveDocument.Content
  k = 0
  gotStarted = False
  For Each wd In rng.Words
  k = k + 1
    If Len(wd) > 2 And LCase(wd) <> UCase(wd) And _
         wd.Font.StrikeThrough = False Then
         padWd = " " & Trim(wd) & " "
      If Application.CheckSpelling(wd, MainDictionary:=langName) = _
           False And InStr(allExceps, padWd) = 0 Then
        pCent = Int((myEnd - wd.End) / myEnd * 100)

        ' Report progress
        If k Mod 20 = 1 Or gotStarted = False Then
          gotStarted = True
          If i = 1 Then myPrompt = "Checking footnote text."
          If i = 2 Then myPrompt = "Checking endnote text."
          If i = 3 Then myPrompt = "Checking main text."
          StatusBar = "Generating errors list. " & myPrompt & _
             " To go:  " & Trim(Str(pCent)) & "%"
            Debug.Print "Generating errors list. " & myPrompt & _
             " To go:  " & Trim(Str(pCent)) & "%"
        End If
        erWord = Trim(wd)
        lastChar = Right(erWord, 1)
        If lastChar = "'" Or lastChar = ChrW(8217) Then
          erWord = Left(erWord, Len(erWord) - 1)
        End If
        If listInitialCapsSeparately = True Then
          cap = Left(erWord, 1)
          If UCase(cap) = cap Then
            If InStr(erList1, CR & erWord & CR) = 0 Then erList1 = erList1 _
                 & erWord & CR
          Else
            If InStr(erList2, CR & erWord & CR) = 0 Then erList2 = erList2 _
                 & erWord & CR
          End If
        Else
          If InStr(erList1, CR & erWord & CR) = 0 Then erList1 = erList1 _
               & erWord & CR
        End If
      End If
    End If
  Next wd
Next i

Selection.EndKey Unit:=wdStory
ActiveDocument.TrackRevisions = False
Selection.TypeText Text:=CR

topList = Selection.Start
excStart = Selection.Start
Selection.TypeText Text:=erList2
listStart = Selection.End
Selection.Start = excStart
Selection.Sort

Selection.Start = topList
Selection.Collapse wdCollapseStart
Selection.TypeText Text:=CR2 & "|Spelling Error List:"
topBothLists = Selection.Start + 1

If erList1 <> CR Then
  Selection.EndKey Unit:=wdStory
  listStart = Selection.Start
  Selection.TypeText Text:=erList1
  Selection.Start = listStart
  Selection.Sort
End If

Selection.Start = excStart
Selection.LanguageID = thisLanguage
Selection.Style = wdStyleNormal
Selection.ParagraphFormat.SpaceAfter = 0
Selection.MoveStart , 2
Selection.Collapse wdCollapseStart
If gotExceptFile = False Then
  mainFileName = ActiveDocument.Name
  Selection.EndKey Unit:=wdStory
  Selection.Start = topBothLists
  Selection.Copy
  Documents.Add
  Selection.Paste
  Selection.HomeKey Unit:=wdStory
  Selection.TypeText Text:=exListName & CR2
  ActiveDocument.Variables.Add "PBname", mainFileName
End If
StatusBar = ""
totTime = Int(10 * (Timer - timeStart) / 60) / 10
If totTime > 2 Then myResponse = MsgBox((totTime & "  minutes"), _
vbOKOnly, "Spelling Toolkit")
If gotExceptFile = False Then myResponse = _
     MsgBox("Now delete actual spelling errors" & CR & _
     "from this exceptions list", vbOKOnly, _
     "Spelling Toolkit")
ActiveDocument.TrackRevisions = myTrack
Exit Sub


'Remove exceptions from the error list
removeExceptions:

Set thisDoc = ActiveDocument
DocName = ActiveDocument.Name
thisIsExceptsList = (InStr(DocName, exListName) > 0)

myFileName = ""
For Each v In ActiveDocument.Variables
  If v.Name = "PBname" Then
  ' read it then delete it
    myFileName = v.Value
    v.Delete
    thisIsExceptsList = True
    Exit For
  End If
Next v


If thisIsExceptsList = False Then
  gotExceptFile = False
  For Each myWnd In Application.Windows
    thisName = myWnd.Document.Name
    If InStr(thisName, exListName) > 0 Then
      myWnd.Document.Activate
      thisIsExceptsList = True
      Exit For
    End If
  Next myWnd
End If


Set rng = ActiveDocument.Content
rng.End = 40
topText = Replace(rng.Text, Chr(13), " ")
If myFileName = "" And InStr(topText, exListName) > 0 Then
     myResponse = MsgBox("Please place the cursor in the file" & _
     CR & "to be edited and run the macro again.", vbOK, "Spelling Toolkit")
  Exit Sub
End If

If thisIsExceptsList = True Then
  Set rng2 = ActiveDocument.Content
  allExceps = rng2.Text
  allExceps = " " & Replace(allExceps, Chr(13), " ") & " "
  gotExceptFile = True
  If myFileName > "" Then
    Documents(myFileName).Activate
  Else
    thisDoc.Activate
  End If
End If


If gottaList = False Then
' Look AGAIN for the start of the spelling error list
  Set rng = ActiveDocument.Content
  endOfDoc = rng.End
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "|Spelling"
    .Wrap = wdFindContinue
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = False
    .Execute
  End With
  
  If rng.Find.Found = True Then
    rng.Expand wdParagraph
    listStart = rng.End + 1
  Else
    MsgBox "I can't find any errors list."
    Exit Sub
  End If
End If

' Now check each word against the exceptions list
ActiveDocument.TrackRevisions = False
Selection.EndKey Unit:=wdStory
i = 0
numDeleted = 0
Set ac = ActiveDocument
Do
  Set testLine = ac.Paragraphs(ac.Paragraphs.Count - i)
  tst = testLine.Range.Words(1)
  tst = " " & Trim(tst) & " "
  If InStr(allExceps, tst) > 0 Then
    testLine.Range.Select
    Selection.Delete
    numDeleted = numDeleted + 1
  Else
    i = i + 1
  End If
Loop Until testLine.Range.Start < listStart
Selection.Start = listStart + 1
Selection.End = listStart + 1

ActiveDocument.TrackRevisions = myTrack
MsgBox "Exceptions removed:" & numDeleted
Exit Sub


' Create suggested changes list
suggestCorrections:
thisLanguage = Selection.LanguageID
langName = Languages(thisLanguage).NameLocal
myLang = "Unknown language. OK?"
If thisLanguage = wdEnglishUK Then myLang = "UK spelling. OK?"
If thisLanguage = wdEnglishUS Then myLang = "US spelling. OK?"
myResponse = MsgBox(myLang, vbQuestion + vbYesNoCancel, "Spelling Toolkit")
If myResponse <> vbYes Then Exit Sub

ActiveDocument.TrackRevisions = False
Do
  Set myLine = rng.Paragraphs(1)
  Set wd = rng.Words(1)
  lineText = myLine.Range.Text
  
  init = Left(lineText, 1)

  ' default = spellcheck it
  myTask = 1
  
  ' If copied from frequency program strip and then copy
  If UCase(init) = init And LCase(init) <> init Then myTask = 2
  
  ' If initial capital then just copy
  If InStr(lineText, ". .") > 0 Then myTask = 3
  
  ' Ignore short lines or lines with a pad charater
  If Len(lineText) < 3 Or InStr(lineText, "|") > 0 Then myTask = 0

  If myTask > 0 Then Debug.Print Trim(Str(myTask)) & ":  " & lineText

  Select Case myTask
    Case 1:
          ' Suggest new word
            Set suggList = wd.GetSpellingSuggestions(MainDictionary:=langName)
            If suggList.Count > 0 Then
              rhs = suggList.Item(1).Name
            Else
              rhs = wd.Text
              wd.HighlightColorIndex = myHighlightColour
            End If

    Case 2: rhs = wd.Text
            wd.HighlightColorIndex = myHighlightColour

    Case 3: myLine.Range.Text = wd.Text & Chr(13): rhs = wd.Text
    
    Case Else: rhs = ""

  End Select

  If rhs > "" Then
    wd.Select
    Selection.Collapse wdCollapseEnd
    Selection.TypeText Text:="|" & rhs
  End If
  rng.Start = myLine.Range.End
Loop Until myLine.Range.End >= ActiveDocument.Content.End
ActiveDocument.TrackRevisions = myTrack
Exit Sub


' Make changes to text (or just highlight)
correctSpellings:
If highlightOnly = False Then
  myResponse = MsgBox("Make changes to actual text", vbQuestion + vbYesNoCancel, "Spelling Toolkit")
  If myResponse <> vbYes Then Exit Sub
End If
numParas = ActiveDocument.Paragraphs.Count
numWords = ActiveDocument.Words.Count

rng.Start = listStart
rng.End = ActiveDocument.Content.End
ActiveDocument.TrackRevisions = False
rng.Font.StrikeThrough = True
ActiveDocument.TrackRevisions = myTrack
fnNum = ActiveDocument.Footnotes.Count
enNum = ActiveDocument.Endnotes.Count


numLines = Len(rng.Text) - Len(Replace(rng.Text, Chr(13), ""))
ReDim myColours(numLines)

For i = 1 To numLines
  myLine = rng.Paragraphs(i).Range.Text
  myLine = Replace(myLine, Chr(13), "")
  If Len(myLine) > 2 Then
  ' Set the highlight colour according to the item's colour
    lnColour = rng.Paragraphs(i).Range.Words(1).HighlightColorIndex
    Options.DefaultHighlightColorIndex = lnColour
  ' Find f and r words
    padPos = InStr(myLine, ChrW(124))
    If padPos > 0 Then
      fWord = Left(myLine, padPos - 1)
      rWord = Mid(myLine, padPos + 1)
    Else
      fWord = myLine
      rWord = myLine
    End If
    If fWord = rWord Or highlightOnly = True Then
      rWord = "^&"
      highlightIt = True
    Else
      highlightIt = False
    End If
    For j = 1 To 3
      If j = 1 And fnNum = 0 Then j = 2
      If j = 2 And enNum = 0 Then j = 3
      Select Case j
        Case 1: Set rng2 = ActiveDocument.StoryRanges(wdFootnotesStory)
        Case 2: Set rng2 = ActiveDocument.StoryRanges(wdEndnotesStory)
        Case 3: Set rng2 = ActiveDocument.Content
      End Select
  
    ' If a highlight is wanted, do it
      If myChangeColour > 0 Or rWord = "^&" Then
        ActiveDocument.TrackRevisions = False
        With rng2.Find
          .ClearFormatting
          .Replacement.ClearFormatting
          .Text = "<" & fWord & ">"
          .Replacement.Text = "^&"
          .Font.StrikeThrough = False
          .Forward = True
          If lnColour = 0 Then
            If rWord = "^&" Then
              Options.DefaultHighlightColorIndex = myHighlightColour
            Else
              Options.DefaultHighlightColorIndex = myChangeColour
            End If
          End If
          .Replacement.Highlight = True
          .MatchWildcards = True
          .Execute Replace:=wdReplaceAll
        End With
        ActiveDocument.TrackRevisions = myTrack
      End If
    ' If a change is needed, make it
      If rWord <> "^&" Then
        With rng2.Find
          .ClearFormatting
          .Replacement.ClearFormatting
          .Text = "<" & fWord & ">"
          .Replacement.Text = rWord
          .Font.StrikeThrough = False
          .Forward = True
          .Replacement.Highlight = True
          .MatchWildcards = True
          .Execute Replace:=wdReplaceAll
        End With
      End If
    Next j
  End If
Next i

Options.DefaultHighlightColorIndex = oldColour
ActiveDocument.TrackRevisions = False
rng.Font.StrikeThrough = False
Selection.MoveLeft , 1
ActiveDocument.TrackRevisions = myTrack
If highlightOnly = True Then
  myCmnt = "Errors have been highlighted"
Else
  myCmnt = "Errors have been edited"
End If

myResponse = MsgBox(myCmnt, vbOKOnly, "Spelling Toolkit")
End Sub


Sub ProperNounAlyse()
' Version 24.06.14
' Analyse similar proper nouns

minLengthCheck = 4

includeAcronyms = True

similarChars = "b,v; sch,sh; ch,sh; ph,f; z,s; ss,s;"
similarChars = similarChars & "mp,m; ll,l; nn,n; nd,n;"

checkFinalLetters = True
' Grey on word only
thisHighlight = wdGray25

doMissingLetter = True
' bold and blue

switchTest = True
' double strikethrough

doSimilarLetters = True
' various highlight colours + underline

doVowelTest = True
' various highlight colours + strikethrough

' These last two tests cycle through these colours:
maxCol = 6
ReDim myCol(maxCol) As Integer
myCol(1) = wdYellow
myCol(2) = wdBrightGreen
myCol(3) = wdTurquoise
myCol(4) = wdRed
myCol(5) = wdPink
myCol(6) = wdGray25

recordTime = True

colcode = 2
oldColour = Options.DefaultHighlightColorIndex
Options.DefaultHighlightColorIndex = wdGray25
leadDots = " . . . "
title2 = "Proper noun list"

' For debugging use only
doSpell = True
doCheckList = True

' In case there are too many for a normal sort...
maxParas = 12000
CR = vbCrLf: CR2 = CR & CR

' Check the language
Selection.HomeKey Unit:=wdStory
thisLanguage = Selection.LanguageID
textLanguage = " neither UK nor US English spelling"
If thisLanguage = wdEnglishUK Then textLanguage = _
     " UK English spelling"
If thisLanguage = wdEnglishUS Then textLanguage = _
     " US English spelling"

myResponse = MsgBox("Main language = " & textLanguage, _
       vbQuestion + vbYesNoCancel, "ProperNounAlyse")
If myResponse <> vbYes Then Exit Sub

Set rng = ActiveDocument.Content
rng.End = 100

If InStr(rng.Text, leadDots) = 0 Then

  ' Copy main text
  Set rng = ActiveDocument.Content
  rng.Copy

  ' collect notes text, if any
  endText = ""
  footText = ""
  If ActiveDocument.Endnotes.Count > 0 Then
    endText = ActiveDocument.StoryRanges(wdEndnotesStory).Text
  End If
  If ActiveDocument.Footnotes.Count > 0 Then
    footText = ActiveDocument.StoryRanges(wdFootnotesStory).Text
  End If

  ' collect text in all the textboxes (if any)
  sh = ActiveDocument.Shapes.Count
  If sh > 0 Then
    ReDim shText(sh)
    i = 0
    For Each shp In ActiveDocument.Shapes
      If shp.TextFrame.HasText Then
        i = i + 1
        shText(i) = shp.TextFrame.TextRange.Text
      End If
    Next
    shCount = i
  End If

  ' Create main working list
  Documents.Add
  Set docMainList = ActiveDocument
  Selection.PasteAndFormat (wdFormatPlainText)
  Selection.EndKey Unit:=wdStory

  Selection.TypeText Text:=endText & CR & footText & CR
  If shCount > 0 Then
    For i = 1 To shCount
      Selection.TypeText Text:=shText(i) & CR
    Next i
  End If

  If recordTime = True Then timeStart = Timer
  StatusBar = "Preparing copied file - 1"
  Debug.Print "Preparing copied file - 1"
  ' Set the correct language
  Set rng = ActiveDocument.Content
  rng.LanguageID = thisLanguage
  rng.NoProofing = False

  ' Avoid apostrophe and single close quotes
  Debug.Print "Preparing copied file - 2"
  StatusBar = "Preparing copied file - 2"
  Set rng = ActiveDocument.Range
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "n[" & ChrW(8216) & ChrW(8217) & "']t"
    .MatchWildcards = True
    .MatchWholeWord = False
    .MatchSoundsLike = False
    .Replacement.Text = " "
    .Execute Replace:=wdReplaceAll
  End With

  Set rng = ActiveDocument.Range
  With rng.Find
    .Text = "O'"
    .MatchCase = True
    .Replacement.Text = "Oqq"
    .MatchWildcards = False
    .Execute Replace:=wdReplaceAll
  End With

   Set rng = ActiveDocument.Range
  With rng.Find
    .Text = "'"
    .Replacement.Text = " "
    .Execute Replace:=wdReplaceAll
  End With

  ' Find initial cap words
  Debug.Print "Preparing copied file - 3"
  StatusBar = "Preparing copied file - 3"
  myChopNum = minLengthCheck - 2
  If myChop < 1 Then myChop = 1
  myChop = Trim(Str(myChopNum))
  myFind = "<[A-Z][a-z][a-zA-Z]{" & myChop & ",}"
  If includeAcronyms = True Then myFind = _
       "<[A-Z][a-zA-Z][a-zA-Z]{" & myChop & ",}"
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = myFind
    .MatchWildcards = True
    .MatchCase = True
    .Replacement.Text = "^&"
    .Replacement.Highlight = True
    .Replacement.Font.StrikeThrough = True
    .Execute Replace:=wdReplaceAll
  End With

  ' Delete all non-strikethrough words
  Debug.Print "Preparing copied file - 4"
  StatusBar = "Preparing copied file - 4"
  Set rng = ActiveDocument.Range
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = ""
    .Font.StrikeThrough = False
    .MatchWildcards = False
    .MatchCase = True
    .Replacement.Text = "^p"
    .Execute Replace:=wdReplaceAll
  End With

  If ActiveDocument.Paragraphs.Count < maxParas Then
    Debug.Print "Sorting whole file"
    StatusBar = "Sorting whole file"
    Selection.Sort , CaseSensitive:=True
    sdfgsdf = 0
  Else
    Debug.Print "Sorting whole file - method for long lists"
    StatusBar = "Sorting whole file - method for long lists"
    midList = "Jaaaaa"
    ' Add markers at top and bottom
    Selection.EndKey Unit:=wdStory
    Selection.TypeText Text:=midList & CR
    Selection.HomeKey Unit:=wdStory
    Selection.TypeText Text:=midList & CR2

    ' Split the list in two
    ActiveDocument.Paragraphs(ActiveDocument.Paragraphs.Count _
         / 2).Range.Select
    Selection.Collapse wdCollapseEnd
    Selection.Start = 0
    Selection.Sort , CaseSensitive:=True
    Selection.Collapse wdCollapseEnd
    Selection.End = ActiveDocument.Range.End - 1
    Selection.Sort , CaseSensitive:=True

    myEnd = Selection.Start
    Set rng = ActiveDocument.Content
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = midList
      .Wrap = wdFindContinue
      .Replacement.Text = ""
      .Forward = True
      .MatchWildcards = False
      .Execute
    End With

    rng.End = myEnd
    rng.Cut
    Selection.EndKey Unit:=wdStory
    Selection.TypeText Text:=CR2
    Selection.MoveLeft , 1
    Selection.Paste

    Selection.HomeKey Unit:=wdStory
    With Selection.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = midList & "^p"
      .Wrap = wdFindContinue
      .Replacement.Text = ""
      .Forward = True
      .MatchWildcards = False
      .Execute
    End With

    Selection.Collapse wdCollapseEnd
    Selection.Start = 0
    Selection.Sort , CaseSensitive:=True
    Selection.Collapse wdCollapseEnd
    Selection.End = ActiveDocument.Range.End
    Selection.Sort , CaseSensitive:=True
    Selection.MoveStart , -(Len(midList)) - 1
    Selection.End = Selection.Start + 2 * (Len(midList)) + 3
    Selection.Delete
    Selection.EndKey Unit:=wdStory
    Selection.TypeText Text:=CR2
    Selection.MoveStart , -2
    Selection.Range.Font.StrikeThrough = False
  End If
  
  Selection.EndKey Unit:=wdStory
  Selection.TypeText Text:=CR & "Zzzzz" & CR
  Selection.MoveStart wdLine, -1
  Selection.MoveEnd wdLine, 1
  Selection.Range.HighlightColorIndex = 0
  Selection.Collapse wdCollapseEnd

  ' Create a frequency for each highlighted word
  thisWord = ""
  myCount = 0
  i = 0
  For Each myPara In ActiveDocument.Paragraphs
    Set rng = myPara.Range.Words(1)
    nextWord = rng
    If Len(nextWord) > 2 Then
      If rng.Font.StrikeThrough = False Then Exit For
      If nextWord <> thisWord Then
      ' This is a new word
        Selection.TypeText Text:=thisWord & leadDots & _
             Trim(Str(myCount)) & CR
        If isToBeCounted = True Then
          Selection.MoveStart wdLine, -1
          Selection.Range.HighlightColorIndex = wdGray25
          Selection.Collapse wdCollapseEnd
        End If
        thisWord = nextWord
        myCount = 1
      Else
        myCount = myCount + 1
      End If
    End If
    isToBeCounted = (rng.HighlightColorIndex > 0)
    i = i + 1:
    If i Mod 400 = 4 Then
      Debug.Print "Preparing words for frequency list - " & thisWord
      StatusBar = "Preparing words for frequency list - " & thisWord
    End If
  Next myPara

  ' Remove all words except frequency counts
  Set rng = ActiveDocument.Content
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = ""
    .Font.StrikeThrough = True
    .Wrap = wdFindContinue
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = False
    .Execute Replace:=wdReplaceAll
  End With

  ' Remove blank lines
  Set rng = ActiveDocument.Content
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "[^13]{2,}"
    .Wrap = wdFindContinue
    .Replacement.Text = "^p"
    .Forward = True
    .MatchWildcards = True
    .Execute Replace:=wdReplaceAll
  End With

  Set rng = ActiveDocument.Range
  With rng.Find
    .Text = "Oqq"
    .MatchCase = True
    .Replacement.Text = "O'"
    .MatchWildcards = False
    .Execute Replace:=wdReplaceAll
  End With
  
  ' Resort case insensitively
  Selection.Sort , CaseSensitive:=False

  ' Delete rubbish from top and bottom of list
  Do
    Set rng = ActiveDocument.Paragraphs(1).Range
    myLen = Len(rng)
    If myLen < 10 Then rng.Delete
  Loop Until myLen > 9
  Do
    lastLine = ActiveDocument.Paragraphs.Count
    Set rng = ActiveDocument.Paragraphs(lastLine).Range
    If Len(rng) < 3 Then rng.Delete
  Loop Until Len(rng) >= 2

  ' Word list now has freq. count.
Else
  If recordTime = True Then timeStart = Timer
  Set docMainList = ActiveDocument

  ' Just in case the title has been left in...
  For Each myPara In ActiveDocument.Paragraphs
    If InStr(myPara.Range.Text, ". .") = 0 Then _
         myPara.Range.Delete
  Next myPara
  
  
  Set rng = ActiveDocument.Content
  rng.Sort , CaseSensitive:=True

' Delete rubbish from top and bottom of list
  Do
    Set rng = ActiveDocument.Paragraphs(1).Range
    myLen = Len(rng)
    If myLen < 10 Then rng.Delete
  Loop Until myLen > 9
  Do
    lastLine = ActiveDocument.Paragraphs.Count
    Set rng = ActiveDocument.Paragraphs(lastLine).Range
    If Len(rng) < 3 Then rng.Delete
  Loop Until Len(rng) >= 2

' There might be more than one list, so concatenate them
  If doCheckList = True Then
    Set rng = ActiveDocument.Range
    rng.HighlightColorIndex = 0
    For i = 2 To ActiveDocument.Paragraphs.Count
      Set rng1 = ActiveDocument.Paragraphs(i - 1).Range
      Set rng2 = ActiveDocument.Paragraphs(i).Range
      word1 = rng1.Words(1)
      word2 = rng2.Words(1)
      If i Mod 100 = 2 Then
        StatusBar = "Checking frequency list: " & word2
        Debug.Print "Checking frequency list: " & word2
      End If
      If word1 = word2 Then
        numWords1 = rng1.Words.Count
        numWords2 = rng2.Words.Count
        num1 = Val(rng1.Words(numWords1 - 1))
        numText2 = rng2.Words(numWords2 - 1)
        num2 = Val(numText2)
        sumNumText = Trim(Str(num1 + num2))
        rng1.HighlightColorIndex = wdYellow
        rng2.Text = Replace(rng2.Text, numText2, sumNumText)
      End If
    Next i
    Set rng = ActiveDocument.Range
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = ""
      .Highlight = True
      .MatchWildcards = False
      .Replacement.Text = ""
      .Execute Replace:=wdReplaceAll
    End With
  End If
End If

' Delete rubbish from bottom of list
Do
  lastLine = ActiveDocument.Paragraphs.Count
  Set rng = ActiveDocument.Paragraphs(lastLine).Range
  If Len(rng) < 3 Then rng.Delete
Loop Until Len(rng) >= 2

' Now add spell errors in grey
Selection.WholeStory
Selection.Range.HighlightColorIndex = wdGray25

If doSpell = True Then
  StatusBar = "Checking spelling"
  Debug.Print "Checking spelling"
' But take grey off correctly spelled words
  i = 0
  For Each myPara In ActiveDocument.Paragraphs
    myWord = myPara.Range.Words(1)
    If Application.CheckSpelling(myWord, _
         MainDictionary:=Languages(thisLanguage).NameLocal) _
              = True Then myPara.Range.HighlightColorIndex = 0
    i = i + 1
    If i Mod 50 = 1 Then
      Debug.Print "Spellchecking frequency list - " & myWord
      StatusBar = "Spellchecking frequency list - " & myWord
    End If
  Next myPara
End If

' Create another copy for doing extra tests
Set rng = ActiveDocument.Content
rng.Copy
Documents.Add
Set extraList = ActiveDocument
Selection.PasteAndFormat (wdFormatPlainText)


' Prepare data for other tests
numWords = ActiveDocument.Paragraphs.Count
For i = 1 To numWords
  aWord = ActiveDocument.Paragraphs(i).Range.Words(1)
  n = AscW(aWord)
  thisChar = ChrW(n)
  If n > 129 Then
    If n >= 217 Then aWord = Replace(aWord, thisChar, "U")
    If n >= 210 Then aWord = Replace(aWord, thisChar, "O")
    If n >= 204 Then aWord = Replace(aWord, thisChar, "I")
    If n >= 200 Then aWord = Replace(aWord, thisChar, "E")
    If n >= 192 Then aWord = Replace(aWord, thisChar, "A")
  End If
  allWords = allWords & aWord
  jmp = 100
  If i Mod jmp = 1 Then
    pq = pq + 1
    Debug.Print "Preparing data for other tests - 1 - " & pq
    StatusBar = "Preparing data for other tests - 1 - " & pq
  End If
Next i

' ...for the vowel test below
Debug.Print "Preparing data for other tests - 2"
StatusBar = "Preparing data for other tests - 2"
noVowelWords = " " & Replace(allWords, "a", "")
noVowelWords = Replace(noVowelWords, "e", "")
noVowelWords = Replace(noVowelWords, "i", "")
noVowelWords = Replace(noVowelWords, "o", "")
noVowelWords = Replace(noVowelWords, "u", "")
noVowelWords = Replace(noVowelWords, "y", "")
For k = 1 To Len(noVowelWords)
  thisChar = Mid(noVowelWords, k, 1)
  If Asc(thisChar) >= 224 Then
    noVowelWords = Replace(noVowelWords, thisChar, ".")
  End If
Next k
noVowelWords = Replace(noVowelWords, ".", "")

' ...for the similar words test
Debug.Print "Preparing data for other tests - 3"
StatusBar = "Preparing data for other tests - 3"
similarAllWords = " " & LCase(allWords)
similarChars = Replace(similarChars, " ", "")
sChars = Replace(similarChars, " ", "")
Do
  commaPos = InStr(sChars, ",")
  charWas = Left(sChars, commaPos - 1)
  sChars = Mid(sChars, commaPos + 1)
  semicolonPos = InStr(sChars, ";")
  charNew = Left(sChars, semicolonPos - 1)
  sChars = Mid(sChars, semicolonPos + 1)
  similarAllWords = Replace(similarAllWords, charWas, charNew)
Loop Until Len(sChars) < 2

' Change all the accented characters to non-accented
Debug.Print "Preparing data for other tests - 4"
StatusBar = "Preparing data for other tests - 4"
sWd = similarAllWords
For k = 1 To Len(sWd)
  thisChar = Mid(sWd, k, 1)
  n = AscW(thisChar)
  If n > 129 Then
    If n >= 249 Then sWd = Replace(sWd, thisChar, "u")
    If n >= 240 Then sWd = Replace(sWd, thisChar, "o")
    If n >= 236 Then sWd = Replace(sWd, thisChar, "i")
    If n >= 232 Then sWd = Replace(sWd, thisChar, "e")
    If n >= 224 Then sWd = Replace(sWd, thisChar, "a")
    If n >= 217 Then sWd = Replace(sWd, thisChar, "u")
    If n >= 210 Then sWd = Replace(sWd, thisChar, "o")
    If n >= 204 Then sWd = Replace(sWd, thisChar, "i")
    If n >= 200 Then sWd = Replace(sWd, thisChar, "e")
    If n >= 192 Then sWd = Replace(sWd, thisChar, "a")
  End If
Next k
similarAllWords = sWd

' Catch words with only the final two letters the same
i = 0
If checkFinalLetters = True Then
  For Each par In ActiveDocument.Paragraphs
    gotOne = False
    myWord = Trim(par.Range.Words(1))
    myLen = Len(myWord)
    If myLen > 6 Then
      myTarget = "^p" & Left(myWord, myLen - 2) & "^$^$ "
      myCut = 2
    Else
      myTarget = "^p" & Left(myWord, myLen - 1) & "^$ "
      myCut = 1
    End If
    Set rng = ActiveDocument.Content
    rng.Start = par.Range.End - 3
    rng.Collapse wdCollapseStart
    With rng.Find
      .Replacement.ClearFormatting
      .ClearFormatting
      .Text = myTarget
      .Replacement.Text = ""
      .Forward = True
      .MatchCase = True
      .MatchWildcards = False
      .Wrap = False
    End With
    rng.Find.Execute
    Do While rng.Find.Found
      gotOne = True
      rng.MoveStart 1
      rng.End = rng.Start + myLen - myCut
      rng.HighlightColorIndex = thisHighlight
      rng.Font.Bold = True
      rng.Find.Execute
    Loop
    If gotOne = True Then
      Set rng = par.Range.Words(1)
      rng.End = rng.Start + myLen - myCut
      rng.HighlightColorIndex = thisHighlight
      rng.Font.Bold = True
    End If
    i = i + 1
    If i Mod 100 = 1 Then
      StatusBar = "Extra test 1 - " & myWord
      Debug.Print "Extra test 1 - " & myWord
    End If
  Next par
End If

' Start of final group of tests
doneWords = ""
doneSimilarWords = ""
McList = ""

For i = 1 To ActiveDocument.Paragraphs.Count - 1
  myWord = ActiveDocument.Paragraphs(i).Range.Words(1)
  n = AscW(myWord)
  thisChar = ChrW(n)
  If n > 129 Then
    If n >= 217 Then myWord = Replace(myWord, thisChar, "U")
    If n >= 210 Then myWord = Replace(myWord, thisChar, "O")
    If n >= 204 Then myWord = Replace(myWord, thisChar, "I")
    If n >= 200 Then myWord = Replace(myWord, thisChar, "E")
    If n >= 192 Then myWord = Replace(myWord, thisChar, "A")
  End If
  If i Mod 50 = 1 Then
    StatusBar = "Other tests on " & myWord
    Debug.Print "Other tests on " & myWord
  End If
  testWords = Replace(allWords, myWord, "")
  capMyLetter = Left(myWord, 1)

' Check if word reappears with one letter missing
  If doMissingLetter = True Then
    For k = 2 To Len(myWord) - 1
      testWord = " " & Left(myWord, k - 1) & Mid(myWord, k + 1)
      WordPos = InStr(allWords, testWord)
      If WordPos > 0 Then
        lastLetter = Mid(myWord, Len(myWord) - 1, 1)
      ' but not "s" at the end, unless it's a spelling error
        If lastLetter = "s" Then
          ignoreIt = (Application.CheckSpelling(myWord, _
               MainDictionary:=Languages(thisLanguage).NameLocal) _
               = True)
        Else
          ignoreIt = False
        End If
        If ignoreIt = False Then
          colcode = colcode + 1
          If colcode > maxCol Then colcode = 1
          thisCol = myCol(colcode)

          ' mark the pair
          leftBit = Left(allWords, InStr(allWords, testWord) _
               + Len(testWord) - 1)
          j = Len(leftBit) - Len(Replace(leftBit, " ", ""))
          Set rng = ActiveDocument.Paragraphs(i).Range
          rng.HighlightColorIndex = thisCol
          rng.Font.Bold = True
          rng.Font.Color = wdColorBlue
          Set rng = ActiveDocument.Paragraphs(j).Range
          rng.HighlightColorIndex = thisCol
          rng.Font.Bold = True
          rng.Font.Color = wdColorBlue
        End If
      End If
    Next k
  End If

' check similar spellings: Perutz/Peruts or Chebyshev/Chevychev
  If doSimilarLetters = True Then
    similarWord = " " & LCase(myWord)
    sChars = similarChars
    Do
      commaPos = InStr(sChars, ",")
      charWas = Left(sChars, commaPos - 1)
      sChars = Mid(sChars, commaPos + 1)
      semicolonPos = InStr(sChars, ";")
      charNew = Left(sChars, semicolonPos - 1)
      sChars = Mid(sChars, semicolonPos + 1)
      similarWord = Replace(similarWord, charWas, charNew)
    Loop Until Len(sChars) < 2
    
    ' Change all the accented characters to non-accented
    For k = 1 To Len(myWord)
      thisChar = Mid(myWord, k, 1)
      n = AscW(thisChar)
      If n >= 249 Then myWord = Replace(myWord, thisChar, "u")
      If n >= 240 Then myWord = Replace(myWord, thisChar, "o")
      If n >= 236 Then myWord = Replace(myWord, thisChar, "i")
      If n >= 232 Then myWord = Replace(myWord, thisChar, "e")
      If n >= 224 Then myWord = Replace(myWord, thisChar, "a")
      If n >= 217 Then myWord = Replace(myWord, thisChar, "u")
      If n >= 210 Then myWord = Replace(myWord, thisChar, "o")
      If n >= 204 Then myWord = Replace(myWord, thisChar, "i")
      If n >= 200 Then myWord = Replace(myWord, thisChar, "e")
      If n >= 192 Then myWord = Replace(myWord, thisChar, "a")
    Next k
gfhjgfhjy = 0
    similarAllWords = Mid(similarAllWords, Len(similarWord))
    theseWords = similarAllWords
    colcode = colcode + 1
    If colcode > maxCol Then colcode = 1
    thisCol = myCol(colcode)
    If InStr(doneSimilarWords, similarWord) = 0 And _
          InStr(theseWords, similarWord) > 0 Then
      Set rng = ActiveDocument.Paragraphs(i).Range
      rng.HighlightColorIndex = thisCol
      rng.Font.Underline = True
      doneSimilarWords = doneSimilarWords & similarWord
      ' search through all the following words
      theseWords = similarAllWords
      For j = 1 To numWords - i
        spPos = InStr(Trim(theseWords) & " ", " ")
        If Left(theseWords, spPos + 1) = similarWord Then
          Set rng = ActiveDocument.Paragraphs(i + j).Range
          rng.HighlightColorIndex = thisCol
          rng.Font.Underline = True
        End If
        theseWords = Mid(theseWords, spPos + 1)
        capThisLetter = Mid(theseWords, 2, 1)
        If capThisLetter <> capMyLetter Then Exit For
      Next j
    End If
  End If

' check for switched chars
  If switchTest = True Then
    wordLen = Len(myWord) - 1
    For k = 1 To Len(myWord) - 3
      otherWord = Left(myWord, k) & Mid(myWord, k + 2, 1) & _
            Mid(myWord, k + 1, 1) & Mid(myWord, k + 3)
      WordPos = InStr(testWords, otherWord)
      If WordPos > 0 Then
      ' Find the position of the matching word
        matchWord = Mid(testWords, WordPos, Len(myWord))
        leftBit = Left(allWords, InStr(allWords, matchWord) + 1)
        j = Len(leftBit) - Len(Replace(leftBit, " ", "")) + 1
        ActiveDocument.Paragraphs(i).Range.Font.DoubleStrikeThrough = True
        ActiveDocument.Paragraphs(i).Range.HighlightColorIndex = thisCol
        ActiveDocument.Paragraphs(j).Range.Font.DoubleStrikeThrough = True
        ActiveDocument.Paragraphs(j).Range.HighlightColorIndex = thisCol
      End If
    Next k
  End If
    

' check if there's a word with different vowels
  If doVowelTest = True Then
    otherWord = " " & Replace(myWord, "a", "")
    otherWord = Replace(otherWord, "e", "")
    otherWord = Replace(otherWord, "i", "")
    otherWord = Replace(otherWord, "o", "")
    otherWord = Replace(otherWord, "u", "")
    otherWord = Replace(otherWord, "y", "")
    
    ' Delete all the accented characters
    For k = 3 To Len(otherWord)
      thisChar = Mid(otherWord, k, 1)
      If Asc(thisChar) > 129 Then
        otherWord = Replace(otherWord, thisChar, ".")
      End If
    Next k
    otherWord = Replace(otherWord, ".", "")

    noVowelWords = Mid(noVowelWords, Len(otherWord))
    theseWords = noVowelWords
    WordPos = InStr(noVowelWords, otherWord)
    colcode = colcode + 1
    If colcode > maxCol Then colcode = 1
    thisCol = myCol(colcode)
    If InStr(doneWords, otherWord) = 0 And WordPos > 0 Then
      Set rng = ActiveDocument.Paragraphs(i).Range
      rng.HighlightColorIndex = thisCol
      rng.Font.StrikeThrough = True
      doneWords = doneWords & otherWord
      For j = 1 To numWords - i
        spPos = InStr(Trim(theseWords) & " ", " ")
        firstWord = Left(theseWords, spPos + 1)
        theseWords = Mid(theseWords, spPos + 1)
        If firstWord = otherWord Then
          Set rng = ActiveDocument.Paragraphs(i + j).Range
          rng.HighlightColorIndex = thisCol
          rng.Font.StrikeThrough = True
        End If
        capThisLetter = Mid(theseWords, 2, 1)
        If capThisLetter > "" And capThisLetter <> capMyLetter Then Exit For
      Next j
    End If
  End If
  If Left(myWord, 2) = "Mc" Or Left(myWord, 3) = "Mac" Or _
       Left(myWord, 3) = "Mag" Then
    McList = McList & ActiveDocument.Paragraphs(i).Range
  End If
Next i

finishOff:
Selection.EndKey Unit:=wdStory
Selection.TypeText Text:=CR2 & McList

Selection.HomeKey Unit:=wdStory
Selection.TypeText Text:=title2 & CR
Do
  Selection.Expand wdParagraph
  If Len(Selection) < 3 Then Selection.Delete
Loop Until Asc(Selection) > 64
Selection.HomeKey Unit:=wdStory, Extend:=wdExtend
Selection.Style = ActiveDocument.Styles("Heading 1")

' Find first highlight
Set rng = ActiveDocument.Content
With rng.Find
  .Text = "Zzzzz"
  .Wrap = False
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = False
  .Execute Replace:=wdReplaceOne
End With
Set rng = ActiveDocument.Content
With rng.Find
  .Text = ""
  .Highlight = True
  .Wrap = False
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = False
  .Execute
End With

rng.Select
Selection.Collapse wdCollapseStart

docMainList.Activate

' Find sets of sounds-like words
If doSpell = True Then
  StatusBar = "Sounds-like tests"
  k = 0
  For Each par In ActiveDocument.Paragraphs
    myWord = Trim(par.Range.Words(1))
    k = k + 1
    If k Mod 100 = 1 Then
      StatusBar = "Sounds-like test: " & myWord
      Debug.Print "Sounds-like test: " & myWord
    End If
    hasAccent = False
    For i = 1 To Len(myWord)
      ascChar = AscW(Mid(myWord, i))
      If ascChar > 128 Or ascChar = Asc("?") Then hasAccent = True
    Next i

  ' Go and find the first sounds-like word
    initLetter = Left(myWord, 1)
    If Len(myWord) > 2 And par.Range.HighlightColorIndex > 0 And _
         hasAccent = False And InStr(allSets, myWord & leadDots) = 0 Then
      Set rng = ActiveDocument.Content
      Do
        With rng.Find
          .ClearFormatting
          .Replacement.ClearFormatting
          .Text = myWord
          .Wrap = False
          .Replacement.Text = ""
          .MatchWildcards = False
          .MatchSoundsLike = True
          .Execute
        End With
        Set myPara = rng.Paragraphs(1).Range
        rng.Collapse wdCollapseEnd
      Loop Until Left(myPara, 1) = initLetter
      setOfWords = myPara
      gottaSet = False
    ' Go and find the next occurrence (if there is one)
      rng.Collapse wdCollapseEnd
      rng.Find.Execute
      Do While rng.Find.Found = True
        Set myPara = rng.Paragraphs(1).Range
        If Left(myPara, 1) = initLetter Then
          gottaSet = True
          setOfWords = setOfWords & myPara
        End If
        rng.Collapse wdCollapseEnd
        rng.Find.Execute
      Loop
      If gottaSet = True Then allSets = allSets & setOfWords & CR
    End If
  Next par
End If

Selection.WholeStory
If Len(allSets) < 2 Then
  Selection.TypeText Text:="None found with this test"
Else
  Selection.TypeText Text:=allSets
End If
Selection.HomeKey Unit:=wdStory
Selection.TypeText Text:="Proper nouns by sound " & ChrW(8211) & _
     textLanguage & CR
Selection.HomeKey Unit:=wdStory, Extend:=wdExtend
Selection.Style = ActiveDocument.Styles("Heading 1")
Selection.HomeKey Unit:=wdStory

Set rng = ActiveDocument.Content
'    rng.HighlightColorIndex = 0

If Len(allSets) < 2 Then extraList.Activate

' Just to clear the soundlike feature
Set rng = ActiveDocument.Content
With rng.Find
  .Text = "e"
  .MatchWildcards = False
  .MatchSoundsLike = False
  .Execute
End With

StatusBar = ""

Options.DefaultHighlightColorIndex = oldColour
If recordTime = True Then
  myTime = (Int(10 * (Timer - timeStart) / 60) / 10)
  If myTime > 0 Then MsgBox myTime & "  minutes" Else Beep
Else
  Beep
End If
End Sub



Sub SpellListAdd()
' Version 04.06.11
' Take current word and add to FRedit list
' Alt-L

' Select current word
Selection.Words(1).Select
aposn = Len(Selection) + 1 - InStr(Selection, ChrW(8217))
If aposn = 1 Or aposn = 2 Then Selection.MoveEnd , aposn _
     * (Len(Selection) > 1)
theWord = Trim(Selection)

Selection.Start = Selection.End

Selection.TypeText Text:="[[["

Selection.HomeKey Unit:=wdStory
Selection.TypeText Text:=theWord & "|" & theWord & vbCrLf
End Sub





Sub IZtoIS()
' Version 05.03.14
' Correct text to give -is-, -ys- spellings

nonoStyles = "DisplayQuote,ReferenceList"

exceptionFile = "IZ_words"

changeColour = wdTurquoise
bothTCandHighlight = False

' Address where exceptions file is held
myFile = "C:\Documents and Settings\Paul\My Documents\IZ_words.doc"

' Start of main program
Set mainDoc = ActiveDocument

gottadoc = False
For Each myWnd In Application.Windows
  thisName = myWnd.Document.Name
  If InStr(thisName, exceptionFile) > 0 Then
    gottadoc = True
    myWnd.Activate
    Exit For
  End If
Next myWnd

On Error Resume Next
If gottadoc = False Then
  Documents.Open myFile
  If Err.Number = 5174 Then
    MsgBox ("Please open the IZ exceptions file")
    Err.Clear
    Exit Sub
  Else
    On Error GoTo 0
  End If
End If

Selection.HomeKey Unit:=wdStory
myResponse = MsgBox("IZ to IS: Edit the text?", vbQuestion + vbYesNoCancel)
If myResponse = vbCancel Then Exit Sub
If ActiveDocument.TrackRevisions = True And _
     myResponse = vbYes And bothTCandHighlight = False Then changeColour = 0


allWords = "!"
For Each wd In ActiveDocument.Words
  thisWord = Trim(wd)
  If Asc(thisWord) > 32 Then allWords = allWords & thisWord & "!"
Next wd
allWords = LCase(allWords)
mainDoc.Activate

myTrack = ActiveDocument.TrackRevisions
If myResponse = vbNo Then ActiveDocument.TrackRevisions = False

totChanges = 0

For hit = 1 To 4
  If hit = 1 Then
    thisMany = ActiveDocument.Endnotes.Count
    If thisMany > 0 Then
      Set rng = ActiveDocument.StoryRanges(wdEndnotesStory)
      Set rng1 = ActiveDocument.StoryRanges(wdEndnotesStory)
    End If
  End If
  If hit = 2 Then
    thisMany = ActiveDocument.Footnotes.Count
    If thisMany > 0 Then
      Set rng = ActiveDocument.StoryRanges(wdFootnotesStory)
      Set rng1 = ActiveDocument.StoryRanges(wdFootnotesStory)
    End If
  End If
  If hit = 3 Then
    Set rng = ActiveDocument.Content
    Set rng1 = ActiveDocument.Content
    thisMany = 1
  End If
  goes = 1
  If hit = 4 Then
    thisMany = ActiveDocument.Shapes.Count
    goes = thisMany
  End If
  If thisMany > 0 Then
    For myGo = 1 To goes
      If hit = 4 Then
        Do
          someText = ActiveDocument.Shapes(myGo).TextFrame.HasText
          If someText Then
            Set rng = ActiveDocument.Shapes(myGo).TextFrame.TextRange
            Set rng1 = ActiveDocument.Shapes(myGo).TextFrame.TextRange
          Else
            myGo = myGo + 1
          End If
        Loop Until someText Or myGo > goes
        If myGo > goes Then Exit For
      End If
      theEnd = rng.End
      Do
        With rng.Find
          .ClearFormatting
          .Replacement.ClearFormatting
          .Text = "[iy]z[iea]"
          .Wrap = False
          .Replacement.Text = ""
          .Forward = True
          .MatchWildcards = True
          .MatchWholeWord = False
          .MatchSoundsLike = False
          .Execute
        End With

        If rng.Find.Found = True Then
          fnd = rng
          opposite = Replace(fnd, "z", "s")

        ' Find end of word
          rng1.Start = rng.End - 1
          rng1.End = rng.End
          Do
            rng1.End = rng1.End + 1
            rng1.Start = rng1.Start + 1
          Loop Until UCase(rng1) = LCase(rng1)
          wdEnd = rng1.Start

        ' find start of word
          rng1.Start = rng.Start
          rng1.End = rng.Start + 1
          Do
            rng1.End = rng1.End - 1
            rng1.Start = rng1.Start - 1
          Loop Until UCase(rng1) = LCase(rng1)
        ' set rng 1 to the whole word
          rng1.Start = rng1.End
          rng1.End = wdEnd

          changeIt = True
          ' But don't make the change if...
          thisStyle = rng.Style
          If InStr(nonoStyles, thisStyle) > 0 Then changeIt = False
          If rng.Font.StrikeThrough = True Then changeIt = False

        ' if it's not in the list of z's
        kjfjasd = rng1
          If InStr(allWords, "!" & LCase(rng1) & "!") = 0 _
               And changeIt = True Then
          ' then definitely change it to an s
            If myResponse = vbYes Then
              rng.Delete
              rng.InsertAfter Text:=opposite
              If ActiveDocument.TrackRevisions = True Then
                rng1.End = wdEnd + 3
              Else
                rng1.End = wdEnd
              End If
              rng1.End = wdEnd
            End If
            rng1.HighlightColorIndex = changeColour
            totChanges = totChanges + 1
          End If
          stopNow = False
        Else
          stopNow = True
        End If
        rng.Start = rng.End
      Loop Until stopNow = True
    Next myGo
  End If
Next hit

ActiveDocument.TrackRevisions = myTrack
If myResponse = vbYes Then
   MsgBox ("IZ words changed:  " & Str(totChanges) & "  ")
Else
   MsgBox ("IZ words to be changed:  " & Str(totChanges) & "  ")
End If
End Sub



Sub IStoIZ()
' Version 05.03.14
' Correct text to give -iz-, -yz- spellings

doExtraWords = False
szExceptions = "analys,reanalys,overanalys,catalys,dialys,"
szExceptions = szExceptions & "electrolys,paralys,hydrolys"

changeColour = wdTurquoise
bothTCandHighlight = False

nonoStyles = "DisplayQuote,ReferenceList"

exceptionFile = "IS_words"

' Address where exceptions file is held
myFile = "C:\Documents and Settings\Paul\My Documents\IS_words.doc"

' Start of main program
Set mainDoc = ActiveDocument

gottadoc = False
For Each myWnd In Application.Windows
  thisName = myWnd.Document.Name
  If InStr(thisName, exceptionFile) > 0 Then
    gottadoc = True
    myWnd.Activate
    Exit For
  End If
Next myWnd

On Error Resume Next
If gottadoc = False Then
  Documents.Open myFile
  If Err.Number = 5174 Then
    MsgBox ("Please open the IS exceptions file")
    Err.Clear
    Exit Sub
  Else
    On Error GoTo 0
  End If
End If

Selection.HomeKey Unit:=wdStory
myResponse = MsgBox("IS to IZ: Edit the text?", vbQuestion + vbYesNoCancel)
If myResponse = vbCancel Then Exit Sub
If ActiveDocument.TrackRevisions = True And _
     myResponse = vbYes And bothTCandHighlight = False Then changeColour = 0


allWords = "!"
For Each wd In ActiveDocument.Words
  thisWord = Trim(wd)
  If Asc(thisWord) > 32 Then allWords = allWords & thisWord & "!"
Next wd

mainDoc.Activate
myTrack = ActiveDocument.TrackRevisions
If myResponse = vbNo Then ActiveDocument.TrackRevisions = False
totChanges = 0


For hit = 1 To 4
  If hit = 1 Then
    thisMany = ActiveDocument.Endnotes.Count
    If thisMany > 0 Then
      Set rng = ActiveDocument.StoryRanges(wdEndnotesStory)
      Set rng1 = ActiveDocument.StoryRanges(wdEndnotesStory)
    End If
  End If
  If hit = 2 Then
    thisMany = ActiveDocument.Footnotes.Count
    If thisMany > 0 Then
      Set rng = ActiveDocument.StoryRanges(wdFootnotesStory)
      Set rng1 = ActiveDocument.StoryRanges(wdFootnotesStory)
    End If
  End If
  If hit = 3 Then
    Set rng = ActiveDocument.Content
    Set rng1 = ActiveDocument.Content
    thisMany = 1
  End If
  goes = 1
  If hit = 4 Then
    thisMany = ActiveDocument.Shapes.Count
    goes = thisMany
  End If
  If thisMany > 0 Then
    For myGo = 1 To goes
      If hit = 4 Then
        Do
          someText = ActiveDocument.Shapes(myGo).TextFrame.HasText
          If someText Then
            Set rng = ActiveDocument.Shapes(myGo).TextFrame.TextRange
            Set rng1 = ActiveDocument.Shapes(myGo).TextFrame.TextRange
          Else
            myGo = myGo + 1
          End If
        Loop Until someText Or myGo > goes
        If myGo > goes Then Exit For
      End If
      theEnd = rng.End
      Do
        With rng.Find
          .ClearFormatting
          .Replacement.ClearFormatting
          .Text = "[iy]s[iea]"
          .Wrap = False
          .Replacement.Text = ""
          .Forward = True
          .MatchWildcards = True
          .MatchWholeWord = False
          .MatchSoundsLike = False
          .Execute
        End With
        If rng.Find.Found = True Then
          fnd = rng
          opposite = Replace(fnd, "s", "z")

        ' Find end of word
          rng1.Start = rng.End - 1
          rng1.End = rng.End
          Do
            rng1.End = rng1.End + 1
            rng1.Start = rng1.Start + 1
          Loop Until UCase(rng1) = LCase(rng1)
          wdEnd = rng1.Start

        ' find start of word
          rng1.Start = rng.Start
          rng1.End = rng.Start + 1
          Do
            rng1.End = rng1.End - 1
            rng1.Start = rng1.Start - 1
          Loop Until UCase(rng1) = LCase(rng1)

        ' set rng 1 to the whole word
          rng1.Start = rng1.End
          rng1.End = wdEnd
          startWord = rng1.Start

          changeIt = True
          ' But don't make the change if...
          thisStyle = rng.Style
          If InStr(nonoStyles, thisStyle) > 0 Then changeIt = False
          If rng.Font.StrikeThrough = True Then changeIt = False

          ' If -is- is  near the beginning of the word...
          If rng.Start - rng1.Start < 4 Then
            ' look for an -is- later in the word
            rng.Start = rng1.Start + 4
            rng.End = wdEnd
            With rng.Find
              .ClearFormatting
              .Replacement.ClearFormatting
              .Text = "is[iea]"
              .Wrap = False
              .Replacement.Text = ""
              .Forward = True
              .MatchWildcards = True
              .Execute
            End With
            opposite = Replace(rng, "s", "z")
            If rng.Find.Found = False Or rng.Start > wdEnd Then changeIt = False
          End If
        ' Check that it's not in the list of s's
          If InStr(allWords, "!" & LCase(rng1) & "!") > 0 Then changeIt = False
          If InStr(szExceptions, Left(LCase(rng1), 6)) > 0 And _
               rng1.LanguageID = wdEnglishUK Then changeIt = False
          If changeIt = True Then
          ' then change it to a z
            If myResponse = vbYes Then
              rng.Delete
              rng.InsertAfter Text:=opposite
              If ActiveDocument.TrackRevisions = True Then
                rng1.End = wdEnd + 3
              Else
                rng1.End = wdEnd
              End If
            End If
            rng1.HighlightColorIndex = changeColour
            totChanges = totChanges + 1
          End If
          stopNow = False
        Else
          stopNow = True
        End If
        rng.Start = wdEnd
        rng.End = wdEnd
        i = theEnd - rng.End
        If (i Mod 100) = 0 And hit = 3 Then StatusBar = "To go: " & Str(i)
      Loop Until stopNow = True
    Next myGo
  End If
Next hit

ActiveDocument.TrackRevisions = myTrack
If myResponse = vbYes Then
   MsgBox ("IS words changed:  " & Str(totChanges) & "  ")
Else
   MsgBox ("IS words to be changed:  " & Str(totChanges) & "  ")
End If
End Sub



Sub IZwords()
' Version 09.01.14
' Exceptions for IZtoIS

assize
assizes
baize
baizes
Belize
bizarre
bizarrely
bizarreness
Bizet
Brize
byzant
Byzantine
Byzantines
Byzantinism
Byzantium
Byzants
capsize
capsized
capsizes
capsizing
citizen
citizenries
citizenry
citizens
citizenship
citizenships
denizen
denizens
downsize
downsized
downsizes
downsizing
Eliza
Elizabeth
Elizabethan
Giza
Gizeh
liza
lizard
lizards
lizas
maize
midsize
midsized
midsizes
mizen
mizenmast
mizenmasts
mizens
outsize
outsized
outsizes
oversize
oversized
oversizes
pizazz
prize
prized
prizefighter
prizefighters
prizes
prizewinner
prizewinners
prizewinning
prizing
Resize
resized
resizes
resizing
seize
seized
seizes
seizing
sizable
sizably
sizar
sizars
Size
sizeable
sizeably
sized
sizer
sizers
sizes
sizes
Sizewell
sizing
sizism
sizist
undersize
undersized
unsized
vizier
vizierate
vizierates
vizierial
viziers
viziership
vizierships
whizes
wizard
wizardly
wizardries
wizardry
wizards
wizen
wizened
wizenedness
wizening
wizens


End Sub




Sub ISwords()
' Version 09.01.14
' Exceptions for IStoIZ

acquisition
acquisitions
acquisitive
advertise
advertised
advertisement
advertisements
advertiser
advertisers
advertises
advertising
advise
advised
advisedly
adviser
advises
advising
analysis
anticlockwise
antivivisectionist
antivivisectionists
anywise
aphrodisia
aphrodisiac
aphrodisiacal
aphrodisiacs
appraisal
appraisals
appraise
appraised
appraiser
appraisers
appraises
appraising
appraisingly
astrophysical
astrophysically
astrophysicist
astrophysicists
astrophysics
biophysical
biophysically
biophysicist
biophysicists
biophysics
bipartisan
catalysis
chastise
chastised
chastiser
chastisers
chastises
chastising
chrysalis
chrysalises
circumcise
circumcised
circumcises
circumcising
circumcision
circumcisions
civilisation
civilisations
clockwise
collision
collisional
collisions
commiserate
commiserated
commiserates
commiserating
commiseration
commiserations
complaisance
complaisances
complaisant
complaisantly
complaisantness
comprise
comprised
compriser
comprisers
comprises
comprising
compromise
compromised
compromiser
compromisers
compromises
compromising
compromisingly
concise
concisely
conciseness
conciser
concisest
concision
concisions
contraclockwise
contrariwise
counterclockwise
counter -clockwise
counterpoise
counterpoised
counterpoises
counterpoising
countryside
countrysides
crosswise
cruise
cruised
cruises
cruising
cuisine
cuisines
decision
decisions
demise
demises
denoise
denoised
denoises
denoising
derision
despise
despised
despises
despising
devisable
devisably
devise
devised
devises
devising
dialysis
disease
diseases
disquisition
disguise
disguised
disguises
disguising
electrolysis
electrophysiological
electrophysiology
elision
elisions
enfranchise
enfranchised
enfranchisement
enfranchisements
enfranchises
enfranchising
enterprise
enterpriser
enterprises
enterprising
enterprisingly
enterprisingness
envision
envisioned
envisioning
envisions
equipoise
equipoised
equipoises
excision
excisional
excisions
exercise
exercised
exerciser
exercisers
exercises
exercising
expertise
exquisite
exquisitely
exquisiteness
franchise
franchised
franchisee
franchisees
franchiser
franchisers
franchises
franchising
fundraise
fundraised
fundraiser
fundraisers
fundraises
fundraising
geophysical
geophysically
geophysicist
geophysicists
geophysics
glycolysis
heaviside
hemidemisemiquaver
hemidemisemiquavers
hollandaise
hydrolysis
imprecise
imprecisely
impreciseness
imprecision
imprecisions
improvisation
improvisational
improvisatorial
improvisatory
improvise
improvised
improviser
improvises
improvising
inadvisabilities
inadvisability
inadvisable
inadvisableness
indecision
indecisions
indecisive
indecisively
indecisiveness
indivisibilities
indivisibility
indivisible
indivisibleness
indivisibly
inquisition
inquisitional
inquisitions
inquisitive
inquisitively
inquisitiveness
inquisitor
inquisitorial
inquisitorially
inquisitorialness
inquisitors
inquisitory
invisible
invisibly
lackadaisical
lackadaisically
lavoisier
likewise
louise
lyse
mademoiselle
malaise
malaysia
malaysian
malaysians
marquise
marquises
mayonnaise
mayonnaises
metaphysical
metaphysically
metaphysician
metaphysicians
metaphysics
microdialysis
millisecond
milliseconds
misprision
misprisions
moonrise
moonrises
mortise
mortised
mortises
mortising
neurophysiology
neurophysiologist
neurophysiologists
nicoise
nonphysical
otherwise
otherwiseness
overadvertise
overadvertised
overappraisal
overexercise
overpraise
overpraised
overpraises
overpraising
overprecise
overprecisely
overwise
overwisely
paradise
paradises
paralysis
partisan
pelvises
perquisite
perquisites
pharisaic
pharisaical
pharisaically
pharisaicalness
pharisee
pharisees
photophysical
photophysics
phthisis
piecewise
polonaise
polonaises
porpoise
porpoises
portcullises
practise
practised
practises
practising
precise
precised
precisely
preciseness
precises
precising
precision
precisions
premise
premised
premises
prerequisite
prerequisites
prevision
previsions
promise
promised
promisee
promisees
promiser
promisers
promises
promising
promisingly
promisingness
provision
provisional
provisionally
provisionalness
provisionary
provisioned
provisioner
provisioners
provisioning
provisions
psychoanalysis
psychophysics
psychophysical
reacquisition
reacquisitions
reappraisal
reappraisals
reappraise
reappraised
reappraises
reappraising
recision
redivision
reprisal
reprisals
reprise
reprised
reprises
reprising
reprovision
requisite
requisitely
requisiteness
requisites
requisition
requisitionary
requisitioned
requisitioning
requisitions
retrovision
spanwise
stepwise
subdivision
subdivisional
subdivisions
sunrise
sunrises
supervise
supervised
supervises
supervising
supervision
supervisionary
supervisions
surmise
surmised
surmises
surmising
surprise
surprised
surprises
surprising
surprisingly
tbilisi
televisable
televise
televised
televises
televising
television
televisionaries
televisionary
televisions
tortoise
tortoises
tortoiseshell
tortoiseshells
treatise
treatises
trellised
trellises
trellising
turquoise
unacquisitive
unacquisitively
unacquisitiveness
uncompromise
uncompromised
uncompromises
uncompromising
uncompromisingly
uncompromisingness
underexercise
undisguised
undisguisedly
undisguisedness
unenterprising
unenterprisingly
unenterprisingness
unphysical
unphysically
unpractised
unpromised
unpromising
unpromisingly
unpromisingness
unraised
unrevised
unsupervised
unsupervisedly
unsurprised
unsurprisedness
unsurprising
unsurprisingly
upraise
upraised
upraises
upraising
vichyssoise
vichyssoises
vivisectionist
vivisectionists

End Sub





Sub IZIScount()
' Version 05.03.14
' Count IS/IZ spellings

szExceptions = "analys,reanalys,overanalys,catalys,dialys,"
szExceptions = szExceptions & "electrolys,paralys,hydrolys"

changeZColour = wdTurquoise
changeSColour = wdBrightGreen
nonoStyles = "DisplayQuote,ReferenceList"

exceptionSFile = "IS_words"
exceptionZFile = "IZ_words"

' Address where IS exceptions file is held
mySFile = "C:\Documents and Settings\Paul\My Documents\IS_words.doc"

' Address where IS exceptions file is held
myZFile = "C:\Documents and Settings\Paul\My Documents\IZ_words.doc"

Set mainDoc = ActiveDocument

gottaSdoc = False
For Each myWnd In Application.Windows
  thisName = myWnd.Document.Name
  If InStr(thisName, exceptionSFile) > 0 Then
    gottaSdoc = True
    myWnd.Activate
    Exit For
  End If
Next myWnd


On Error Resume Next

If gottaSdoc = False Then
  Documents.Open mySFile
  If Err.Number = 5174 Then
    MsgBox ("Please open the IS exceptions file")
    Err.Clear
    Exit Sub
  Else
    On Error GoTo 0
  End If
End If

allSWords = "!"
For Each wd In ActiveDocument.Words
  thisWord = Trim(wd)
  If Asc(thisWord) > 32 Then allSWords = allSWords & thisWord & "!"
Next wd
allSWords = LCase(allSWords)

gottaZdoc = False
For Each myWnd In Application.Windows
  thisName = myWnd.Document.Name
  If InStr(thisName, exceptionZFile) > 0 Then
    gottaZdoc = True
    myWnd.Activate
    Exit For
  End If
Next myWnd

If gottaZdoc = False Then
  Documents.Open myZFile
  If Err.Number = 5174 Then
    MsgBox ("Please open the IZ exceptions file")
    Err.Clear
    Exit Sub
  Else
    On Error GoTo 0
  End If
'  Set myZdoc = ActiveDocument
End If

allZWords = "!"
For Each wd In ActiveDocument.Words
  thisWord = Trim(wd)
  If Asc(thisWord) > 32 Then allZWords = allZWords & thisWord & "!"
Next wd
allZWords = LCase(allZWords)

myResponse = MsgBox("Highlight the IS/IZ words?", vbQuestion + vbYesNoCancel, "ISIZcount")
If myResponse = vbCancel Then Exit Sub
If myResponse = vbYes Then doHighlight = True

mainDoc.Activate

totZwords = 0
For hit = 1 To 3
  If hit = 1 Then
    thisMany = ActiveDocument.Endnotes.Count
    If thisMany > 0 Then
      Set rng = ActiveDocument.StoryRanges(wdEndnotesStory)
      Set rng1 = ActiveDocument.StoryRanges(wdEndnotesStory)
    End If
  End If
  If hit = 2 Then
    thisMany = ActiveDocument.Footnotes.Count
    If thisMany > 0 Then
      Set rng = ActiveDocument.StoryRanges(wdFootnotesStory)
      Set rng1 = ActiveDocument.StoryRanges(wdFootnotesStory)
    End If
  End If
  If hit = 3 Then
    Set rng = ActiveDocument.Content
    Set rng1 = ActiveDocument.Content
    thisMany = 1
  End If

  If thisMany > 0 Then
    theEnd = rng.End
    Do
      With rng.Find
        .ClearFormatting
        .Replacement.ClearFormatting
        .Text = "[iy]z[iea]"
        .Wrap = False
        .Replacement.Text = ""
        .Forward = True
        .MatchWildcards = True
        .MatchWholeWord = False
        .MatchSoundsLike = False
        .Execute
      End With

      If rng.Find.Found = True Then
        fnd = rng
        opposite = Replace(fnd, "z", "s")

      ' Find end of word
        rng1.Start = rng.End - 1
        rng1.End = rng.End
        Do
          rng1.End = rng1.End + 1
          rng1.Start = rng1.Start + 1
        Loop Until UCase(rng1) = LCase(rng1)
        wdEnd = rng1.Start

      ' find start of word
        rng1.Start = rng.Start
        rng1.End = rng.Start + 1
        Do
          rng1.End = rng1.End - 1
          rng1.Start = rng1.Start - 1
        Loop Until UCase(rng1) = LCase(rng1)

      ' set rng 1 to the whole word
        rng1.Start = rng1.End
        rng1.End = wdEnd

        changeIt = True
        ' But don't make the change if...
        thisStyle = rng.Style
        If InStr(nonoStyles, thisStyle) > 0 Then changeIt = False
        If rng.Font.StrikeThrough = True Then changeIt = False

      ' if it's not in the list of z's
        If InStr(allZWords, "!" & LCase(rng1) & "!") = 0 And changeIt = True Then
          If doHighlight = True Then rng1.HighlightColorIndex = changeZColour
          totZwords = totZwords + 1
        End If
        stopNow = False
      Else
        stopNow = True
      End If
      rng.Start = rng.End
      i = 2 * theEnd - rng.End
      If (i Mod 100) = 0 And hit = 3 Then StatusBar = "To go: " & Str(i)
    Loop Until stopNow = True
  End If
Next hit

totSwords = 0
For hit = 1 To 3
  If hit = 1 Then
    thisMany = ActiveDocument.Endnotes.Count
    If thisMany > 0 Then
      Set rng = ActiveDocument.StoryRanges(wdEndnotesStory)
      Set rng1 = ActiveDocument.StoryRanges(wdEndnotesStory)
    End If
  End If
  If hit = 2 Then
    thisMany = ActiveDocument.Footnotes.Count
    If thisMany > 0 Then
      Set rng = ActiveDocument.StoryRanges(wdFootnotesStory)
      Set rng1 = ActiveDocument.StoryRanges(wdFootnotesStory)
    End If
  End If
  If hit = 3 Then
    Set rng = ActiveDocument.Content
    Set rng1 = ActiveDocument.Content
    thisMany = 1
  End If

  If thisMany > 0 Then
    theEnd = rng.End
    Do
      With rng.Find
        .ClearFormatting
        .Replacement.ClearFormatting
        .Text = "[iy]s[iea]"
        .Wrap = False
        .Replacement.Text = ""
        .Forward = True
        .MatchWildcards = True
        .Execute
      End With

      If rng.Find.Found = True Then
        fnd = rng
        opposite = Replace(fnd, "s", "z")

      ' Find end of word
        rng1.Start = rng.End - 1
        rng1.End = rng.End
        Do
          rng1.End = rng1.End + 1
          rng1.Start = rng1.Start + 1
        Loop Until UCase(rng1) = LCase(rng1)
        wdEnd = rng1.Start

      ' find start of word
        rng1.Start = rng.Start
        rng1.End = rng.Start + 1
        Do
          rng1.End = rng1.End - 1
          rng1.Start = rng1.Start - 1
        Loop Until UCase(rng1) = LCase(rng1)

      ' set rng 1 to the whole word
        rng1.Start = rng1.End
        rng1.End = wdEnd
        startWord = rng1.Start

        changeIt = True
        ' But don't make the change if...
        thisStyle = rng.Style
        If InStr(nonoStyles, thisStyle) > 0 Then changeIt = False
        If rng.Font.StrikeThrough = True Then changeIt = False

        ' If -is- is  near the beginning of the word...
        If rng.Start - rng1.Start < 4 Then
          ' look for an -is- later in the word
          rng.Start = rng1.Start + 4
          rng.End = wdEnd
          With rng.Find
            .ClearFormatting
            .Replacement.ClearFormatting
            .Text = "is[iea]"
            .Wrap = False
            .Replacement.Text = ""
            .Forward = True
            .MatchWildcards = True
            .Execute
          End With
          opposite = Replace(rng, "s", "z")
          If rng.Find.Found = False Or rng.Start > wdEnd Then changeIt = False
        End If
      ' Check that it's not in the list of s's
        If InStr(allSWords, "!" & LCase(rng1) & "!") > 0 Then changeIt = False
        If InStr(szExceptions, Left(LCase(rng1), 6)) > 0 And rng1.LanguageID = wdEnglishUK _
             Then changeIt = False
        If changeIt = True Then
          If doHighlight = True Then rng1.HighlightColorIndex = changeSColour
          totSwords = totSwords + 1
        End If
        stopNow = False
      Else
        stopNow = True
      End If
      rng.Start = wdEnd
      rng.End = wdEnd
      i = theEnd - rng.End
      If (i Mod 100) = 0 And hit = 3 Then StatusBar = "To go: " & Str(i)
    Loop Until stopNow = True
  End If
Next hit
StatusBar = ""
MsgBox ("IZ words:  " & Str(totZwords) & vbCrLf & vbCrLf & "IS words:  " & Str(totSwords))
End Sub



Sub UKUScount()
' Version 04.01.13
' Count relative spellings between UK and US English

minLengthSpell = 5
countIt = True

timeStart = Timer
UKcount = 0
UScount = 0
i = ActiveDocument.Words.Count
istart = i
StatusBar = "Spellchecking. To go:   100%"
For Each wd In ActiveDocument.Words
  If Len(wd) >= minLengthSpell And wd.Font.StrikeThrough = False Then
    UKok = Application.CheckSpelling(wd, _
         MainDictionary:=Languages(wdEnglishUK).NameLocal)
    USok = Application.CheckSpelling(wd, _
         MainDictionary:=Languages(wdEnglishUS).NameLocal)
    fkgsjkdf = wd.Text
    If UKok <> USok Then
      If UKok Then
        UKcount = UKcount + 1
      Else
        UScount = UScount + 1
      End If
      wd.Select
      StatusBar = "Spellchecking. To go:   " & Trim(Str(Int((i / istart) _
           * 100))) & "%                           UK:  " & UKcount & _
           "      US:  " & UScount
    End If
  End If
  i = i - 1
  If i Mod 1000 = 0 Then StatusBar = "Spellchecking. To go:   " & _
       Trim(Str(Int((i / istart) * 100))) & _
       "%                           UK:  " & UKcount & "      US:  " & UScount
Next wd
endTime = Timer
MsgBox "UK: " & UKcount & vbCrLf & vbCrLf & "US: " & UScount
If countIt = True Then MsgBox ((Int(10 * (endTime - timeStart) / 60) / 10) & "  minutes")
Selection.HomeKey Unit:=wdStory
End Sub




Sub UKUShighlight()
' Version 04.01.13
' Mark US spellings within UK text and vice versa

myColour = wdBrightGreen
minLengthSpell = 3

' List here any words that Word erroneously thinks are correct spellings
' Highlight these in a UK text
UKexceptions = "practicing,licencing"
' Highlight these in a US text
USexceptions = "practicing,licencing"

If Selection.LanguageID = wdEnglishUK Then
  mainLanguage = wdEnglishUK: altLanguage = wdEnglishUS
  myList = UKexceptions
Else
  mainLanguage = wdEnglishUS: altLanguage = wdEnglishUK
  myList = USexceptions
End If

' Find words from prefix wordlist
myList = myList & ","
myList = Replace(myList, ",,", ",")
numExceptions = Len(myList) - Len(Replace(myList, ",", ""))
ReDim exWord(numExceptions) As String
For i = 1 To numExceptions
  nextComma = InStr(myList, ",")
  exWord(i) = Left(myList, nextComma - 1)
  myList = Mid(myList, nextComma + 1)
Next i

' To measure the time taken
timeStart = Timer

' Check that tracking is off!
nowTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

' Blank off all apostrophe-s
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ChrW(8217) & "s"
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .MatchCase = True
  .Replacement.Text = " zczc"
  .Execute Replace:=wdReplaceAll
End With

' Spellcheck the endnotes
myJump = 100
If ActiveDocument.Endnotes.Count > 0 Then
  Set rng = ActiveDocument.StoryRanges(wdEndnotesStory)
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = ChrW(8217) & "s"
    .Replacement.Text = " zczc"
    .Execute Replace:=wdReplaceAll
  End With
  countWds = rng.Words.Count
  i = 0
  For Each wd In rng.Words
    If i Mod myJump = 0 Then StatusBar = "Checking words in endnotes: " _
         & Str(Int(i / myJump) * myJump)
    i = i + 1
    If Len(wd) >= minLengthSpell And Application.CheckSpelling(wd, _
         MainDictionary:=Languages(mainLanguage).NameLocal) = False Then
      If Application.CheckSpelling(wd, _
           MainDictionary:=Languages(altLanguage).NameLocal) _
            = True Then wd.HighlightColorIndex = myColour
    End If
  Next wd
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = " zczc"
    .Replacement.Text = ChrW(8217) & "s"
    .Execute Replace:=wdReplaceAll
  End With
  For i = 1 To numExceptions
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = exWord(i)
      .Replacement.Text = ""
      .Replacement.Highlight = True
      .MatchCase = False
      .Execute Replace:=wdReplaceAll
    End With
  Next i
End If

' Spellcheck the footnotes
If ActiveDocument.Footnotes.Count > 0 Then
  Set rng = ActiveDocument.StoryRanges(wdFootnotesStory)
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = ChrW(8217) & "s"
    .Replacement.Text = " zczc"
    .Execute Replace:=wdReplaceAll
  End With
  countWds = rng.Words.Count
  i = 0
  For Each wd In rng.Words
    If i Mod myJump = 0 Then StatusBar = "Checking words in footnotes: " _
         & i
    i = i + 1
    If Len(wd) >= minLengthSpell And Trim(wd) <> "zczc" And _
         Application.CheckSpelling(wd, _
         MainDictionary:=Languages(mainLanguage).NameLocal) = False Then
      If Application.CheckSpelling(wd, Languages(altLanguage).NameLocal) _
          = True Then wd.HighlightColorIndex = myColour
    End If
  Next wd
  With rng.Find
    .Text = " zczc"
    .Replacement.Text = ChrW(8217) & "s"
    .Execute Replace:=wdReplaceAll
  End With
  For i = 1 To numExceptions
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = exWord(i)
      .Replacement.Text = ""
      .Replacement.Highlight = True
      .MatchCase = False
      .Execute Replace:=wdReplaceAll
    End With
  Next i
End If

' Spellcheck the main text
i = ActiveDocument.Words.Count
For Each wd In ActiveDocument.Words
  If Len(wd) >= minLengthSpell And Trim(wd) <> "zczc" And _
       Application.CheckSpelling(wd, _
       MainDictionary:=Languages(mainLanguage).NameLocal) = False Then
    If Application.CheckSpelling(wd, _
         MainDictionary:=Languages(altLanguage).NameLocal) = True _
         Then wd.HighlightColorIndex = myColour
  End If
  i = i - 1
  If i Mod 100 = 0 Then StatusBar = "Spellchecking. To go: " & Str(i)
Next wd

' restore all apostrophe-s
Set rng = ActiveDocument.Range
With rng.Find
  .Text = " zczc"
  .Replacement.Text = ChrW(8217) & "s"
  .Replacement.Highlight = False
  .Execute Replace:=wdReplaceAll
End With
For i = 1 To numExceptions
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = exWord(i)
    .Replacement.Text = ""
    .Replacement.Highlight = True
    .MatchCase = False
    .Execute Replace:=wdReplaceAll
  End With
Next i

totTime = Timer - timeStart
If totTime > 60 Then MsgBox ((Int(10 * (totTime) _
     / 60) / 10) & "  minutes")
ActiveDocument.TrackRevisions = nowTrack
StatusBar = ""
End Sub



Sub AccentAlyse()
' Version 12.11.14
' Analyse the words in a word-frequency list

' These are the accents to watch out for
allAccents = "��ˉ劀̋������������떄����Ϳ�������__��"

' Minimum word length
minLength = 2

Set rng = ActiveDocument.Content
rng.Copy
Documents.Add
Selection.TypeText Text:=vbCr
' Selection.PasteAndFormat (wdFormatPlainText)
Selection.PasteSpecial DataType:=wdPasteText
' Selection.PasteAndFormat (wdPasteDefault)
Selection.WholeStory
Selection.Sort

'Convert to tab-separated
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = " {1,}"
  .Wrap = wdFindContinue
  .Replacement.Text = "^t"
  .Forward = True
  .MatchCase = False
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

Set rng = ActiveDocument.Content
With rng.Find
  .Text = "^p^t"
  .Wrap = wdFindContinue
  .Replacement.Text = "^p"
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

myPointer = 1
Set rng = ActiveDocument.Content
With rng.Find
  .Text = "[" & allAccents & "]"
  .Wrap = wdFindContinue
  .Replacement.Text = "^&"
  .Forward = True
  .MatchWildcards = True
  .Execute
End With

Do While rng.Find.Found = True
  rng.MoveEndUntil cset:=Chr(9), Count:=wdForward
  rng.MoveStartUntil cset:=Chr(13), Count:=wdBackward
  myPointer = rng.End
  myWord = rng.Text
  If Len(myWord) >= minLength Then
    findWord = "^p"
    For i = 1 To Len(myWord)
      myChar = Mid(myWord, i, 1)
      If Asc(myChar) > 122 Then myChar = "^?"
      findWord = findWord & myChar
    Next i
    findWord = findWord & "^t"

    gotTwo = False
    Set rng = ActiveDocument.Content
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = findWord
      .Wrap = False
      .Replacement.Text = "^&"
      .Forward = True
      .MatchCase = True
      .MatchWildcards = False
      .Execute
    End With
    If rng.Text <> Chr(13) & myWord & Chr(9) Then gotTwo = True
    rng.Collapse wdCollapseEnd
    rng.Find.Execute

    Do While rng.Find.Found = True And gotTwo = False
      If rng.Text <> Chr(13) & myWord & Chr(9) Then gotTwo = True
      rng.Collapse wdCollapseEnd
      rng.Find.Execute
    Loop

    If gotTwo = True Then
      Debug.Print "Checking : " & myWord
      Set rng = ActiveDocument.Content
      With rng.Find
        .Text = findWord
        .Wrap = wdFindContinue
        .Replacement.Text = "^&"
        .Replacement.Highlight = True
        .MatchWildcards = False
        .Execute Replace:=wdReplaceAll
      End With
    End If
  End If

  ' Find the next accented word
  Set rng = ActiveDocument.Content
  rng.Start = myPointer
  With rng.Find
   .Text = "[" & allAccents & "]"
   .Wrap = False
   .Replacement.Text = "^&"
   .Forward = True
   .MatchWildcards = True
   .Execute
  End With
Loop

' Unhighlight all line ends
Set rng = ActiveDocument.Content
With rng.Find
  .Text = "^p"
  .Wrap = wdFindContinue
  .Highlight = False
  .Replacement.Text = "^&"
  .Replacement.Highlight = False
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

' Apply highlight to whole line
For Each par In ActiveDocument.Paragraphs
  If par.Range.HighlightColorIndex > 0 Then
    par.Range.HighlightColorIndex = Options.DefaultHighlightColorIndex
  End If
Next par

' Delete unhighlighted lines
Set rng = ActiveDocument.Content
With rng.Find
  .Text = ""
  .Wrap = wdFindContinue
  .Highlight = False
  .Replacement.Text = ""
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

Set rng = ActiveDocument.Content
With rng.Find
  .Text = "^p^p"
  .Wrap = wdFindContinue
  .Replacement.Text = "^p"
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

Selection.HomeKey Unit:=wdStory
Selection.TypeText Text:="Accent Use" & vbCr
startTable = Selection.End + 1
ActiveDocument.Paragraphs(1).Style = ActiveDocument.Styles("Heading 1")
ActiveDocument.Paragraphs(1).Range.HighlightColorIndex = 0
Selection.Start = startTable
Selection.End = ActiveDocument.Range.End
Selection.ConvertToTable Separator:=wdSeparateByTabs
Selection.Tables(1).Style = "Table Grid"
Selection.Tables(1).AutoFitBehavior (wdAutoFitContent)
Selection.Tables(1).Borders(wdBorderTop).LineStyle = wdLineStyleNone
Selection.Tables(1).Borders(wdBorderLeft).LineStyle = wdLineStyleNone
Selection.Tables(1).Borders(wdBorderBottom).LineStyle = wdLineStyleNone
Selection.Tables(1).Borders(wdBorderRight).LineStyle = wdLineStyleNone
Selection.Tables(1).Borders(wdBorderHorizontal).LineStyle = wdLineStyleNone
Selection.Tables(1).Borders(wdBorderVertical).LineStyle = wdLineStyleNone
Selection.Tables(1).Columns(2).Select
Selection.ParagraphFormat.Alignment = wdAlignParagraphRight
Selection.HomeKey Unit:=wdStory

' Clear highlighting and restore F&R to normal
Set rng = ActiveDocument.Content
rng.HighlightColorIndex = 0

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ""
  .Wrap = False
  .Replacement.Text = ""
  .Execute
End With
Beep
End Sub




Sub PDFpagerSimple()
' Version 11.05.11
' Highlight all the page numbers

numDashes = 20
FontSize = 24

' Find the first number
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "\>\>[0-9]@\<\<"
  .MatchWildcards = True
  .Replacement.Text = ""
  .Execute
End With

If rng.Find.Found = False Then
  MsgBox ("Mark first and last page numbers, e.g. >>1<<")
  Exit Sub
End If

startHere = rng.Start
rng.Start = rng.Start + 2
firstNum = Val(rng)
rng.Start = rng.End + 2

' Find the final number
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "\>\>[0-9]@\<\<"
  .MatchWildcards = True
  .Replacement.Text = ""
  .Execute
End With
If rng.Find.Found = False Then
  MsgBox ("Mark first and last page numbers like this: >>123<<")
  Exit Sub
End If

rng.Start = rng.Start + 2
lastNum = Val(rng)

Set rng = ActiveDocument.Range
rng.Start = startHere
For i = lastNum - 1 To firstNum + 1 Step -1
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "^p" & Trim(Str(i))
    .MatchWildcards = False
    .Forward = False
    .Replacement.Text = ""
    .Execute
  End With
  If rng.Find.Found = True Then
    rng.MoveStart wdCharacter, 1
    rng.InsertBefore ">>"
    rng.InsertAfter "<<"
  Else
    rng.InsertBefore vbCrLf & ">>" & Trim(Str(i)) & "<<" & vbCrLf
  End If
  rng.End = rng.Start
  StatusBar = "Page: " & Str(i)
Next i

dottedLine = ""
For i = 1 To numDashes
  dottedLine = dottedLine & ChrW(8211) & " "
Next i

Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^13([ixv]@)^13"
  .Replacement.Text = "^p>>\1<<^p"
  .Replacement.Font.Size = FontSize
  .Forward = True
  .Replacement.Font.Bold = True
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "\>\>[0-9ixv]@\<\<"
  .Replacement.Text = dottedLine & "^p^&"
  .Replacement.Font.Size = FontSize
  .Replacement.Font.Bold = True
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

Selection.HomeKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ">>" & Trim(Str(firstNum)) & "<<"
  .MatchWildcards = False
  .Replacement.Text = ""
  .Execute
End With
Selection.End = Selection.Start
End Sub



Sub PDFpagerOddEven()
' Version 11.05.11
' Highlight all the page numbers alternately left & right

numDashes = 20
FontSize = 24

' Find the first number
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "\>\>[0-9]@\<\<"
  .MatchWildcards = True
  .Replacement.Text = ""
  .Execute
End With

If rng.Find.Found = False Then
  MsgBox ("Mark first and last page numbers, e.g. >>1<<")
  Exit Sub
End If

startHere = rng.Start
rng.Start = rng.Start + 2
firstNum = Val(rng)
rng.Start = rng.End + 2

' Find the final number
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "\>\>[0-9]@\<\<"
  .MatchWildcards = True
  .Replacement.Text = ""
  .Execute
End With
endHere = rng.Start
If rng.Find.Found = False Then
  MsgBox ("Mark first and last page numbers like this: >>123<<")
  Exit Sub
End If

rng.Start = rng.Start + 2
lastNum = Val(rng)

Set rng = ActiveDocument.Range
'  rng.Start = endHere
For i = lastNum - 1 To firstNum + 1 Step -1
  If i Mod 2 = 0 Then
    findText = "^p" & Trim(Str(i))
  Else
    findText = Trim(Str(i)) & "^p"
  End If
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = findText
    .Forward = False
    .MatchWildcards = False
    .Replacement.Text = ""
    .Execute
  End With
  If rng.Find.Found = True Then
    If i Mod 2 = 0 Then
      rng.MoveStart wdCharacter, 1
    Else
      rng.MoveEnd wdCharacter, -1
    End If
    rng.InsertBefore ">>"
    rng.InsertAfter "<<"
  Else
    rng.InsertBefore vbCrLf & ">>" & Trim(Str(i)) & "<<" & vbCrLf
  End If
  rng.End = rng.Start
  StatusBar = "Page: " & Str(i)
Next i

dottedLine = ""
For i = 1 To numDashes
  dottedLine = dottedLine & ChrW(8211) & " "
Next i

Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^13([ixv]@)^13"
  .Replacement.Text = "^p>>\1<<^p"
  .Replacement.Font.Size = FontSize
  .Replacement.Font.Bold = True
  .Forward = True
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "\>\>[0-9ixv]@\<\<"
  .Replacement.Text = "^p" & dottedLine & "^p^&"
  .Replacement.Font.Size = FontSize
  .Replacement.Font.Bold = True
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^p^p"
  .Replacement.Text = "^p"
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

Selection.HomeKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ">>" & Trim(Str(firstNum)) & "<<"
  .MatchWildcards = False
  .Replacement.Text = ""
  .Execute
End With
Selection.End = Selection.Start
End Sub



Sub PhraseCount()
' Version 04.05.10
' Count a selected phrase

CaseSensitive = False

' Hide the hyphens
Dim rng As Range
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .MatchCase = False
  .MatchWildcards = False
  .Text = "-"
  .Replacement.Text = "zczc"
  .Execute Replace:=wdReplaceAll
End With

' Find beginning of phrase list
Selection.HomeKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Text = "......^p"
  .MatchWildcards = False
  .Execute
End With
Selection.Collapse wdCollapseEnd
listStart = Selection.End

totaltotal = 0
diffWords = 0
myPhrase = "something"
Do While Asc(myPhrase) > 13
  ' Select the next phrase
  With Selection.Find
    .ClearFormatting
    .MatchWildcards = True
    .Text = "*^13"
    .Execute
  End With

  Selection.End = Selection.End - 1
  myPhrase = Selection

  If Asc(myPhrase) > 13 Then
    ' Prepare to find the phrase
    Set rng = ActiveDocument.Range
    With rng.Find
      .ClearFormatting
      If CaseSensitive = False Then
        .MatchCase = False
      Else
        .MatchCase = True
      End If
      .MatchWholeWord = True
      .MatchWildcards = False
      .Text = myPhrase
      .Execute
    End With

    ' Count the no. of occurrences
    WordsTotal = 0
    Do
      WordsTotal = WordsTotal + 1
      rng.Find.Execute
    Loop Until rng.End > listStart
    ' Type frequency figure next to the phrase
    Selection.Collapse wdCollapseEnd
    Selection.TypeText Text:=vbTab & Str(WordsTotal)
    Selection.MoveRight Unit:=wdCharacter, Count:=1
    ' Update totals
    totaltotal = totaltotal + WordsTotal
    diffWords = diffWords + 1
  End If
Loop

' Restore the hyphens
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .MatchCase = False
  .MatchWildcards = False
  .Text = "zczc"
  .Replacement.Text = "-"
  .Execute Replace:=wdReplaceAll
End With

Selection.TypeText Text:=vbCrLf & _
     "Words counted = " & Str(totaltotal) & _
     vbCrLf & "Different words =" & Str(diffWords)
End Sub



Sub HyphenAlyse()
' Version 17.03.14
' Compare all hyphenated words with single/double words

myList = "anti,eigen,hyper,inter,meta,mid,multi,non,over,post,pre,pseudo,quasi,semi,sub,super"
myColour = wdColorBlue
includeNumbers = False
Link = ". . . . "
addPlurals = True

CR = vbCrLf
For i = 1 To 15
  spcs = "    " & spcs
Next i
Msg = "Words collected = "
Dim allPairs(4000) As String
timeNow = Timer

myResponse = MsgBox("Analyse hyphenated words?", vbQuestion _
          + vbYesNoCancel, "HyphenAlyse")
If myResponse <> vbYes Then Exit Sub

' Clear down the F&R, for safety
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = " "
  .Wrap = wdFindContinue
  .Replacement.Text = " "
  .Forward = True
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .MatchWildcards = False
  .Execute
End With

Set mainFile = ActiveDocument
' Add endnote and footnotes
Selection.EndKey Unit:=wdStory
If ActiveDocument.Endnotes.Count > 0 Then
  ActiveDocument.StoryRanges(wdEndnotesStory).Copy
  Selection.TypeText CR
  Selection.Paste
  Selection.TypeText CR
End If
If ActiveDocument.Footnotes.Count > 0 Then
  ActiveDocument.StoryRanges(wdFootnotesStory).Copy
  Selection.TypeText CR
  Selection.Paste
  Selection.TypeText CR
End If
' copy all the textboxes to the end of the text
Selection.EndKey Unit:=wdStory
sdfsdf = ActiveDocument.Shapes.Count
If ActiveDocument.Shapes.Count > 0 Then
  myResponse = MsgBox("Include text in textboxes?", vbQuestion _
          + vbYesNoCancel, "HyphenAlyse")
  If myResponse = vbYes Then
    Selection.TypeText CR
    For Each shp In ActiveDocument.Shapes
      If shp.TextFrame.HasText Then
        Set rng = shp.TextFrame.TextRange
        rng.Copy
        Selection.Paste
        Selection.TypeText CR
      End If
    Next
  End If
End If

Selection.WholeStory
Selection.Fields.Unlink
Selection.WholeStory
Selection.Copy
Selection.HomeKey Unit:=wdStory
Documents.Add
Set tempFile = ActiveDocument
Selection.Paste
Selection.WholeStory
Selection.Range.Revisions.AcceptAll
Selection.Range.Style = ActiveDocument.Styles(wdStyleNormal)
Selection.Font.Reset
Selection.Cut
Selection.TypeText Text:=CR
Selection.PasteAndFormat (wdFormatPlainText)
Selection.EndKey Unit:=wdStory
Selection.WholeStory
Selection.Range.Case = wdLowerCase
Selection.Range.HighlightColorIndex = wdGray25

' Get rid of apostrophes
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "'"
  .Wrap = wdFindContinue
  .Replacement.Text = " "
  .Forward = True
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

pairs = 0
Set rng2 = ActiveDocument.Content
If includeNumbers = True Then
  chrs = "[a-zA-Z0-9]"
Else
  chrs = "[a-zA-Z]"
End If

' Find all hyphenated five-word sets, e.g. state-of-the-jolly-art
stopNow = False
Do
  Set rng = ActiveDocument.Content
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = chrs & "@-" & chrs & "@-" & chrs & "@-" & chrs & "@-" & chrs & "{1,}"
    .Highlight = True
    .Wrap = False
    .Replacement.Text = ""
    .MatchWildcards = True
    .Execute
  End With
  If rng.Find.Found = True Then
    pairs = pairs + 1
    StatusBar = spcs & Msg & Str(pairs)
    Debug.Print Msg & Str(pairs)
    myHyphWord = rng
    rng.HighlightColorIndex = wdNoHighlight
    Set rng = ActiveDocument.Range
    thisMany = 1
    hitBottom = False
    Do While hitBottom = False
      With rng.Find
        .ClearFormatting
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = True
        .Highlight = True
        .Text = myHyphWord
        .Wrap = False
        .Execute
      End With
      If rng.Find.Found = True Then
        gotOne = True
        rng.MoveStart , -1
        rng.MoveEnd , 1
        charBefore = Left(rng, 1)
        charAfter = Right(rng, 1)
        If LCase(charBefore) = UCase(charBefore) And LCase(charAfter) _
             = UCase(charAfter) Then
          rng.MoveStart , 1
          rng.MoveEnd , -1
          rng.HighlightColorIndex = wdNoHighlight
          thisMany = thisMany + 1
        End If
        rng.Collapse wdCollapseEnd
      Else
        hitBottom = True
      End If
    Loop
    hyphCount = Trim(Str(thisMany))
    allPairs(pairs) = myHyphWord & Link & hyphCount
  Else
    stopNow = True
  End If
Loop Until stopNow = True

' Find all hyphenated four-word sets, e.g. state-of-the-art
stopNow = False
Do
  Set rng = ActiveDocument.Content
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = chrs & "@-" & chrs & "@-" & chrs & "@-" & chrs & "{1,}"
    .Highlight = True
    .Wrap = False
    .Replacement.Text = ""
    .MatchWildcards = True
    .Execute
  End With
  If rng.Find.Found = True Then
    pairs = pairs + 1
    StatusBar = spcs & Msg & Str(pairs)
    Debug.Print Msg & Str(pairs)
    myHyphWord = rng
    rng.HighlightColorIndex = wdNoHighlight
    Set rng = ActiveDocument.Range
    thisMany = 1
    hitBottom = False
    Do While hitBottom = False
      With rng.Find
        .ClearFormatting
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = True
        .Highlight = True
        .Text = myHyphWord
        .Wrap = False
        .Execute
      End With
      If rng.Find.Found = True Then
        gotOne = True
        rng.MoveStart , -1
        rng.MoveEnd , 1
        charBefore = Left(rng, 1)
        charAfter = Right(rng, 1)
        If LCase(charBefore) = UCase(charBefore) And LCase(charAfter) _
             = UCase(charAfter) Then
          rng.MoveStart , 1
          rng.MoveEnd , -1
          rng.HighlightColorIndex = wdNoHighlight
          thisMany = thisMany + 1
        End If
        rng.Collapse wdCollapseEnd
      Else
        hitBottom = True
      End If
    Loop
    hyphCount = Trim(Str(thisMany))
    allPairs(pairs) = myHyphWord & Link & hyphCount
  Else
    stopNow = True
  End If
Loop Until stopNow = True

' Add hyphenated three-word sets, e.g. day-to-day
stopNow = False
Do
  Set rng = ActiveDocument.Content
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = chrs & "@-" & chrs & "@-" & chrs & "{1,}"
    .Highlight = True
    .Wrap = False
    .Replacement.Text = ""
    .MatchWildcards = True
    .Execute
  End With
  If rng.Find.Found = True Then
    pairs = pairs + 1
    StatusBar = spcs & Msg & Str(pairs)
    Debug.Print Msg & Str(pairs)
    myHyphWord = rng
    rng.HighlightColorIndex = wdNoHighlight
    Set rng = ActiveDocument.Range
    thisMany = 1
    hitBottom = False
    Do While hitBottom = False
      With rng.Find
        .ClearFormatting
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = True
        .Highlight = True
        .Text = myHyphWord
        .Wrap = False
        .Execute
      End With
      If rng.Find.Found = True Then
        gotOne = True
        rng.MoveStart , -1
        rng.MoveEnd , 1
        charBefore = Left(rng, 1)
        charAfter = Right(rng, 1)
        If LCase(charBefore) = UCase(charBefore) And LCase(charAfter) _
             = UCase(charAfter) Then
          rng.MoveStart , 1
          rng.MoveEnd , -1
          rng.HighlightColorIndex = wdNoHighlight
          thisMany = thisMany + 1
        End If
        rng.Collapse wdCollapseEnd
      Else
        hitBottom = True
      End If
    Loop
    hyphCount = Trim(Str(thisMany))
    allPairs(pairs) = myHyphWord & Link & hyphCount
  Else
    stopNow = True
  End If
Loop Until stopNow = True

' Add hyphenated word-pairs to the list
stopNow = False
Do
  Set rng = ActiveDocument.Content
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = chrs & "{1,}-" & chrs & "{1,}"
    .Highlight = True
    .Wrap = wdFindContinue
    .Replacement.Text = ""
    .MatchWildcards = True
    .Execute
  End With
  If rng.Find.Found = True Then
    pairs = pairs + 1
    myHyphWord = rng
    StatusBar = spcs & Msg & Str(pairs)
    Debug.Print Msg & Str(pairs)
    rng.HighlightColorIndex = wdNoHighlight
    Set rng = ActiveDocument.Range
    thisMany = 1
    hitBottom = False
    Do While hitBottom = False
      With rng.Find
        .ClearFormatting
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = True
        .Highlight = True
        .Text = myHyphWord
        .Wrap = False
        .Execute
      End With
      If rng.Find.Found = True Then
        gotOne = True
        rng.MoveStart , -1
        rng.MoveEnd , 1
        charBefore = Left(rng, 1)
        charAfter = Right(rng, 1)
        If LCase(charBefore) = UCase(charBefore) And LCase(charAfter) _
             = UCase(charAfter) Then
          rng.MoveStart , 1
          rng.MoveEnd , -1
          rng.HighlightColorIndex = wdNoHighlight
          thisMany = thisMany + 1
        End If
        rng.Collapse wdCollapseEnd
      Else
        hitBottom = True
      End If
    Loop
    hyphCount = Trim(Str(thisMany))
    allPairs(pairs) = myHyphWord & Link & hyphCount
  Else
    stopNow = True
  End If
Loop Until stopNow = True

' Find words from prefix wordlist
myList = myList & ","
myList = Replace(myList, ",,", ",")
myPrefixNum = Len(myList) - Len(Replace(myList, ",", ""))
ReDim myPrefix(myPrefixNum) As String

' Create an array of the prefixes
For i = 1 To myPrefixNum
  myLen = InStr(myList, ",")
  myPrefix(i) = Left(myList, myLen - 1)
  myList = Mid(myList, myLen + 1)
Next i

' Find all words with each prefix
For i = 1 To myPrefixNum
  Do
    stopNow = False
    Set rng = ActiveDocument.Content
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = "<" & myPrefix(i) & "[ a-z][a-z]{1,}"
      .Highlight = True
      .Wrap = False
      .Replacement.Text = ""
      .Replacement.Highlight = False
      .MatchWildcards = True
      .Execute
    End With
    If rng.Find.Found = True Then
      newWord = rng
      Set rng = ActiveDocument.Content
      With rng.Find
        .Text = newWord
        .Highlight = True
        .Wrap = wdFindContinue
        .Replacement.Text = ""
        .Replacement.Highlight = False
        .MatchWildcards = False
        .Execute Replace:=wdReplaceAll
      End With
      myHyphWord = myPrefix(i) & "-" & Trim(Mid(newWord, Len(myPrefix(i)) + 1))
      gotOne = True
      For j = 1 To pairs
        If InStr(allPairs(j), myHyphWord & Link) > 0 Then
          gotOne = False
          Exit For
        End If
      Next j
      If gotOne = True Then
        pairs = pairs + 1
        StatusBar = spcs & Msg & Str(pairs)
        Debug.Print Msg & Str(pairs)
        allPairs(pairs) = myHyphWord & Link & "0"
      End If
    Else
      stopNow = True
    End If
  Loop Until stopNow = True
Next

' Add plurals to singular words and vice versa
mainPairs = pairs
For i = 1 To mainPairs
  myHyphWord = allPairs(i)
  myHyphWord = Left(myHyphWord, InStr(myHyphWord, Link) - 1)
  OldOne = myHyphWord
  If Right(myHyphWord, 1) = "s" Then
     If Right(myHyphWord, 2) <> "ss" Then myHyphWord = _
          Left(myHyphWord, Len(myHyphWord) - 1)
  Else
     If Right(myHyphWord, 1) <> "y" And Right(myHyphWord, 1) <> "s" _
          Then myHyphWord = myHyphWord & "s"
  End If
  If myHyphWord <> OldOne Then
    gotOne = True
    For j = 1 To mainPairs
      If InStr(allPairs(j), myHyphWord & Link) = 1 Then
        gotOne = False
        Exit For
      End If
    Next j
    If gotOne = True Then
      pairs = pairs + 1
      StatusBar = spcs & Msg & Str(pairs)
      Debug.Print Msg & Str(pairs)
      allPairs(pairs) = myHyphWord & Link & "0"
    End If
  End If
Next i

' Count spaced versions and one-word versions
tempFile.Activate
For i = 1 To pairs
  StatusBar = spcs & "Counting words: " & Trim(Str(i)) & " of " & Trim(Str(pairs))
  Debug.Print "Counting words: " & Trim(Str(i)) & " of " & Trim(Str(pairs))
  myPair = allPairs(i)
  If myPair > "" Then
    myPair = Left(myPair, InStr(myPair, Link) - 1)
    spacePair = Replace(myPair, "-", " ")
    oneWord = Replace(myPair, "-", "")
    Set rng = ActiveDocument.Range
    thisMany = 0
    With rng.Find
      .ClearFormatting
      .MatchCase = False
      .MatchWholeWord = False
      .Text = "<" & spacePair & ">"
      .MatchWildcards = True
      .Execute
    End With
    Do While rng.Find.Found = True
      thisMany = thisMany + 1
      rng.Find.Execute
      rng.Collapse wdCollapseEnd
    Loop
    spaceCount = Trim(Str(thisMany))
 
    Set rng = ActiveDocument.Range
    thisMany = 0
    With rng.Find
      .MatchCase = False
      .MatchWholeWord = True
      .ClearFormatting
      .Text = oneWord
      .MatchWildcards = False
      .Execute
    End With
    Do While rng.Find.Found = True
      thisMany = thisMany + 1
      rng.Find.Execute
      rng.Collapse wdCollapseEnd
    Loop
    oneWordCount = Trim(Str(thisMany))
    allPairs(i) = allPairs(i) & vbTab & spacePair & Link & _
         spaceCount & vbTab & oneWord & Link & oneWordCount
  End If
Next i

ActiveDocument.Close SaveChanges:=False
Documents.Add
For i = 1 To pairs
  Selection.TypeText Text:=allPairs(i) & CR
Next i
Selection.WholeStory
Selection.Sort

' Delete the zero items
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[a-z0-9 .\-]@" & Link & "0"
  .Wrap = wdFindContinue
  .Replacement.Text = ""
  .MatchSoundsLike = False
  .MatchWholeWord = False
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

' Delete the blank lines
With rng.Find
  .Text = "^p^t"
  .Replacement.Text = "^pzczc^t"
  .Wrap = wdFindContinue
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .Text = "zczc^t^t^p"
  .Wrap = wdFindContinue
  .Replacement.Text = ""
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .Text = "^t^t^p"
  .Wrap = wdFindContinue
  .Replacement.Text = "^p"
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .Text = "^t^p"
  .Wrap = wdFindContinue
  .Replacement.Text = "^p"
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .Text = "zczc"
  .Wrap = wdFindContinue
  .Replacement.Text = ""
  .Execute Replace:=wdReplaceAll
End With

'Colour the prefix words
For i = 1 To myPrefixNum
  With Selection.Find
    .Text = "[^13^t]" & myPrefix(i) & "[a-z\- .0-9]{1,}"
    .Replacement.Text = ""
    .Replacement.Font.Color = myColour
    .Wrap = wdFindContinue
    .MatchWildcards = True
    .Execute Replace:=wdReplaceAll
  End With
Next i

For Each par In ActiveDocument.Paragraphs
  pText = par.Range.Text
  numTabs = Len(pText) - Len(Replace(pText, Chr(9), ""))
  If numTabs = 2 And Left(pText, 2) <> Chr(9) & Chr(9) Then _
       par.Range.Font.ColorIndex = wdRed
Next par


Selection.HomeKey Unit:=wdStory
Selection.TypeText Text:="Hyphenation Use"
startTable = Selection.End + 1
ActiveDocument.Paragraphs(1).Style = ActiveDocument.Styles("Heading 1")
Selection.Start = startTable
Selection.End = ActiveDocument.Range.End
Selection.ConvertToTable Separator:=wdSeparateByTabs
Selection.Tables(1).Style = "Table Grid"
Selection.Tables(1).AutoFitBehavior (wdAutoFitContent)
Selection.Tables(1).Borders(wdBorderTop).LineStyle = wdLineStyleNone
Selection.Tables(1).Borders(wdBorderLeft).LineStyle = wdLineStyleNone
Selection.Tables(1).Borders(wdBorderBottom).LineStyle = wdLineStyleNone
Selection.Tables(1).Borders(wdBorderRight).LineStyle = wdLineStyleNone
Selection.Tables(1).Borders(wdBorderHorizontal).LineStyle = wdLineStyleNone
Selection.Tables(1).Borders(wdBorderVertical).LineStyle = wdLineStyleNone

Selection.HomeKey Unit:=wdStory
' Tidy up the F&R
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ""
  .Replacement.Text = ""
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With
newTime = Timer
totTime = newTime - timeNow
MsgBox ("Items: " & Trim(Str(pairs)) & vbCrLf & _
     vbCrLf & (Int(10 * (totTime) / 60) / 10) & "  minutes")
End Sub


Sub WordPairAlyse()
' Version 20.11.14
' Analyse the words that are in pairs or as a single word

' min length of (combined) word to check
minLength = 5

showTime = True
code1 = ChrW(8224): ' "�"
code2 = ChrW(8225): ' "�"

Dim twinWd(200), twinWdFreq(200)

myResponse = MsgBox("Has the frequency list been added at the end?", _
     vbQuestion + vbYesNoCancel, "WordPairAlyse")
  If myResponse <> vbYes Then Exit Sub

myResponse = MsgBox("Is the cursor on the first line of the list?", _
          vbQuestion + vbYesNoCancel, "WordPairAlyse")
  If myResponse <> vbYes Then Exit Sub

timeStart = Timer
Set fullDoc = ActiveDocument
Selection.HomeKey Unit:=wdLine
hereNow = Selection.Start

' Copy just the word list
Set rng = ActiveDocument.Content
rng.Start = hereNow
rng.Copy

Documents.Add
Debug.Print "Checking the word freq. list"
Set listDoc = ActiveDocument
Selection.TypeText Text:=vbCr
' Selection.PasteAndFormat (wdFormatPlainText)
Selection.PasteSpecial DataType:=wdPasteText
' Selection.PasteAndFormat (wdPasteDefault)
Selection.TypeText Text:=vbCr

Debug.Print "Formatting the list"

' Convert list to tab-separated
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = " {1,}"
  .Wrap = wdFindContinue
  .Replacement.Text = "^t"
  .Forward = True
  .MatchCase = False
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^p^t"
  .Wrap = wdFindContinue
  .Replacement.Text = "^p"
  .Forward = True
  .MatchCase = False
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

'Delete items with hyphen or apostrophe
Set rng = ActiveDocument.Content
With rng.Find
  .Text = "^13[a-z]@[\-'" & ChrW(8217) & _
       "0-9]*[0-9]{1,}"
  .Wrap = wdFindContinue
  .Replacement.Text = ""
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

' Set to all lowercase, then join duplicate items
Set rng = ActiveDocument.Content
rng.Case = wdLowerCase
For i = 2 To ActiveDocument.Paragraphs.Count - 3
  Set p1 = ActiveDocument.Paragraphs(i).Range
  Set p2 = ActiveDocument.Paragraphs(i + 1).Range
  If Len(p1.Text & p2.Text) > 8 And p1.Words(1) = _
       p2.Words(1) Then
    num1 = Val(p1.Words(3).Text)
    num2 = Val(p2.Words(3).Text)
    p1.Words(3).Text = "zzz"
    p2.Words(3).Text = Trim(Str(num1 + num2))
  End If
Next i

Set rng = ActiveDocument.Content
With rng.Find
  .Text = "^13[a-z]{1,}^tzzz"
  .Wrap = wdFindContinue
  .Replacement.Text = ""
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

'But now delete the frequency numbers
Set rng = ActiveDocument.Content
With rng.Find
  .Text = "^t[0-9]{1,}"
  .Wrap = wdFindContinue
  .Replacement.Text = ""
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

' Pull list of words into a variable
Set rng = ActiveDocument.Content
allWords = rng.Text
wd = Split(allWords, Chr(13))

' Put the frequency numbers back again, so
' listDoc now contains list with frequencies
WordBasic.EditUndo

' Go to original and collect the text only,
' not the list
fullDoc.Activate
Set rng = ActiveDocument.Content
rng.End = hereNow
rng.Start = 0
rng.Select
Selection.Copy
Selection.Collapse wdCollapseEnd

' Set up text-only document
Documents.Add
Debug.Print "Creating a words-only copy of the text"
Set textDoc = ActiveDocument

Selection.TypeText Text:=" zczc "
' Selection.PasteAndFormat (wdFormatPlainText)
Selection.PasteSpecial DataType:=wdPasteText
' Selection.PasteAndFormat (wdPasteDefault)
Selection.TypeText Text:=" "

Debug.Print "Formatting the text"
Debug.Print "This may take some time!"
' Hyphenated words must be excluded
Set rng = ActiveDocument.Content
With rng.Find
  .Text = "^$-^$"
  .Wrap = wdFindContinue
  .Replacement.Text = "xxx"
  .MatchCase = False
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

' Highlight text and spaces
Set rng = ActiveDocument.Content
With rng.Find
  .Text = "[a-zA-Z ]{1,}"
  .Wrap = wdFindContinue
  .Replacement.Text = "^&"
  .Replacement.Highlight = True
  .MatchCase = False
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

' Replace all the non-text characters by spaces
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ""
  .Wrap = wdFindContinue
  .Replacement.Text = " "
  .Highlight = False
  .MatchCase = False
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

' Multiple spaces
Set rng = ActiveDocument.Content
With rng.Find
  .Text = " {1,}"
  .Wrap = wdFindContinue
  .Replacement.Text = " "
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

' Delete single-letter words
Set rng = ActiveDocument.Content
With rng.Find
  .Text = " ^$ "
  .Wrap = wdFindContinue
  .Replacement.Text = " "
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

' Remember what the text was like
' with just its words and spaces
Selection.WholeStory
Selection.Copy

Documents.Add
Set spacedTextDoc = ActiveDocument
Selection.Paste

textDoc.Activate
' Use codes to split into pairs of words
Set rng = ActiveDocument.Content
With rng.Find
  .Text = " (*) "
  .Wrap = wdFindContinue
  .Replacement.Text = code1 & "\1" & code2
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

' remove alternate markers
Set rng = ActiveDocument.Content
With rng.Find
  .Text = code2
  .Wrap = wdFindContinue
  .Replacement.Text = ""
  .MatchCase = False
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

' Check all words in word list to find matching pairs
' in first version of paired words
twinCount = 0
allWords = "%"
Set rng = ActiveDocument.Content
For i = 1 To UBound(wd)
  Set rng = ActiveDocument.Content
  If Len(wd(i)) >= minLength Then
    Debug.Print "1st pass: " & wd(i)
    With rng.Find
      .Text = code1 & wd(i) & code1
      .Wrap = wdFindContinue
      .Replacement.Text = ""
      .Forward = True
      .MatchCase = False
      .MatchWildcards = False
      .Execute
    End With
    If rng.Find.Found = True Then
      twinCount = twinCount + 1
      twinWd(twinCount) = wd(i)
      allWords = allWords & wd(i) & "%"
    End If
  End If
Next i

Debug.Print "Reformatting the text"
' Reverse the code2 deletion
WordBasic.EditUndo

' remove the other alternate markers
Set rng = ActiveDocument.Content
With rng.Find
  .Text = code1
  .Wrap = wdFindContinue
  .Replacement.Text = ""
  .MatchCase = False
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

' Check all words in word list to find matching pairs
' in second version of paired words
Set rng = ActiveDocument.Content
For i = 1 To UBound(wd)
  Set rng = ActiveDocument.Content
  If Len(wd(i)) >= minLength Then
    Debug.Print "2nd pass: " & wd(i)
    With rng.Find
      .Text = code2 & wd(i) & code2
      .Wrap = wdFindContinue
      .Replacement.Text = ""
      .Forward = True
      .MatchCase = False
      .MatchWildcards = False
      .Execute
    End With
    If rng.Find.Found = True And InStr(allWords, _
         "%" & wd(i) & "%") = 0 Then
      twinCount = twinCount + 1
      twinWd(twinCount) = wd(i)
    End If
  End If
Next i

Debug.Print "Reformatting the text"
ActiveDocument.Close SaveChanges:=False

' Find the equivalent word-pairs from the original
spacedTextDoc.Activate
Set rng = ActiveDocument.Content
myTot = ActiveDocument.Range.End
For j = 1 To twinCount
  myWd = twinWd(j)
  If Len(myWd) > minLength Then
    For i = 2 To Len(myWd) - 1
      leftWd = Left(myWd, i)
      rightWd = Mid(myWd, i + 1)
      wdPair = " " & leftWd & " " & rightWd & " "
      ' search for wdPair
      With rng.Find
        .Text = wdPair
        .Wrap = wdFindContinue
        .Replacement.Text = ""
        .MatchCase = False
        .MatchWildcards = False
        .Execute
      End With
      If rng.Find.Found = True Then Exit For
    Next i
    If rng.Find.Found = True Then
      rng.Find.Execute Replace:=wdReplaceAll
      myCount = myTot - ActiveDocument.Range.End
      twinWdFreq(j) = Int(myCount / (Len(twinWd(j)) + 3))
      twinWd(j) = Trim(wdPair)
      WordBasic.EditUndo
    Else
     ' What do you do if you can't find it?!
      twinWd(j) = twinWd(j) & " Problems"
    End If
  End If
Next j

ActiveDocument.Close SaveChanges:=False

' Copy the list and put it in a new doc for tidiness
listDoc.Activate
Set rng = ActiveDocument.Content
rng.Copy
ActiveDocument.Close SaveChanges:=False

Documents.Add
Selection.Paste

Debug.Print "Formatting the final list"
' Add a line start marker
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^p"
  .Wrap = wdFindContinue
  .Replacement.Text = "^pzczc"
  .Forward = True
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

' Add word pair + frequency
For j = 1 To twinCount
  myWd = Replace(twinWd(j), " ", "")
  Set rng = ActiveDocument.Content
  If Len(myWd) > minLength Then
    With rng.Find
      .Text = "zczc" & myWd & "^t(*)^13"
      .Wrap = wdFindContinue
      .Replacement.Text = myWd & "^t\1^13" & twinWd(j) _
           & "^t" & Trim(twinWdFreq(j)) & "^p^p"
      .MatchWildcards = True
      .Execute Replace:=wdReplaceOne
    End With
  End If
Next j

' Delete all lines still having zczc
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "zczc*^13"
  .Wrap = wdFindContinue
  .Replacement.Text = ""
  .Forward = True
  .MatchCase = False
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

' Tidy up the results sheet
Selection.HomeKey Unit:=wdStory
Selection.TypeText Text:="Word pairs" & vbCr
startTable = Selection.End
ActiveDocument.Paragraphs(1).Style = _
     ActiveDocument.Styles("Heading 1")
ActiveDocument.Paragraphs(1).Range.HighlightColorIndex = 0
Selection.Start = startTable
Selection.End = ActiveDocument.Range.End
Selection.ConvertToTable Separator:=wdSeparateByTabs
Set myTable = Selection.Tables(1)
myTable.Style = "Table Grid"
myTable.AutoFitBehavior (wdAutoFitContent)
myTable.Borders(wdBorderTop).LineStyle = wdLineStyleNone
myTable.Borders(wdBorderLeft).LineStyle = wdLineStyleNone
myTable.Borders(wdBorderBottom).LineStyle = wdLineStyleNone
myTable.Borders(wdBorderRight).LineStyle = wdLineStyleNone
myTable.Borders(wdBorderHorizontal).LineStyle = wdLineStyleNone
myTable.Borders(wdBorderVertical).LineStyle = wdLineStyleNone
myTable.Columns(2).Select
Selection.ParagraphFormat.Alignment = wdAlignParagraphRight

' Dummy copy to reduce clipbaord use
Selection.Copy
Selection.HomeKey Unit:=wdStory

' Clear highlighting and restore F&R to normal
Set rng = ActiveDocument.Content
rng.HighlightColorIndex = 0

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ""
  .Wrap = False
  .Replacement.Text = ""
  .Execute
End With

totTime = Timer - timeStart
If showTime = True And totTime > 60 Then _
  MsgBox ((Int(10 * (totTime) / 60) / 10) & _
       "  minutes")
End Sub


Sub SentenceAlyse()
' Version 04.11.10
' Analyse the size of sentences

myStep = 3
showReadability = False

Dim tot(100) As Integer
Selection.WholeStory
Selection.Copy
Selection.EndKey Unit:=wdStory

Documents.Add
Selection.Paste
ActiveDocument.TrackRevisions = False
WordBasic.AcceptAllChangesInDoc

' Remove multispaces
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = True
  .Text = " {2,}"
  .Replacement.Text = " "
  .Wrap = False
  .Execute Replace:=wdReplaceAll
End With

' and multi-returns
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = True
  .Text = "^13{2,}"
  .Replacement.Text = "^p"
  .Wrap = False
  .Execute Replace:=wdReplaceAll
End With

' Abbreviations that Word thinks are sentences!
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = True
  .Text = ". ([a-z])"
  .Replacement.Text = " \1"
  .Wrap = False
  .Execute Replace:=wdReplaceAll
End With

' Spaces off paragraph ends
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = False
  .Text = " ^p"
  .Replacement.Text = "^p"
  .Wrap = False
  .Execute Replace:=wdReplaceAll
End With

' Spaces off dashes
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = True
  .Text = " [-�] "
  .Replacement.Text = " "
  .Wrap = False
  .Execute Replace:=wdReplaceAll
End With

' Spaces off dashes
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = True
  .Text = "([a-zA-Z])�([a-zA-Z])"
  .Replacement.Text = "\1 \2"
  .Wrap = False
  .Execute Replace:=wdReplaceAll
End With

'Find average sentence length
totalWords = 0
maxLen = 0
maxCol = 0
For Each sn In ActiveDocument.Sentences
  mySnt = sn
  mySnt = Replace(mySnt, Chr(13), "")
  If Right(mySnt, 1) = " " Then myCrrn = 0 Else myCrrn = 1
  wds = Len(mySnt) - Len(Replace(mySnt, " ", "")) + myCrrn
  totalWords = totalWords + wds
  If wds > maxLen Then maxLen = wds
  col = Int((wds - 1) / myStep)
  tot(col) = tot(col) + 1
  If col > maxCol Then maxCol = col
Next sn
sentNum = ActiveDocument.Sentences.Count
meanLength = totalWords / sentNum

' Find standard deviation
Dsq = 0
sntCnt = 0
For Each sn In ActiveDocument.Sentences
  sntCnt = sntCnt + 1
  mySnt = sn
  mySnt = Replace(mySnt, Chr(13), "")
  If Right(mySnt, 1) = " " Then myCrrn = 0 Else myCrrn = 1
  myWds = Len(mySnt) - Len(Replace(mySnt, " ", "")) + myCrrn
  Dsq = Dsq + (meanLength - myWds) ^ 2
Next sn
SD = Sqr(Dsq / sentNum)

' Prepare text of results printout
blank = vbCrLf & vbCrLf
myAnswer = blank & "zczcComplete textczcz" & vbCrLf
myAnswer = myAnswer & "Words = " & Str(totalWords) & vbCrLf
myAnswer = myAnswer & "Sentences = " & Str(sntCnt) & vbCrLf
myAnswer = myAnswer & "Average sentence length = " & Str(Int(meanLength * 10) / 10) & vbCrLf
myAnswer = myAnswer & "Standard deviation = " & Str(Int(SD * 10) / 10) & blank

' Now create the set of frequencies
myFreq = blank & "zczcWith headingsczcz" & vbCrLf
For col = 0 To Int((maxLen - 1) / myStep)
  myFreq = myFreq & Str((col * myStep) + 1) & _
       " to " & Str((col + 1) * myStep) & " = " & _
       tot(col) & vbCrLf
Next

' Delete headings = any line with no full stop
' and then do the stats again
Selection.HomeKey Unit:=wdStory
Do
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .MatchWildcards = True
    .Text = "[a-zA-Z0-9]^13"
    .Replacement.Text = " "
    .Wrap = False
    .Execute
  End With
 
  If Selection.Find.Found = True Then
    keepGoing = True
    Selection.MoveUp Unit:=wdParagraph, Count:=1
    Selection.MoveDown Unit:=wdParagraph, Count:=1, Extend:=wdExtend
    Selection.Delete
  Else
    keepGoing = False
  End If
Loop Until keepGoing = False

'Find average sentence length
totalWords = 0
' Zero the counts ready for next set
For i = 1 To maxCol
  tot(i) = 0
Next
maxLen = 0
maxCol = 0

For Each sn In ActiveDocument.Sentences
  mySnt = sn
  mySnt = Replace(mySnt, Chr(13), "")
  If Right(mySnt, 1) = " " Then myCrrn = 0 Else myCrrn = 1
  wds = Len(mySnt) - Len(Replace(mySnt, " ", "")) + myCrrn
  totalWords = totalWords + wds
  If wds > maxLen Then maxLen = wds
  col = Int((wds - 1) / myStep)
  tot(col) = tot(col) + 1
  If col > maxCol Then maxCol = col
Next sn
sentNum = ActiveDocument.Sentences.Count
meanLength = totalWords / sentNum


' Find standard deviation
Dsq = 0
sntCnt = 0
For Each sn In ActiveDocument.Sentences
  sntCnt = sntCnt + 1
  mySnt = sn
  mySnt = Replace(mySnt, Chr(13), "")
  If Right(mySnt, 1) = " " Then myCrrn = 0 Else myCrrn = 1
  myWds = Len(mySnt) - Len(Replace(mySnt, " ", "")) + myCrrn
  Dsq = Dsq + (meanLength - myWds) ^ 2
Next sn
SD = Sqr(Dsq / sentNum)

' Prepare text of results printout
myAnswer = myAnswer & blank & "zczcWithout headingsczcz" & vbCrLf
myAnswer = myAnswer & "Words = " & Str(totalWords) & vbCrLf
myAnswer = myAnswer & "Sentences = " & Str(sntCnt) & vbCrLf
myAnswer = myAnswer & "Average sentence length = " & Str(Int(meanLength * 10) / 10) & vbCrLf
myAnswer = myAnswer & "Standard deviation = " & Str(Int(SD * 10) / 10) & blank

' Now create the set of frequencies
myFreq = myFreq & blank & "zczcWithout headingsczcz" & vbCrLf
For col = 0 To Int((maxLen - 1) / myStep)
  myFreq = myFreq & Str((col * myStep) + 1) & _
       " to " & Str((col + 1) * myStep) & " = " & _
       tot(col) & vbCrLf
Next

ActiveDocument.TrackRevisions = False
ActiveDocument.Close SaveChanges:=False

Selection.EndKey Unit:=wdStory
Selection.TypeText Text:=myAnswer
Selection.TypeText Text:=myFreq

Set rng = ActiveDocument.Content
rng.LanguageID = wdEnglishUK
rng.NoProofing = False
Application.CheckLanguage = False

If showReadability = True Then
  DocStats = blank & "zczcWord�s Readability Statistics:czcz" & blank
  With ActiveDocument.Content
    For i = 1 To 10
    If i = 1 Or i = 6 Then DocStats = DocStats & "zczc"
    DocStats = DocStats & .ReadabilityStatistics(i) & ": " _
            & .ReadabilityStatistics(i).Value & vbCrLf
    If i = 1 Or i = 6 Then DocStats = DocStats & "czcz"
    Next i
  End With
  Selection.TypeText Text:=DocStats
End If

' Embolden headings
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = True
  .Text = "zczc(*)czcz"
  .Replacement.Text = "\1"
  .Wrap = False
  .Replacement.Font.Bold = True
  .Execute Replace:=wdReplaceAll
End With
End Sub



Sub LongSentenceHighlighter()
' Version 19.05.12
' Highlight all sentences more than a certain length

mediumLength = 80
megaLength = 120

For Each mySent In ActiveDocument.Sentences
  If mySent.Words.Count > megaLength Then
    mySent.HighlightColorIndex = wdRed
    mySent.Select
  Else
    If mySent.Words.Count > mediumLength Then _
         mySent.HighlightColorIndex = wdYellow
    mySent.Select
  End If
Next

End Sub



Sub CountCase()
' Version 23.11.11
' Count the numbers of upper and lowercase characters

myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
myTot = ActiveDocument.Range.End
Set rng = ActiveDocument.Content

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[a-z]"
  .Replacement.Text = "^&!"
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With
totLC = ActiveDocument.Range.End - myTot
If totLC > 0 Then WordBasic.EditUndo

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[A-Z]"
  .Replacement.Text = "^&!"
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With
totUC = ActiveDocument.Range.End - myTot
If totUC > 0 Then WordBasic.EditUndo
  MsgBox "Uppercase: " & totUC & vbCrLf & "Lowercase: " & _
       totLC & vbCrLf & "Percentage: " & Int(1000 * _
       totUC / totLC) / 10 & "%"
ActiveDocument.TrackRevisions = myTrack
End Sub






Sub TooDifficultWordHighlighter()
' Version 15.02.12
' Highlight any words not included in word list

ignoreLength = 3
easyWords = "this that these those" & " "
easyWords = easyWords & "your yours their theirs some" & " "
easyWords = easyWords & "will would could were have give take" & " "

' How many files are open. Warn if wrong number.
numDocs = Word.Documents.Count
If numDocs < 2 Then
  MsgBox "Please open a text file and a word list file."
  Exit Sub
End If
If numDocs > 2 Then
  MsgBox "Too many files open."
  Exit Sub
End If

' Work out which file is which
For Each myDoc In Documents
  myDoc.Activate
  Set rng = ActiveDocument.Content
  rng.End = rng.Start + 50
  myText = LCase(rng.Text)
  If InStr(LCase(rng.Text), "word list") > 0 Then
    Set TheList = myDoc
  Else
    Set theText = myDoc
  End If
Next myDoc

' Pick up all the words in the word list, and add the easy words
TheList.Activate
Set rng = ActiveDocument.Content
allWords = Replace(rng.Text & " " & easyWords, Chr(13), " ")

' Highlight any words longer than ignoreLength
' and that are not in the word list or the easy words
theText.Activate
For Each myWord In ActiveDocument.Words
  thisWord = Trim(myWord.Text) & " "
  longer = Len(thisWord) > ignoreLength + 1
  notInList = InStr(allWords, thisWord) = 0
  If longer And notInList Then myWord.HighlightColorIndex = wdYellow
Next myWord
End Sub





Sub SpecialSortsLister()
' Version 22.03.12
' Collect all special sorts in a file

listAccentedChars = False
Set rng = ActiveDocument.Range
rng.Copy
Documents.Add
Selection.Paste
Selection.WholeStory
Selection.Range.Style = ActiveDocument.Styles(wdStyleNormal)
Selection.HomeKey Unit:=wdStory
numberCmnts = ActiveDocument.Comments.Count
If numberCmnts > 0 Then ActiveDocument.DeleteAllComments
Set rng = ActiveDocument.Content

If listAccentedChars = True Then
  mainChars = "[abcdefghijklmnopqrstuvwxyz" & _
       "ABCDEFGHIJKLMNOPQRSTUVWXYZ^+ ]{1,}"
Else
  mainChars = "[a-zA-Z^+ ]{1,}"
End If

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = mainChars
  .Wrap = wdFindContinue
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With
Beep
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[0-9^13^t,.:;\!\?^=^+\-\(\)�]{1,}"
  .Wrap = wdFindContinue
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With
Beep
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[" & ChrW(8216) & ChrW(8217) & ChrW(8221) & ChrW(8220) _
       & ChrW(8230) & ChrW(174) & ChrW(176) & "]{1,}"
  .Wrap = wdFindContinue
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With
Beep

allSorts = ""
For Each myChar In ActiveDocument.Characters
  uCode = AscW(myChar)
  If InStr(allSorts, myChar) = 0 And uCode > 128 Then
    allSorts = allSorts & myChar & vbTab
    Select Case uCode
      Case 160: extrabit = "non-breaking space"
      Case 176: extrabit = "degree symbol"
      Case 178: extrabit = "dodgy squared symbol"
      Case 179: extrabit = "dodgy cubed symbol"
      Case 184: extrabit = "cedilla"
      Case 186: extrabit = "masculine ordinal"
      Case 215: extrabit = "proper multiply symbol"
      Case 8194: extrabit = "en space"
      Case 8195: extrabit = "em space"
      Case 8201: extrabit = "thin space"
      Case 8222: extrabit = "German open curly quote"
      Case 8226: extrabit = "ordinary bullet"
      Case 8242: extrabit = "unicode: single prime"
      Case 8243: extrabit = "unicode: double prime"
      Case 8249: extrabit = "French open quote"
      Case 8250: extrabit = "French close quote"
      Case 8722: extrabit = "minus sign"

      Case Else: extrabit = "zczc"
    End Select
    allSorts = allSorts & extrabit & vbCrLf
  End If
Next myChar
Selection.WholeStory
If allSorts = "" Then allSorts = "No special sorts used"
Selection.TypeText Text:=allSorts
Selection.WholeStory
Selection.Range.Style = ActiveDocument.Styles(wdStyleNormal)
Selection.Font.Reset
Selection.Sort
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^tzczc"
  .Wrap = wdFindContinue
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With
Selection.EndKey Unit:=wdStory
Selection.TypeText Text:=vbCrLf
Selection.HomeKey Unit:=wdStory
Selection.TypeText "Special sorts used:" & vbCrLf
Beep
End Sub





PT
Sub FRedit()
' Version 08.02.14
' Scripted find and replace
' �2009-2014 Paul Beverley

doFinalBeep = True
showTime = True
speedUpLevel = 5
minShow = 20

debugging = False

promptForSelectedText = True

funnyCode = "Blank"
' used as the code to mean "leave the Find/Replace box blank"

caseCode = ChrW(172)
' This is the 'bent pipe' ('�') character (horizontal line with bent end)

maxLines = 1000
' the maximum number of F&R lines in your list

isMacro = "DoMacro"
repeatMacro = "FindAndDo"

ReDim findText(maxLines) As String, ReplaceText(maxLines) As String
ReDim fHlight(maxLines) As Integer, rHlight(maxLines) As Integer
ReDim fTxtCol(maxLines) As Long, rTxtCol(maxLines) As Long
ReDim fFontSize(maxLines) As Integer, rFontSize(maxLines) As Integer
ReDim styleArray(maxLines, 4) As String, funct(maxLines, 18) As Boolean

timeStart = Timer

For i = 1 To 20
  mySpaces = mySpaces & ">   "
Next i
myBaseStyle = ActiveDocument.Styles(wdStyleNormal)

' FRedit the selected text only?
If Selection.End <> Selection.Start Then
  If promptForSelectedText = True Then
    myResponse = MsgBox("Work on selected text only?", _
         vbQuestion + vbYesNoCancel, "FRedit")
    If myResponse = vbCancel Then Exit Sub
  Else
    myResponse = vbYes
  End If
  wasSelectedText = False
  If myResponse = vbYes Then
    Set rng = ActiveDocument.Range
    rng.Font.DoubleStrikeThrough = True
    Selection.Font.DoubleStrikeThrough = False
    wasSelectedText = True
  End If
End If

' Remember the existing highlight colour
oldColour = Options.DefaultHighlightColorIndex
If debugging = False Then On Error GoTo ReportIt

' Remember which is the currently active file
Set startFile = ActiveDocument

' Assume we haven't found the list
startIsListFile = False

' How many Word files are loaded?
numDocs = Word.Documents.Count
If docs = 1 Then myError = 11: GoTo myErrorReport

NoOfLists = 0
NoOfTexts = 0

allFileNames = ""
For Each myWnd In Application.Windows
  MyfullName = myWnd.Document.FullName
  If InStr(allFileNames, MyfullName) = 0 Then
  Set myDoc = myWnd.Document
    myDoc.Activate
    Set rng = ActiveDocument.Content
  ' Finding a '|' in the first 250 characters means it's a list
    rng.End = rng.Start + 250
    If InStr(rng.Text, "|") > 0 Then
      NoOfLists = NoOfLists + 1
      Set TheList = myDoc
      If myDoc = startFile Then startIsListFile = True
    Else
      NoOfTexts = NoOfTexts + 1
      Set theText = myDoc
    End If
    allFileNames = allFileNames & MyfullName
  Else
    MsgBox "Is this that error?!  " & myDoc.Name
  End If
Next myWnd

If NoOfLists > 1 And NoOfTexts = 1 And Not startIsListFile _
     Then myError = 8: GoTo myErrorReport
If NoOfLists = 1 And NoOfTexts > 1 And startIsListFile _
     Then myError = 7: GoTo myErrorReport
If NoOfLists > 1 And NoOfTexts > 1 Then myError = 9: _
     GoTo myErrorReport

If startIsListFile = False Then Set theText = startFile

' At this point, theList is the script document
' and theText is the file to work on.

If TheList > "" Then
  TheList.Activate
Else
  myError = 11: GoTo myErrorReport
End If
' Create the list of F&Rs
Selection.HomeKey Unit:=wdStory
FRitem = 0: hilight = 1: countEm = False
fNotes = False: eNotes = False: Etext = True: BoxText = False

' FRitem is used to count the actual lines that are F&R commands
lastLine = ActiveDocument.Paragraphs.Count
For i = 1 To lastLine
  ' Look through the list for things that aren't actual F&Rs
  Do
    Do
      Set rng = ActiveDocument.Paragraphs(i).Range
      If lastLine > minShow Then
        StatusBar = mySpaces & "Reading line: " & Str(FRitem)
        Debug.Print "Reading line: " & Str(FRitem)
      End If
      rng.End = rng.End - 1
      allLine = rng
      i = i + 1
      ' Keep going until you find a non-blank line
    Loop Until Len(allLine) > 0 Or i > lastLine
    ' Check if it's a comment line, i.e. starting with a pad
    firstChar = Left(allLine, 1)
    If firstChar = "|" Then
      If InStr(allLine, "ount =") > 0 Then
        ' Check for | count command
        Select Case LCase(Right(allLine, 3))
          Case " no": countEm = False
          Case "yes": countEm = True
          Case Else
          myError = 4: GoTo myErrorReport
        End Select
      End If
      If InStr(allLine, "light =") > 0 Then
        ' Check for | Highlight command
        Select Case LCase(Right(allLine, 3))
          Case "= 0": hilight = 0
          Case "= 1": hilight = 1
          Case " no": hilight = 0
          Case "yes": hilight = 1
          Case Else
          myError = 3: GoTo myErrorReport
        End Select
      End If
      If InStr(allLine, "ootnotes =") > 0 Then
        ' Check for | Footnote command
        Select Case LCase(Right(allLine, 2))
          Case "es": fNotes = True
          Case "no": fNotes = False
          Case Else: myError = 13: GoTo myErrorReport
        End Select
      End If
      If InStr(allLine, "ndnotes =") > 0 Then
        ' Check for | Endnote command
        Select Case LCase(Right(allLine, 2))
          Case "es": eNotes = True
          Case "no": eNotes = False
          Case Else: myError = 14: GoTo myErrorReport
        End Select
      End If
      If InStr(allLine, "ext =") > 0 Then
        ' Check for | Text command
        Select Case LCase(Right(allLine, 2))
          Case "es": Etext = True
          Case "no": Etext = False
          Case Else: myError = 15: GoTo myErrorReport
        End Select
      End If
      If InStr(allLine, "box =") > 0 Or InStr(allLine, "boxes =") > 0 Then
        ' Check for | Textboxes command
        Select Case LCase(Right(allLine, 2))
          Case "es": BoxText = True
          Case "no": BoxText = False
          Case Else: myError = 16: GoTo myErrorReport
        End Select
      End If
    End If
  Loop Until firstChar <> "|" Or i > lastLine
  i = i - 1
 
  ' If you find hashes, stop looking for F&R lines
  If Left(allLine, 1) = "#" Then i = lastLine + 1

  ' It's an F&R line, so check for highlighting
  If i <= lastLine And Len(allLine) > 1 Then
    lineStart = rng.Start
    lineEnd = rng.End

    ' Has it got a pad character in it?
    padPosition = InStr(allLine, "|")
    ' If not, it's a two-line F&R
    If padPosition = 0 Then
      ' We've got two lines
      fText = rng
      ' Clip off MatchCase and Wildcard indicators
      mchWild = False
      mchCase = True
      If Left(fText, 1) = caseCode Then
        fText = Right(fText, Len(fText) - 1)
        mchCase = False
        rng.Start = rng.Start + 1
      End If
      If Left(fText, 1) = "~" Then
        fText = Right(fText, Len(fText) - 1)
        mchWild = True
        rng.Start = rng.Start + 1
      End If
     
      ' What style is the Find in?
      fStyle = rng.Style
      If fStyle = myBaseStyle Then fStyle = ""

      rng.End = lineStart + 1
      ' Check format & type colour of first char of Find
      fItalic = rng.Italic
      fBold = rng.Bold
      fSuper = rng.Font.Superscript
      fSub = rng.Font.Subscript
      fUline = rng.Underline
      fSmall = rng.Font.SmallCaps
      fAllcaps = rng.Font.AllCaps
      fDstrike = rng.Font.DoubleStrikeThrough
      fFont = rng.Font.Name
      fSize = rng.Font.Size
      fHiColour = rng.HighlightColorIndex * hilight
      fTxtColour = rng.Font.Color

      i = i + 1
      Set rng = ActiveDocument.Paragraphs(i).Range
      rng.End = rng.End - 1
      lineEnd = rng.End
      rText = rng
      padPosition = InStr(rText, "|")
      If padPosition > 0 Then myError = 2: GoTo myErrorReport
      ' What style is the Replace in?
      rstyle = rng.Style
      If rstyle = myBaseStyle Then rstyle = ""
      rng.End = rng.Start + 1

      ' Check format & type colour of first char of Replace
      rItalic = rng.Italic
      rBold = rng.Bold
      rSuper = rng.Font.Superscript
      rSub = rng.Font.Subscript
      rUline = rng.Underline
      rSmall = rng.Font.SmallCaps
      rAllcaps = rng.Font.AllCaps
      rDstrike = rng.Font.DoubleStrikeThrough
      rFont = rng.Font.Name
      rSize = rng.Font.Size
      rHiColour = rng.HighlightColorIndex * hilight
      rTxtColour = rng.Font.Color


    Else
      ' It's all on one line, so no style info
      fStyle = ""
      rstyle = ""
      ' Chop up the line into F and R
      fText = Left(allLine, padPosition - 1)
      rText = Right(allLine, Len(allLine) - padPosition)
     
      ' Clip off MatchCase and Wildcard indicators
      mchWild = False
      mchCase = True
      If Left(fText, 1) = caseCode Then
        fText = Right(fText, Len(fText) - 1)
        mchCase = False
        rng.Start = rng.Start + 1
      End If
      If Left(fText, 1) = "~" Then
        fText = Right(fText, Len(fText) - 1)
        mchWild = True
        rng.Start = rng.Start + 1
      End If
     
      rng.End = lineStart + 1
      ' Check format & type colour of first char of Find
      fItalic = rng.Italic
      fBold = rng.Bold
      fSuper = rng.Font.Superscript
      fSub = rng.Font.Subscript
      fUline = rng.Underline
      fSmall = rng.Font.SmallCaps
      fAllcaps = rng.Font.AllCaps
      fDstrike = rng.Font.DoubleStrikeThrough
      fFont = rng.Font.Name
      fSize = rng.Font.Size
      fHiColour = rng.HighlightColorIndex * hilight
      fTxtColour = rng.Font.Color

      rng.End = lineStart + padPosition + 1
      rng.Start = lineStart + padPosition

      ' Check format & type colour of first char of Replace
      rItalic = rng.Italic
      rBold = rng.Bold
      rSuper = rng.Font.Superscript
      rSub = rng.Font.Subscript
      rUline = rng.Underline
      rSmall = rng.Font.SmallCaps
      rAllcaps = rng.Font.AllCaps
      rDstrike = rng.Font.DoubleStrikeThrough
      rFont = rng.Font.Name
      rSize = rng.Font.Size
      rHiColour = rng.HighlightColorIndex * hilight
      rTxtColour = rng.Font.Color
      trackIt = Not (rng.Font.StrikeThrough)
    End If
   
    FRitem = FRitem + 1
    ' Save all the F&R info in arrays
    findText(FRitem) = fText
    ReplaceText(FRitem) = rText
    styleArray(FRitem, 1) = fStyle
    styleArray(FRitem, 2) = rstyle
    styleArray(FRitem, 3) = fFont
    styleArray(FRitem, 4) = rFont
    fHlight(FRitem) = fHiColour
    rHlight(FRitem) = rHiColour
    fTxtCol(FRitem) = fTxtColour
    rTxtCol(FRitem) = rTxtColour
    fFontSize(FRitem) = fSize
    rFontSize(FRitem) = rSize
    funct(FRitem, 1) = mchWild
    funct(FRitem, 2) = mchCase
    funct(FRitem, 3) = fBold
    funct(FRitem, 4) = rBold
    funct(FRitem, 5) = fItalic
    funct(FRitem, 6) = rItalic
    funct(FRitem, 7) = fSuper
    funct(FRitem, 8) = rSuper
    funct(FRitem, 9) = fSub
    funct(FRitem, 10) = rSub
    funct(FRitem, 11) = fUline
    funct(FRitem, 12) = rUline
    funct(FRitem, 13) = fSmall
    funct(FRitem, 14) = rSmall
    funct(FRitem, 15) = fAllcaps
    funct(FRitem, 16) = rAllcaps
    funct(FRitem, 17) = trackIt
    funct(FRitem, 18) = fDstrike

    ' ^p is not allowed in wildcard searches!
    If mchWild And InStr(fText, "^" & "p") > 0 Then
      myError = 5: GoTo myErrorReport
    End If
    If InStr(fText, "^" & "{") > 0 Then
      myError = 95: GoTo myErrorReport
    End If
    ' You can't do case insensitive AND wildcard
    If mchWild And mchCase = False Then
      myError = 6: GoTo myErrorReport
    End If
  End If
Next i
lastItem = FRitem

' Check the Normal font
normalSize = ActiveDocument.Styles(myBaseStyle).Font.Size
normalFont = ActiveDocument.Styles(myBaseStyle).Font.Name

theText.Activate
' Remember if TC is on or off
trackNow = ActiveDocument.TrackRevisions

docLen = ActiveDocument.Range.End
jobSize = docLen * lastItem / 1000000

If jobSize > speedUpLevel And Selection.End > 1000 Then
  speedUp = True
  Beep
  oldStart = Selection.Start
  Selection.Start = Selection.End
  Selection.TypeText Text:="!CursorEnd!"
  Selection.Start = oldStart
  Selection.End = Selection.Start
  Selection.TypeText Text:="!CursorStart!"
  Selection.HomeKey Unit:=wdStory
End If

' Get the data out of the arrays
For FRitem = 1 To lastItem
  fText = findText(FRitem)
  rText = ReplaceText(FRitem)
  fStyle = styleArray(FRitem, 1)
  rstyle = styleArray(FRitem, 2)
  fFont = styleArray(FRitem, 3)
  rFont = styleArray(FRitem, 4)
  fHiColour = fHlight(FRitem)
  rHiColour = rHlight(FRitem)
  fSize = fFontSize(FRitem)
  rSize = rFontSize(FRitem)
  fTxtColour = fTxtCol(FRitem)
  rTxtColour = rTxtCol(FRitem)
  mchWild = funct(FRitem, 1)
  mchCase = funct(FRitem, 2)
  fBold = funct(FRitem, 3)
  rBold = funct(FRitem, 4)
  fItalic = funct(FRitem, 5)
  rItalic = funct(FRitem, 6)
  fSuper = funct(FRitem, 7)
  rSuper = funct(FRitem, 8)
  fSub = funct(FRitem, 9)
  rSub = funct(FRitem, 10)
  fUline = funct(FRitem, 11)
  rUline = funct(FRitem, 12)
  fSmall = funct(FRitem, 13)
  rSmall = funct(FRitem, 14)
  fAllcaps = funct(FRitem, 15)
  rAllcaps = funct(FRitem, 16)
  trackIt = funct(FRitem, 17)
  fDstrike = funct(FRitem, 18)
  If trackNow = True Then ActiveDocument.TrackRevisions = trackIt
  If fText = isMacro Or fText = repeatMacro Then
    If fText = isMacro Then Application.Run MacroName:=rText
    If fText = repeatMacro Then
      Selection.HomeKey Unit:=wdStory
      Do
        posBefore = Selection.Start
        Application.Run MacroName:=rText
        posAfter = Selection.Start
      Loop Until posAfter = posBefore
    End If
  Else
    ' funnyCode means fText should be blank
    If fText = funnyCode Then fText = ""
    BlankIt = False
    If rText = funnyCode Then rText = "": BlankIt = True
    If fText = "<Symbol>" Then
      fText = "": BlankIt = True
      fFont = "Symbol"
      rFont = normalFont
    End If
    ' Replace hex code strings with codes
    codePos = InStr(fText, "<&H")
    codeLen = InStr(fText, ">") - codePos
    If codePos > 0 Then
      uText = Mid(fText, codePos, codeLen)
      uCode = Val(Right(uText, codeLen - 1))
      uChar = ChrW(uCode)
      myLen = Len(fText)
      If myLen > codeLen Then
        fText = Left(fText, codePos - 1) & uChar & _
             Right(fText, myLen - codePos - codeLen)
      Else
        fText = uChar
      End If
    End If
 
    codePos = InStr(rText, "<&H")
    codeLen = InStr(rText, ">") - codePos
    If codePos > 0 Then
      uText = Mid(rText, codePos, codeLen)
      uCode = Val(Right(uText, codeLen - 1))
      uChar = ChrW(uCode)
      myLen = Len(rText)
      If myLen > codeLen Then
        rText = Left(rText, codePos - 1) & uChar & _
             Right(rText, myLen - codePos - codeLen)
      Else
        rText = uChar
      End If
    End If
 
    For hit = 1 To 4
      If hit = 1 Then
        If fNotes = True Then
          goes = ActiveDocument.Footnotes.Count
        Else
          hit = 2
        End If
      End If
      If hit = 2 Then
        If eNotes = True Then
          goes = ActiveDocument.Endnotes.Count
        Else
          hit = 3
        End If
      End If
      If hit = 3 Then
        If Etext = True Then
          goes = 1
        Else
          hit = 4
        End If
      End If
      If hit = 4 Then
        If BoxText = True Then
          goes = ActiveDocument.Shapes.Count
        Else
          hit = 5
        End If
      End If
      If hit < 5 Then
        For myGo = 1 To goes
          If hit = 1 Then Set rng = ActiveDocument.Footnotes(myGo).Range
          If hit = 2 Then Set rng = ActiveDocument.Endnotes(myGo).Range
          If hit = 3 Then Set rng = ActiveDocument.Content
          If hit = 4 Then
            Do
              someText = ActiveDocument.Shapes(myGo).TextFrame.HasText
              If someText Then
                Set rng = ActiveDocument.Shapes(myGo).TextFrame.TextRange
              Else
                myGo = myGo + 1
              End If
            Loop Until someText Or myGo > goes
          End If
          If myGo > goes Then Exit For
          ' Now do the F&R with the appropriate conditions set
          Set rngNow = rng
          endBit = rngNow.End
          Options.DefaultHighlightColorIndex = rHiColour
          If rHiColour <> fHiColour Then
            ' But first emboss all text in fHiColour
            Do
              foundHlight = False
              thisColour = 0
              rngNow.Start = 0
              rngNow.End = 0
              With rngNow.Find
                .ClearFormatting
                .Replacement.ClearFormatting
                .Text = ""
                .Highlight = True
                .Wrap = wdFindContinue
                .Replacement.Text = ""
                .Forward = True
                .MatchWildcards = False
                .MatchWholeWord = False
                .MatchSoundsLike = False
                .Execute
              End With
              ' Special case: all text is in selected highlight colour!
              If rng.Start = 0 And rng.End = ActiveDocument.Range.End _
                   Then rng.Font.Emboss = True
              If rngNow.Find.Found = True Then
                thisColour = rngNow.HighlightColorIndex
                foundHlight = True
              End If
              If thisColour > 100 Then
                rngNow.Start = rngNow.Start - 1
                rngNow.End = rngNow.Start + 1
                Do
                  rngNow.Start = rngNow.Start + 1
                  rngNow.End = rngNow.End + 1
                  thisColour = rngNow.HighlightColorIndex
                Loop Until thisColour = fHiColour _
                     Or rngNow.End >= endBit
                Do
                  rngNow.End = rngNow.End + 1
                  thisColour = rngNow.HighlightColorIndex
                Loop Until thisColour <> fHiColour
                rngNow.End = rngNow.End - 1
                thisColour = fHiColour
              End If
              ' we've found a bit in the fHiColour, and have selected it
              If foundHlight = True Then
                If rngNow.Start = rngNow.End Then
                  foundHlight = False
                Else
                  If thisColour = fHiColour Then rngNow.Font.Emboss = True
                  rngNow.Start = rngNow.End
                End If
              End If
            Loop Until foundHlight = False
          End If
 
          If hit = 1 Then Set rng = ActiveDocument.Footnotes(myGo).Range
          If hit = 2 Then Set rng = ActiveDocument.Endnotes(myGo).Range
          If hit = 3 Then Set rng = ActiveDocument.Content
          If hit = 4 Then Set rng = ActiveDocument.Shapes(myGo).TextFrame.TextRange
          ' Now do the F&R with the appropriate conditions set
          Set rngNow = rng
          With rngNow.Find
            .ClearFormatting
            .Replacement.ClearFormatting
            .Wrap = wdFindContinue
            .Text = fText
            .Replacement.Text = rText
            .MatchWildcards = mchWild
            .MatchCase = mchCase
 
            If fStyle > "" Or rstyle > "" Then
              If rstyle = "" Then rstyle = myBaseStyle
              If fStyle > "" Then .Style = fStyle
              If rstyle > "" Then .Replacement.Style = rstyle
            Else
              ' N.B. If changing styles, don't try to
              ' change bold, italic, etc, etc.
              If fBold <> rBold Then
                .Font.Bold = fBold
                .Replacement.Font.Bold = rBold
              End If
              If rBold Then .Replacement.Font.Bold = True
 
              If fItalic <> rItalic Then
                .Font.Italic = fItalic
                .Replacement.Font.Italic = rItalic
              End If
              If rItalic Then .Replacement.Font.Italic = True
 
              If fSuper <> rSuper Then
                .Font.Superscript = fSuper
                .Replacement.Font.Superscript = rSuper
              End If
              If rSuper Then .Replacement.Font.Superscript = True
 
              If fSub <> rSub Then
                .Font.Subscript = fSub
                .Replacement.Font.Subscript = rSub
              End If
              If rSub Then .Replacement.Font.Subscript = True
 
              If fUline <> rUline Then
                .Font.Underline = fUline
                .Replacement.Font.Underline = rUline
              End If
              If rUline Then .Replacement.Font.Underline = True
 
              If fSmall <> rSmall Then
                .Font.SmallCaps = fSmall
                .Replacement.Font.SmallCaps = rSmall
              End If
              If rSmall Then .Replacement.Font.SmallCaps = True
 
              If fAllcaps <> rAllcaps Then
                .MatchCase = fAllcaps
                .Font.AllCaps = rAllcaps
                .Replacement.Font.AllCaps = False
              End If
              If rAllcaps Then
                .MatchCase = False
                .Replacement.Font.AllCaps = True
              End If
 
              If rDstrike Then .Replacement.Font.StrikeThrough = True
 
              If fStyle = rstyle Then
                If fFont = rFont And fFont <> normalFont Then
                 .Replacement.Font.Name = rFont
                End If
                If fFont <> rFont Then
                  .Font.Name = fFont
                  .Replacement.Font.Name = rFont
                End If
               
                If fSize = rSize And fSize <> normalSize Then
                 .Replacement.Font.Size = rSize
                End If
                If fSize <> rSize Then
                  .Font.Size = fSize
                  .Replacement.Font.Size = rSize
                End If
              End If
            End If
            If rHiColour <> fHiColour And rText > "" Then .Font.Emboss = True
            If rText > "" Then
              .Replacement.Highlight = True
              If rHiColour = 0 Then .Replacement.Highlight = False
            End If
            If rTxtColour = fTxtColour Then
              If fTxtColour > 0 Then .Replacement.Font.Color = fTxtColour
            Else
              If rTxtColour > 0 Or fTxtColour > 0 Then
                .Font.Color = fTxtColour
                If BlankIt = False Then .Replacement.Font.Color = rTxtColour
              End If
            End If
            .Font.StrikeThrough = False
            .Font.DoubleStrikeThrough = False
          End With
          If countEm = False Then
            rngNow.Find.Execute Replace:=wdReplaceAll
            If lastItem > minShow Then
            StatusBar = mySpaces & "F&Ring line: " & Str(FRitem) & "  of " _
                 & Str(lastItem) & "  >  >  >  " & fText & "|" & rText
            Debug.Print "F&Ring line: " & Str(FRitem) & "  of " & Str(lastItem) _
                 & ": " & fText & "|" & rText
            End If
          Else
            ' or count them and stop after each one
            thisMany = -1
            lastStart = 1
            findStart = 0
            Do
              thisMany = thisMany + 1
              rngNow.Find.Execute Replace:=wdReplaceOne
              findStart = rngNow.Start
              If lastStart = findStart Then myError = 20: GoTo myErrorReport
              rngNow.Start = rngNow.End
              rngNow.End = ActiveDocument.Range.End
              lastStart = findStart
            Loop Until rngNow.Start = ActiveDocument.Range.End - 1
           
            ' If selected text contained spaces, make that clear ...
            myResponse = MsgBox(fText & " -> " & rText & " = " & thisMany _
                 & vbCrLf & vbCrLf & "Continue?", vbQuestion + vbYesNo)
            If myResponse = vbNo Then Exit Sub
          End If
          If hit = 1 Then Set rng = ActiveDocument.Footnotes(myGo).Range
          If hit = 2 Then Set rng = ActiveDocument.Endnotes(myGo).Range
          If hit = 3 Then Set rng = ActiveDocument.Content
          If hit = 4 Then Set rng = ActiveDocument.Shapes(myGo).TextFrame.TextRange
          If rHiColour <> fHiColour Then
            rng.Font.Emboss = False
          End If
        Next myGo
      End If
    Next hit
  End If
  ActiveDocument.TrackRevisions = trackNow
Next FRitem

StatusBar = ""
FinishHere:
' Restore highlight colour to normal
Options.DefaultHighlightColorIndex = oldColour
If speedUp = True Then
  Selection.HomeKey Unit:=wdStory
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "!CursorStart!"
    .Replacement.Text = ""
    .MatchWildcards = False
    .Wrap = wdFindContinue
    .Execute Replace:=wdReplaceOne
  End With
  oldStart = Selection.Start
  Selection.MoveLeft Unit:=wdCharacter, Count:=1
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "!CursorEnd!"
    .Replacement.Text = ""
    .Execute Replace:=wdReplaceOne
  End With
  Selection.Start = oldStart
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = ""
  End With
End If
If wasSelectedText = True Then
  Set rng = ActiveDocument.Content
  rng.Font.DoubleStrikeThrough = False
End If
totTime = Timer - timeStart
If showTime = True And totTime > 60 Then
  MsgBox ((Int(10 * (totTime) / 60) / 10) & _
       "  minutes")
Else
  If doFinalBeep = True Then Beep
End If
Exit Sub

' Warn the user about problems that the macro has detected
myErrorReport:
If myError <= 6 Or (myError > 11 And myError < 17) Or myError = 95 Then
  rng.Select
  Selection.Expand wdParagraph
End If
Select Case myError
  Case 2: myPrompt = "No matching replace text"
          Selection.MoveUp Unit:=wdLine, Count:=1
          Selection.Expand wdParagraph
  Case 3: myPrompt = "A 'Highlight =' line should say 'yes' or 'no'."
  Case 4: myPrompt = "A 'Count =' line should say 'yes' or 'no'."
  Case 5: myPrompt = "Sorry, Word can't use ^p in a wildcard search." _
       & vbCrLf & vbCrLf & "On Word for Mac, try [^13]." & vbCrLf & _
       vbCrLf & "On Word for Windows, try ^13."
  Case 95: myPrompt = "Sorry, Word can't use ^{ in a search." _
       & vbCrLf & vbCrLf & "Use ^94{ instead."
  Case 6: myPrompt = "Sorry, Word can't do case insensitive searches with wildcards."
  Case 7: myPrompt = "Sorry, I can't work out which file is which." & vbCrLf & vbCrLf & _
       "Please click in the file to be edited and then rerun FRedit."
  Case 8: myPrompt = "Sorry, I can't work out which file is which." & vbCrLf & vbCrLf & _
       "Please click in the script file and then rerun FRedit."
  Case 9: myPrompt = "More than one FRedit list file is open." & vbCr & vbCr & _
       "Please close unused files and rerun the macro."
  Case 10: myPrompt = "Please create or load a script."
  Case 11: myPrompt = "FRedit needs a file to work on, and a file containing a list of F&Rs."
  Case 12: myPrompt = "Please copy the files into a suitable folder before loading them into Word."
  Case 13: myPrompt = "A 'Footnotes =' line should say 'yes' or 'no'."
  Case 14: myPrompt = "An 'Endnotes =' line should say 'yes' or 'no'."
  Case 15: myPrompt = "A 'Text =' line should say 'yes' or 'no'."
  Case 16: myPrompt = "A 'Textboxes =' line should say 'yes' or 'no'."
  Case 17: myPrompt = "You have used an unacceptable ^(something) in a search."
  Case 18: myPrompt = "Sorry, but Word is playing silly whatsits." _
       & vbCrLf & vbCrLf & "Please close the FRedit script, re-open it and try again. Thanks."
  Case 19: myPrompt = "Unacceptable pattern match in this F&R line."
  Case 20: myPrompt = "Sorry, there's a program error." _
       & vbCrLf & vbCrLf & "Please inform Paul Bev, and say it's an error 20!"
  Case 21: myPrompt = "Can't find a macro called: " & rText
' Problems with counting, when tables are involved.
  Case Else: myPrompt = "Progam error; please inform Paul Bev."
End Select
If jobSize > speedUpLevel Then
  Selection.HomeKey Unit:=wdStory
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "!CursorStart!"
    .Replacement.Text = ""
    .Wrap = wdFindContinue
    .MatchWildcards = False
    .Execute Replace:=wdReplaceOne
  End With
  oldStart = Selection.Start
  Selection.MoveLeft Unit:=wdCharacter, Count:=1
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "!CursorEnd!"
    .Replacement.Text = ""
    .Execute Replace:=wdReplaceOne
  End With
  Selection.Start = oldStart
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = ""
  End With
End If
MsgBox myPrompt, vbOKOnly + vbExclamation, "FRedit"
Exit Sub

' Errors that Word generates end up here
ReportIt:
' There are no files open at all
aksjdhfjkasd = Err.Number
' DoMacro call to unknown macro
If Err.Number < 0 Then myError = 21: GoTo myErrorReport
' Can't find the files it needs
If Err.Number = 4248 Then myError = 11: GoTo myErrorReport
' Trying to run FRedit from the Zip file
If Err.Number = 5941 Then myError = 12: GoTo myErrorReport

errNow = Err.Number
' If we've found the script, select it
TheList.Activate
Set rng = ActiveDocument.Content

' and look for the current line in the script
' which is probably where the problem lies.
fText2 = ""
For j = 1 To Len(fText)
  thisChar = Mid(fText, j, 1)
  fText2 = fText2 & thisChar
  If thisChar = "^" Then fText2 = fText2 & "^"
Next j
fText = fText2

rText2 = ""
For j = 1 To Len(rText)
  thisChar = Mid(rText, j, 1)
  rText2 = rText2 & thisChar
  If thisChar = "^" Then rText2 = rText2 & "^"
Next j
rText = rText2

Selection.HomeKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Text = fText & "|" & rText
  .Forward = True
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute
End With

' If we couldn't find it, maybe it's a two-line F&R
If Not Selection.Find.Found Then
  With Selection.Find
    .ClearFormatting
    .Text = fText & vbCrLf & rText
    .Forward = True
    .MatchWildcards = False
    .MatchWholeWord = False
    .MatchSoundsLike = False
    .Execute
  End With
End If

If Err.Number = 5625 Or Err.Number = 5692 Then
  myError = 17
  GoTo myErrorReport
End If
If errNow = 5560 Then
  myError = 19
  GoTo myErrorReport
End If

' Display Word's error message
MsgBox Err.Description, vbExclamation, "Error from Word"
Resume FinishHere
End Sub


PT
Sub QuotationMarker()
' Version 10.03.12
' Mark all quotes + displayed text

' Do you want displayed text marked?
markDisplayed = True

' Add coding to existing displayed quotes?
addCodes = True
preCode = "<DIS>"
postCode = "</DIS>"
codeOnNextLine = False

' Minimum length of quotes (words)
minLength = 0

' Minimum indent of quotes (cm)
minIndent = 0

' Colour the font of quotations
colourFont = True
myColour = wdColorLightBlue

' Colour of the possible plural possessive problems
possessiveColour = wdColorRed
' Possible possessive errors only shown if within
' this many paragraphs
maxParasPossessive = 5

' Add a highlight
highlightText = True
myHighlight = wdTurquoise

' Strike it through
strikeSingle = True
strikeDouble = False

' What kind of quote?
doDoubleQuotes = True
doSingleQuotes = True

' Do you want the notes checked (if there are any)?
doFootnotes = True
doEndnotes = True


myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
nowColour = Options.DefaultHighlightColorIndex
Options.DefaultHighlightColorIndex = myHighlight
soCalledWords = "---,.;:!?�" & ChrW(8220) & ChrW(8221)

' Clear old codes, in case you're running it a second time
If addCodes = True Then
  Set rng = ActiveDocument.Content
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = preCode
    .Wrap = wdFindContinue
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = False
    .Execute Replace:=wdReplaceAll
  End With
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = postCode
    .Wrap = wdFindContinue
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = False
    .Execute Replace:=wdReplaceAll
  End With
End If

If markDisplayed = True Then
  For Each myPara In ActiveDocument.Paragraphs
    If myPara.Range.ParagraphFormat.LeftIndent > _
         CentimetersToPoints(minIndent) Then
      myPara.Range.Font.Emboss = True
      If addCodes = True Then
        myPara.Range.InsertBefore Text:=preCode
        myPara.Range.InsertAfter Text:=postCode
      End If
    End If
  Next

  Set rng = ActiveDocument.Content
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = preCode & postCode
    .Wrap = wdFindContinue
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = False
    .Execute Replace:=wdReplaceAll
  End With

  If codeOnNextLine = False Then
    With rng.Find
      .Text = "^p" & postCode
      .Replacement.Text = postCode & "^p"
      .Execute Replace:=wdReplaceAll
    End With
  End If
End If

gottaFunny = False
funnyCount = 0
For i = 1 To 3
  If i = 1 And (ActiveDocument.Footnotes.Count = 0 Or _
       doFootnotes = False) Then i = 2
  If i = 2 And (ActiveDocument.Endnotes.Count = 0 Or _
       doEndnotes = False) Then i = 3
  Select Case i
    Case 1: Set rng = ActiveDocument.StoryRanges(wdFootnotesStory)
    Case 2: Set rng = ActiveDocument.StoryRanges(wdEndnotesStory)
    Case 3: Set rng = ActiveDocument.Content
  End Select

' Single quotes
  If doSingleQuotes = True Then
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = ChrW(8216) & "*" & ChrW(8217)
      .Wrap = wdFindContinue
      .Replacement.Text = ""
      .Replacement.Font.Emboss = True
      .Forward = True
      .MatchWildcards = True
      .Execute Replace:=wdReplaceAll
    End With
  End If

' Check for any apostrophe-letter that is half struck-through
  If doSingleQuotes = True Then
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = ChrW(8217) & "^$"
      .Wrap = False
      .Replacement.Text = ""
      .Forward = True
      .MatchWildcards = False
      .Execute
    End With

    Do While rng.Find.Found = True
      ' If this is half struckthrough
      If rng.Font.Emboss > 999 Then
      ' Extend to the following close single quote
        rng.MoveEndUntil cset:=ChrW(8217), Count:=wdForward
        rng.MoveEnd wdCharacter, 1
        rng.Font.Emboss = True
        rng.Start = rng.End - 1
        rng.Collapse wdCollapseStart
      Else
        rng.Collapse wdCollapseEnd
      End If
      rng.Find.Execute
    Loop

  ' Now check for plural possessives: s'
    Select Case i
      Case 1: Set rng = ActiveDocument.StoryRanges(wdFootnotesStory)
      Case 2: Set rng = ActiveDocument.StoryRanges(wdEndnotesStory)
      Case 3: Set rng = ActiveDocument.Content
    End Select
  ' Add a dummy open quote at the end of the text
    rng.InsertAfter ChrW(8216)
    theVeryEnd = rng.End
  ' Check from each of the s-apostrophes to the next open quote
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = "s" & ChrW(8217) & "*" & ChrW(8216)
      .Wrap = False
      .Replacement.Text = ""
      .Forward = True
      .MatchWildcards = True
      .Execute
    End With

    Do While rng.Find.Found = True
      endNow = rng.End
      rng.End = rng.Start + 1
      isQuote = rng.Font.Emboss
      rng.End = endNow
      rng.MoveStart wdCharacter, 2
      endQuoteWas = rng.Start
      myText = rng.Text
      If InStr(myText, ChrW(8217)) > 0 And isQuote = True Then
      ' How many apostrophe/close quotes are there?
        aposNum = Len(myText) - Len(Replace(myText, ChrW(8217), ""))
        myPointer = endQuoteWas
        endQuoteNow = myPointer
        For j = 1 To aposNum
          aposPos = InStr(myText, ChrW(8217))
          nextChar = Mid(myText, aposPos + 1, 1)
          myPointer = myPointer + aposPos
        ' Is nextChar non-alpha? If so, this is the *real* end of the quote
          If LCase(nextChar) = UCase(nextChar) Then
            endQuoteNow = myPointer
          End If
          myText = Mid(myText, aposPos + 1)
        Next
        If endQuoteNow > endQuoteWas Then
          endNow = rng.End
          rng.End = endQuoteNow
          rng.Font.Emboss = True
          myChar = Right(rng.Text, 2)
          thisLot = rng.Text
          numParas = Len(thisLot) - Len(Replace(thisLot, Chr(13), ""))
          If Asc(myChar) = Asc("s") And numParas < maxParasPossessive Then
            gottaFunny = True
            rng.Font.Emboss = False
            rng.Font.Color = possessiveColour
            funnyCount = funnyCount + 1
          End If
        End If
        rng.End = myPointer
      End If
      rng.Collapse wdCollapseEnd
      rng.Find.Execute
    Loop
    rng.Start = theVeryEnd - 2
    rng.End = theVeryEnd - 1
    rng.Delete
  End If

' Double quotes
  If doDoubleQuotes = True Then
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = ChrW(8220) & "*" & ChrW(8221)
      .Wrap = wdFindContinue
      .Replacement.Text = ""
      .Replacement.Font.Emboss = True
      .Forward = True
      .MatchWildcards = True
      .Execute Replace:=wdReplaceAll
    End With
  End If

' Remove embossing from short quotes
  If minLength > 0 Then
    Select Case i
      Case 1: Set rng = ActiveDocument.StoryRanges(wdFootnotesStory)
      Case 2: Set rng = ActiveDocument.StoryRanges(wdEndnotesStory)
      Case 3: Set rng = ActiveDocument.Content
    End Select
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = ""
      .Font.Emboss = True
      .Wrap = False
      .Replacement.Text = ""
      .Forward = True
      .MatchWildcards = False
      .Execute
    End With
    Do While rng.Find.Found = True
    ' Find the true word count
      myText = rng.Text
      wdsOff = 1
      For j = 1 To Len(myText)
        If InStr(soCalledWords, Mid(myText, j, 1)) > 0 Then _
             wdsOff = wdsOff + 1
      Next j
      If j > 3 Then
    ' To correct Word's weird wordcount: 'hello' = count OK
    ' 'hello?' error by one word, and 'hello??' = count OK
        charOne = Mid(myText, j - 3, 1)
        charTwo = Mid(myText, j - 2, 1)
        oneIsAlpha = (LCase(charOne) <> UCase(charOne))
        twoIsNotAlpha = (LCase(charTwo) = UCase(charTwo))
        If oneIsAlpha And twoIsNotAlpha Then wdsOff = wdsOff + 1
        If rng.Words.Count - wdsOff < minLength Then
          rng.Font.Emboss = False
          rng.Collapse wdCollapseEnd
        End If
      End If
      rng.Find.Execute
    Loop
  End If
 
' Apply whatever effect is wanted on the embossed text
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Font.Emboss = True
    .Text = ""
    .Wrap = wdFindContinue
    .Replacement.Text = ""
    .Replacement.Font.Emboss = False
    If highlightText = True Then .Replacement.Highlight = True
    If strikeSingle = True Then .Replacement.Font.StrikeThrough = True
    If strikeDouble = True Then .Replacement.Font.DoubleStrikeThrough = True
    If colourFont = True Then .Replacement.Font.Color = myColour
    .Forward = True
    .MatchWildcards = False
    .Execute Replace:=wdReplaceAll
  End With
Next i

' Warn the user of possible plural possessives
If gottaFunny = True Then
  Selection.HomeKey Unit:=wdStory
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = ""
    .Wrap = wdFindContinue
    .Font.Color = possessiveColour
    .MatchWildcards = False
    .Execute
  End With
  Selection.End = Selection.Start - 1
  Selection.Find.Execute
  MsgBox "Please check possible plural possessives (" _
       & Trim(Str(funnyCount)) & ")"
End If

Options.DefaultHighlightColorIndex = nowColour
ActiveDocument.TrackRevisions = myTrack
Beep
End Sub


PT
Sub GermanQuotes()
' Version 14.02.13
' Switch UK quotes to German quotes

Options.AutoFormatAsYouTypeReplaceQuotes = False
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ChrW(8220)
  .Wrap = wdFindContinue
  .Replacement.Text = ChrW(8222)
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute Replace:=wdReplaceAll
End With
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ChrW(8221)
  .Wrap = wdFindContinue
  .Replacement.Text = ChrW(8220)
  .Execute Replace:=wdReplaceAll
End With
Options.AutoFormatAsYouTypeReplaceQuotes = True
End Sub


PT
Sub FrenchQuotes()
' Version 14.02.13
' Switch UK quotes to German quotes

Options.AutoFormatAsYouTypeReplaceQuotes = False
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ChrW(8220)
  .Wrap = wdFindContinue
  .Replacement.Text = ChrW(171)
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute Replace:=wdReplaceAll
End With
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ChrW(8221)
  .Wrap = wdFindContinue
  .Replacement.Text = ChrW(187)
  .Execute Replace:=wdReplaceAll
End With
Options.AutoFormatAsYouTypeReplaceQuotes = True
End Sub

PT
Sub TrackON()
' Version 12.02.13
' Check that track changes is switched on (for use with FRedit)
CR2 = Chr(13) & Chr(13)
If ActiveDocument.TrackRevisions = False Then
  ActiveDocument.TrackRevisions = True
  MsgBox "You forgot track changes!" & CR2 & "Press <Ctrl-Break>."
End If
End Sub


PT
Sub AutoCurlyQuotesOFF()
' Version 14.02.13
' Switch off auto curly quotes

Options.AutoFormatAsYouTypeReplaceQuotes = False
End Sub

PT
Sub AutoCurlyQuotesON()
' Version 14.02.13
' Switch on auto curly quotes

Options.AutoFormatAsYouTypeReplaceQuotes = True
End Sub


PT
Sub Ignore()
' Version 14.03.12
' Toggle strike-through (or remove all if no text selected)
myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
If Selection.Start = Selection.End Then
  Set rng = ActiveDocument.Content
  rng.Font.StrikeThrough = False
Else
  Selection.Font.StrikeThrough = wdToggle
End If
ActiveDocument.TrackRevisions = myTrack
End Sub



PT
Sub FullNameType()
' Version 31.03.11
' Types out the full filename of the open file
Selection.TypeText Text:=vbCrLf & ActiveDocument.FullName & vbCrLf
End Sub


PT
Sub FReditScriptRun()
' Version 31.03.11
' Load, run and close a specific FRedit script
Set thisFile = ActiveDocument

listFile = "C:\Documents and Settings\Paul\My Documents\myList.doc"

Documents.Open listFile
Set FRlist = ActiveDocument
thisFile.Activate
Call FRedit
FRlist.Activate
ActiveDocument.Close SaveChanges:=False
' Beep to let you know it has finished
Beep
End Sub



PT
Sub MultiFileFRedit()
' Version 01.02.13
' Multi-file version of FRedit

acceptAllRevs = False


Dim allMyFiles(200) As String
Set rng = ActiveDocument.Content
myExtent = 250
If rng.End - rng.Start > myExtent Then rng.End = rng.Start + myExtent

If InStr(rng.Text, ".doc") = 0 And InStr(rng.Text, ".rtf") = 0 Then
' If not a file list then open a file in the relevant folder
  docCount = Documents.Count
  Dialogs(wdDialogFileOpen).Show
  If docCount = Documents.Count Then Exit Sub
  dirPath = Replace(ActiveDocument.Path, ":" & ActiveDocument.Name, "")
  ActiveDocument.Close
' Read the names of all the files in this directory
  myFile = Dir("")
  Documents.Add
  numFiles = 0
  Do While myFile <> ""
    If InStr(myFile, ".doc") > 0 Or InStr(myFile, ".rtf") > 0 Then
      Selection.TypeText myFile & vbCrLf
      numFiles = numFiles + 1
    End If
    myFile = Dir()
  Loop

' Now sort the file list (only actually needed for Macs)
  Selection.WholeStory
  Selection.Sort
  Selection.EndKey Unit:=wdStory
  Selection.TypeParagraph
  Selection.HomeKey Unit:=wdStory
  Selection.TypeText dirPath
' Go back until you hit myDelimiter
  Selection.MoveStartUntil cset:=":\", Count:=wdBackward
  dirName = Selection
  Selection.HomeKey Unit:=wdStory

  myResponse = MsgBox("Run FRedit on ALL the files in directory:" & dirName _
       & " ?", vbQuestion + vbYesNoCancel, "Multifile FRedit")
  If myResponse <> vbYes Then Exit Sub
Else
  myResponse = MsgBox("Run FRedit on just the files listed here?", _
       vbQuestion + vbYesNoCancel, "Multifile FRedit")
  If myResponse <> vbYes Then Exit Sub
End If

' Pick up the folder name and the filenames from the file list
numFiles = 0
myFolder = ""
For Each myPara In ActiveDocument.Paragraphs
  myPara.Range.Select
  Selection.MoveEnd , -1
  lineText = Selection
  If myFolder = "" Then
    myFolder = lineText
    Selection.Collapse wdCollapseEnd
    Selection.MoveStartUntil cset:=":\", Count:=wdBackward
    Selection.MoveStart , -1
    myDelimiter = Left(Selection, 1)
  Else
    thisFile = lineText
    If Len(thisFile) > 2 Then
      If Left(thisFile, 1) <> "|" Then
        numFiles = numFiles + 1
        allMyFiles(numFiles) = thisFile
      End If
    End If
  End If
Next myPara
myResponse = MsgBox("Are you SURE you want to run FRedit on all" & Str(numFiles) _
     & " files?" & vbCrLf & vbCrLf & "There's no going back, you know!", _
     vbQuestion + vbYesNoCancel, "WARNING!   WARNING!   WARNING!")
If myResponse <> vbYes Then Exit Sub

For i = 1 To numFiles
  thisFile = myFolder & myDelimiter & allMyFiles(i)
  Set myDoc = Application.Documents.Open(fileName:=thisFile)
  StatusBar = allMyFiles(i)
  If acceptAllRevs = True Then ActiveDocument.AcceptAllRevisions
  Application.Run MacroName:="Normal.NewMacros.FRedit"
  myDoc.Save
  ActiveWindow.Close
Next
Selection.HomeKey Unit:=wdStory
MsgBox ("FRedited " + Str(numFiles) + " files")
End Sub


PT
Sub FReditCopy()
' Version 08.03.14
' Copy word to make FRedit script item

Selection.Expand wdParagraph
tabPos = InStr(Selection, Chr(9))
If tabPos > 0 Then
  Selection.MoveStart wdCharacter, tabPos - 1
  Selection.MoveEnd wdCharacter, -1
  Selection.Delete
  Selection.Expand wdParagraph
End If
Selection.MoveEnd wdCharacter, -1
dotPos = InStr(Selection, ". .")
lineStart = Selection.Start
If dotPos > 0 Then
  Selection.MoveStart , dotPos - 1
  Selection.Delete
  Selection.Start = lineStart
End If
Selection.Copy
Selection.EndKey Unit:=wdLine
Selection.TypeText Text:=ChrW(124)
Selection.Paste
Selection.MoveRight Unit:=wdCharacter, Count:=1
End Sub


PT
Sub FReditCopyPlus()
' Version 16.05.14
' Copy word to make FRedit script item + highlight + case insensitive
' Shift-Alt-f11

makeItCaseInsensitive = True
makeItNotTracked = True

myColour = wdTurquoise
Selection.Expand wdParagraph
If Len(Selection) > 3 Then
  Selection.Collapse wdCollapseEnd
  Selection.MoveLeft , 1
  Selection.TypeParagraph
Else
  Selection.Collapse wdCollapseStart
  Selection.TypeParagraph
  Selection.MoveLeft , 1
End If
Selection.PasteAndFormat (wdFormatPlainText)
Set rng = ActiveDocument.Content
rng.Start = Selection.Start - 1
rng.End = Selection.Start
If rng = " " Then rng.Delete
Selection.Expand wdParagraph
tabPos = InStr(Selection, Chr(9))
If tabPos > 0 Then
  Selection.MoveStart wdCharacter, tabPos - 1
  Selection.MoveEnd wdCharacter, -1
  Selection.Delete
  Selection.Expand wdParagraph
End If
Selection.MoveEnd wdCharacter, -1
If Len(Selection) < 3 Then
  Selection.MoveRight Count:=1
  Exit Sub
End If
dotPos = InStr(Selection, ". .")
lineStart = Selection.Start
If dotPos > 0 Then
  Selection.MoveStart , dotPos - 1
  Selection.Delete
  Selection.Start = lineStart
End If
Selection.Copy
Selection.EndKey Unit:=wdLine
Selection.TypeText Text:=ChrW(124)
Selection.Paste
Selection.Expand wdParagraph
Selection.Range.HighlightColorIndex = myColour
If makeItNotTracked = True Then Selection.Range.Font.StrikeThrough = True
Selection.HomeKey Unit:=wdLine
If makeItCaseInsensitive = True Then Selection.TypeText Text:=ChrW(172)
Selection.EndKey Unit:=wdLine
Selection.TypeText Text:=vbCrLf
End Sub


PT
Sub FReditCopyWholeWord()
' Version 17.01.12
' Create FRedit item for whole-word F&R

Selection.Paragraphs(1).Range.Select
tabPos = InStr(Selection, Chr(9))
If tabPos > 0 Then
  Selection.MoveStart wdCharacter, tabPos - 1
  Selection.MoveEnd , -1
  Selection.Delete
  Selection.Paragraphs(1).Range.Select
End If
Selection.MoveEnd , -1
Selection.Copy
Selection.Collapse wdCollapseStart
Selection.TypeText Text:="~<"
Selection.EndKey Unit:=wdLine
Selection.TypeText Text:=">" & ChrW(124)
Selection.Paste
Selection.MoveRight Unit:=wdCharacter, Count:=1
End Sub

PT
Sub FReditSame()
' Version 17.01.12
' Create FRedit item with ^&

Selection.Paragraphs(1).Range.Select
Selection.MoveEnd wdCharacter, -1
tabPos = InStr(Selection, Chr(9))
If tabPos > 0 Then
  Selection.MoveStart wdCharacter, tabPos - 1
  Selection.Delete
Else
  Selection.Collapse wdCollapseEnd
End If
Selection.TypeText Text:=ChrW(124) & "^&"
Selection.MoveRight Unit:=wdCharacter, Count:=1
End Sub




PT
Sub FReditSwap()
' Version 20.10.11
' Swap the two sides of a FRedit item

Selection.Paragraphs(1).Range.Select
mT = Selection
padPos = InStr(mT, "|")
mT = Replace(mT, Chr(13), "")
mT = Mid(mT, padPos + 1) & "|" & Left(mT, padPos - 1)
Selection.TypeText mT
End Sub



PT
Sub EmailFormatter()
' Version 05.11.13
' Format paragraphs in an email

Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^11"
  .Wrap = wdFindContinue
  .Replacement.Text = " "
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .Text = "^p^p"
  .Replacement.Text = "zczc"
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .Text = "^p"
  .Replacement.Text = " "
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .Text = "zczc"
  .Replacement.Text = "^p^p"
  .Execute Replace:=wdReplaceAll
End With

End Sub


PT
Sub PDFsoftHyphenRemove()
' Version 04.01.14
' Unhyphenate split words

' Highlight the result (use zero for no highlight)
myColour = wdGray25

Selection.HomeKey Unit:=wdStory
langText = Languages(Selection.LanguageID).NameLocal

' Go and find the first occurrence
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[a-zA-Z]-^13"
  .Wrap = False
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = True
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute
End With

myCount = 0
Do While rng.Find.Found = True
' If you want to count them...
  splitPoint = rng.Start + 1
  rng.End = rng.Start + 1
  rng.Start = rng.Start - 60
  wordOne = rng.Words.Last
  myStart = rng.End - Len(wordOne)
  rng.Start = splitPoint + 2
  rng.End = rng.Start + 25
  wordTwo = Trim(rng.Words.First)
  myEnd = rng.Start + Len(wordTwo)
  gotOne = False
' If it's a spelling error...
  oneWord = wordOne & wordTwo
  If InStr(oneWord, "_") = 0 Then
    If Application.CheckSpelling(oneWord, _
         MainDictionary:=langText) = True Then
      rng.Start = splitPoint
      rng.End = splitPoint + 2
      rng.Delete
      rng.Select
      gotOne = True
      rng.Start = myEnd - 2
      rng.End = myEnd - 1
      rng.Select
      rng.Text = vbCr
    End If
  End If
  myCount = myCount + 1
  If myColour > 0 And gotOne = True Then
    rng.Start = myStart
    rng.End = myEnd - 2
    rng.HighlightColorIndex = myColour
  End If
  rng.Collapse wdCollapseEnd
  rng.Find.Execute
Loop
rng.Select
MsgBox "Changed: " & myCount
End Sub






PT
Sub PDFhardHyphenRestore()
' Version 18.04.13
' Hyphenate falsely concatenated words

' Highlight the result?
hyphColour = wdNoHighlight
' or
hyphColour = wdGray25

minWordLen = 5

' For diagnostic purposes use yellow
singleColour = wdYellow
singleColour = wdNoHighlight

Set rng = ActiveDocument.Content
langText = Languages(Selection.LanguageID).NameLocal
Selection.HomeKey Unit:=wdStory

timeNow = Timer
For Each myPara In ActiveDocument.Paragraphs
  Set rng = myPara.Range
  rng.End = rng.End - 1
  myWord = rng.Words.Last
  myLen = Len(myWord)
' If it's a spelling error...
  If myLen > minWordLen Then
    If Application.CheckSpelling(myWord, MainDictionary:=langText) _
           = False Then
      For i = 2 To myLen - 2
        wordOne = Left(myWord, myLen - i)
        If Application.CheckSpelling(wordOne, _
               MainDictionary:=langText) = True Then
          wordTwo = Right(myWord, i)
          If Application.CheckSpelling(wordTwo, _
                 MainDictionary:=langText) = True Then
            myPara.Range.Select
            Selection.MoveEnd , -1
            Selection.Start = Selection.End - myLen
            Selection.InsertBefore Text:="zczc"
            Selection.Start = Selection.End - i
            Selection.InsertBefore Text:="pqpq"
            Exit For
          End If
        End If
      Next i
    End If
  End If
Next myPara

' Now check all zczc
myCount = 0
oldColour = Options.DefaultHighlightColorIndex
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "zczc*^13"
  .Replacement.Text = ""
  .MatchCase = False
  .MatchWildcards = True
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute
End With

myCount = 0
Selection.HomeKey Unit:=wdStory
While rng.Find.Found
  rng.End = rng.End - 1
  wholeBit = rng
  pqpqWord = Replace(wholeBit, "zczc", "")
  hyphWord = Replace(pqpqWord, "pqpq", "-")
  singleWord = Replace(pqpqWord, "pqpq", "")
  Set rng = ActiveDocument.Content
  With rng.Find
    .Text = hyphWord
    .Execute
  End With
' Does the hyphenated version occur anywhere else in the text?
  If rng.Find.Found Then
  ' If so, change it and highlight it
    Set rng = ActiveDocument.Content
    Options.DefaultHighlightColorIndex = hyphColour
    myCount = myCount + 1
    With rng.Find
      .Text = wholeBit
      .Replacement.Text = hyphWord
      .MatchCase = False
      .Replacement.Highlight = True
      .Execute Replace:=wdReplaceAll
    End With
    StatusBar = "Hyphenating :  " & hyphWord
  Else
  ' If not restore the original word
    Set rng = ActiveDocument.Content
    Options.DefaultHighlightColorIndex = singleColour
    With rng.Find
      .Text = wholeBit
      .Replacement.Text = singleWord
      .MatchCase = False
      .Replacement.Highlight = True
      .Execute Replace:=wdReplaceAll
    End With
    StatusBar = "Not hyphenating :  " & singleWord
  End If
  Set rng = ActiveDocument.Content
  With rng.Find
    .Text = "zczc*^13"
    .MatchCase = False
    .MatchWildcards = True
    .Execute
  End With
Wend
Options.DefaultHighlightColorIndex = oldColour
MsgBox "Changed:  " & myCount & "  different words."
End Sub






PT
Sub PDFunderlineToLigature()
' Version 04.01.14
' Restore underline characters into ligatures

' Colour if a suitable word found
okColour = wdGray25

' Colour if no suitable word found
nogoColour = wdTurquoise

' replace underlines with...
ch = ChrW(126): ' A tilde ~


myJump = 100
Selection.HomeKey Unit:=wdStory
langText = Languages(Selection.LanguageID).NameLocal

' first get rid of odd underlines
StatusBar = "Removing obvious non-words"
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "__"
  .Wrap = wdFindContinue
  .Replacement.Text = ch & ch
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute Replace:=wdReplaceAll
End With

StatusBar = "Removing obvious non-words"
With rng.Find
  .Text = "_([A-Z])"
  .Replacement.Text = "\1" & ChrW(8211) & "\1"
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

StatusBar = "Removing obvious non-words"
With rng.Find
  .Text = "([!a-zA-Z])_([!a-zA-Z])"
  .Replacement.Text = "\1" & ChrW(8211) & "\2"
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

StatusBar = "Removing obvious non-words"
With rng.Find
  .Text = "([!a-zA-Z])_([!bngrtx][!a-zA-Z])"
  .Replacement.Text = "\1" & ch & "\2"
  .Execute Replace:=wdReplaceAll
End With

StatusBar = "Removing obvious non-words"
With rng.Find
  .Text = "([!a-zA-Z])_([!a-zA-Z])"
  .Replacement.Text = "\1" & ch & "\2"
  .Execute Replace:=wdReplaceAll
End With

StatusBar = "Removing obvious non-words"
With rng.Find
  .Text = "<([a-zA-Z])_([!a-zA-Z])"
  .Replacement.Text = "\1" & ch & "\2"
  .Execute Replace:=wdReplaceAll
End With


' Go and find the first underline character
Set rng = ActiveDocument.Content
With rng.Find
  .Text = "_"
  .Wrap = False
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = False
  .Execute
End With
StatusBar = ""

okChars = "abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ"
myCount = 0
Do While rng.Find.Found = True
' find the whole word
  rng.MoveStartWhile cset:=okChars, Count:=wdBackward
  rng.MoveEndWhile cset:=okChars, Count:=wdForward
  foundWord = rng
  newWord = ""
' Try each of the four ligatures in turn (most likely first)
  For i = 1 To 4
    Select Case i
      Case 1: lig = "fi"
      Case 2: lig = "ff"
      Case 3: lig = "fl"
      Case 4: lig = "ffi"
    End Select
    tryWord = Replace(foundWord, "_", lig)
    isOK = Application.CheckSpelling(tryWord, MainDictionary:=langText)
    If isOK = True Then
      newWord = tryWord
      Exit For
    End If
  Next i
  If newWord > "" Then
  ' If we've found a new word, replace it
    rng.Text = newWord
    rng.HighlightColorIndex = okColour
  Else
  ' If not, highlight them all
    rng.Text = Replace(foundWord, "_", ch)
    rng.HighlightColorIndex = nogoColour
  End If
  myCount = myCount + 1
  myTest = myCount / myJump
  If Int(myTest) = myTest Then rng.Select
  rng.Collapse wdCollapseEnd
  rng.Find.Execute
Loop
Beep
Selection.HomeKey Unit:=wdStory
End Sub



PT
Sub PDFfunniesToLigatures()
' Version 18.04.13
' Restore ligatures that have been converted to odd characters

fi_Code = "W"
fl_Code = "U"
ffi_Code = "Z"
ff_Code = "V"

' Colour if a suitable word found
okColour = wdGray25

' Colour if no suitable word found
nogoColour = wdYellow
nogoColour = wdNoHighlight

Selection.HomeKey Unit:=wdStory
langText = Languages(Selection.LanguageID).NameLocal

oldColour = Options.DefaultHighlightColorIndex
okCount = 0
nogoCount = 0
okChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
For i = 1 To 4
  Select Case i
    Case 1: lig = "fi": myCode = fi_Code
    Case 2: lig = "ff": myCode = ff_Code
    Case 3: lig = "fl": myCode = fl_Code
    Case 4: lig = "ffi": myCode = ffi_Code
  End Select
  If myCode > "" Then
  ' Go and find the first code character
    Set rng = ActiveDocument.Content
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = myCode
      .Font.Color = wdColorAutomatic
      .Wrap = False
      .Replacement.Text = ""
      .Forward = True
      .MatchCase = True
      .MatchWildcards = False
      .MatchWholeWord = False
      .MatchSoundsLike = False
      .Execute
    End With
    Do While rng.Find.Found = True
    ' find the whole word
      rng.MoveStartWhile cset:=okChars, Count:=wdBackward
      rng.MoveEndWhile cset:=okChars, Count:=wdForward
      foundWord = rng
      newWord = ""
    ' Try each of the four ligatures in turn (most likely first)
      tryWord = Replace(foundWord, myCode, lig)
      If Application.CheckSpelling(tryWord, MainDictionary:=langText) _
               = True Then newWord = tryWord
      If newWord > "" And Len(newWord) > 2 Then
    ' If we've found a new word, replace them all.
        Options.DefaultHighlightColorIndex = okColour
        Set rng = ActiveDocument.Content
        With rng.Find
          .ClearFormatting
          .Replacement.ClearFormatting
          .Text = "([!a-zA-Z])" & foundWord & "([!a-zA-Z])"
          .MatchCase = True
          .Wrap = wdFindContinue
          .MatchWildcards = True
          .Replacement.Text = "\1" & newWord & "\2"
          .Replacement.Highlight = True
          .Replacement.Font.Color = wdColorBlue
          .Execute Replace:=wdReplaceAll
        End With
        Set rng = ActiveDocument.Content
        okCount = okCount + 1
        StatusBar = newWord & "  � OK"
      Else
      ' If not, colour them all
        Options.DefaultHighlightColorIndex = nogoColour
        Set rng = ActiveDocument.Content
        With rng.Find
          .ClearFormatting
          .Replacement.ClearFormatting
          .Text = "[!a-zA-Z]" & foundWord & "[!a-zA-Z]"
          .MatchCase = True
          .Wrap = wdFindContinue
          .Replacement.Text = ""
          .Replacement.Font.Color = wdColorRed
          .MatchWildcards = True
          .Replacement.Highlight = True
          .Execute Replace:=wdReplaceAll
        End With
        StatusBar = foundWord & "  � Can't find it"
        nogoCount = nogoCount + 1
      End If
     
      Set rng = ActiveDocument.Content
      With rng.Find
        .ClearFormatting
        .Replacement.ClearFormatting
        .Text = myCode
        .Font.Color = wdColorAutomatic
        .Wrap = False
        .Replacement.Text = ""
        .Forward = True
        .MatchWildcards = False
        .MatchCase = True
        .Execute
      End With
    Loop
  End If
Next i
Set rng = ActiveDocument.Content
rng.Font.Color = wdColorAutomatic
Options.DefaultHighlightColorIndex = oldColour
MsgBox "Successfully changed words: " & okCount & vbCrLf & vbCrLf _
     & "Words not to be changed: " & nogoCount
End Sub

PT
Sub PDFspellAll()
' Version 19.04.13
' Underline all spelling errors

' Highlight the result (use zero for no highlight)
myColour = wdYellow

Selection.HomeKey Unit:=wdStory
langText = Languages(Selection.LanguageID).NameLocal
wasIgnore = Options.IgnoreMixedDigits
Options.IgnoreMixedDigits = False

j = ActiveDocument.Words.Count
StatusBar = "Spellchecking. To go: " & Str(j)
Set rng = ActiveDocument.Content

For Each wd In ActiveDocument.Words
  wd.MoveEndWhile cset:=ChrW(8217) & "' ", Count:=wdBackward
  If Right(wd, 2) = ChrW(8217) & "s" Then wd.MoveEnd , -2
  If Right(wd, 2) = "'s" Then wd.MoveEnd , -2
  If wd.Font.StrikeThrough = False Then
    If Application.CheckSpelling(wd, MainDictionary:=langText) = False Then
      wd.Font.Underline = True
      wd.HighlightColorIndex = wdYellow
    End If
  End If
  j = j - 1
  If j Mod 100 = 0 Then StatusBar = "Spellchecking. To go: " & Str(j)
Next wd
StatusBar = ""
doneSpell = True
Options.IgnoreMixedDigits = wasIgnore
End Sub



PT
Sub PDFspellIgnoreProperNouns()
' Version 19.04.13
' Underline all spelling errors

' Highlight the result (use zero for no highlight)
myColour = wdYellow
myColour = wdNoHighlight

Selection.HomeKey Unit:=wdStory
langText = Languages(Selection.LanguageID).NameLocal
wasIgnore = Options.IgnoreMixedDigits
Options.IgnoreMixedDigits = False
allAlphas = ""
For j = 192 To 255
  If j <> 215 And j <> 247 Then allAlphas = allAlphas & ChrW(j)
Next j
For j = 65 To 90
  allAlphas = allAlphas & ChrW(j)
Next j
For j = 97 To 122
  allAlphas = allAlphas & ChrW(j)
Next j

j = ActiveDocument.Words.Count
StatusBar = "Spellchecking. To go: " & Str(j)
Set rng = ActiveDocument.Content

For Each wd In ActiveDocument.Words
  wd.MoveEndWhile cset:=ChrW(8217) & "' ", Count:=wdBackward
  If Right(wd, 2) = ChrW(8217) & "s" Then wd.MoveEnd , -2
  If Right(wd, 2) = "'s" Then wd.MoveEnd , -2
 
  If wd.Font.StrikeThrough = False Then
    If Application.CheckSpelling(wd, MainDictionary:=langText) = False Then
      cap = Left(wd, 1)
      isApropernoun = (LCase(cap) <> cap)
    ' but if it contains numbers, it's not a proper noun
      For k = 1 To Len(wd)
        If Asc(Mid(wd, k, 1)) < 65 Then isApropernoun = False: Exit For
      Next k
    ' Check the previous character
   
      rng.Start = wd.Start - 1
      rng.End = wd.Start
      If rng > "" Then
        If Asc(rng) = 13 Or Asc(rng) = 9 Then isApropernoun = False
        If Left(rng, 1) = "(" Then rng.MoveStart , -1
        rng.MoveStart , -1
        If InStr(allAlphas & ";:,", Left(rng, 1)) = 0 Then isApropernoun = False
      End If
      If isApropernoun = False Then
        wd.Font.Underline = True
        wd.HighlightColorIndex = wdYellow
      End If
    End If
  End If
  j = j - 1
  If j Mod 10 = 0 Then StatusBar = "Spellchecking. To go: " & Str(j)
Next wd
StatusBar = ""
doneSpell = True
Options.IgnoreMixedDigits = wasIgnore
End Sub


PT
Sub MultiFileText()
' Version 19.03.12
' Collecting pure text from multiple files

Dim allMyFiles(200) As String
Set rng = ActiveDocument.Content
myExtent = 250
If rng.End - rng.Start > myExtent Then rng.End = rng.Start + myExtent

If InStr(rng.Text, ".doc") = 0 And InStr(rng.Text, ".rtf") = 0 Then
' If not a file list then open a file in the relevant folder
  docCount = Documents.Count
  Dialogs(wdDialogFileOpen).Show
  If docCount = Documents.Count Then Exit Sub
  dirPath = Replace(ActiveDocument.Path, ":" & ActiveDocument.Name, "")
  ActiveDocument.Close
' Read the names of all the files in this directory
  myFile = Dir("")
  Documents.Add
  numFiles = 0
  Do While myFile <> ""
    If InStr(myFile, ".doc") > 0 Or InStr(myFile, ".rtf") > 0 Then
      Selection.TypeText myFile & vbCrLf
      numFiles = numFiles + 1
    End If
    myFile = Dir()
  Loop

' Now sort the file list (only actually needed for Macs)
  Selection.WholeStory
  Selection.Sort
  Selection.EndKey Unit:=wdStory
  Selection.TypeParagraph
  Selection.HomeKey Unit:=wdStory
  Selection.TypeText dirPath
' Go back until you hit myDelimiter
  Selection.MoveStartUntil cset:=":\", Count:=wdBackward
  dirName = Selection
  Selection.HomeKey Unit:=wdStory

  myResponse = MsgBox("Collect unformatted text from ALL the files in" & _
       " directory:" & dirName & " ?", vbQuestion + vbYesNoCancel, _
       "Multifile Text Collection")
  If myResponse <> vbYes Then Exit Sub
Else
  myResponse = MsgBox("Collect unformatted text " & moreText & _
       "from the files listed here?", vbQuestion + vbYesNoCancel, _
       "Multifile Text Collection")
  If myResponse <> vbYes Then Exit Sub
End If

' Pick up the folder name and the filenames from the file list
numFiles = 0
myFolder = ""
For Each myPara In ActiveDocument.Paragraphs
  myPara.Range.Select
  Selection.MoveEnd , -1
  lineText = Selection
  If myFolder = "" Then
    myFolder = lineText
    Selection.Collapse wdCollapseEnd
    Selection.MoveStartUntil cset:=":\", Count:=wdBackward
    Selection.MoveStart , -1
    myDelimiter = Left(Selection, 1)
  Else
    thisFile = lineText
    If Len(thisFile) > 2 Then
      If Left(thisFile, 1) <> "|" Then
        numFiles = numFiles + 1
        allMyFiles(numFiles) = thisFile
      End If
    End If
  End If
Next myPara

Documents.Add
For i = 1 To numFiles
' Get the folder name, and then the text for the files in the list
  thisFile = myFolder & myDelimiter & allMyFiles(i)
  Set myDoc = Application.Documents.Open(fileName:=thisFile, ReadOnly:=True)
  StatusBar = allMyFiles(i)
  Selection.EndKey Unit:=wdStory
  If ActiveDocument.Endnotes.Count > 0 Then
    ActiveDocument.StoryRanges(wdEndnotesStory).Copy
    Selection.TypeText Text:=vbCrLf
    Selection.Paste
    Selection.TypeText Text:=vbCrLf
  End If
  If ActiveDocument.Footnotes.Count > 0 Then
    ActiveDocument.StoryRanges(wdFootnotesStory).Copy
    Selection.TypeText Text:=vbCrLf
    Selection.Paste
    Selection.TypeText Text:=vbCrLf
  End If
  ' copy all the textboxes to the end of the text
  Selection.EndKey Unit:=wdStory
  If ActiveDocument.Shapes.Count > 0 Then
    Selection.TypeText Text:=vbCrLf
    For Each shp In ActiveDocument.Shapes
      If shp.TextFrame.HasText Then
        Set rng = shp.TextFrame.TextRange
        rng.Cut
        Selection.Paste
        Selection.TypeText Text:=vbCrLf
      End If
    Next
  End If

  Selection.WholeStory
  Selection.Fields.Unlink
  ' Accept all track changes
  Selection.Range.Revisions.AcceptAll
  ' Delete all comments
  For Each cmt In ActiveDocument.Comments
    cmt.Delete
  Next
  Selection.Copy
  myDoc.Close SaveChanges:=wdDoNotSaveChanges
  ' Having closed the file, paste text into combined file
  Selection.Range.PasteSpecial DataType:=wdPasteText
  Selection.EndKey Unit:=wdStory
Next i
Selection.HomeKey Unit:=wdStory
End Sub






PT
Sub MultiFileWord()
' Version 06.05.12
' Multiple Word file concatenation

deleteImages = True
insertNotesWithinText = True
embedTextboxText = True
acceptTCs = True

addTitle = False
myFontSize = 30
titleHighlightColour = wdYellow

Dim allMyFiles(200) As String
If deleteImages = False Then moreText = "and images "
Set rng = ActiveDocument.Content
myExtent = 250
If rng.End - rng.Start > myExtent Then rng.End = rng.Start + myExtent

If InStr(rng.Text, ".doc") = 0 And InStr(rng.Text, ".rtf") = 0 Then
' If not a file list then open a file in the relevant folder
  docCount = Documents.Count
  Dialogs(wdDialogFileOpen).Show
  If docCount = Documents.Count Then Exit Sub
  dirPath = Replace(ActiveDocument.Path, ":" & ActiveDocument.Name, "")
  ActiveDocument.Close
' Read the names of all the files in this directory
  myFile = Dir("")
  Documents.Add
  numFiles = 0
  Do While myFile <> ""
    If InStr(myFile, ".doc") > 0 Or InStr(myFile, ".rtf") > 0 Then
      Selection.TypeText myFile & vbCrLf
      numFiles = numFiles + 1
    End If
    myFile = Dir()
  Loop

' Now sort the file list (only actually needed for Macs)
  Selection.WholeStory
  Selection.Sort
  Selection.EndKey Unit:=wdStory
  Selection.TypeParagraph
  Selection.HomeKey Unit:=wdStory
  Selection.TypeText dirPath
' Go back until you hit myDelimiter
  Selection.MoveStartUntil cset:=":\", Count:=wdBackward
  dirName = Selection
  Selection.HomeKey Unit:=wdStory
  myResponse = MsgBox("Collect formatted text " & moreText & _
       "from ALL the files in" & " directory:" & dirName & _
       " ?", vbQuestion + vbYesNoCancel, "Multifile Word")
  If myResponse <> vbYes Then Exit Sub
Else
  myResponse = MsgBox("Collect formatted text " & moreText & _
       "from the files listed here?", vbQuestion + vbYesNoCancel, _
       "Multifile Word")
  If myResponse <> vbYes Then Exit Sub
End If

' Pick up the folder name and the filenames from the file list
numFiles = 0
myFolder = ""
For Each myPara In ActiveDocument.Paragraphs
  myPara.Range.Select
  Selection.MoveEnd , -1
  lineText = Selection
  If myFolder = "" Then
    myFolder = lineText
    Selection.Collapse wdCollapseEnd
    Selection.MoveStartUntil cset:=":\", Count:=wdBackward
    Selection.MoveStart , -1
    myDelimiter = Left(Selection, 1)
  Else
    thisFile = lineText
    If Len(thisFile) > 2 Then
      If Left(thisFile, 1) <> "|" Then
        numFiles = numFiles + 1
        allMyFiles(numFiles) = thisFile
      End If
    End If
  End If
Next myPara

' Now pick up formatted text from all the files
Documents.Add
For i = 1 To numFiles
  thisFile = myFolder & myDelimiter & allMyFiles(i)
  Set myDoc = Application.Documents.Open(fileName:=thisFile, ReadOnly:=True)
  StatusBar = allMyFiles(i)
  ActiveDocument.TrackRevisions = False
  embedTextboxText = True

' Delete all images
  If ActiveDocument.InlineShapes.Count > 0 And _
       deleteImages = True Then
    For Each myFig In ActiveDocument.InlineShapes
      If myFig.Type = wdInlineShapePicture Then myFig.Delete
    Next
  End If

  If insertNotesWithinText = True Then
    If ActiveDocument.Footnotes.Count > 0 Then
      ActiveDocument.StoryRanges(wdFootnotesStory).Copy
      Selection.EndKey Unit:=wdStory
      Selection.InsertAfter Text:=CR & "Footnotes:" & CR2
      Selection.Font.Bold = True
      Selection.Collapse wdCollapseEnd
      Selection.Paste
    End If
    If ActiveDocument.Endnotes.Count > 0 Then
      ActiveDocument.StoryRanges(wdEndnotesStory).Copy
      Selection.EndKey Unit:=wdStory
      Selection.InsertAfter Text:=CR & "Endnotes:" & CR2
      Selection.Font.Bold = True
      Selection.Collapse wdCollapseEnd
      Selection.Paste
    End If
  ' Delete all notes
    Set rng = ActiveDocument.Range
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .MatchWildcards = False
      .Text = "^2"
      .Replacement.Text = ChrW(8211)
      .Replacement.Font.Superscript = False
      .Execute Replace:=wdReplaceAll
    End With
  End If

  ' copy all the textbox text into the text itself
  If embedTextboxText = True And ActiveDocument.Shapes.Count > 0 Then
    Selection.EndKey Unit:=wdStory
    For Each shp In ActiveDocument.Shapes
      If shp.TextFrame.HasText Then
        Set rng2 = shp.TextFrame.TextRange
        rng2.Copy
        Selection.TypeText CR2
        Selection.Paste
      End If
    Next
    For sh = ActiveDocument.Shapes.Count To 1 Step -1
      If ActiveDocument.Shapes(sh).TextFrame.HasText Then
        ActiveDocument.Shapes(sh).Delete
      End If
    Next
  End If


  Selection.WholeStory
  Selection.Fields.Unlink
  ' Accept all track changes
  If acceptTCs = True Then Selection.Range.Revisions.AcceptAll
  Selection.WholeStory
  Selection.Copy
  myDoc.Close SaveChanges:=wdDoNotSaveChanges
  ' Having closed the file, paste text into combined file
  If addTitle = True Then
    fileTitle = allMyFiles(i)
    fileTitle = Replace(fileTitle, ".docx", "")
    fileTitle = Replace(fileTitle, ".doc", "")
    fileTitle = Replace(fileTitle, ".rtf", "")
    Selection.TypeText fileTitle & vbCrLf
    Selection.MoveUp Unit:=wdLine, Count:=1, Extend:=wdExtend
    Selection.Font.Size = myFontSize
    Selection.Font.Bold = True
    Selection.Range.HighlightColorIndex = titleHighlightColour
    Selection.Start = Selection.End
  End If
  Selection.Range.Paste
  Selection.EndKey Unit:=wdStory
  FilesTotal = FilesTotal + 1

Next i
Selection.HomeKey Unit:=wdStory
End Sub


PT
Sub ChapterChopper()
' Version 10.09.10
' Chop text into chapters

Dim wordsTrue, wordsCount As Long

'find start of special heading
Set rng = ActiveDocument.Content
endOfFile = rng.End
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "Summary of chapter contents"
  .Execute
End With

' If that heading is already there, you're
' ready to chop up the file

If rng.Find.Found = True Then GoTo chopItUp

If Selection.End = Selection.Start Then
  myResponse = MsgBox("Chapter chopper" & vbCrLf & _
     "Please select text to define chapter start")
  Exit Sub
End If

chapTitle = Selection

' Find actual total wordcount
wordsTrue = ActiveDocument.Content.ReadabilityStatistics(1).Value

' Find apparent total word count
wordsCount = ActiveDocument.Content.Words.Count
correctionFactor = wordsTrue / wordsCount

Set rng = ActiveDocument.Content

' Find start of first chapter
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = chapTitle
  .MatchCase = True
  .Execute
End With
chapStart = rng.Start
' If it's a long chapter heading, cut it down a bit.
rng.End = rng.End + 50
rng.Start = chapStart
chapName = rng
' If we've picked up more then one line
' of text, chop it short at the line end
anyNewLine = InStr(chapName, Chr(11))
anyReturn = InStr(chapName, Chr(13))
If anyNewLine > 2 Then
  If anyNewLine < anyReturn Then anyReturn = anyNewLine
End If
If anyReturn > 2 Then chapName = Left(chapName, anyReturn - 1)
rng.End = rng.Start
rng.Start = 0
wordsTrue = Int(rng.Words.Count * correctionFactor)
Selection.EndKey Unit:=wdStory
Selection.TypeText Text:="Summary of chapter contents" & vbCrLf & vbCrLf
Selection.TypeText Text:="Prelims" & vbTab & Str(wordsTrue) _
    & vbCrLf & vbCrLf
   
' Note where the list starts
listStart = Selection.Start

Do
  Set rng = ActiveDocument.Content
  rng.Start = chapStart + 1
  lastChapStart = chapStart
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = chapTitle
    .MatchCase = True
    .Execute
  End With
 
  If rng.Start > listStart Then Exit Sub
  rng.End = rng.End + 50
  nextChapName = rng
 
  ' If we've picked up more then one line
  ' of text, chop it short at the line end
  anyNewLine = InStr(nextChapName, Chr(11))
  anyReturn = InStr(nextChapName, Chr(13))
  If anyNewLine > 2 Then
    If anyNewLine < anyReturn Then anyReturn = anyNewLine
  End If
  If anyReturn > 2 Then nextChapName = Left(nextChapName, anyReturn - 1)
  chapStart = rng.Start
  rng.End = rng.Start
  rng.Start = lastChapStart
  wordsTrue = Int(rng.Words.Count * correctionFactor)
  Selection.EndKey Unit:=wdStory
  Selection.TypeText Text:=chapName & vbTab & Str(wordsTrue) _
      & vbCrLf & vbCrLf
  chapName = nextChapName

Loop Until finished = True
Exit Sub


' Start of chopping up mechanism
chopItUp:
' Text at beginning of every filename
myPrefix = InputBox("Filename?", "Chapter Chopper", "Chapter")

rng.Select
Dim startNextText(100) As String

' First find out if the prelims have been
' lumped into chapter 1
startNumber = 0
Selection.MoveDown Unit:=wdLine, Count:=3
Selection.HomeKey Unit:=wdLine
Selection.MoveDown Unit:=wdLine, Count:=1, Extend:=wdExtend
myLine = Selection
Selection.Start = Selection.End
' If the second line is longer than two
' then prelims and chapter 1 are together as file 01
If Len(myLine) > 2 Then
  startNumber = 1
  Selection.MoveDown Unit:=wdLine, Count:=1
End If

' Starting from zero (prelims file) or
' one (prelims + chapter 1), store the
' text that follows, i.e. the start of the
' next file
fileNumber = startNumber
Do
  Selection.End = Selection.Start
  Selection.MoveDown Unit:=wdLine, Count:=1, Extend:=wdExtend
  thisBit = Selection
  ' Chop off the tab and the wordcount
  thisBit = Left(thisBit, InStr(thisBit, Chr(9)) - 1)
  startNextText(fileNumber) = thisBit
  ' Skip any files that have been bunched together
  ' i.e. are to be stored in a single file
  Do
    Selection.Start = Selection.End
    Selection.MoveDown Unit:=wdLine, Count:=1, Extend:=wdExtend
  Loop Until Len(Selection) < 3
  Selection.Start = Selection.End
  fileNumber = fileNumber + 1
Loop Until Selection.End > endOfFile - 3
startNextText(fileNumber) = "Summary of chapter contents"

' Chop up the text, section by section
' according to the numbering and sections
' defined above
startText = 0
For i = startNumber To fileNumber
  Set rng = ActiveDocument.Content

  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = startNextText(i)
    .Execute
  End With
  rng.End = rng.Start
  rng.Start = startText
  If rng.Start <> rng.End Then
    rng.Copy
    startText = rng.End
    Documents.Add
    Selection.Paste
    myFile = Str(i)
    If Len(myFile) = 2 Then myFile = Replace(myFile, " ", " 0")
    myFile = myPrefix & myFile
    ActiveDocument.SaveAs fileName:=myFile
    ActiveDocument.Close
  End If
Next i
End Sub


PT
Sub ChapterMarker()
' Version 17.05.10
' For use with ChapterChopper

chapMarker = "<CH>"
If Selection.End = Selection.Start Then
  myResponse = MsgBox("Chapter Marker" & vbCrLf & _
     "Please select text to define chapter start")
  Exit Sub
End If

chapTitle = Selection
Selection.HomeKey Unit:=wdStory
Do
  With Selection.Find
    .Text = chapTitle
    .MatchWildcards = False
    .MatchCase = True
    .Execute
  End With
  If Selection.Find.Found = False Then
    giveUp = True
  Else
    startBit = Selection.Start
    Selection.MoveUp Unit:=wdParagraph, Count:=2
    Selection.MoveDown Unit:=wdParagraph, Count:=1
    If Selection.Start = startBit Then
      Selection.TypeText Text:=chapMarker
    Else
      Selection.MoveDown Unit:=wdParagraph, Count:=2
    End If
  End If
Loop Until giveUp = True

End Sub


PT
Sub AcronymLister()
' Version 28.11.11
' List all acronyms

createFReditScript = False
doBeeps = True

myResponse = MsgBox("Acronym Lister: Include numbers?", _
     vbQuestion + vbYesNoCancel)
If myResponse = vbCancel Then Exit Sub

If Options.DefaultHighlightColorIndex = 0 Then _
     Options.DefaultHighlightColorIndex = wdYellow

' First create a copy of the existing document
ActiveDocument.Content.Copy
Documents.Add
Selection.TypeParagraph
Selection.PasteSpecial DataType:=wdPasteText
Selection.HomeKey Unit:=wdStory

' Remove all apostrophes (close single quotes)
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[" & ChrW(8217) & "']"
  .Replacement.Text = " "
  .Forward = True
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

' Add highlight to all words with/without numbers
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  If myResponse = vbYes Then
    .Text = "<[-0-9A-Za-z]{2,}>"
  Else
    .Text = "<[-A-Za-z]{2,}>"
  End If
  .Replacement.Text = "^&"
  .Replacement.Highlight = True
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With
If doBeeps = True Then Beep

' Remove highlight from words in all lowercase
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "<[-a-z0-9]@>"
  .Replacement.Text = "^&"
  .Replacement.Highlight = False
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With
If doBeeps = True Then Beep

' Remove highlight from initial-cap words
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "<[A-Z][-a-z]@>"
  .Replacement.Text = "^&"
  .Replacement.Highlight = False
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

' Remove highlight from words ending in hyphen
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "<[-A-Za-z]@->"
  .Replacement.Text = "^&"
  .Replacement.Highlight = False
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

' Remove highlight from words starting in hyphen
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "<-[-A-Za-z]@>"
  .Replacement.Text = "^&"
  .Replacement.Highlight = False
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

' Remove all unhighlighted characters
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ""
  .Highlight = False
  .Replacement.Text = "^p"
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

' Remove all blank lines
Do
  Set rng = ActiveDocument.Content
  wasEnd = rng.End
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "^13{2,}"
    .Replacement.Text = "^p"
    .MatchWildcards = True
    .Execute Replace:=wdReplaceAll
  End With
Loop Until wasEnd = rng.End

' Create a list of unique abbrevs
Set dict = CreateObject("Scripting.Dictionary")

'Set comparemode; use vbBinaryCompare
'for case-sensitive filtering
dict.comparemode = vbTextCompare

'Iterate through all the paragraphs in the doc.
For Each myPara In ActiveDocument.Paragraphs

'If we've already encountered this item,
'then delete the paragraph.
  If dict.Exists(myPara.Range.Text) Then
    myPara.Range.Delete
  Else
  'If we haven't already encountered this item,
  'then add it to the dictionary's keys.
    dict.Add myPara.Range.Text, ""
  End If
Next myPara
Set dict = Nothing

Selection.WholeStory
Selection.Sort
Selection.Range.HighlightColorIndex = 0

Selection.HomeKey Unit:=wdStory

If createFReditScript = False Then
  If doBeeps = True Then Beep
  Exit Sub
End If
' Create a FRedit script
ActiveDocument.Content.Copy

Set rng = ActiveDocument.Content

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^p"
  .Replacement.Text = "zczc^p"
  .Replacement.Highlight = True
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^13(*)zczc"
  .Replacement.Text = "^p\1|^^&"
  .Replacement.Highlight = True
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "zczc"
  .Replacement.Text = ""
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

Selection.HomeKey Unit:=wdStory
Selection.Range.HighlightColorIndex = wdAutomatic
Selection.InsertAfter Text:=vbCrLf & "| FRedit script:" & vbCrLf

Selection.HomeKey Unit:=wdStory
Selection.Paste
If doBeeps = True Then Beep
End Sub



PT
Sub SortIt()
' Version 03.07.11
' Sort the selected text

If Selection.End = Selection.Start Then
  myResponse = MsgBox("Sort the WHOLE file?", _
       vbQuestion + vbYesNo)
  If myResponse = vbNo Then Exit Sub
  Selection.WholeStory
End If
Selection.Sort ExcludeHeader:=False, FieldNumber:="Paragraphs", _
     SortOrder:=wdSortOrderAscending, _
     SortFieldType:=wdSortFieldAlphanumericEnd Sub

PT
Sub DuplicatesRemove()
' Version 30.12.11
' Remove duplicate items from a list

If Selection.End = Selection.Start Then
  myResponse = MsgBox("Remove duplicates from the WHOLE file?", _
       vbQuestion + vbYesNo)
  If myResponse = vbNo Then Exit Sub
  listEnd = ActiveDocument.Paragraphs.Count
  listStart = 1
Else
  myStart = Selection.Start
  myEnd = Selection.End
    For j = 1 To ActiveDocument.Paragraphs.Count
      If ActiveDocument.Paragraphs(j).Range.Start > myStart Then Exit For
    Next j
    listStart = j - 1
    For j = ActiveDocument.Paragraphs.Count To 1 Step -1
      If ActiveDocument.Paragraphs(j).Range.Start < myEnd Then Exit For
    Next j
    listEnd = j
End If

For j = listEnd To listStart + 1 Step -1
  Set rng1 = ActiveDocument.Paragraphs(j).Range
  Set rng2 = ActiveDocument.Paragraphs(j - 1).Range
  If rng1 = rng2 Then rng1.Delete
  StatusBar = "Lines to go: " & Str(j)
Next j
StatusBar = ""
ActiveDocument.Paragraphs(listStart).Range.Select
Selection.End = Selection.Start
End Sub

PT
Sub SortAndRemoveDups()
' Version 30.12.11
' Sort the selected text

If Selection.End = Selection.Start Then
  myResponse = MsgBox("Sort the WHOLE file?", _
       vbQuestion + vbYesNo)
  If myResponse = vbNo Then Exit Sub
  Selection.WholeStory
End If
myStart = Selection.Start
myEnd = Selection.End
Selection.Sort ExcludeHeader:=False, FieldNumber:="Paragraphs", SortOrder:=wdSortOrderAscending
Selection.Start = myStart
Selection.End = myEnd
For j = 1 To ActiveDocument.Paragraphs.Count
  If ActiveDocument.Paragraphs(j).Range.Start > myStart Then Exit For
Next j
listStart = j - 1
For j = ActiveDocument.Paragraphs.Count To 1 Step -1
  If ActiveDocument.Paragraphs(j).Range.Start < myEnd Then Exit For
Next j
listEnd = j
For j = listEnd To listStart + 1 Step -1
  Set rng1 = ActiveDocument.Paragraphs(j).Range
  Set rng2 = ActiveDocument.Paragraphs(j - 1).Range
  If rng1 = rng2 Then rng1.Delete
  StatusBar = "Lines to go: " & Str(j)
Next j
StatusBar = ""
ActiveDocument.Paragraphs(listStart).Range.Select
Selection.End = Selection.Start
End Sub


PT
Sub SortCaseSense()
' Version 17.02.11
' Sort into separate lists: Lcase/Ucase
' <ctrl-alt-shift-S>

Selection.HomeKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^13([A-Z])"
  .Replacement.Text = "^pzzzz\1"
  .Forward = True
  .Wrap = wdFindContinue
  .Format = False
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

Selection.WholeStory
Selection.Sort

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "zzzz"
  .Replacement.Text = ""
  .Forward = True
  .Wrap = wdFindContinue
  .Format = False
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With
Selection.HomeKey Unit:=wdStory
End Sub


PT
Sub SortTextBlocks()
' Version 23.01.12
' Alpha sorts blocks of text by first line

If Selection.Start = Selection.End Then
  doingAll = True
  Set rng = ActiveDocument.Content
Else
  doingAll = False
  Selection.Copy
  Documents.Add
  Selection.Paste
  Set rng = ActiveDocument.Content
End If

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^p^p"
  .Wrap = wdFindContinue
  .Replacement.Text = "zczc"
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^p"
  .Replacement.Text = "pqpq"
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "zczc"
  .Wrap = wdFindContinue
  .Replacement.Text = "zczc^p"
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

rng.Sort ExcludeHeader:=False, FieldNumber:="Paragraphs"

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "pqpq"
  .Wrap = wdFindContinue
  .Replacement.Text = "^p"
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "zczc"
  .Wrap = wdFindContinue
  .Replacement.Text = "^p"
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

If doingAll = False Then
  Selection.WholeStory
  Selection.Copy
  ActiveDocument.Close SaveChanges:=False
  Selection.Paste
End If
End Sub


PT
Sub SortNumberedList()
' Version 03.07.12
' Sort references, ignoring the number at the beginning

numChars = 20
myDelimiter = " "
' If it's a tab, use...
' myDelimiter = Chr(9)

For Each myPara In ActiveDocument.Paragraphs
  If Len(myPara.Range.Text) > numChars Then
    myPara.Range.Select
    firstChars = Left(myPara.Range.Text, numChars)
    firstChars = Right(firstChars, Len(firstChars) - InStr(firstChars, myDelimiter))
    Selection.InsertBefore Text:="xx" & firstChars & "yy"
  End If
Next myPara

Selection.WholeStory
Selection.Sort ExcludeHeader:=False, FieldNumber:="Paragraphs", _
     SortOrder:=wdSortOrderAscending, SortFieldType:=wdSortFieldAlphanumeric
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "xx*yy"
  .Wrap = wdFindContinue
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With
End Sub


PT
Sub AcronymFinder()
' Version 11.02.11
' Find a group of words for an acronym

myText = Trim(Selection)
Selection.Start = Selection.End
Selection.TypeText Text:="[[["

If Len(myText) = 1 Then
  myText = InputBox("Find?", "Acronym finder", Selection)
  If Len(myText) = 0 Then Exit Sub
End If
myFind = "<"
abbrLength = Len(myText)
For i = 1 To abbrLength
  If i = 4 Then Exit For
  myChar = Mid(myText, i, 1)
  myChar = "[" & LCase(myChar) & UCase(myChar) & "]"
  myFind = myFind & myChar & "[a-z]{1,}^32"
Next i
myFind = Left(myFind, Len(myFind) - 3)
 
Selection.HomeKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = myFind
  .Wrap = False
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = True
  .Execute
End With


If Selection.Find.Found = True Then
  If abbrLength = 4 Then Selection.MoveRight Unit:=wdWord, Count:=2, Extend:=wdExtend
  If abbrLength = 5 Then Selection.MoveRight Unit:=wdWord, Count:=3, Extend:=wdExtend
  Exit Sub
End If
Selection.HomeKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Text = "[[["
  .MatchWildcards = False
  .Execute
End With

Beep
Selection.MoveUp Unit:=wdLine, Count:=2
Selection.MoveDown Unit:=wdLine, Count:=2
Selection.Find.Execute
Selection.Find.Text = myFind
Selection.Find.MatchWildcards = True
End Sub


PT
Sub AcronymToSmallCaps()
' Version 31.12.13
' Create a list of all citations

myColour = False
' or if you want them highlighted:
' myColour = wdTurquoise

Selection.HomeKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[A-Z]{3,}"
  .Wrap = False
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = True
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute
End With

myCount = 0
Do While Selection.Find.Found = True
  Selection.Text = LCase(Selection.Text)
  If myColour <> False Then Selection.Range.HighlightColorIndex = myColour
  Selection.Font.SmallCaps = True
  Selection.Collapse wdCollapseEnd
  Selection.Find.Execute
Loop
End Sub


PT
Sub TableStripper()
' Version 19.06.12
' Strip out all tables into a separate file

myFormat = "[xxx near here]"

Set thisDoc = ActiveDocument
Documents.Add
Set tabDoc = ActiveDocument
thisDoc.Activate
Selection.HomeKey Unit:=wdStory
thisMany = 0

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "Table "
  .Replacement.Text = ""
  .MatchWildcards = False
  .Execute
End With

Do While Selection.Find.Found = True
  myCaptionStart = Selection.Start
  myNumberStart = Selection.End
  Selection.Paragraphs(1).Range.Select
' Only do anything if 'Table' is the first word of the paragraph
  If Selection.Start = myCaptionStart Then
    myCaptionEnd = Selection.End
    tabCaption = Selection
 
  ' Move back and pick up, say, 'Table 1.6'
    Selection.End = myNumberStart
    Selection.MoveEndUntil cset:=ChrW(9) & " :", Count:=wdForward
    tabTitle = Selection

  ' now check for table below
    Selection.Collapse wdCollapseEnd
    Selection.MoveDown Unit:=wdParagraph, Count:=2
    gotOne = False
    If Selection.Information(wdWithInTable) = True Then
      gotOne = True
      Do
        Selection.MoveDown Unit:=wdParagraph, Count:=1
      Loop Until Selection.Information(wdWithInTable) = False
      Selection.MoveUp Unit:=wdParagraph, Count:=1
      Selection.Start = myCaptionEnd
      Selection.Cut
    Else
      Selection.Start = myCaptionStart
      Selection.End = myCaptionStart
      Selection.MoveUp Unit:=wdParagraph, Count:=2
      If Selection.Information(wdWithInTable) = True Then
        gotOne = True
        Do
          Selection.MoveUp Unit:=wdLine, Count:=1
        Loop Until Selection.Information(wdWithInTable) = False
        Selection.MoveDown Unit:=wdParagraph, Count:=1
        Selection.End = myCaptionStart
        Selection.Cut
        Selection.MoveDown Unit:=wdParagraph, Count:=1
      End If
    End If
  ' Paste it into the tables document
    If gotOne = True Then
      Selection.InsertAfter Replace(myFormat, "xxx", tabTitle) & vbCrLf
      Selection.HomeKey Unit:=wdLine
      tabDoc.Activate
      Selection.TypeText Text:=tabCaption
      Selection.Paste
      Selection.TypeParagraph
      Selection.TypeParagraph
      thisMany = thisMany + 1
      thisDoc.Activate
      Selection.MoveDown Unit:=wdParagraph, Count:=1
    Else
      Selection.MoveDown Unit:=wdParagraph, Count:=2
    End If
  Else
    Selection.Collapse wdCollapseEnd
  End If
  Selection.Find.Execute
Loop
tabDoc.Activate
Selection.TypeText Text:=Str(thisMany) & " tables extracted" & vbCrLf
End Sub


PT
Sub FigCallouts()
' Version 18.12.10
' Figure callout inserter

findThis = "Fig. "
orThis = "Figure "
orMaybeThis = "Figures "
orEvenThis = "Figs "
orHowAbout = "Figs. "

thisChapter = InputBox("Chapter number?", "Figure callouts")

' Either use these two lines...
chapNumsExist = MsgBox("Existing chapter numbers?", vbQuestion + vbYesNo)
addChapNums = MsgBox("Add chapter numbers?", vbQuestion + vbYesNo)

' ...or these two lines
' chapNumsExist = vbYes
' addChapNums = vbNo

Callout = "<Figure " & thisChapter & ".XXXX about here>"
If chapNumsExist = vbYes Then
  chapNum = thisChapter & "."
Else
  chapNum = ""
End If


nowTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
i = 0
Selection.HomeKey Unit:=wdStory
Do
  i = i + 1
  figNum = Trim(Str(i))
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = findThis & chapNum & figNum & "[!0-9]"
    .Forward = True
    .MatchWildcards = True
    .Execute
  End With
  gotOne = Selection.Find.Found
  If gotOne = False Then
    With Selection.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = orThis & chapNum & figNum & "[!0-9]"
      .Forward = True
      .MatchWildcards = True
      .Execute
    End With
    gotOne = Selection.Find.Found
  End If
  If gotOne = False Then
    With Selection.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = orEvenThis & chapNum & figNum & "[!0-9]"
      .Forward = True
      .MatchWildcards = True
      .Execute
    End With
  If gotOne = False Then
    With Selection.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = orHowAbout & chapNum & figNum & "[!0-9]"
      .Forward = True
      .MatchWildcards = True
      .Execute
    End With
    gotOne = Selection.Find.Found
  End If
  If gotOne = False Then
    With Selection.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = orMaybeThis & chapNum & figNum & "[!0-9]"
      .Forward = True
      .MatchWildcards = True
      .Execute
    End With
    gotOne = Selection.Find.Found
  End If
  If gotOne = False Then
    myResponse = MsgBox(findThis & figNum & " missing!" _
         & vbCrLf & vbCrLf & "Continue?", vbQuestion + vbYesNo)
    If myResponse = vbNo Then Exit Sub
  Else
    If addChapNums = vbYes Then
      With Selection.Find
        .ClearFormatting
        .Replacement.ClearFormatting
        .Text = " "
        .Forward = True
        .MatchWildcards = False
        .Execute
      End With
      Selection.TypeText Text:=" " & thisChapter & "."
    End If
' Or make the following line Selection.MoveDown
' if you prefer the callout AFTER the paragraph
    Selection.MoveUp Unit:=wdParagraph, Count:=1
    myText = Replace(Callout, "XXXX", figNum)
    Selection.TypeText Text:=myText & vbCrLf
    Selection.MoveUp Unit:=wdParagraph, Count:=1, Extend:=wdExtend
    Selection.Style = ActiveDocument.Styles(wdStyleNormal)
    Selection.Range.HighlightColorIndex = wdYellow
    Selection.MoveRight Unit:=wdCharacter, Count:=1
  End If
Loop Until 0
ActiveDocument.TrackRevisions = nowTrack
Selection.Find.MatchWildcards = False
End Sub


PT
Sub TableCallouts()
' Version 14.12.10
' Table callout inserter

thisChapter = InputBox("Chapter number?", "Table callouts")

chapNumsExist = MsgBox("Existing chapter numbers?", vbQuestion + vbYesNo)
addChapNums = MsgBox("Add chapter numbers?", vbQuestion + vbYesNo)

' chapNumsExist = vbYes
' addChapNums = vbNo


Callout = "<Table " & thisChapter & ".XXXX about here>"
findThis = "Table "
orThis = "Tables "

If chapNumsExist = vbYes Then
  chapNum = thisChapter & "."
Else
  chapNum = ""
End If


nowTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
i = 0
Selection.HomeKey Unit:=wdStory
Do
  i = i + 1
  tableNum = Trim(Str(i))
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = findThis & chapNum & tableNum & "[!0-9]"
    .Forward = True
    .MatchWildcards = True
    .Execute
  End With
  gotOne = Selection.Find.Found
  If gotOne = False Then
    With Selection.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = orThis & chapNum & tableNum & "[!0-9]"
      .Forward = True
      .MatchWildcards = True
      .Execute
    End With
    gotOne = Selection.Find.Found
  End If
  If gotOne = False Then
    myResponse = MsgBox(findThis & tableNum & " missing!" _
         & vbCrLf & vbCrLf & "Continue?", vbQuestion + vbYesNo)
    If myResponse = vbNo Then Exit Sub
  Else
    If addChapNums = vbYes Then
      With Selection.Find
        .ClearFormatting
        .Replacement.ClearFormatting
        .Text = " "
        .Forward = True
        .MatchWildcards = False
        .Execute
      End With
      Selection.TypeText Text:=" " & thisChapter & "."
    End If
    Selection.MoveUp Unit:=wdParagraph, Count:=1
    typeThis = Replace(Callout, "XXXX", tableNum)
    Selection.TypeText Text:=typeThis & vbCrLf
    Selection.MoveUp Unit:=wdParagraph, Count:=1, Extend:=wdExtend
    Selection.Style = ActiveDocument.Styles(wdStyleNormal)
    Selection.Range.HighlightColorIndex = wdYellow
    Selection.MoveRight Unit:=wdCharacter, Count:=1
  End If
Loop Until 0
ActiveDocument.TrackRevisions = nowTrack
Selection.Find.MatchWildcards = False

End Sub


PT
Sub FigStrip()
' Version 01.09.10
' Strip out all figures and leave a callout

myFormat = "<xxx here>"
myFig = "Fig"
captionWithText = True
captionWithFigs = True

Set thisDoc = ActiveDocument
Documents.Add
Set figDoc = ActiveDocument
thisDoc.Activate

oldFind = Selection.Find.Text
Selection.HomeKey Unit:=wdStory
thisMany = 0
Do
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "[^13^n^m^l]" & myFig
    .Replacement.Text = ""
    .MatchWildcards = True
    .Execute
  End With
  gotOne = Selection.Find.Found
  Selection.MoveDown Unit:=wdParagraph, Count:=1
  myEnd = Selection.Start
  Selection.MoveUp Unit:=wdParagraph, Count:=1
  ' Cursor now at start of caption line
  captionStart = Selection.Start
  Do
    Selection.MoveRight Unit:=wdCharacter, Count:=1
  Loop Until Selection = " " Or Selection = vbTab
  Do
    Selection.MoveRight Unit:=wdCharacter, Count:=1
  Loop Until Selection = " " Or Selection = vbTab
  Selection.Start = captionStart
  figTitle = Selection
 
  ' Now start to look for one or more figures
  figStart = Selection.Start
  If gotOne Then
    Do
      figEnd = figStart
      Selection.MoveUp Unit:=wdParagraph, Count:=1
      figStart = Selection.Start
    Loop Until figStart < (figEnd - 3)
    Selection.End = myEnd
    Selection.Start = figEnd
  ' Is this a figure I've found?
    figsBefore = ActiveDocument.InlineShapes.Count _
         + ActiveDocument.Shapes.Count
    Selection.Cut
    figsAfter = ActiveDocument.InlineShapes.Count _
         + ActiveDocument.Shapes.Count
    If figsAfter = figsBefore Then
    ' If not, put it back
      Selection.Paste
      Selection.MoveLeft Unit:=wdWord, Count:=2
    Else
      Selection.InsertAfter Replace(myFormat, "xxx", figTitle) & vbCrLf
      Selection.HomeKey Unit:=wdLine
      figDoc.Activate
      Selection.Paste
      ' But mark the caption ready to copy it back
      Selection.MoveUp Unit:=wdParagraph, Count:=1, Extend:=wdExtend
      ' Either
      If captionWithFigs = True Then
        Selection.Copy
      Else
        Selection.Cut
        Selection.TypeText figTitle
      End If
      Selection.Start = Selection.End
      Selection.TypeParagraph
      Selection.TypeParagraph
      thisMany = thisMany + 1
      thisDoc.Activate
      ' If caption wanted in text, paste it back in
      If captionWithText = True Then
        Selection.MoveDown Unit:=wdParagraph, Count:=1
        Selection.Paste
      End If
    End If
  End If
Loop Until gotOne = False
figDoc.Activate

Selection.TypeText Text:=Str(thisMany) & " figures extracted" & vbCrLf

Selection.Find.Text = oldFind
Selection.Find.MatchWildcards = False
End Sub


PT
Sub TableEdit()
' Version 13.01.12
' Edit items in the cells of some or all tables

stripEnds = True

doHighlight = True
myColour = wdTurquoise

myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

If Selection.Start = Selection.End Then
  Set workRange = ActiveDocument.Tables
  doTheLot = True
Else
  Selection.Font.Shadow = True
  Set workRange = Selection.Tables
End If

For Each myTable In workRange
  For Each myCell In myTable.Range.Cells
    myText = Left(myCell.Range, Len(myCell.Range) - 2)
    newText = ""
   
    ' an em dash
    If myText = "-" Then newText = ChrW(8212): myText = ""
    ' set myText = "" to avoid the following, similar change
   
    ' hyphen to minus sign
    If Left(myText, 1) = "-" Then
      newText = Replace(myText, "-", ChrW(8722))
    End If
   
    ' space-hyphen to minus sign
    If Left(myText, 2) = " -" Then
      newText = Replace(myText, " -", ChrW(8722))
    End If
   
    If doTheLot = True Then
      doIt = True
    Else
      doIt = myCell.Range.Font.Shadow
    End If
    If newText > "" And doIt Then
      myCell.Range = newText
      If doHighlight = True Then myCell.Range.HighlightColorIndex = myColour
    End If

  ' Now strip any trailing tabs or CRs
    If stripEnds = True Then
      Set rng = myCell.Range
      rng.MoveEnd , -1
      rng.Start = rng.End - 1

      ' Add here any characters you want to chop off
      ' the end of each cell
      Do While rng = "." Or rng = Chr(13) Or rng = Chr(9)
        rng.Delete
        rng.MoveStart , -1
      Loop
    End If
  Next myCell
Next myTable

Set rng = ActiveDocument.Content
rng.Font.Shadow = False
ActiveDocument.TrackRevisions = myTrack
End Sub


PT
Sub TableEmDasher()
' Version 05.03.13
' Empty cells and hyphen/en dash to em dash

dashIfEmpty = True
trackIt = False

myTrack = ActiveDocument.TrackRevisions
If trackIt = False Then ActiveDocument.TrackRevisions = False
For Each myTable In ActiveDocument.Tables
  For Each myCell In myTable.Range.Cells
    myText = Trim(Left(myCell.Range, Len(myCell.Range) - 2))
    If myText = "-" Or myText = ChrW(8211) Or (dashIfEmpty = True _
         And myText = "") Then
      myCell.Range = ChrW(8212)
    End If
  Next myCell
Next myTable
ActiveDocument.TrackRevisions = myTrack
End Sub



PT
Sub CellsAddChar()
' Version 20.10.11
' Check that there is a full point ending each cell
myChar = "."
Selection.Font.Shadow = True
For Each myCell In Selection.Tables(1).Range.Cells
  Set rng = myCell.Range
  myText = rng
  If rng.Font.Shadow = True And InStr(Right(myText, 3), myChar) = 0 Then
    rng.InsertAfter myChar
  End If
Next myCell
Selection.Tables(1).Range.Font.Shadow = False
End Sub


PT
Sub TextBoxFrameCut()
' Version 01.06.10
' Remove textboxes and frames
' Initial version by richardwalshe@prufrock.co.uk
Dim sh As Shape
Dim fr As Frame
For Each sh In ActiveDocument.Shapes
    If sh.Type = msoGroup Then sh.Ungroup
Next sh

For Each sh In ActiveDocument.Shapes
    If sh.TextFrame.HasText Then
      ' Leaves images intact
        sh.TextFrame.TextRange.Copy
      ' Finds where the anchor for the textbox is
      ' N.B. This is not necessarily where the textbox
      '  has ended up
        sh.Anchor.Paragraphs(1).Range.Select
        Selection.Collapse
        sh.Delete
      ' Marks material so correctness of position
      '  can be checked
        Selection.TypeText Text:="<TBX>"
        Selection.Paste
        Selection.TypeText Text:="</TBX>"
    End If
Next sh

For Each fr In ActiveDocument.Frames
  ' Removes frames with similar tagging for later checking
    fr.Select
    Selection.Collapse
    Selection.TypeText Text:="<FRM>"
    fr.Select
    Selection.Collapse Direction:=wdCollapseEnd
    Selection.TypeText Text:="</FRM>"
  ' This is the bit that actually removes the frames
    fr.Select
    fr.Delete
Next fr
End Sub


PT
Sub SetTextBoxStyle()
' Version 01.06.10
' Apply style to all textboxes
  Dim myStoryRange As Range
  Set myStoryRange = ActiveDocument.StoryRanges(wdTextFrameStory)
  myStoryRange.Style = "List Bullet"
  While Not (myStoryRange.NextStoryRange Is Nothing)
    Set myStoryRange = myStoryRange.NextStoryRange
    myStoryRange.Style = "List Bullet"
  Wend
End Sub


PT
Sub FootNoteFiddle()
' Version 01.06.10
' Make changes to all footnotes
For i = 1 To ActiveDocument.Footnotes.Count
  Set rng = ActiveDocument.Footnotes(i).Range
  rng.Start = rng.End - 1
  myChar = rng
  ' If the final character is a space, delete it

  If myChar = " " Then
    rng.Delete
    rng.Start = rng.End - 1
    myChar = rng
  End If
  ' If the final character is now NOT a full point, add one
  If myChar <> "." Then rng.InsertAfter "."
Next
End Sub

PT
Sub FootNoteFiddle3()
' Version 01.06.10
' More footnote fiddling
For i = 1 To ActiveDocument.Footnotes.Count
  Set rng = ActiveDocument.Footnotes(i).Range
  rng.End = rng.Start
  rng.Start = rng.Start - 3
  rng.Font.Italic = False
Next
End Sub


PT
Sub NoteDeleteDblSpace()
' Version 01.06.10
' Delete double spaces from endnotes
For i = 1 To ActiveDocument.Endnotes.Count
  Set rng = ActiveDocument.Endnotes(i).Range
  rng.Start = rng.Start - 1
  rng.End = rng.Start + 1
  myChar = rng
  myASC = 0
  ' Check the very first character of the endnote
  If Len(myChar) > 0 And i > 1 Then myASC = Asc(myChar)
  ' If it�s a newline or a paragraph mark, delete it
  If myASC = 13 Or myASC = 11 Then rng.Delete

  ' Then do the same right through the endnotes
  ' i.e. delete any newlines, paras or spaces that
  ' occur at the beginning of an endnote, before
  ' the actual text of the note starts
  Set rng = ActiveDocument.Endnotes(i).Range
  rng.Start = rng.End - 1
  keepGoing = True
  Do
    myChar = rng
    If Len(myChar) > 0 Then
      myASC = Asc(myChar)
    Else
      myASC = 0
    End If
    If myASC = 13 Or myASC = 11 Or myASC = 32 Then
      rng.Delete
      rng.Start = rng.Start - 1
    Else
      keepGoing = False
    End If
  Loop Until keepGoing = False
Next
End Sub


PT
Sub EndNoteFiddleSuperscript()
' Version 25.07.11
' Make changes to all endnotes
For i = 1 To ActiveDocument.Endnotes.Count
  Set rng = ActiveDocument.Endnotes(i).Range
  rng.Start = rng.Start - 2
  rng.End = rng.Start + 1
  rng.Font.Superscript = True
Next
End Sub


PT
Sub DeleteEndnotes()
' Version 01.06.10
' Delete all endnotes
numberENs = ActiveDocument.Endnotes.Count
If numberENs > 0 Then
  For Each EN In ActiveDocument.Endnotes
    EN.Delete
  Next
End If
MsgBox ("Endnotes deleted: " & Str(numberENs))
End Sub


PT
Sub DeleteFoots()
' Version 01.06.10
' Delete all footnotes
numberFoots = ActiveDocument.Footnotes.Count
If numberFoots > 0 Then
  For Each fn In ActiveDocument.Footnotes
    fn.Delete
  Next
End If
MsgBox ("Footnotes deleted: " & Str(numberFoots))
End Sub


PT
Sub NotesUnembed()
' Version 29.08.10
' Unembed footnotes or endnotes

doHighlight = True

ActiveDocument.Footnotes.Convert
Selection.EndKey Unit:=wdStory
Selection.TypeParagraph

For Each myNote In ActiveDocument.Endnotes
  myNote.Range.Copy
  myNote.Reference.InsertBefore "zc" & myNote.Index & "cz"
  Selection.EndKey Unit:=wdStory
  Selection.TypeText myNote.Index & ". "
  Selection.Paste
  Selection.TypeText vbCrLf
Next myNote

For Each myNote In ActiveDocument.Endnotes
  myNote.Delete
Next myNote


' Remember the existing highlight colour
oldColour = Options.DefaultHighlightColorIndex
' Select preferred colour
Options.DefaultHighlightColorIndex = wdTurquoise

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "zc([0-9]{1,})cz"
  .Replacement.Text = "\1"
  .Replacement.Font.Superscript = True
  If doHighlight = True Then .Replacement.Highlight = True
  .Wrap = wdFindContinue
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

' Restore original colour
Options.DefaultHighlightColorIndex = oldColour

End Sub


PT
Sub NotesEmbed()
' Version 25.06.11
' Embed footnotes or endnotes
Selection.HomeKey Unit:=wdLine
If Selection <> "1" And Asc(Selection) <> 9 Then
  myResponse = MsgBox("Is this the first line of the notes?", _
        vbQuestion + vbYesNo)
  If myResponse = vbNo Then Exit Sub
End If

'Put a bookmark at the beginning of the footnotes
ActiveDocument.Bookmarks.Add Name:="PBnoteStart"

' Make sure there's a CR at the end of the file
Selection.EndKey Unit:=wdStory
Selection.TypeParagraph

Do
  Selection.GoTo What:=wdGoToBookmark, Name:="PBnoteStart"
  ' Read the note number
  myStart = Selection.Start
  Selection.End = Selection.Start + 5
  myNote = Val(Selection)

  ' Give up if you've reached the end
  If myNote = 0 Then Exit Do

  ' Select the footnote
  Selection.Start = myStart
  Selection.End = myStart
  Selection.MoveDown Unit:=wdParagraph, Count:=1, Extend:=wdExtend
  myPara = Selection

  ' If it's a blank line delete it and select the next paragraph
  If Len(myPara) < 5 Then
    Selection.Delete
    Selection.MoveDown Unit:=wdParagraph, Count:=1, Extend:=wdExtend
  End If

  ' Find a tab or, if not, the first space, i.e. after the note number
  spacePlace = InStr(myPara, Chr(9))
  If spacePlace = 0 Then spacePlace = InStr(myPara, " ")
  Selection.MoveEnd , -1
  Selection.MoveStart , spacePlace
  Selection.Copy
  Selection.Start = myStart

  ' Delete the used footnote
  Selection.MoveEnd , 1
  Selection.Delete
  If myNote = 1 Then
    Selection.HomeKey Unit:=wdStory
  Else
    ' Go back to the previous citation
    Selection.GoTo What:=wdGoToBookmark, Name:="PBlastNote"
  End If

  ' Find the next citation (superscript number)
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = Trim(Str(myNote))
    .Font.Superscript = True
    .Replacement.Font.Superscript = False
    .Replacement.Text = ""
    .Wrap = wdFindContinue
    .Forward = True
    .Execute
  End With
  ' Delete the superscript number and add a footnote
  Selection.Delete

  ' Bookmark the place
  ActiveDocument.Bookmarks.Add Name:="PBlastNote"

  ' Add a footnote and aste in the text of the footnote
  With ActiveDocument.Range(Start:=ActiveDocument.Content.Start, End:= _
    ActiveDocument.Content.End)
    .Footnotes.Add Range:=Selection.Range, Reference:=""
  End With
  Selection.Paste
Loop Until myNote = 0

' Tidy up and go to the end
ActiveDocument.Bookmarks("PBnoteStart").Delete
ActiveDocument.Bookmarks("PBlastNote").Delete
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ""
End With
Selection.EndKey Unit:=wdStory
myResponse = MsgBox("Convert to endnotes?", vbQuestion + vbYesNo)
If myResponse = vbYes Then ActiveDocument.Footnotes.Convert
End Sub



PT
Sub NotesUnembedBySections()
' Version 28.12.11
' Unembed endnotes that are numbered in sections
doHighlight = True
' Remember the existing highlight colour
oldColour = Options.DefaultHighlightColorIndex
' Select preferred colour
Options.DefaultHighlightColorIndex = wdTurquoise

myNoteNum = 0
Set rng = ActiveDocument.Content
For i = 1 To ActiveDocument.Sections.Count
  mySectionNotes = ""
  Set secRng = ActiveDocument.Sections(i).Range
  mySectionStart = secRng.Start
  mySectionEnd = secRng.End
  mySectionNoteNum = 0
  charsAdded = 0
  rng.Start = mySectionStart
  rng.End = mySectionStart
  Do
    myNoteNum = myNoteNum + 1
    mySectionNoteNum = mySectionNoteNum + 1
  ' Copy the note text
    If myNoteNum <= ActiveDocument.Endnotes.Count Then
      ActiveDocument.Endnotes(myNoteNum).Range.Copy
    End If
  ' Find next note citation
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = "^2"
      .Wrap = False
      .Replacement.Text = ""
      .Forward = True
      .MatchWildcards = False
      .Execute
    End With
    foundOne = rng.Find.Found
    absNotePos = rng.Start
    absNotePos2 = rng.End

    If foundOne = True Then
    ' If still within current section...
      If rng.Start < mySectionEnd Then
        ' We've found a note in the section
        myCitation = "zct" & mySectionNoteNum & "zct"
        charsAdded = charsAdded + Len(myCitation)
        rng.Start = absNotePos
      ' Add citation number as text
        rng.InsertBefore myCitation
        rng.Start = rng.End
        startAgainHere = rng.Start
        Set secRng = ActiveDocument.Sections(i).Range
        mySectionEnd = secRng.End
        secRng.Start = secRng.End - 1
        secRng.End = secRng.Start
      ' Paste the note text
        secRng.PasteAndFormat (wdFormatSurroundingFormattingWithEmphasis)
      ' or use just:        secRng.Paste
      ' Add the note number (within this section)
        secRng.InsertBefore vbCrLf & "znt" & mySectionNoteNum & "znt"
      Else
        myNoteNum = myNoteNum - 1
      End If
    End If
    StatusBar = "Adding note number: " & Str(myNoteNum)
  Loop Until foundOne = False Or absNotePos > mySectionEnd
Next i

' Convert citation numbers to superscript
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "zct([0-9]{1,})zct"
  .Replacement.Text = "\1"
  .Replacement.Font.Superscript = True
  If doHighlight = True Then .Replacement.Highlight = True
  .Wrap = wdFindContinue
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

' Convert note numbers to superscript
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "znt([0-9]{1,})znt"
  .Replacement.Text = "\1"
  .Replacement.Font.Superscript = True
  .Wrap = wdFindContinue
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

' Delete original notes
For Each myNote In ActiveDocument.Endnotes
  myNote.Delete
  myNoteNum = myNoteNum - 1
  StatusBar = "Deleting note number: " & Str(myNoteNum)
Next myNote
StatusBar = "Finished! But please wait for it to reformat the document."
Selection.EndKey Unit:=wdStory
Beep
End Sub



PT
Sub RenumberSuperscript()
' Version 18.12.10
' Renumber all superscript numbers
Selection.HomeKey Unit:=wdStory
i = 1
Do
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .MatchWildcards = True
    .Text = "[0-9]{1,}"
    .Font.Superscript = True
    .Replacement.Text = ""
    .Wrap = False
    .Execute
  End With
 
  keepGoing = Selection.Find.Found
  If keepGoing = True Then
    Selection.TypeText Text:=Trim(Str(i))
    i = i + 1
  End If
Loop Until keepGoing = False
End Sub


PT
Sub RenumberNotes()
' Version 18.12.10
' Renumber all note numbers
Selection.HomeKey Unit:=wdLine
If Selection <> "1" And Asc(Selection) <> 9 Then
  myResponse = MsgBox("Is this the first line of the notes?", _
   vbQuestion + vbYesNo)
  If myResponse = vbNo Then Exit Sub
End If

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = True
  .Text = "[0-9]{1,}"
  .Replacement.Text = ""
  .Wrap = False
  .Execute
End With
Selection.TypeText Text:="1"

i = 2
Do
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .MatchWildcards = True
    .Text = "^13[0-9]{1,}"
    .Replacement.Text = ""
    .Wrap = False
    .Execute
  End With
 
  keepGoing = Selection.Find.Found
  If keepGoing = True Then
    Selection.MoveStart wdCharacter, 1
    Selection.TypeText Text:=Trim(Str(i))
    i = i + 1
  End If
Loop Until keepGoing = False
Selection.Find.MatchWildcards = False
End Sub


PT
Sub DeleteAllBookmarks()
' Version 18.06.10
' Delete all bookmarks
ActiveDocument.Bookmarks.ShowHidden = True
numberBMs = ActiveDocument.Bookmarks.Count
If numberBMs > 0 Then
  For Each myBM In ActiveDocument.Bookmarks
    myBM.Delete
  Next
End If
MsgBox ("Bookmarks deleted: " & Str(numberBMs))
End Sub


PT
Sub DeleteComments()
' Version 18.06.10
' Delete all comments
For Each cmt In ActiveDocument.Comments
 cmt.Delete
Next
End Sub


PT
Sub DeleteAllLinks()
' Version 30.10.12
' Delete all hyperlinks

' First the main text
linksHere = ActiveDocument.Hyperlinks.Count
linksTotal = linksHere
If linksHere > 0 Then
  Selection.WholeStory
  Selection.Fields.Unlink
End If

' then the endnotes, if there are any
linksHere = ActiveDocument.Endnotes.Count
linksTotal = linksTotal + linksHere
If linksHere > 0 Then
  ActiveDocument.StoryRanges(wdEndnotesStory).Select
  Selection.Fields.Unlink
End If

' then the footnotes, if there are any
linksHere = ActiveDocument.Footnotes.Count
linksTotal = linksTotal + linksHere
If linksHere > 0 Then
  ActiveDocument.StoryRanges(wdFootnotesStory).Select
  Selection.Fields.Unlink
End If
MsgBox ("Links deleted: " & Str(linksTotal))
End Sub



PT
Sub DeleteSomeLinks()
' Version 31.10.12
' Delete hyperlinks that are not URLs

myColour = 0
myColour = wdGray25

linksDel = 0
linksTotal = 0
For pass = 1 To 3
  Select Case pass
    Case 1
      linksHere = ActiveDocument.Hyperlinks.Count
      If linksHere > 0 Then Set rng = ActiveDocument.Content
    Case 2
      linksHere = ActiveDocument.StoryRanges(wdEndnotesStory).Hyperlinks.Count
      If linksHere > 0 Then Set rng = ActiveDocument.StoryRanges(wdEndnotesStory)
    Case 3
      linksHere = ActiveDocument.StoryRanges(wdFootnotesStory).Hyperlinks.Count
      If linksHere > 0 Then Set rng = ActiveDocument.StoryRanges(wdFootnotesStory)
  End Select

  If linksHere > 0 Then
    linksTotal = linksTotal + linksHere
    For i = linksHere To 1 Step -1
      myText = rng.Hyperlinks(i).TextToDisplay
      myAddress = rng.Hyperlinks(i).Address
      If InStr(myText, "www") = 0 And InStr(myText, "http") = 0 _
           And InStr(myAddress, "mailto") = 0 Then
        Set rng2 = ActiveDocument.Hyperlinks(i).Range
        If myColour > 0 Then rng2.HighlightColorIndex = myColour
        ActiveDocument.Hyperlinks(i).Delete
        linksDel = linksDel + 1
      End If
    Next i
  End If
Next pass

linksDel = 0
MsgBox ("Links deleted: " & Str(linksDel) & " out of " & Str(linksTotal))

End Sub



PT
Sub FieldsUnlink()
' Version 14.02.11
' Selective field removal
myResponse = MsgBox("Unlink all field except equations?", _
     vbQuestion + vbYesNo)
If myResponse = vbNo Then Exit Sub

myCount = ActiveDocument.Fields.Count
' Do main text first
For Each fld In ActiveDocument.Fields
  If fld.Type <> 58 Then
    fld.Unlink
    i = i + 1
    myCount = myCount - 1
    StatusBar = "Links:  " & Str(myCount)
  End If
Next fld

' Now do the textboxes, if there are any
If ActiveDocument.Shapes.Count > 0 Then
  For shp = 1 To ActiveDocument.Shapes.Count
  ' Only check the textbox if it has any text in it
    If ActiveDocument.Shapes(shp).TextFrame.HasText Then
      Set rng = ActiveDocument.Shapes(shp).TextFrame.TextRange
      For Each fld In rng.Fields
        If fld.Type <> 58 Then
          fld.Unlink
          i = i + 1
          myCount = myCount - 1
          StatusBar = "Links:  " & Str(myCount)
        End If
      Next fld
    End If
  Next shp
End If
StatusBar = ""
MsgBox ("Fields unlinked: " & Str(i))
End Sub


PT
Sub UnlinkCitationsAndRefs()
' Version 02.05.13
' Unlinks reference citations (ignoring equations)

makeCitationItalic = True

' Sort the citations
For Each fld In ActiveDocument.Fields
  myType = fld.Type
  If myType = wdFieldCitation Then
    fld.Select
    myStart = Selection.Start
    Selection.Cut
    Selection.PasteAndFormat (wdFormatPlainText)
    Selection.Start = myStart - 1
    If makeCitationItalic = True Then Selection.Font.Italic = True
  End If
Next fld

' Now the bibliography (reference list)
For Each fld In ActiveDocument.Fields
  myType = fld.Type
  If myType = wdFieldBibliography Then
    fld.Select
    fld.Unlink
    Selection.Collapse wdCollapseStart
    Selection.MoveLeft , 1
    Exit Sub
  End If
Next fld
End Sub


PT
Sub ColonLowerCase()
' Version 01.06.10
' Changes the initial letter after a colon to lowercase
oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = True
  .Text = ": [A-Z]"
  .Replacement.Text = ""
  .Execute
End With

While Selection.Find.Found
  Selection.TypeText Text:=LCase(Selection)

' If you want them all highlighted use these three lines
'  Selection.MoveLeft Unit:=wdCharacter, Count:=3, Extend:=wdExtend
'  Selection.Range.HighlightColorIndex = wdTurquoise
'  Selection.Start = Selection.End

  Selection.Find.Execute
Wend

With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchWildcards = False
End With

End Sub


PT
Sub ColonUpperCase()
' Version 01.06.10
' Changes the initial letter after a colon to uppercase

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = True
  .Text = ": [a-z]"
  .Replacement.Text = ""
  .Execute
End With

While Selection.Find.Found
  Selection.TypeText Text:=UCase(Selection)

' If you want them all highlighted use these three lines
'  Selection.MoveLeft Unit:=wdCharacter, Count:=3, Extend:=wdExtend
'  Selection.Range.HighlightColorIndex = wdTurquoise
'  Selection.Start = Selection.End

  Selection.Find.Execute
Wend

End Sub


PT
Sub AutoListOff()
' Version 01.06.10
' Auto-lists to text
  ActiveDocument.ConvertNumbersToText
End Sub






PT
Sub AutoListOff2()
' Version 21.02.12
' Change auto-bulleted listing to real bullets

changeBullets = True

newCharacter = ChrW(8226): ' a bullet
' newCharacter = "*": ' asterisk

myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

ActiveDocument.ConvertNumbersToText

If changeBullets = True Then
  normalFont = ActiveDocument.Styles(wdStyleNormal).Font.Name

' One common type of bullet uses Symbol font
  Set rng = ActiveDocument.Range
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .MatchWildcards = False
    .Text = ChrW(&HF0B7) & "^t"
    .Forward = True
    .Font.Name = "Symbol"
    .Replacement.Text = newCharacter & "^t"
    .Replacement.Font.Name = normalFont
    .Wrap = wdFindContinue
    .Execute Replace:=wdReplaceAll
  End With

  ' The other uses Wingding font
  Set rng = ActiveDocument.Range
  With rng.Find
    .Text = ChrW(&HF0FC) & "^t"
    .Font.Name = "Wingding"
    .Replacement.Text = newCharacter & "^t"
    .Replacement.Font.Name = normalFont
    .Execute Replace:=wdReplaceAll
  End With
End If
ActiveDocument.TrackRevisions = myTrack
End Sub


PT
Sub FirstNotIndent()
' Version 01.06.10
' Full-out paragraph under all headings
StyleList = "Heading 1, Heading 2, Heading 3, and any more you want"
NoIndentStyle = "Body Text"
IndentStyle = "Body Text First Indent"

For i = 1 To (ActiveDocument.Paragraphs.Count - 1)
  StyleNow = ActiveDocument.Paragraphs(i).Range.Style
  StyleNext = ActiveDocument.Paragraphs(i + 1).Range.Style

  If InStr(StyleList, StyleNow) And StyleNext = IndentStyle Then
    ActiveDocument.Paragraphs(i + 1).Range.Style = NoIndentStyle
  End If
Next
End Sub


PT
Sub JustifyOFF()
' Version 01.06.10
' Turn format off on all paragraphs
For Each myPara In ActiveDocument.Paragraphs
 If myPara.Range.ParagraphFormat.Alignment = wdAlignParagraphJustify Then
   myPara.Range.ParagraphFormat.Alignment = wdAlignParagraphLeft
 End If
Next
End Sub


PT
Sub FunnyChange()
' Version 01.06.10
' Make various complicated changes to all paragraphs
For Each myPara In ActiveDocument.Paragraphs
 If myPara.Range.ParagraphFormat.Alignment = wdAlignParagraphJustify _
      And myPara.Range.Font.Size = 14 And myPara.Range.Font.Bold = True Then
   myPara.Range.ParagraphFormat.Alignment = wdAlignParagraphLeft
   myPara.Range.Font.Size = 12
   myPara.Range.Font.Bold = False
   myPara.Range.Font.Italic = True
 End If
Next
End Sub



PT
Sub SuperSubConvert()
' Version 24.10.13
' Change weird super/subscript format to proper ones

myColour = wdYellow
myUline = wdPink
myColour2 = wdRed
maxRaise = 7
minRaise = 3

maxLower = -7
minLower = -2

If Selection.Start <> Selection.End Then
  Set rng = Selection
  doSelection = True
Else
  Set rng = ActiveDocument.Content
  doSelection = False
End If

For Each myChar In rng.Characters
  myPos = myChar.Font.Position
  If myChar.Font.Underline = 1 Then
    myChar.Font.Underline = False
    myChar.HighlightColorIndex = myUline
    If doSelection = False Then myChar.Select
  End If
' Raise text to superscript
  If myPos >= minRaise And myPos <= maxRaise Then
    myChar.Font.Position = 0
    myChar.Font.Superscript = True
    myChar.HighlightColorIndex = myColour
    If doSelection = False Then myChar.Select
  End If
' Lowered text to subscript
  If myPos <= minLower And myPos >= maxLower Then
    myChar.Font.Position = 0
    myChar.Font.Subscript = True
    myChar.HighlightColorIndex = myColour2
    If doSelection = False Then myChar.Select
  End If
Next
Beep
End Sub


PT
Sub SymbolFontCheck()
' Version 23.02.10
' Greek symbol font checker
startNumber = &HF000
endNumber = &HF0FF
For i = startNumber To endNumber
  Set rng = ActiveDocument.Range
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = ChrW(i)
    .MatchCase = True
    .Execute
  End With
  myFont = rng.Font.Name
  If rng.Find.Found = True Then
    If myFont = "Symbol" Then rng.HighlightColorIndex = wdYellow
    If Left(myFont, 4) = "Wing" Then rng.HighlightColorIndex = wdRed
    rng.Start = rng.End
    rng.Select
    Selection.TypeText Text:="<&H" & Hex(i) & ">|"
    Selection.Start = rng.End
    Selection.Font.Reset
    Selection.Range.Style = ActiveDocument.Styles(wdStyleNormal)
  End If
  StatusBar = "            checking: " & Str(endNumber - i)
Next i
StatusBar = "Finished"

Selection.HomeKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "<&H^?^?^?^?^?^?"
  .Forward = True
  .Wrap = wdFindContinue
  .MatchCase = False
  .MatchWholeWord = False
  .MatchWildcards = False
  .Execute
End With
End Sub


PT
Sub SymbolToUnicode()
' Version 31.10.13
' Convert Symbol font characters to unicode

myColourFound = wdYellow
myColourNotFound = wdTurquoise
myColourWarning = wdRed

myResponse = MsgBox("Go ahead without a test run?", vbQuestion _
        + vbYesNoCancel, "SymbolToUnicode")
If myResponse = vbCancel Then Exit Sub
If myResponse = vbNo Then
  testRun = True
Else
  testRun = False
End If

' Greek type
myList = "F044,0394; F046,03A6; F061,03B1; F062,03B2; F065,03B5;"
myList = myList & "F067,03B3; F068,03B7; F071,03B8; F069,03B9; F063,03C7;"
myList = myList & "F056,03C2; F074,03C4; F077,03C9; F078,03BE; F079,03C8;"
myList = myList & "F057,03A9; F066,03D5; F06B,03BA; F072,03C1; F073,03C3;"
myList = myList & "F06A,03C6; F06C,03BB; F06D,03BC; F06E,03BD; F070,03C0;"
myList = myList & "F075,03C5; F076,03C9; F047,0393; F07A,03B6; F059,03A8;"
myList = myList & "F04C,039B; F050,03A0; F051,0398; F053,03A3; F058,039E;"
myList = myList & "F04A,03D1; F064,03B4; F009,03B4; (both delta)"
myList = myList & ""

' Maths symbol type
myList = myList & "F0AC,2190; F0AD,2191; F0AE,2192; F0AF,2193; F0B8,00F7;"
myList = myList & "F0DC,21D0; F0DD,21D1; F0DE,21D2; F0DF,21D3; F0A3,2264;"
myList = myList & "F0D7,22C5; F0C5,2295; F0C7,2229; F0C8,222A; F0C9,2283;"
myList = myList & "F0CA,2287; F0CB,2284; F0CC,2282; F0CD,2286; F0A5,221E;"
myList = myList & "F0B5,221D; F0B9,2260; F0BB,2248; F0CE,220A; F0CF,2209;"
myList = myList & "F0DB,21D4; F0C1,2111; F0B6,2202; F0C2,211C; F0C3,2118;"
myList = myList & "F0D6,221A; F0B4,00D7; F0A4,2265; F0B1,00B1; F0D1,2207;"
myList = myList & "F02D,2212; F0B3,2265; F0BA,2261; F022,2200; F0A2,2032;"
myList = myList & "F0B7,2022; F052,211d; "

' Ordinary characters, space, comma, etc
myList = myList & "F020,0020; F02C,002C; F07D,007D; F07B,007B;"
myList = myList & "F028,0028; F029,0029; F02B,002B; F03D,003D;"
myList = myList & "F030,0030; F031,0031; F032,0032; F033,0033;"
myList = myList & "F034,0034; F035,0035; F036,0036; F037,0037;"
myList = myList & "F038,0038; F039,0039; F02F,002F; F03D,002F;"
myList = myList & "F0B0,00B0; F03C,003C; F03E,003E; F02E,002E;"
myList = myList & "F03B,003B; F07C,007C; F02A,002A;"

myFont = ActiveDocument.Styles(wdStyleNormal).Font.Name
ActiveDocument.TrackRevisions = False
gotOne = False
For Each myChar In ActiveDocument.Characters
  ascChar = Asc(myChar)
  ascWChar = AscW(myChar)
  myFontName = myChar.Font.Name
  If ascChar = 40 Or ascChar = 63 Then
    myChar.Select
    myFontName = Selection.Font.Name
    ascWChar = Dialogs(wdDialogInsertSymbol).charnum
  End If
  If gotOne = False And ascWChar < 0 Then
    myChar.Select
    symbolCode = Replace(Hex(ascWChar), "FFFF", "")
    myPos = InStr(myList, symbolCode)
    If myPos > 0 And ascWChar <> 40 Then
      gotOne = True
      uCode = Val("&H" & Mid(myList, myPos + 5, 4))
      If testRun = False Then
        Selection.Font.Name = myFont
        Selection.TypeText Text:=ChrW(uCode)
        Selection.MoveStart , -1
      Else
        Selection.Collapse wdCollapseEnd
        Selection.TypeText Text:=" "
        Selection.MoveStart , -1
        Selection.Font.Name = myFont
        Selection.TypeText Text:=ChrW(uCode)
        Selection.MoveStart , -1
      End If
      Selection.Range.HighlightColorIndex = myColourFound
      If myFontName <> "Symbol" Then
        Selection.Range.HighlightColorIndex = myColourWarning
        Beep
      End If
    Else
      If Not (myFontName = myFont And Asc(myChar) = 40) Then _
           Selection.Range.HighlightColorIndex = myColourNotFound
      gotOne = False
      Beep
    End If
  Else
    gotOne = False
  End If
Next
Selection.HomeKey Unit:=wdStory
Beep
End Sub


PT
Sub FontHighlight()
' Version 03.10.11
' Highlight all fonts NOT named in the list

myBestColour = wdTurquoise

nonHighlight = 5
ReDim myFont(nonHighlight) As String
myFont(1) = "Calibri"
myFont(2) = "Times New Roman"
myFont(3) = "Garamond"
myFont(4) = "Cambria"
myFont(5) = ""


nowColour = Options.DefaultHighlightColorIndex
Options.DefaultHighlightColorIndex = myBestColour
Set rng = ActiveDocument.Content
rng.Font.Shadow = True

For i = 1 To nonHighlight
  If myFont(i) > "" Then
    thisFont = myFont(i)
    With Selection.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .MatchWildcards = False
      .Text = ""
      .Replacement.Text = ""
      .Font.Name = thisFont
      .Wrap = wdFindContinue
      .Replacement.Font.Shadow = False
      .Execute Replace:=wdReplaceAll
    End With
  End If
Next i

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = False
  .Text = ""
  .Wrap = wdFindContinue
  .Font.Shadow = True
  .Replacement.Font.Shadow = False
  .Replacement.Highlight = True
  .Execute Replace:=wdReplaceAll
End With
Options.DefaultHighlightColorIndex = nowColour

End Sub



PT
Sub FontEliminate()
' Version 16.02.13
' Restore anything in this font to the default font

myUnwantedFont = Selection.Font.Name
myBaseStyle = ActiveDocument.Styles(wdStyleNormal)
normalFont = ActiveDocument.Styles(myBaseStyle).Font.Name

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ""
  .Wrap = wdFindContinue
  .Font.Name = myUnwantedFont
  .Replacement.Text = ""
  .Replacement.Font.Name = normalFont
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute Replace:=wdReplaceAll
End With
End Sub

PT
Sub LanguageSetUK()
' Version 03.01.13
' Set language as UK English

myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
Set rng = ActiveDocument.Content
rng.LanguageID = wdEnglishUK
rng.NoProofing = False
If ActiveDocument.Shapes.Count > 0 Then
  For Each shp In ActiveDocument.Shapes
    If shp.TextFrame.HasText Then
      shp.TextFrame.TextRange.LanguageID = wdEnglishUK
    End If
  Next
End If
Application.CheckLanguage = True
ActiveDocument.Styles(wdStyleNormal).LanguageID = wdEnglishUK
ActiveDocument.TrackRevisions = myTrack
End Sub

PT
Sub LanguageSetUS()
' Version 03.01.13
' Set language as US English

myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
Set rng = ActiveDocument.Content
rng.LanguageID = wdEnglishUS
rng.NoProofing = False
If ActiveDocument.Shapes.Count > 0 Then
  For Each shp In ActiveDocument.Shapes
    If shp.TextFrame.HasText Then
      shp.TextFrame.TextRange.LanguageID = wdEnglishUS
    End If
  Next
End If
Application.CheckLanguage = True
ActiveDocument.Styles(wdStyleNormal).LanguageID = wdEnglishUS
ActiveDocument.TrackRevisions = myTrack
End Sub


PT
Sub LanguageHighlight()
' Version 05.12.11
' Highlight all text not in main language

myColour = wdGray25

mainLanguage = Selection.LanguageID
For Each myPara In ActiveDocument.Paragraphs
  If myPara.Range.LanguageID <> mainLanguage Then
    If myPara.Range.LanguageID = 9999999 Then
      For Each myWord In myPara.Range.Words
        If myWord.LanguageID <> mainLanguage Then
          myWord.HighlightColorIndex = myColour
        End If
      Next myWord
    Else
      myPara.Range.HighlightColorIndex = myColour
    End If
  End If
Next myPara
End Sub


PT
Sub LongSentenceCheck()
' Version 06.01.11
' Colour long sentences
myCol1 = wdColorBlue
myCol2 = wdColorSeaGreen
minLen = 40

' nowTrack = ActiveDocument.TrackRevisions
' ActiveDocument.TrackRevisions = False
myCol = myCol1
For Each snt In ActiveDocument.Sentences
  allWords = snt
  wordNum = Len(allWords) - Len(Replace(allWords, " ", ""))
  If wordNum > minLen Then
    snt.Font.Color = myCol
  ' Swap between the two colours ready for next long sentence
    If myCol = myCol1 Then
      myCol = myCol2
    Else
      myCol = myCol1
    End If
  End If
Next snt
' ActiveDocument.TrackRevisions = nowTrack
End Sub


PT
Sub HighlightAllQuestions()
' Version 15.02.11
' Highlights all questions

qnColour = wdBrightGreen

For Each sntce In ActiveDocument.Sentences
  myWords = sntce
  If Asc(Right(myWords, 2)) = Asc("?") Then
    sntce.HighlightColorIndex = qnColour
  End If
Next
End Sub


PT
Sub HighlightLongQuotesDouble()
' Version 11.08.11
' Highlight all long quotes (double)
' Alt-Ctrl-Num-9
myColour = wdBrightGreen
wordLimit = 50
Set rng = ActiveDocument.Content
Do
  stopNow = False
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = ChrW(8220)
    .Wrap = False
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = False
    .Execute
  End With
  If rng.Find.Found = True Then
    quoteStart = rng.Start
    rng.Start = rng.End
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = ChrW(8221)
      .Wrap = False
      .Replacement.Text = ""
      .Forward = True
      .MatchWildcards = False
      .Execute
    End With
  ' Highlight it
    rng.Start = quoteStart
    myText = rng
    myText = Replace(myText, "  ", " ")
    myText = Replace(myText, ChrW(8211) & " ", "")
    myText = Replace(myText, " -", "")
    myText = Replace(myText, ChrW(8212), " ")
    myLen = Len(myText) - Len(Replace(myText, " ", "")) + 1
    If myLen >= wordLimit Then
      rng.HighlightColorIndex = myColour
    End If
  Else
    stopNow = True
  End If
  rng.Start = rng.End
Loop Until stopNow = True
Beep
End Sub


PT
Sub HighlightLongQuotesSingle()
' Version 11.08.11
' Highlight all long quotes (single)
myColour = wdPink
wordLimit = 60

Set rng = ActiveDocument.Content
Do
  stopNow = False
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = ChrW(8216)
    .Wrap = False
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = False
    .Execute
  End With

  If rng.Find.Found = True Then
    quoteStart = rng.Start
    rng.Start = rng.End
    Do
      stopNow = False
      gotOne = False
      With rng.Find
        .ClearFormatting
        .Replacement.ClearFormatting
        .Text = ChrW(8217)
        .Wrap = False
        .Replacement.Text = ""
        .Forward = True
        .MatchWildcards = False
        .Execute
      End With

      If rng.Find.Found = True Then
      ' You've found a close quote/apostrophe
        rng.Start = rng.Start - 1
        rng.End = rng.Start + 1
        ch1 = rng
        rng.Start = rng.Start + 2
        rng.End = rng.Start + 1
        ch2 = rng
        rng.Start = rng.End

        gotOne = True
        If LCase(ch2) <> UCase(ch2) Then gotOne = False
        ' i.e. there's a letter after the apostrophe
        If gotOne = True And ch1 = "s" Then
          ' This could be an s-apostrophe, so test it
          hereNow = rng.Start
          With rng.Find
            .Text = ChrW(8216)
            .Wrap = False
            .Forward = True
            .MatchWildcards = False
            .Execute
          End With
          nextOpen = rng.Start
          rng.Start = hereNow
          rng.End = hereNow
          With rng.Find
            .Text = ChrW(8217) & "[!a-z]"
            .Wrap = False
            .Forward = True
            .MatchWildcards = True
            .Execute
          End With
          nextClosed = rng.Start
          rng.Start = hereNow
          rng.End = hereNow
          If nextOpen > nextClosed Then gotOne = False
        End If
        If gotOne = True Then
        ' Highlight it
          rng.Start = quoteStart
          rng.End = rng.End - 1
          myText = rng
          myText = Replace(myText, "  ", " ")
          myText = Replace(myText, ChrW(8211) & " ", "")
          myText = Replace(myText, " -", "")
          myText = Replace(myText, ChrW(8212), " ")
          myLen = Len(myText) - Len(Replace(myText, " ", "")) + 1
          If myLen >= wordLimit Then
            rng.HighlightColorIndex = myColour
          End If
        End If
      Else
        stopNow = True
      End If
    Loop Until stopNow = True Or gotOne = True
  Else
    stopNow = True
  End If
  rng.Start = rng.End
Loop Until stopNow = True

Set rng = ActiveDocument.Content
Do
  With rng.Find
   .ClearFormatting
   .Replacement.ClearFormatting
   .Text = "s" & ChrW(8217) & "^?"
   .Highlight = True
   .Wrap = False
   .Replacement.Text = ""
   .Forward = True
   .MatchWildcards = False
   .Execute
  End With
  gotOne = rng.Find.Found
  foundColour = rng.HighlightColorIndex
  If foundColour = myColour Then rng.HighlightColorIndex = 0
  rng.Start = rng.End
Loop Until gotOne = False
Set rng = ActiveDocument.Content

Do
  With rng.Find
   .ClearFormatting
   .Replacement.ClearFormatting
   .Text = "^?" & ChrW(8216) & "^?"
   .Highlight = True
   .Wrap = False
   .Replacement.Text = ""
   .Forward = True
   .MatchWildcards = False
   .Execute
  End With
  gotOne = rng.Find.Found
  foundColour = rng.HighlightColorIndex
  If foundColour = myColour Then rng.HighlightColorIndex = 0
  rng.Start = rng.End
Loop Until gotOne = False
Beep
End Sub


PT
Sub DisplayQuote()
' Version 11.08.11
' Display a hightlighted (or selected) quote

nextParaStyle = "Para-no-indent"
displayedQuoteStyle = "DisplayQuote"
ActiveDocument.TrackRevisions = False

Set rng = ActiveDocument.Content
If Selection.Start = Selection.End Then
  rng.Start = Selection.Start
  rng.End = Selection.End
  ' Find beginning to the nearest word
  nowCol = rng.HighlightColorIndex
  Do
    rng.MoveStart wdWord, -1
  Loop Until rng.HighlightColorIndex <> nowCol
  rng.MoveStart wdWord, 1
  wasEnd = rng.End
  rng.End = rng.Start + 1
 
  ' Find beginning to the nearest character
  Do
    rng.MoveStart , -1
    rng.MoveEnd , -1
  Loop Until rng.HighlightColorIndex <> nowCol
  rng.MoveStart , 1
  startHere = rng.Start
 
  ' Find end to the nearest word
  rng.Start = wasEnd
  rng.End = wasEnd
  Do
    rng.MoveEnd wdWord, 1
  Loop Until rng.HighlightColorIndex <> nowCol
  rng.Start = rng.End
  rng.MoveEnd , 1
  ' Find end to the nearest character
  Do
    rng.MoveStart , -1
    rng.MoveEnd , -1
  Loop Until rng.HighlightColorIndex = nowCol
  rng.Start = startHere
End If
rng.Select
sh = Selection.Start
Selection.Start = Selection.End
Selection.MoveStart wdCharacter, -1
Selection.Delete
Selection.MoveEnd wdCharacter, 1

' If there's a note marker, move past it
If Asc(Selection) = 2 Then
  Selection.Start = Selection.End
Else
  Selection.End = Selection.Start
End If

' Is it followed by a space?
' i.e. does the paragraph continue?
Selection.MoveEnd wdCharacter, 1
If Selection = " " Then
  Selection.TypeParagraph
  Selection.Style = ActiveDocument.Styles(nextParaStyle)
End If
Selection.Start = sh
Selection.Range.HighlightColorIndex = 0
Selection.End = Selection.Start + 1
Selection.TypeParagraph
Selection.Style = ActiveDocument.Styles(displayedQuoteStyle)
End Sub


PT
Sub HighlightIndentedParas()
' Version 01.03.11
' Highlight all indented paragraphs

myColour = wdBrightGreen
' myStyleName = "Displayed Quote"
myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
For Each myPara In ActiveDocument.Paragraphs
  If myPara.Range.ParagraphFormat.LeftIndent > 0 Then
    myPara.Range.HighlightColorIndex = myColour
'    myPara.Range.Style = ActiveDocument.Styles(myStyleName)
  End If
Next
ActiveDocument.TrackRevisions = myTrack
End Sub


PT
Sub IndentChanger()
' Version 07.07.11
' Change paras of one indent value to another

wasIndent = 1.25
newIndent = 0

wasPoints = CentimetersToPoints(wasIndent)

For Each myPara In ActiveDocument.Paragraphs
  Set rng = myPara.Range
  rng.Select
  StatusBar = (rng.ParagraphFormat.LeftIndent)
  myGuess = rng.ParagraphFormat.LeftIndent
  If myGuess < wasPoints + 1 And myGuess > wasPoints - 1 Then
    rng.ParagraphFormat.LeftIndent = CentimetersToPoints(newIndent)
  End If
Next myPara
Beep
End Sub


PT
Sub CountHighlightColour()
' Version 04.03.11
' Count how many times a highlight colour occurs

selColour = Selection.Range.HighlightColorIndex
hereNow = Selection.Start

If selColour = 0 Then
  MsgBox ("Select colour to be counted")
  Exit Sub
End If

i = 0
Set rng = ActiveDocument.Content
theEnd = rng.End
Do
  If rng.Start > 0 Then rng.Start = rng.End
  With rng.Find
   .ClearFormatting
   .Replacement.ClearFormatting
   .Text = ""
   .Highlight = True
   .Wrap = False
   .Replacement.Text = ""
   .Forward = True
   .MatchWildcards = False
   .Execute
  End With
  If rng.Find.Found = True Then
    If rng.HighlightColorIndex = selColour Then i = i + 1
    If i Mod 10 = 0 Then StatusBar = "To go: " _
        & Str(Int((theEnd - rng.Start) / 100))
  End If
Loop Until rng.End = rng.Start Or rng.End = ActiveDocument.Range.End
StatusBar = ""

MsgBox ("Found: " & Str(i))

Selection.Start = hereNow
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = oldFind
  .Replacement.Text = oldReplace
  .Wrap = wdFindContinue
End With
End Sub


PT
Sub BodyTexter()
' Version 02.02.11
' Apply 'Body Text' to every paragraph in 'Normal'

fromStyle = "Normal"
toStyle = "Body Text"
' Or use two other styles:
' fromStyle = "Caption"
' toStyle = "Figure Heading"

myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
i = ActiveDocument.Paragraphs.Count
For Each para In ActiveDocument.Paragraphs
  thisStyle = para.Style
  If thisStyle = ActiveDocument.Styles(fromStyle) Then
    para.Style = ActiveDocument.Styles(toStyle)
  End If
  i = i - 1
  StatusBar = "Paragraphs to go: " & Str(i)
Next para
Beep
ActiveDocument.TrackRevisions = myTrack
End Sub


PT
Sub CodeBoldParas()
' Version 13.01.11
' Tag/code every bold heading
myCode1 = "<B>"
myCode2 = "<A>"
' If you don't want to change the numbered headings
' to code 2, use myCode2 = ""

nowTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

For Each para In ActiveDocument.Paragraphs
  Set rng = para.Range
  rng.End = rng.End - 1
  If rng.Font.Bold = True Then
    rng.End = rng.Start
    rng.InsertAfter myCode1
    rng.Select
  End If
Next para

' And take out any examples of the code
' being applied to a blank line
Selection.HomeKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = myCode1 & "^p"
  .Replacement.Text = "^p"
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

' Make sure there are no spaces at the beginnings
' of coded lines
With Selection.Find
  .Replacement.ClearFormatting
  .ClearFormatting
  .Text = myCode1 & " "
  .Replacement.Text = myCode1
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With


' Optionally change the coding of any heading
' that starts with a number
If myCode2 > "" Then
  mCod1 = Replace(myCode1, "<", "\<")
  mCod1 = Replace(mCod1, ">", "\>")
  Selection.HomeKey Unit:=wdStory
  With Selection.Find
    .ClearFormatting
    .Text = mCod1 + "([0-9])"
    .Replacement.Text = myCode2 & "\1"
    .MatchWildcards = True
    .Execute Replace:=wdReplaceAll
  End With
End If

' Now clear up
With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchCase = False
End With
ActiveDocument.TrackRevisions = nowTrack
End Sub



PT
Sub AutoTagger()
' Version 19.01.13
' Tag/code automatically

myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
For Each para In ActiveDocument.Paragraphs
  Set rng = para.Range
  startText = "": endText = ""
  styleTitle = rng.Style
  Select Case styleTitle

' List your styles and tags here
' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Case "Heading 1"
      startText = "<A>"
    Case "Heading 2"
      startText = "<B>"
      startText = "<CH>"
    Case "Heading 3"
      startText = "<C>"
      startText = "<Part>"
    Case "Definition"
      startText = "<DF>": endText = "</DF>"
    Case "Caption"
      startText = "<Cap>"
    Case "citation"
      startText = "<Cap>"
    Case "Table3"
      startText = "<Cap>"
    Case "Level A"
      startText = "<A>"
  End Select

' For any "funny" styles, selection part of the name
' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  If InStr(styleTitle, "heading4") > 0 Then startText = "<A>"
  If InStr(styleTitle, "level4") > 0 Then startText = "<A>"
  If InStr(styleTitle, "Heading 5") > 0 Then startText = "<B>"
  If InStr(styleTitle, "Table") > 0 Then startText = "<Cap>"

  If rng.Characters(1) <> "<" And startText > "" Then
    rng.InsertBefore startText
    rng.End = rng.End - 1
    rng.InsertAfter endText
  End If
Next para
ActiveDocument.TrackRevisions = myTrack
Beep
End Sub


PT
Sub CodeIndentedParas()
' Version 07.07.11
' Add codes to indented paragraphs

beforeText = "<disp>"
afterText = "</disp>"

For Each para In ActiveDocument.Paragraphs
  Set rng = para.Range
  If rng.ParagraphFormat.LeftIndent <> CentimetersToPoints(0) _
       And Len(rng) > 10 Then
    rng.Select
    Selection.InsertBefore Text:=beforeText
    Selection.Start = Selection.End - 1
    Selection.End = Selection.Start
    Selection.TypeText Text:=afterText
  End If
Next para
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = afterText & "^p" & beforeText
  .Wrap = wdFindContinue
  .Replacement.Text = "^p"
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With
End Sub



PT
Sub TagBulletLists()
' Version 30.04.12
' Add tags to all bullet lists

For Each myPara In ActiveDocument.Paragraphs
  If myPara.FirstLineIndent < 0 Then
    myPara.Range.InsertBefore "<BL>"
    myPara.Range.InsertAfter "</BL>"
  End If
Next myPara

Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "<BL></BL>"
  .Wrap = wdFindContinue
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With
End Sub


PT
Sub HeadingStyler()
' Version 02.02.11
' Styles all headings by depth of section number

delimiter = Chr(9)
' delimiter = " "

myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

For Each para In ActiveDocument.Paragraphs
  myText = para
  tabPos = InStr(myText, delimiter) - 1
  If tabPos > 2 Then
    myText = Left(myText, tabPos)
    ' this should now hold just the heading number, e.g. 2.4.5.1
    ' but let's just check there are no alpha characters in there
    If LCase(myText) = UCase(myText) Then
      noPeriods = Replace(myText, ".", "")
      headLevel = Len(myText) - Len(noPeriods)
    ' No of periods = heading level
      Select Case headLevel
        Case 1: para.Style = ActiveDocument.Styles("Heading 1")
        Case 2: para.Style = ActiveDocument.Styles("Heading 2")
        Case 3: para.Style = ActiveDocument.Styles("Heading 3")
        Case 4: para.Style = ActiveDocument.Styles("Heading 4")
        Case 5: para.Style = ActiveDocument.Styles("Heading 5")
        Case 6: para.Style = ActiveDocument.Styles("Heading 6")
        Case Else
          para.Range.Select
          Beep
          Exit Sub
      End Select
    End If
  End If
Next para
Beep
ActiveDocument.TrackRevisions = myTrack
End Sub


PT
Sub CodeFirstLines()
' Version 05.12.12
' Prepare the file by adding codes, etc.

code1 = "<CN>"
code2 = "<CH>"
code3 = "<CA>"
code4 = "<A>"

myLine = 1
For Each myPara In ActiveDocument.Paragraphs
  If Len(myPara.Range) > 1 Then
    If myLine = 4 Then
      myPara.Range.InsertBefore Text:=code4
      Exit For
    End If
    If myLine = 3 Then
      myPara.Range.InsertBefore Text:=code3
      myLine = 4
    End If
    If myLine = 2 Then
      myPara.Range.InsertBefore Text:=code2
      myLine = 3
    End If
    If myLine = 1 Then
      myPara.Range.InsertBefore Text:=code1
      myLine = 2
    End If
  End If
Next myPara


' Set 1.15 spacing
Set rng = ActiveDocument.Content
rng.ParagraphFormat.LineSpacing = ActiveDocument.Styles(wdStyleNormal).Font.Size * 1.15

' Set language UK

ActiveDocument.TrackRevisions = False
Set rng = ActiveDocument.Content
rng.LanguageID = wdEnglishUK
rng.NoProofing = False
Application.CheckLanguage = True
ActiveDocument.Styles(wdStyleNormal).LanguageID = wdEnglishUK

' Switch on track changes
ActiveDocument.TrackRevisions = True
End Sub




PT
Sub ShowStyles()
' Version 06.05.12
' Show style names as <A>-type codes in the text

noShow = ",N,Normal,TOC 1,TOC 2,TOC 3,,,,"
noShow = noShow & ",Table of Figures,P1,,,"

abbrvs = ",MTDisplayEquation,Disp,Heading 1,A,Heading 2,B,,,"
abbrvs = abbrvs & ",Heading 3,C,Heading 4,D,Normal,N,"

removePads = False

doTables = True
' Merged cells generate an error, so ignore it
' and carry on regardless!
If doTables = True Then On Error Resume Next

' Find return <|
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "<["
  .Wrap = wdFindContinue
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = False
  .Execute
End With

gotCode = rng.Find.Found
myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
'If the codes are there, remove them
If gotCode Then
  Set rng = ActiveDocument.Content
  With rng.Find
    .Text = "\<\[*\]\>"
    .Wrap = wdFindContinue
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = True
    .Execute Replace:=wdReplaceAll
  End With
Else
' If no codes then add codes
  i = ActiveDocument.Paragraphs.Count
  abbrvs = abbrvs & ","
  For Each myPara In ActiveDocument.Paragraphs
    thisStyle = myPara.Style
    typeIt = True
    If InStr(noShow, "," & thisStyle & ",") > 0 Then typeIt = False
    If doTables = False And myPara.Range.Information(wdWithInTable) = True Then typeIt = False
    If typeIt = True Then
      myPos = InStr(abbrvs, thisStyle)
      If myPos > 0 Then
        thisStyle = Mid(abbrvs, myPos + Len(thisStyle) + 1)
        thisStyle = Left(thisStyle, InStr(thisStyle, ",") - 1)
      End If
      myPara.Range.InsertBefore Text:="<[" & thisStyle & "]>"
    End If
    i = i - 1
    StatusBar = "Paragraphs to go: " & Str(i)
  Next myPara
End If

If removePads = True And gotCode = False Then
' Check whether the user wants to remove the pads
  myResponse = MsgBox("Remove pad characters?", vbQuestion + vbYesNo)
  If myResponse = vbYes Then
    Set rng = ActiveDocument.Content
    With rng.Find
      .Text = "<["
      .Wrap = wdFindContinue
      .Replacement.Text = "<"
      .Forward = True
      .MatchWildcards = False
      .Execute Replace:=wdReplaceAll
    End With
    With rng.Find
      .Text = "]>"
      .Wrap = wdFindContinue
      .Replacement.Text = ">"
      .Forward = True
      .MatchWildcards = False
      .Execute Replace:=wdReplaceAll
    End With
  End If
End If
StatusBar = ""
ActiveDocument.TrackRevisions = myTrack
End Sub


PT
Sub NumberSequenceChecker()
' Version 06.02.12
' Check the sequence of section numbers

allowSingleNumbers = False

highlightError = True
captionWordsMax = 80
endAtLastRightItem = True

showProgress = True

Dim numText() As String
Dim num(9) As Integer

' Select line to be the model
If Selection.Start = Selection.End Then
  Selection.Paragraphs(1).Range.Select
End If
If Len(Selection) < 10 Then
  myResponse = MsgBox("Is this the starting line?", vbQuestion + vbYesNo)
  If myResponse = vbNo Then Exit Sub
End If

myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

' Look at the current heading line
Set rng = Selection.Range
If Asc(rng.Text) < 32 Then rng.MoveStart 1
rng.MoveEnd , -1
Selection.Collapse
lineText = rng.Text

' See if there's any text before the number
Set rng2 = rng.Duplicate
rng2.MoveStartUntil cset:="123456789", Count:=wdForward
posStart = InStr(rng.Text, Left(rng2.Text, 3)) - 1
leftText = Left(lineText, posStart)

' Find what's after the numbers: tab? space?
rng2.MoveStartWhile cset:="1234567890.", Count:=wdForward
posEnd = InStr(rng.Text, Left(rng2.Text, 3)) - 1
stopper = Mid(lineText, posEnd + 1, 1)
If Asc(stopper) = 9 Then stopper = "^t"
If AscW(stopper) = 8195 Then stopper = ChrW(8195)
If stopper = ")" Then stopper = "\)"
If stopper = "]" Then stopper = "\]"
startNum = Mid(lineText, posStart + 1, posEnd - posStart)
If Right(startNum, 1) = "." Then startNum = Left(startNum, Len(startNum) - 1)
' Analyse the number, splitting it into sections
numText = Split(startNum, ".")
startDepth = UBound(numText) + 1
For i = 1 To startDepth
  num(i) = numText(i - 1)
Next i
Set rng = rng2.Duplicate

leftTextSearch = Replace(leftText, "(", "\(")
leftTextSearch = Replace(leftTextSearch, "[", "\[")
Do
  Set rng2 = rng.Duplicate
  If allowSingleNumbers = False Then
    findThis = leftTextSearch & "[0-9.]{3,}" & stopper
  Else
    findThis = leftTextSearch & "[0-9.]@" & stopper
  End If
  Do
    rng.Collapse wdCollapseEnd
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = findThis
      .Wrap = False
      .Replacement.Text = ""
      .Forward = True
      .MatchWildcards = True
      .Execute
    End With
    Set rng3 = rng.Duplicate
    rng3.MoveStart , -1
  If showProgress = True Then rng.Select
    rng3.End = rng3.Start + 1

    If rng3.Text > "" Then crTest = Asc(rng3.Text)
    rng3.MoveEnd wdParagraph, 1
  ' Check if this really is likely to be a heading/caption
    If rng.Find.Found = False Then
      lookForAnother = False
    Else
      If (rng3.Words.Count) < captionWordsMax And (crTest = 12 _
           Or crTest = 13) And Right(newNum, 1) <> "." Then
        lookForAnother = False
        If InStr(rng.Text, ".") = 0 And allowSingleNumbers = False _
             Then lookForAnother = True
      Else
        lookForAnother = True
      End If
    End If
  Loop Until lookForAnother = False
  If rng.Find.Found = False Then
    rng.Move , 1
    rng.Select
    Beep
    endAtLastRightItem = False
    GoTo theEnd
  End If
  rng.MoveEnd , -1
  If showProgress = True Then rng.Select
  newNum = rng.Text
 
 
 
  newNum = Replace(newNum, leftText, "")
  If Right(newNum, 1) = "." Then newNum = Left(newNum, Len(newNum) - 1)
  keepGoing = True

  newDepth = Len(newNum) - Len(Replace(newNum, ".", "")) + 1
  depthChange = newDepth - startDepth
  Select Case (depthChange)
    Case Is > 1
    ' This must be an error!
      keepGoing = False
    Case 1:
    ' Test for, e.g. 3.4.5.6 -> 3.4.5.6.1
      If newNum <> startNum & ".1" Then keepGoing = False
      startDepth = startDepth + 1
      num(startDepth) = 1
    Case 0:
    ' Test for, e.g. 3.4.5.6 -> 3.4.5.7
      keepGoing = False
      num(startDepth) = num(startDepth) + 1
      testNum = ""
      For i = 1 To startDepth
        testNum = testNum & Trim(Str(num(i))) & "."
      Next i
      If Replace(testNum, newNum, "") = "." Then
        keepGoing = True
      Else
        If allowSingleNumbers = False Then
          If newNum = Trim(Str(num(1) + 1)) & ".1" Then
            keepGoing = True
            num(1) = num(1) + 1: num(2) = 1
          End If
        End If
      End If
    Case Is < 0:
   
    ' Test for, e.g. 3.4.5.6 -> 3.4.6 (or 3.5) (or 4)
      num(newDepth) = num(newDepth) + 1
      testNum = ""
      For i = 1 To newDepth
        testNum = testNum & Trim(Str(num(i))) & "."
      Next i
      If Replace(testNum, newNum, "") = "." Then
        keepGoing = True
      Else
        If allowSingleNumbers = False Then
          If newNum = Trim(Str(num(1) + 1)) & ".1" Then
            keepGoing = True
            num(1) = num(1) + 1: num(2) = 1
          End If
        End If
      End If
  End Select
  startNum = newNum
  startDepth = newDepth
Loop Until keepGoing = False
Beep
If highlightError = True Then
  rng2.HighlightColorIndex = wdTurquoise
  rng.HighlightColorIndex = wdRed
End If

' Second beep to show end
myTime = Timer
Do
Loop Until Timer > myTime + 0.2
Beep

theEnd:
If endAtLastRightItem = True Then
  rng2.Collapse
  rng2.Select
  startHere = Selection.Start - 1
  Selection.MoveDown Unit:=wdScreen, Count:=2
' Make sure that you've not dropped into a footnote ...
  Do While Selection.Information(wdInFootnote) = True
    Selection.MoveDown Unit:=wdLine, Count:=2
  Loop
Else
  rng.Collapse
  rng.Select
  startHere = Selection.Start - 1
  Selection.MoveUp Unit:=wdScreen, Count:=2
' Make sure that you've not dropped into a footnote ...
  Do While Selection.Information(wdInFootnote) = True
    Selection.MoveUp Unit:=wdLine, Count:=2
  Loop
End If


Selection.End = startHere
Selection.MoveRight Unit:=wdCharacter, Count:=1

With Selection.Find
  .Text = "^13" & findThis
  .MatchWildcards = True
End With
ActiveDocument.TrackRevisions = myTrack
End Sub



PT
Sub MatchBrackets()
' Version 07.12.12
' Check whether double quotes match up

For Each myPara In ActiveDocument.Paragraphs
  myText = myPara.Range.Text
  L = Len(myText)
  opens = L - Len(Replace(myText, "(", ""))
  closes = L - Len(Replace(myText, ")", ""))
 
' Ignore "1)", or "a)" type lists
  Set rng = myPara.Range
  rng.End = rng.Start + 2
  If InStr(rng.Text, ")") > 0 Then closes = closes - 1
 
  If opens <> closes Then
    myPara.Range.Font.Underline = True
    myCount = myCount + 1
    StatusBar = "Found: " & myCount
  End If
Next
StatusBar = ""
If myCount = 0 Then
  MsgBox ("All clear!")
Else
  MsgBox ("Number of suspect paragraphs: " & Trim(myCount))
End If
Selection.HomeKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ""
  .Font.Underline = True
  .Replacement.Text = ""
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute
End With
End Sub

PT
Sub MatchDoubleQuotes()
' Version 07.12.12
' Check whether double quotes match up

For Each myPara In ActiveDocument.Paragraphs
  myText = myPara.Range.Text
  L = Len(myText)
  L1 = Len(Replace(myText, Chr(34), ""))
  Lopen = Len(Replace(myText, ChrW(8220), ""))
  Lclose = Len(Replace(myText, ChrW(8221), ""))
 
  If (L - L1) Mod 2 <> 0 Or Lopen <> Lclose Then
    myPara.Range.Font.Underline = True
    myCount = myCount + 1
    StatusBar = "Found: " & myCount
  End If
Next
StatusBar = ""
If myCount = 0 Then
  MsgBox ("All clear!")
Else
  MsgBox ("Number of suspect paragraphs: " & Trim(myCount))
End If
Selection.HomeKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ""
  .Font.Underline = True
  .Replacement.Text = ""
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute
End With

End Sub

PT
Sub MatchSingleQuotes()
' Version 07.12.12
' Check whether single quotes match up

For Each myPara In ActiveDocument.Paragraphs
  myText = LCase(myPara.Range.Text)

  'Strip out the all apostrophe-s and s-apostrophe
  myText = Replace(myText, ChrW(8217) & "s", "")
  myText = Replace(myText, ChrW(8217) & "t", "")
  myText = Replace(myText, ChrW(8217) & "v", "")
  myText = Replace(myText, "s" & ChrW(8217), "")
  myText = Replace(myText, Chr(39) & "s", "")
  myText = Replace(myText, Chr(39) & "t", "")
  myText = Replace(myText, Chr(39) & "v", "")
  myText = Replace(myText, "s" & Chr(39), "")
 
  L = Len(myText)
  qts = L - Len(Replace(myText, Chr(39), ""))
  opens = L - Len(Replace(myText, ChrW(8216), ""))
  closes = L - Len(Replace(myText, ChrW(8217), ""))

  If qts Mod 2 <> 0 Or opens <> closes Then
    myPara.Range.Font.Underline = True
    myCount = myCount + 1
    StatusBar = "Found: " & myCount
  End If
Next
StatusBar = ""
If myCount = 0 Then
  MsgBox ("All clear!")
Else
  MsgBox ("Number of suspect paragraphs: " & Trim(myCount))
End If

Selection.HomeKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ""
  .Font.Underline = True
  .Replacement.Text = ""
  .Execute
End With
End Sub

PT
Sub AddSectionNumber()
' Version 07.02.11
' Add indexed section number

afterText = Chr(9): ' a tab

Dim v As Variable, i As Integer
varsExist = False
For Each v In ActiveDocument.Variables
  If v.Name = "indexNum" Then varsExist = True: Exit For
Next v

If varsExist = False Then
  ActiveDocument.Variables.Add "indexNum", 1
End If

' If no text is selected ...
If Selection.Start = Selection.End Then
' Add an index number
  i = ActiveDocument.Variables("indexNum")
  Selection.HomeKey Unit:=wdLine
  Selection.PasteAndFormat (wdFormatPlainText)
  Selection.TypeText Text:="." & Trim(Str(i)) + afterText
  Selection.HomeKey Unit:=wdLine
  If Selection = Chr(9) Then Selection.Delete Unit:=wdCharacter, Count:=1
  ActiveDocument.Variables("indexNum") = i + 1
Else:  ' if some text is selected ...
  ' set up a new index number
  i = Val(Selection)
  If i = 0 Then i = 1
  myInput = InputBox("Start number?", "Section numberer", Trim(Str(i)))
  If Len(myInput) = 0 Then Exit Sub
  i = Val(myInput)
  ActiveDocument.Variables("indexNum") = i
End If
Selection.End = Selection.Start
End Sub




PT
Sub FindAndDo()
' Version 08.02.12
' Find something specific and do things to each one


' Start from the top
Selection.HomeKey Unit:=wdStory

' Go and find the first occurrence
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "e"
  .Wrap = False
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = False
  .MatchCase = False
  .Execute
End With

myCount = 0
Do While Selection.Find.Found = True
' If you want to count them...
  myCount = myCount + 1
' Note where the end of the found item is
  endNow = Selection.End

' Make that character the same highlight colour
  If Selection.Text = "e" Then
    Selection.Range.Case = wdUpperCase
    Selection.Range.HighlightColorIndex = wdYellow
  Else
    Selection.Range.Case = wdLowerCase
    Selection.Range.HighlightColorIndex = wdRed
  End If
  ' Be sure you're past the previous occurrence
  Selection.End = endNow
  Selection.Collapse wdCollapseEnd

' Go and find the next occurence (if there is one)
  Selection.Find.Execute
Loop
MsgBox "Changed: " & myCount
End Sub


PT
Sub SampleFindAndDo()
' Version 08.02.12
' Find something specific and do things to each one


' Start from the top
Selection.HomeKey Unit:=wdStory

' Go and find the first occurrence
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "([0-9])-([0-9])"
  .Wrap = False
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = True
  .Execute
End With

myCount = 0
Do While Selection.Find.Found = True
' If you want to count them...
  myCount = myCount + 1
' Note where the end of the found item is
  endNow = Selection.End

' Move the start of the selection one space right
' i.e. past the first number and in front of the hyphen
  Selection.Start = Selection.Start + 1
' Put the end of the selection one place to the right of this
' i.e. select the hyphen
  Selection.End = Selection.Start + 1
' Type a dash, which will replace the selected hyphen
  Selection.TypeText Text:=ChrW(8211)
 
  ' Be sure you're past the previous occurrence
  Selection.End = endNow
  Selection.Collapse wdCollapseEnd

' Go and find the next occurence (if there is one)
  Selection.Find.Execute
Loop
MsgBox "Changed: " & myCount

End Sub


PT
Sub MultiChoiceTidierGlobal()
' Version 26.01.12
' Lower case first word and remove end spaces and punctuation

myCol = wdYellow
' For no highlighting
' myCol = 0
Set rng = ActiveDocument.Content
Do
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
  ' List here the possible item indicators
    .Text = "^13[ABCDE].[ " & Chr(9) & "]"
    .Wrap = False
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = True
    .Execute
  End With
  wasEnd = rng.End
  rng.Collapse wdCollapseEnd
 
  If rng.Find.Found = True Then
    rng.MoveEnd wdCharacter, 1
    rng.Case = wdLowerCase
    If myCol > 0 Then rng.HighlightColorIndex = myCol
    rng.Collapse wdCollapseEnd
    With rng.Find
      .Text = "^p"
      .MatchWildcards = False
      .Execute
    End With
    rng.MoveEnd wdCharacter, -1
  ' List here possible erroneous characters
    rng.MoveStartWhile cset:=". :;!?", Count:=wdBackward
    If rng.Start <> rng.End Then rng.Delete
    stopNow = False
  Else
    stopNow = True
  End If
  Selection.Start = Selection.End
Loop Until stopNow = True
End Sub

PT
Sub MultiChoiceTidierSingle()
' Version 28.01.11
' Lowercase initial character of answer + remove end spaces + punctuation

myMarker = "." & " "
'myMarker = "." & Chr(9)
'myMarker = ")" & " "
'myMarker = ")" & Chr(9)
myChars = "ABCDE"
' Find a line that looks like an answer line
isAnAnswer = False

' Find an answer line
Do
  Selection.Paragraphs(1).Range.Select
  startHere = Selection.Start
  endHere = Selection.End - 1
  myText = Selection.Text
  Selection.Collapse wdCollapseEnd
  If Len(myText) > 2 Then
    startLetter = Left(myText, 1)
    If InStr(myText, myMarker) = 2 And InStr(myChars, _
         startLetter) > 0 Then isAnAnswer = True
  End If
  isTheEnd = (ActiveDocument.Content.End - Selection.End < 2)
Loop Until isAnAnswer = True Or isTheEnd

Do
  If isAnAnswer = True Then
    Set rng = ActiveDocument.Content
    rng.Start = endHere
    rng.End = endHere
  ' Strip off spaces and punctuation
    rng.MoveStartWhile cset:=". :;!?", Count:=wdBackward
    If Len(rng.Text) > 0 Then rng.Delete
    rng.Start = startHere + 3
    rng.End = startHere + 4
    rng.Case = wdLowerCase
  End If
  isAnAnswer = False
' Check the following line
  Selection.Paragraphs(1).Range.Select
  startHere = Selection.Start
  endHere = Selection.End - 1
  myText = Selection.Text
  Selection.Collapse wdCollapseEnd
  If Len(myText) > 2 Then
    startLetter = Left(myText, 1)
    If InStr(myText, myMarker) = 2 And InStr(myChars, _
         startLetter) > 0 Then isAnAnswer = True
  End If
Loop Until isAnAnswer = False
End Sub

PT
Sub SerialCommaHighlight()
' Version 26.02.13
' Highlight or underline text that appears to have a serial comma

maxWords = 10
doUnderline = True
doHighlight = False
myColour = wdYellow

Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[a-zA-Z\-]@, [a-zA-Z\- ]@, and "
  .Replacement.Text = ""
  .MatchWildcards = True
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute
End With

While rng.Find.Found
  If rng.Words.Count < maxWords Then
    If doUnderline = True Then rng.Font.Underline = True
    If doHighlight = True Then rng.HighlightColorIndex = myColour
  End If
  rng.Start = rng.End
  rng.Find.Execute
Wend

Set rng = ActiveDocument.Content
With rng.Find
  .Text = "[a-zA-Z\-]@, [a-zA-Z\- ]@, or "
  .Replacement.Text = ""
  .Execute
End With

While rng.Find.Found
  If rng.Words.Count < maxWords Then
    If doUnderline = True Then rng.Font.Underline = True
    If doHighlight = True Then rng.HighlightColorIndex = myColour
  End If
  rng.Start = rng.End
  rng.Find.Execute
Wend
End Sub

PT
Sub SerialNotCommaHighlight()
' Version 26.02.13
' Highlight or underline text that appears not to have a serial comma

maxWords = 10
doUnderline = True
doHighlight = False
myColour = wdYellow

Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[a-zA-Z\-]@, [a-zA-Z\- ]@ and "
  .Replacement.Text = ""
  .MatchWildcards = True
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute
End With

While rng.Find.Found
  If rng.Words.Count < maxWords Then
    If doUnderline = True Then rng.Font.Underline = True
    If doHighlight = True Then rng.HighlightColorIndex = myColour
  End If
  rng.Start = rng.End
  rng.Find.Execute
Wend

Set rng = ActiveDocument.Content
With rng.Find
  .Text = "[a-zA-Z\-]@, [a-zA-Z\- ]@ or "
  .Replacement.Text = ""
  .Execute
End With

While rng.Find.Found
  If rng.Words.Count < maxWords Then
    If doUnderline = True Then rng.Font.Underline = True
    If doHighlight = True Then rng.HighlightColorIndex = myColour
  End If
  rng.Start = rng.End
  rng.Find.Execute
Wend
End Sub


ET
Sub ToggleNextCharCase()
' Version 28.06.09
' Change case of the next character/selection
myChar = Selection
If Asc(myChar) > 96 Then
  myChar = UCase(myChar)
Else
  myChar = LCase(myChar)
End If
Selection.Delete Unit:=wdCharacter, Count:=1
Selection.TypeText Text:=myChar
End Sub


ET
Sub ToggleNextCharCase2()
' Version 28.09.09
' Change case of the next character/selection
Selection.MoveRight Unit:=wdCharacter, Count:=1, Extend:=wdExtend
Selection.Range.Case = wdToggleCase
Selection.MoveRight Unit:=wdCharacter, Count:=1
End Sub


ET
Sub ToggleNextCharCase3()
' Version 08.02.10
' Change case of the next character/selection
trackIt = True

' If an area of text is NOT selected ...
If Selection.End = Selection.Start Then
  If trackIt = False Then
    Selection.MoveRight Unit:=wdCharacter, Count:=1, Extend:=wdExtend
    Selection.Range.Case = wdToggleCase
    Selection.MoveRight Unit:=wdCharacter, Count:=1
  Else
    myChar = Selection
    If Asc(myChar) > 96 Then
      myChar = UCase(myChar)
    Else
      myChar = LCase(myChar)
    End If
    Selection.MoveRight Unit:=wdCharacter, Count:=1
    Selection.TypeBackspace
    Selection.TypeText Text:=myChar
  End If
Else
' If an area of text is selected ...
  myText = Selection
  myUpper = True
  ' Look through the selection to find an a-z/A-Z
  ' character to decide which way to switch the case.
  If ActiveDocument.TrackRevisions = True Then
    ActiveDocument.TrackRevisions = False
    TrackON = True
  Else
    TrackON = False
  End If
  For myCount = 1 To Len(myText)
    myChar = Asc(Mid(myText, myCount, 1))
    If myChar > 96 And myChar < 123 Then myUpper = True: Exit For
    If myChar > 64 And myChar < 91 Then myUpper = False: Exit For
  Next myCount
  If trackIt = False Then
    If myUpper = True Then
      Selection.Range.Case = wdUpperCase
    Else
      Selection.Range.Case = wdLowerCase
    End If
  Else
    startWas = Selection.Start
    If myUpper = True Then
      myText = UCase(myText)
    Else
      myText = LCase(myText)
    End If
    wasBold = Selection.Font.Bold
    wasItalic = Selection.Font.Italic
    Selection.Delete Unit:=wdCharacter, Count:=1
    Selection.TypeText Text:=myText
    Selection.Start = startWas
    Selection.Font.Bold = wasBold
    Selection.Font.Italic = wasItalic
  End If
  ActiveDocument.TrackRevisions = TrackON
End If
End Sub


ET
Sub CaseTrackSwitch()
' Version 12.08.10
' Switch case-change tracking on and off
Dim v As Variable, trackNow As Boolean
varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "caseTrack" Then varExists = True: Exit For
Next v

If varExists = False Then
  ActiveDocument.Variables.Add "caseTrack", ActiveDocument.TrackRevisions
Else
 trackIt = ActiveDocument.Variables("caseTrack")
 ActiveDocument.Variables("caseTrack") = Not (trackIt)
End If

End Sub


ET
Sub CaseNextChar()
' Version 20.06.14
' Change case of the next character/selection
' Alt-S
trackIt = True

Dim v As Variable, trackNow As Boolean
varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "caseTrack" Then varExists = True: Exit For
Next v

If varExists = False Then
  ActiveDocument.Variables.Add "caseTrack", trackIt
Else
  trackIt = ActiveDocument.Variables("caseTrack")
End If

' Don't track case change if TC is OFF
If ActiveDocument.TrackRevisions = False Then trackIt = False

' If an area of text is NOT selected ...
If Selection.End = Selection.Start Then
  If trackIt = False Then
    Selection.MoveRight Unit:=wdCharacter, Count:=1, Extend:=wdExtend
    Selection.Range.Case = wdToggleCase
    Selection.MoveRight Unit:=wdCharacter, Count:=1
  Else
    myChar = Selection
    Selection.MoveRight Unit:=wdCharacter, Count:=1
    Char = Asc(myChar)
    If Char > 64 And Char < 123 Then
      If Char > 96 Then
        myChar = UCase(myChar)
      Else
        myChar = LCase(myChar)
      End If
      Selection.TypeBackspace
      Selection.TypeText Text:=myChar
    End If
  End If
Else
' If an area of text is selected ...
  myText = Selection
  ' Look through the selection to find an a-z/A-Z
  ' character to decide which way to switch the case.
  If ActiveDocument.TrackRevisions = True Then
    ActiveDocument.TrackRevisions = False
    trackIsON = True
  Else
    trackIsON = False
  End If
  voteUpper = 0
  voteLower = 0
  For myCount = 1 To Len(myText)
    myChar = Asc(Mid(myText, myCount, 1))
    If myChar > 96 And myChar < 123 Then voteLower = voteLower + 1
    If myChar > 64 And myChar < 91 Then voteUpper = voteUpper + 1
  Next myCount
  myUpper = (voteUpper > voteLower)
  If voteLower = 0 Then myUpper = False
  If trackIt = False Then
    If myUpper = True Then
      Selection.Range.Case = wdUpperCase
    Else
      For Each myWd In Selection.Range.Words
        If Len(myWd) > 1 Then
          myCh1 = Left(myWd, 1)
          myCh2 = Mid(myWd, 2, 1)
          If LCase(myCh1) <> myCh1 And UCase(myCh2) <> myCh2 Then _
               myWd.Case = wdLowerCase
        End If
      Next myWd
    End If
    If Selection = myText Then Selection.Range.Case = wdUpperCase
    If Selection = myText Then Selection.Range.Case = wdLowerCase
  Else
    startWas = Selection.Start
    If myUpper = True Then
      myTextNew = UCase(myText)
    Else
      myTextNew = LCase(myText)
    End If
    If myTextNew = myText Then myTextNew = UCase(myText)
    If myTextNew = myText Then myTextNew = LCase(myText)
    wasBold = Selection.Font.Bold
    wasItalic = Selection.Font.Italic
    Selection.Delete
    Selection.TypeText Text:=myTextNew
    Selection.Start = startWas
    Selection.Font.Bold = wasBold
    Selection.Font.Italic = wasItalic
  End If
  Selection.Font.Color = wdColorAutomatic
  ActiveDocument.TrackRevisions = trackIsON
End If
End Sub


ET
Sub CaseNextWord()
' Version 06.07.13
' Change case of initial letter of next word or selection

trackIt = True

' If an area of text is selected (more than one character) ...
If Selection.End > Selection.Start + 1 Then
  myText = Selection
  myUpper = True
  ' Count numbers of upper and lower case characters
  ' to decide which way to switch the case.
  myUpper = 0: myLower = 0
  For myCount = 1 To Len(myText)
    myChar = Asc(Mid(myText, myCount, 1))
    If myChar > 96 And myChar < 123 Then myLower = myLower + 1
    If myChar > 64 And myChar < 91 Then myUpper = myUpper + 1
  Next myCount
  If myUpper = 0 Then
    Selection.Range.Case = wdTitleWord
  Else
    Selection.Range.Case = wdLowerCase
  End If
Else

' If no text is selected ...
' Find a word start
  Selection.Words(1).Select
  Selection.Collapse wdCollapseEnd
  Selection.MoveStart , -1
  Selection.MoveEnd , 1

  If trackIt = False Then
    Selection.MoveStart , 1
    myChar = Selection
    If myChar = "-" Then
      Selection.Collapse wdCollapseEnd
      Selection.MoveEnd , 1
    End If
 
    Selection.Range.Case = wdToggleCase
    Selection.MoveRight Unit:=wdCharacter, Count:=1
  Else
    Selection.Start = Selection.Start + 1
    myChar = Selection
    If myChar = "-" Then
      Selection.Collapse wdCollapseEnd
      Selection.MoveEnd , 1
      myChar = Selection
    End If
    If Asc(myChar) > 64 Then
      If Asc(myChar) > 96 Then
        myChar = UCase(myChar)
      Else
        myChar = LCase(myChar)
      End If
      Selection.Start = Selection.End
      Selection.TypeBackspace
      Selection.TypeText Text:=myChar
    Else
      Selection.Start = Selection.End
    End If
  End If
End If
End Sub



ET
Sub CaseNextWord2()
' Version 18.12.10
' Change case of initial letter of second next word

trackIt = True

Dim v As Variable, trackNow As Boolean
varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "caseTrack" Then varExists = True: Exit For
Next v

If varExists = False Then
  ActiveDocument.Variables.Add "caseTrack", trackIt
Else
  trackIt = ActiveDocument.Variables("caseTrack")
End If

' Don't track case change if TC is OFF
If ActiveDocument.TrackRevisions = False Then trackIt = False

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

' If an area of text is selected (more than one character) ...
If Selection.Start > Selection.End + 1 Then
  If ActiveDocument.TrackRevisions = True Then
    ActiveDocument.TrackRevisions = False
    TrackON = True
  Else
    TrackON = False
  End If
  myText = Selection
  myUpper = True
  ' Count numbers of upper and lower case characters
  ' to decide which way to switch the case.
  myUpper = 0: myLower = 0
  For myCount = 1 To Len(myText)
    myChar = Asc(Mid(myText, myCount, 1))
    If myChar > 96 And myChar < 123 Then myLower = myLower + 1
    If myChar > 64 And myChar < 91 Then myUpper = myUpper + 1
  Next myCount
  If trackIt = False Then
    If (myUpper < myLower And myUpper > 0) Or myLower = 0 Then
      Selection.Range.Case = wdLowerCase
    Else
      Selection.Range.Case = wdUpperCase
    End If
  Else
    startWas = Selection.Start
    If (myUpper < myLower And myUpper > 0) Or myLower = 0 Then
      myText = LCase(myText)
    Else
      myText = UCase(myText)
    End If
    wasBold = Selection.Font.Bold
    wasItalic = Selection.Font.Italic
    Selection.Delete Unit:=wdCharacter, Count:=1
    Selection.TypeText Text:=myText
    Selection.Start = startWas
    Selection.Font.Bold = wasBold
    Selection.Font.Italic = wasItalic
  End If
  ActiveDocument.TrackRevisions = TrackON
  Exit Sub
End If

' If no text is selected ...
' Find a word start
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[!a-zA-Z" & ChrW(8217) & "][0-9a-zA-Z]"
  .Replacement.Text = ""
  .Wrap = wdFindContinue
  .Forward = True
  .MatchWildcards = True
  .Execute
End With

If trackIt = False Then
  Selection.Range.Case = wdToggleCase
  Selection.MoveRight Unit:=wdCharacter, Count:=1
Else
  Selection.Start = Selection.Start + 1
  myChar = Selection
   If Asc(myChar) > 96 Then
    myChar = UCase(myChar)
  Else
    myChar = LCase(myChar)
  End If
  Selection.Start = Selection.End
  Selection.TypeBackspace
  Selection.TypeText Text:=myChar
End If

With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchWildcards = False
End With
   
End Sub


ET
Sub CaseSecondNextWord()
' Version 12.08.10
' Capitalise toggle next-but-one word

trackIt = True

Set rng = ActiveDocument.Content
rng.Start = Selection.Start
rng.End = rng.Start
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[!a-zA-Z][0-9a-zA-Z]"
  .Replacement.Text = ""
  .Wrap = wdFindContinue
  .Forward = True
  .MatchWildcards = True
  .Execute
End With
rng.Start = rng.End
rng.Find.Execute

rng.Select

If Selection.End < wasEnd + 2 Then
  Selection.MoveRight Unit:=wdWord, Count:=1
  Selection.MoveLeft Unit:=wdCharacter, Count:=1
End If

If trackIt = False Then
  Selection.Range.Case = wdToggleCase
  Selection.MoveRight Unit:=wdCharacter, Count:=1
Else
  Selection.Start = Selection.Start + 1
  myChar = Selection
   If Asc(myChar) > 96 Then
    myChar = UCase(myChar)
  Else
    myChar = LCase(myChar)
  End If
  Selection.Start = Selection.End
  Selection.TypeBackspace
  Selection.TypeText Text:=myChar
End If
End Sub


ET
Sub CaseSecondNextWord2()
' Version 12.08.10
' Capitalise toggle next-but-one word

trackIt = True

Dim v As Variable, trackNow As Boolean
varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "caseTrack" Then varExists = True: Exit For
Next v

If varExists = False Then
  ActiveDocument.Variables.Add "caseTrack", trackIt
Else
  trackIt = ActiveDocument.Variables("caseTrack")
End If

' Don't track case change if TC is OFF
If ActiveDocument.TrackRevisions = False Then trackIt = False

Set rng = ActiveDocument.Content
rng.Start = Selection.Start
rng.End = rng.Start
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[!a-zA-Z][0-9a-zA-Z]"
  .Replacement.Text = ""
  .Wrap = wdFindContinue
  .Forward = True
  .MatchWildcards = True
  .Execute
End With
rng.Start = rng.End
rng.Find.Execute

rng.Select

If Selection.End < wasEnd + 2 Then
  Selection.MoveRight Unit:=wdWord, Count:=1
  Selection.MoveLeft Unit:=wdCharacter, Count:=1
End If

If trackIt = False Then
  Selection.Range.Case = wdToggleCase
  Selection.MoveRight Unit:=wdCharacter, Count:=1
Else
  Selection.Start = Selection.Start + 1
  myChar = Selection
   If Asc(myChar) > 96 Then
    myChar = UCase(myChar)
  Else
    myChar = LCase(myChar)
  End If
  Selection.Start = Selection.End
  Selection.TypeBackspace
  Selection.TypeText Text:=myChar
End If
End Sub


ET
Sub LcaseNextUcase()
' Version 08.01.11
' Lowercase the next uppercase letter
' Alt-Z
oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[A-Z]"
  .Replacement.Text = ""
  .Wrap = wdFindContinue
  .Forward = True
  .MatchWildcards = True
  .Execute
End With

Selection.Range.Case = wdToggleCase
Selection.MoveRight Unit:=wdCharacter, Count:=1

With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchWildcards = False
End With
End Sub



ET
Sub LcaseNextUcase2()
' Version 15.02.11
' Lowercase the next uppercase letter
' Alt-\
oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

trackIt = True

Dim v As Variable, trackNow As Boolean
varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "caseTrack" Then varExists = True: Exit For
Next v

If varExists = False Then
  ActiveDocument.Variables.Add "caseTrack", trackIt
Else
  trackIt = ActiveDocument.Variables("caseTrack")
End If

' Don't track case change if TC is OFF
If ActiveDocument.TrackRevisions = False Then trackIt = False

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[A-Z]"
  .Replacement.Text = ""
  .Wrap = wdFindContinue
  .Forward = True
  .MatchWildcards = True
  .Execute
End With

If trackIt = False Then
  Selection.Range.Case = wdLowerCase
Else
  myChar = Selection
  myChar = LCase(myChar)
  Selection.Delete
  Selection.TypeText Text:=myChar
End If

Selection.MoveRight Unit:=wdCharacter, Count:=1

With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchWildcards = False
End With

End Sub


ET
Sub CaseNextPara()
' Version 12.08.10
' Change case of initial letter of the paragraph

trackIt = True

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

Selection.HomeKey Unit:=wdLine

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[a-zA-Z]"
  .Replacement.Text = ""
  .Wrap = wdFindContinue
  .Forward = True
  .MatchWildcards = True
  .Execute
End With

If trackIt = False Then
  Selection.Range.Case = wdToggleCase
Else
  myChar = Selection
  If Asc(myChar) > 96 Then
    myChar = UCase(myChar)
  Else
    myChar = LCase(myChar)
  End If
  Selection.Delete
  Selection.TypeText Text:=myChar
End If
Selection.MoveDown Unit:=wdParagraph, Count:=1


With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchWildcards = False
End With
   
End Sub


ET
Sub CaseNextPara()
' Version 12.08.10
' Change case of initial letter of the paragraph

trackIt = True

Dim v As Variable, trackNow As Boolean
varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "caseTrack" Then varExists = True: Exit For
Next v

If varExists = False Then
  ActiveDocument.Variables.Add "caseTrack", trackIt
Else
  trackIt = ActiveDocument.Variables("caseTrack")
End If

' Don't track case change if TC is OFF
If ActiveDocument.TrackRevisions = False Then trackIt = False

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

Selection.HomeKey Unit:=wdLine
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[a-zA-Z]"
  .Replacement.Text = ""
  .Wrap = wdFindContinue
  .Forward = True
  .MatchWildcards = True
  .Execute
End With

If trackIt = False Then
  Selection.Range.Case = wdToggleCase
Else
  myChar = Selection
  If Asc(myChar) > 96 Then
    myChar = UCase(myChar)
  Else
    myChar = LCase(myChar)
  End If
  Selection.Delete
  Selection.TypeText Text:=myChar
End If
Selection.MoveDown Unit:=wdParagraph, Count:=1

With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchWildcards = False
End With
End Sub


ET
Sub SemicolonEndPara()
' Version 09.02.11
' Lowercase first character + add semicolon at end

Selection.Paragraphs(1).Range.Select
Selection.End = Selection.Start
lineStart = Selection.Start
startChar = Selection
If LCase(startChar) <> startChar Then
  Selection.Delete Unit:=wdCharacter, Count:=1
  Selection.TypeText Text:=LCase(startChar)
End If

Selection.End = Selection.End + 3
Selection.Start = Selection.Start + 2
startChar = Selection
If LCase(startChar) <> startChar Then
  Selection.Delete Unit:=wdCharacter, Count:=1
  Selection.TypeText Text:=LCase(startChar)
End If

Selection.Paragraphs(1).Range.Select
Selection.Start = Selection.End - 2
Selection.End = Selection.End - 1
lastChar = Selection
If lastChar = " " Then
  Selection.Delete
  Selection.Start = Selection.Start - 1
  lastChar = Selection
End If
If LCase(lastChar) = UCase(lastChar) And lastChar <> ";" _
    And lastChar <> ")" Then
  Selection.Delete
Selection.TypeText Text:=";"
Else
  Selection.MoveRight Unit:=wdCharacter, Count:=1
Selection.TypeText Text:=";"
End If
Selection.MoveRight Unit:=wdCharacter, Count:=2
End Sub


ET
Sub ListLowercaseSemicolon()
' Version 21.05.11
' Add semicolons to bulleted list + lowercase initial char

trackThis = True

addAnd = True
checkLength = 10
notThese = ";,. "

nowTrack = ActiveDocument.TrackRevisions
If trackThis = False Then ActiveDocument.TrackRevisions = False
Selection.Paragraphs(1).Range.Select
startStyle = Selection.Style
i = 0
Do
  endItem = Selection.End - 1
' select LH end until you meet an alpha character
  Selection.End = Selection.Start + 1
  If UCase(Selection) = LCase(Selection) Then Selection.MoveEnd , 1
  If UCase(Selection) = LCase(Selection) Then Selection.MoveEnd , 1
  Selection.Range.Case = wdLowerCase
' Get rid of any stray characters you don't want
  Selection.Start = endItem
  Do
    gotOne = False
    Selection.MoveStart , -1
    If InStr(notThese, Selection) > 0 Then
      Selection.Delete
      gotOne = True
    End If
  Loop Until gotOne = False
  Selection.InsertAfter ";"
' Now select the next item/para
  Selection.MoveStart , 3
  Selection.Paragraphs(1).Range.Select
  nowStyle = Selection.Style
  i = i + 1
  If i = checkLength Then
    myResponse = MsgBox("Continue?", vbQuestion + vbYesNo)
    If myResponse = vbNo Then Exit Sub
    i = 0
  End If
Loop Until nowStyle <> startStyle
Selection.End = Selection.Start - 1
Selection.Start = Selection.End - 1
If Selection = ";" Then Selection.Delete
Selection.MoveStart wdCharacter, -1
If Selection <> "." Then Selection.InsertAfter "."
If addAnd = True Then
  ' Go back to penultimate
  Selection.Paragraphs(1).Range.Select
  Selection.Start = Selection.Start - 1
  Selection.End = Selection.Start
  Selection.TypeText Text:=" and"
  Selection.MoveStart wdCharacter, -10
  If Selection = "; and; and" Then
    Selection.MoveStart wdCharacter, 5
    Selection.Delete
  End If
  Selection.MoveStart , 1
  If Selection = " and; and" Then
    Selection.MoveEnd , -5
    Selection.Delete
  End If
End If
Selection.Start = Selection.End
ActiveDocument.TrackRevisions = nowTrack
End Sub


ET
Sub ListSemicolon()
' Version 15.02.11
' Add semicolons to bulleted list

addAnd = True
checkLength = 10

nowTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
Selection.Paragraphs(1).Range.Select
startStyle = Selection.Style
i = 0
Do
  Selection.End = Selection.End - 1
  Selection.Start = Selection.End
  notThese = ";,. "
  Do
    gotOne = False
    Selection.MoveStart , -1
    If InStr(notThese, Selection) > 0 Then
      Selection.Delete
      gotOne = True
    End If
  Loop Until gotOne = False
  Selection.InsertAfter ";"
' Now select the next item/para
  Selection.MoveStart , 3
  Selection.Paragraphs(1).Range.Select
  nowStyle = Selection.Style
  i = i + 1
  If i = checkLength Then
    myResponse = MsgBox("Continue?", vbQuestion + vbYesNo)
    If myResponse = vbNo Then Exit Sub
    i = 0
  End If
Loop Until nowStyle <> startStyle
Selection.End = Selection.Start - 1
Selection.Start = Selection.End - 1
If Selection = ";" Then Selection.Delete
Selection.MoveStart wdCharacter, -1
If Selection <> "." Then Selection.InsertAfter "."
If addAnd = True Then
  ' Go back to penultimate
  Selection.Paragraphs(1).Range.Select
  Selection.Start = Selection.Start - 1
  Selection.End = Selection.Start
  Selection.TypeText Text:=" and"
  Selection.MoveStart wdCharacter, -10
  If Selection = "; and; and" Then
    Selection.MoveStart wdCharacter, 5
    Selection.Delete
  End If
  Selection.MoveStart , 1
  If Selection = " and; and" Then
    Selection.MoveEnd , -5
    Selection.Delete
  End If
End If
Selection.Start = Selection.End
ActiveDocument.TrackRevisions = nowTrack
End Sub



ET
Sub ListLowercaseNoPunct()
' Version 28.10.13
' Lowercase initial char + remove end punctuation

trackThis = True

nowTrack = ActiveDocument.TrackRevisions
If trackThis = False Then ActiveDocument.TrackRevisions = False

Selection.Paragraphs(1).Range.Select
startHere = Selection.Start
endHere = Selection.End - 1
posTab = InStr(Selection, Chr(9))
' select first character
Selection.End = startHere + 1

' If there's a bullet + a tab, go past it
If posTab > 0 Then
  Selection.MoveStart wdCharacter, posTab
  Selection.Collapse wdCollapseStart
End If
If trackThis = False Then
  Selection.Range.Case = wdLowerCase
Else
  newText = LCase(Selection)
  Selection.Delete
  Selection.TypeText Text:=newText
End If

Selection.Start = endHere - 1
Selection.End = endHere
If Selection = " " Then Selection.Delete: Selection.MoveStart wdCharacter, -1
If Selection = "." Then Selection.Delete
If Selection = ";" Then Selection.Delete
If Selection = "," Then Selection.Delete
If Asc(Selection) = 2 Then
  Selection.MoveLeft Unit:=wdCharacter, Count:=2
  Selection.Delete
End If
Selection.Start = endHere + 2
ActiveDocument.TrackRevisions = nowTrack
End Sub




ET
Sub TypeThat()
' Version 15.05.13
' Type 'that' between two words

Selection.MoveStart wdCharacter, -1
Selection.MoveEnd wdCharacter, 10
spPos = InStr(Selection, vbCr)
If spPos = 0 Then
  spPos = InStr(Selection, " ")
  Selection.MoveStart wdCharacter, spPos
  Selection.Collapse wdCollapseStart
  Selection.TypeText Text:="that "
Else
  Selection.MoveStart wdCharacter, spPos
  Selection.Collapse wdCollapseStart
  Selection.MoveLeft , 1
  Selection.TypeText Text:=" that"
End If
End Sub


ET
Sub TypeThe()
' Version 14.02.14
' Type 'the' or 'The' between two words

If Len(Selection) > 2 Then
  Selection.Expand wdWord
  cap = Left(Selection, 1)
  If LCase(cap) = cap Then
    Selection.TypeText Text:="the "
  Else
    Selection.TypeText Text:="The "
  End If
  Selection.MoveLeft , 1
  Exit Sub
End If

' special actions for use with LaTeX files
If Asc(Selection) = 13 Then Selection.TypeText Text:=" the": Exit Sub

Set rng = ActiveDocument.Content
rng.Start = Selection.Start - 1
rng.End = Selection.Start
prevChar = Asc(rng)
If prevChar = 13 And UCase(a) <> a Then Selection.TypeText Text:="the ": Exit Sub

Selection.MoveStart , -2
Selection.MoveEnd , 1
newPos = InStr(Selection, " ")
If newPos = 0 Then newPos = 3
Selection.MoveStart , newPos
Selection.Expand wdWord
Selection.MoveEndWhile cset:=Chr(13) & " ", Count:=wdBackward

Select Case Selection
  Case "that": Selection.TypeText Text:="which": Exit Sub
  Case "which": Selection.TypeText Text:="that": Exit Sub

  Case "A": Selection.TypeText Text:="The": Exit Sub
  Case "The": Selection.TypeText Text:="A": Exit Sub
 
  Case "a": Selection.TypeText Text:="the": Exit Sub
  Case "the": Selection.TypeText Text:="a": Exit Sub
 
  Case "has": Selection.TypeText Text:="have": Exit Sub
  Case "have": Selection.TypeText Text:="has": Exit Sub
 
  Case "was": Selection.TypeText Text:="were": Exit Sub
  Case "were": Selection.TypeText Text:="was": Exit Sub
 
  Case "is": Selection.TypeText Text:="are": Exit Sub
  Case "are": Selection.TypeText Text:="is": Exit Sub
End Select

' For the/The, sort out the capitalisation
firstChar = Left(Selection, 1)
secondChar = Mid(Selection, 2, 1)
If Len(Selection) > 2 Then
  thirdChar = Mid(Selection, 3, 1)
Else
  thirdChar = "a": ' not an uppercase char
End If
Selection.Collapse wdCollapseStart
 
' Check for sentence start indicator
Set rng = Selection.Range
rng.MoveStart wdCharacter, -5
spPos = InStr(rng, Chr(32))
crPos = InStr(rng, vbCr)
tabPos = InStr(rng, Chr(9))
fpPos = InStr(rng, ".")
gotanFP = tabPos + fpPos + crPos
thisText = rng
If Len(thisText) - Len(Replace(thisText, vbCr, "")) > 1 Then gotanFP = 1
If Len(thisText) - Len(Replace(thisText, " ", "")) > 1 Then gotanFP = 0
If gotanFP > 0 Then
  gotAnAcronym = False
  If LCase(secondChar) <> secondChar Or LCase(secondChar) = _
       UCase(secondChar) Or LCase(thirdChar) <> thirdChar Or _
       LCase(thirdChar) = UCase(thirdChar) Then gotAnAcronym = True
       
  If gotAnAcronym = False And firstChar <> LCase(firstChar) Then
    Selection.Delete
    Selection.TypeText Text:=LCase(firstChar)
    Selection.MoveEnd wdCharacter, -1
  End If
  Selection.TypeText Text:="The "
Else
  Selection.TypeText Text:="the "
End If
End Sub



ET
Sub ListBulleter()
' Version 09.06.12
' Add a bullet to every paragraph in a list

funnyFont = "Wingdings 2"
' If you don't want a funny font, use:
' funnyFont = ""

mySeparator = " "
' If you prefer a tab, use:
' mySeparator = Chr(9)

theEnd = Selection.End
Selection.End = Selection.Start

Do
  ' Select the current paragraph
  Selection.Paragraphs(1).Range.Select
  Selection.InsertBefore "zczc" & mySeparator
  Selection.Start = Selection.End
Loop Until Selection.End > theEnd

Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "zczc"
  .Wrap = wdFindContinue
  .Replacement.Text = ChrW(8226)
  If Len(funnyFont) > 2 Then .Replacement.Font.Name = funnyFont
  .Forward = True
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

End Sub


ET
Sub InitialNext()
' Version 29.05.10
' Find run of caps
' Alt-I

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

Selection.MoveStart wdWord, 1

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[A-Z][A-Z][A-Z][A-Z]"
  .MatchWildcards = True
  .Execute
End With

With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchWildcards = False
End With
End Sub


ET
Sub InitialCapOnly()
' Version 29.05.10
' Reduce all-capitals to initial cap
' Crtl-Alt-I

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

Selection.MoveStart wdCharacter, 1
Selection.MoveEnd wdWord, 1
Selection.MoveStart wdWord, -1

Selection.Range.Case = wdTitleWord
Selection.Start = Selection.End
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[A-Z][A-Z][A-Z][A-Z]"
  .MatchWildcards = True
  .Execute
End With

With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchWildcards = False
End With
End Sub


ET
Sub LowercaseGlobal()
' Version 10.02.11
' Lowercase this phrase throughout text

myHighlight = True
myHighlightColour = wdTurquoise
myOldColour = Options.DefaultHighlightColorIndex
Options.DefaultHighlightColorIndex = myHighlightColour
myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
myText = Trim(Selection)
Selection.Start = Selection.End

Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = myText
  .Wrap = wdFindContinue
  .Replacement.Text = LCase(myText)
  If myHighlight = True Then .Replacement.Highlight = True
  .Forward = True
  .MatchCase = True
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

Options.DefaultHighlightColorIndex = myOldColour
ActiveDocument.TrackRevisions = myTrack
End Sub


ET
Sub SelectWord()
' Version 04.06.11
' Select current word

Selection.Words(1).Select
aposn = Len(Selection) + 1 - InStr(Selection, ChrW(8217))
If aposn = 1 Or aposn = 2 Then Selection.MoveEnd , aposn _
     * (Len(Selection) > 1)
End Sub


ET
Sub SelectSentence()
' Version 04.06.11
' Select current sentence

Selection.End = Selection.Start
hereNow = Selection.Start
Selection.Sentences(1).Select
If Right(Selection, 1) = Chr(13) Then
  Selection.MoveEnd , -2
End If
If Asc(Right(Selection, 1)) = 146 Or Asc(Right(Selection, 1)) = 148 Then
  Selection.MoveEnd , -1
End If
If Asc(Left(Selection, 1)) = 145 Or Asc(Left(Selection, 1)) = 147 Then
  Selection.MoveStart , 1
End If

' That should be it - sentence selected -
' but Word sometimes picks up the following sentence...
If Selection.Start > hereNow Then
  Selection.MoveStart wdSentence, -1
  Selection.MoveEnd wdSentence, -1
End If

' or it picks up two sentences...
startSent = Selection.Start
endSent = Selection.End
mySent = Selection
sentMiddle = InStr(mySent, ".' ")
If sentMiddle = 0 Then sentMiddle = InStr(mySent, ".' ")
If sentMiddle = 0 Then sentMiddle = InStr(mySent, "."" ")
If sentMiddle = 0 Then sentMiddle = InStr(mySent, "." & ChrW(8221) & " ")
If sentMiddle = 0 Then sentMiddle = InStr(mySent, "!' ")
If sentMiddle = 0 Then sentMiddle = InStr(mySent, "!' ")
If sentMiddle = 0 Then sentMiddle = InStr(mySent, "!"" ")
If sentMiddle = 0 Then sentMiddle = InStr(mySent, "!" & ChrW(8221) & " ")
If sentMiddle = 0 Then sentMiddle = InStr(mySent, "?' ")
If sentMiddle = 0 Then sentMiddle = InStr(mySent, "?' ")
If sentMiddle = 0 Then sentMiddle = InStr(mySent, "?"" ")
If sentMiddle = 0 Then sentMiddle = InStr(mySent, "?" & ChrW(8221) & " ")
If sentMiddle > 0 And sentMiddle < Len(mySent) - 2 Then
  If sentMiddle > hereNow - startSent Then
    Selection.End = Selection.Start + sentMiddle + 2
  Else
    Selection.Start = Selection.Start + sentMiddle + 2
  End If
End If


' and it can get confused by curly quotes at the beginning...
mySent = Selection
If Left(mySent, 2) = ChrW(8217) & " " Or Left(mySent, 2) = ChrW(8221) & " " Then
  Selection.MoveStart , 2
End If

' or end of a sentence.
endChars = Right(Selection, 2)
aposn = Len(Selection) + 1 - InStr(Selection, ChrW(8217))
If aposn = 1 Or aposn = 2 Then Selection.MoveEnd , aposn _
     * (Len(Selection) > 1)
End Sub


ET
Sub SelectParagraph()
' Version 24.11.10
' Select current paragraph

Selection.Paragraphs(1).Range.Select
End Sub


ET
Sub SelectCurrentPage()
' Version 26.01.11
' Select the current page
Selection.GoTo What:=wdGoToPage, Which:=wdGoToNext, Count:=1
endHere = Selection.Start
Selection.GoTo What:=wdGoToPage, Which:=wdGoToPrevious, Count:=1
Selection.End = endHere
End Sub


ET
Sub SelectTheseWords()
' Version 06.12.11
' Extend selection to whole words
Selection.MoveEnd Unit:=wdWord, Count:=1
Selection.MoveEndWhile cset:=" ", Count:=wdBackward
Selection.MoveEndWhile cset:=ChrW(8217), Count:=wdBackward
Selection.MoveStart Unit:=wdWord, Count:=-1
End Sub



ET
Sub DeleteWord()
' Version 09.11.13
' Delete current word, but no punctuation
' Alt-D

Selection.Words(1).Select
Selection.MoveEndWhile cset:=ChrW(8217) & "' ", Count:=wdBackward
Set rng = Selection.Range
rng.Collapse wdCollapseEnd
rng.MoveEnd , 1
nextChar = rng.Text
Set rng = Selection.Range
rng.Collapse wdCollapseStart
rng.MoveStart , -1
prevChar = rng.Text
If nextChar = " " Then Selection.MoveEnd , 1
If prevChar = " " And nextChar <> " " Then Selection.MoveStart , -1
Selection.Delete
End Sub




ET
Sub PunctOffRight()
' Version 17.09.10
' Remove final character of word

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

Selection.End = Selection.Start
With Selection.Find
  .ClearFormatting
  .Text = "[ ^13^11]"
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = True
  .Execute
End With

Selection.MoveLeft Unit:=wdCharacter, Count:=1
Selection.TypeBackspace

With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchWildcards = False
End With
End Sub


ET
Sub PunctoffBothEnds()
' Version 01.06.10
' Remove punctuation from both ends of a word
Selection.MoveLeft Unit:=wdCharacter, Count:=1
Selection.MoveRight Unit:=wdWord, Count:=1
Selection.MoveLeft Unit:=wdCharacter, Count:=2
thisChar = Selection
If UCase(thisChar) <> thisChar Or _
  LCase(thisChar) <> thisChar Then
  Selection.MoveRight Unit:=wdCharacter, Count:=1
End If
Selection.Delete Unit:=wdCharacter, Count:=1
Selection.MoveLeft Unit:=wdWord, Count:=1
Selection.TypeBackspace
Selection.MoveRight Unit:=wdCharacter, Count:=2
End Sub


ET
Sub ScareQuoteAdd()
' Version 01.06.10
' Add single quotes round a word
Selection.MoveLeft Unit:=wdCharacter, Count:=1
Selection.MoveRight Unit:=wdWord, Count:=1
Selection.MoveLeft Unit:=wdCharacter, Count:=1
thisChar = Selection
If UCase(thisChar) <> thisChar Or _
    LCase(thisChar) <> thisChar Then
  Selection.MoveRight Unit:=wdCharacter, Count:=1
End If
Selection.TypeText Text:=Chr(146)
Selection.MoveLeft Unit:=wdWord, Count:=1
Selection.TypeText Text:=Chr(145)
Selection.MoveRight Unit:=wdCharacter, Count:=2
End Sub


ET
Sub NonCurlyApostrophe()
' Version 01.01.10
' Add non-curly single quote
  Options.AutoFormatReplaceQuotes = False
  Selection.TypeText Text:="'"
  Options.AutoFormatReplaceQuotes = True
End Sub


ET
Sub NonCurlyQuote()
' Version 01.01.10
' Add non-curly double quote
  Options.AutoFormatReplaceQuotes = False
  Selection.TypeText Text:=""""
  Options.AutoFormatReplaceQuotes = True
End Sub


ET
Sub SinglePrime()
' Version 01.01.10
' Add double prime
Selection.InsertSymbol Font:="Times New Roman", CharacterNumber:=8242, Unicode:=True
End Sub


ET
Sub DoublePrime()
' Version 01.01.10
' Add single prime
Selection.InsertSymbol Font:="Times New Roman", CharacterNumber:=8243, Unicode:=True
End Sub



ET
Sub SwapCharacters()
' Version 01.01.12
' Switch chars either side of caret

If Selection.End = Selection.Start Then
  Selection.MoveEnd wdCharacter, 1
  Selection.MoveStart wdCharacter, -1
Else
  Selection.End = Selection.Start + 2
End If
Set ch1 = Selection.Range
ch1.End = ch1.End - 1
Set ch2 = Selection.Range
ch2.Start = ch2.Start + 1

' If case change is needed, do it before transposing
char1 = ch1.Text
char2 = ch2.Text

myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

' If both chars are letters
If LCase(char2) <> UCase(char2) And LCase(char1) <> UCase(char1) Then
' If uppercase followed by lowercase
  If ((char1 = UCase(char1)) And (char2 = LCase(char2))) Then
    ch2.Text = UCase(ch2.Text)
    ch1.Text = LCase(ch1.Text)
    ch2.Start = ch2.Start + 1
  End If
End If
ActiveDocument.TrackRevisions = myTrack

' create new range at the end of the second character
Set ch2e = ch2.Duplicate
ch2e.Start = ch2.End

' Copy the formatted versions of the two characters
ch2e.FormattedText = ch2.FormattedText
ch2e.Start = ch2e.End
ch2e.FormattedText = ch1.FormattedText

' Delete the original words
ch1.Delete
ch2.Delete
Selection.MoveRight Unit:=wdCharacter, Count:=1
End Sub

ET
Sub SwapCharacters2()
' Version 01.01.12
' Switch the previous two chars

Selection.MoveStart wdCharacter, -2
Set ch1 = Selection.Range
ch1.End = ch1.End - 1
Set ch2 = Selection.Range
ch2.Start = ch2.Start + 1

' If case change is needed, do it before transposing
char1 = ch1.Text
char2 = ch2.Text

myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

' If both chars are letters
If LCase(char2) <> UCase(char2) And LCase(char1) <> UCase(char1) Then
' If uppercase followed by lowercase
  If ((char1 = UCase(char1)) And (char2 = LCase(char2))) Then
    ch2.Text = UCase(ch2.Text)
    ch1.Text = LCase(ch1.Text)
    ch2.Start = ch2.Start + 1
  End If
End If
ActiveDocument.TrackRevisions = myTrack

' create new range at the end of the second character
Set ch2e = ch2.Duplicate
ch2e.Start = ch2.End

' Copy the formatted versions of the two characters
ch2e.FormattedText = ch2.FormattedText
ch2e.Start = ch2e.End
ch2e.FormattedText = ch1.FormattedText

' Delete the original words
ch1.Delete
ch2.Delete
Selection.MoveRight Unit:=wdCharacter, Count:=2
End Sub


ET
Sub SwapWords()
' Version 01.01.12
' Swap adjacent words, including formatting

' Find the two words
Selection.Words(1).Select
Set w1 = Selection.Range
Selection.Start = Selection.End + 1
Selection.Words(1).Select
Set w2 = Selection.Range

' Pick up the space after the first word
Set w1sp = w1.Duplicate
w1sp.Start = w1sp.End - 1
' remove any following space(s)
w1.MoveEndWhile cset:=" ", Count:=wdBackward
w2.MoveEndWhile cset:=" ", Count:=wdBackward
' remove any following single quote
w2.MoveEndWhile cset:=ChrW(8217), Count:=wdBackward

' create a range at the end of the second word
Set w2e = w2.Duplicate
w2e.Start = w2e.End

' Copy the second word (so it is track changed)
w2e.FormattedText = w2.FormattedText
w2.Delete
w2e.Start = w2e.End
' Add a copy of the space
w2e.FormattedText = w1sp.FormattedText
w2e.Start = w2e.End
' Add the first word after that
w2e.FormattedText = w1.FormattedText

' Delete the original first word and the space
w1.Delete
w1sp.Delete
End Sub


ET
Sub SwapWordsOldVersion()
' Version 30.01.10
' Swap adjacent words

Selection.MoveRight Unit:=wdCharacter, Count:=1
Selection.MoveLeft Unit:=wdWord, Count:=1
startHere = Selection.Start
Selection.MoveRight Unit:=wdWord, Count:=1, Extend:=wdExtend
firstWord = Trim(Selection)
Selection.Start = Selection.End
Selection.MoveRight Unit:=wdWord, Count:=1, Extend:=wdExtend
secondWord = Trim(Selection)
Selection.Start = startHere
wholeThing = Selection
wholeThing = Replace(wholeThing, firstWord, "zcpzc")
wholeThing = Replace(wholeThing, secondWord, firstWord)
wholeThing = Replace(wholeThing, "zcpzc", secondWord)
Selection.Delete
Selection.InsertAfter wholeThing
Selection.End = Selection.Start
End Sub




ET
Sub SwapThreeWords()
' Version 31.12.11
' Swap three adjacent words (A and B to B and A)
' <Shift-Ctrl-Alt-Q>

' Find the two words
Selection.Words(1).Select
Set w1 = Selection.Range
Selection.MoveRight Unit:=wdWord, Count:=2
Selection.Words(1).Select
Set w3 = Selection.Range
w1.Select

' avoid any following space(s)
w1.MoveEndWhile cset:=" ", Count:=wdBackward
w3.MoveEndWhile cset:=" ", Count:=wdBackward
' avoid any following single quote
w3.MoveEndWhile cset:=ChrW(8217), Count:=wdBackward

' create ranges w1e and w2e as the ends of the two words
Set w1e = w1.Duplicate
w1e.Start = w1e.End
Set w3e = w3.Duplicate
w3e.Start = w3e.End

' Copy the formatted versions of the two words
w1e.FormattedText = w3.FormattedText
w3e.FormattedText = w1.FormattedText

' Delete the original words
w1.Delete
w3.Delete
End Sub




ET
Sub AbbrSwap()
' Version 15.07.13
' Swap abbreviation into or out of brackets

addPreEditCodes = False

' Find the start ...
Selection.MoveRight Unit:=wdCharacter, Count:=1
Selection.MoveLeft Unit:=wdWord, Count:=1
startHere = Selection.Start
' ... and the end of the words (close parenthesis)
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ")"
  .Forward = True
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute
End With
Selection.Start = startHere
allWords = Selection
' find the open parenthesis, to find the two bits of text to swap
parenPos = InStr(allWords, "(")
leftWords = Left(allWords, parenPos - 2)
rightWords = Mid(allWords, parenPos + 1, Len(allWords) - parenPos - 1)
' swap them round and type them back in
If addPreEditCodes = True And InStr(leftWords, ">") = 0 Then
  If Len(rightWords) > Len(leftWords) Then
    newWords = "<termDef>" & rightWords & "</termDef> (<abbrev>" & leftWords + "</abbrev>)"
  Else
    newWords = "<termDef>" & leftWords & "</termDef> (<abbrev>" & rightWords + "</abbrev>)"
  End If
Else
  newWords = rightWords + " (" + leftWords + ")"
End If
Selection.TypeText Text:=newWords
End Sub



ET
Sub FullPoint()
' Version 06.04.13
' Make adjacent words into sentence end
' <Ctrl-Alt-.>

newBit = ". "
myQuotes = Chr(34) & Chr(39) & ChrW(8220) & ChrW(8216)
Selection.Collapse wdCollapseEnd
While (LCase(Selection) <> UCase(Selection))
  Selection.MoveRight 1
Wend
myStart = Selection.Start
Do
  Selection.MoveRight 1
Loop Until LCase(Selection) <> UCase(Selection) Or Asc(Selection) = 1
myEnd = Selection.Start
If UCase(Selection) <> Selection Then
' It needs uppercasing
  Selection.Start = Selection.Start - 1
  preChar = Selection
  Selection.MoveStart 1
  Selection.MoveEnd 1
  newLetter = UCase(Selection)
  If InStr(myQuotes, preChar) > 0 Then
    Selection.Delete
    Selection.TypeText Text:=newLetter
    Selection.End = myEnd - 1
  Else
    newBit = newBit & newLetter
  End If
  Selection.Start = myStart
Else
  newBit = Trim(newBit)
  Selection.MoveLeft 1
End If
Selection.Start = myStart
If Right(Selection, 1) = " " And Len(Selection) > 1 Then
  Selection.End = Selection.End - 1
  newBit = Trim(newBit)
End If
If Selection.End <> myStart Then Selection.Delete
Selection.TypeText Text:=newBit
End Sub


ET
Sub Comma()
' Version 06.04.13
' Make adjacent words into comma separated
' <Ctrl-Alt-,>

newBit = ", "
myQuotes = Chr(34) & Chr(39) & ChrW(8220) & ChrW(8216)
Selection.Collapse wdCollapseEnd
While (LCase(Selection) <> UCase(Selection))
  Selection.MoveRight 1
Wend
myStart = Selection.Start
Do
  Selection.MoveRight 1
Loop Until LCase(Selection) <> UCase(Selection) Or Asc(Selection) = 1
myEnd = Selection.Start
Set rng = ActiveDocument.Content
rng.End = myEnd
rng.Start = myStart
wasMiddle = rng
lastChar = Right(rng, 1)
If LCase(Selection) <> Selection Then
' It needs lowercasing
  Selection.Start = Selection.Start - 1
  preChar = Selection
  Selection.MoveStart 1
  Selection.MoveEnd 1
  newLetter = LCase(Selection)
  If InStr(myQuotes, preChar) > 0 Then
    Selection.Delete
    Selection.TypeText Text:=newLetter
    Selection.End = myEnd - 1
  Else
    newBit = newBit & newLetter
  End If
  Selection.Start = myStart
Else
  If lastChar = " " And Len(rng) > 1 Then newBit = Trim(newBit)
  Selection.MoveLeft 1
End If
Selection.Start = myStart
Selection.Delete
Selection.TypeText Text:=newBit
Selection.MoveRight Count:=2
End Sub

ET
Sub Semicolon()
' Version 06.04.13
' Make adjacent words into semicolon separated
' <Ctrl-Alt-;>

newBit = "; "
myQuotes = Chr(34) & Chr(39) & ChrW(8220) & ChrW(8216)
Selection.Collapse wdCollapseEnd
While (LCase(Selection) <> UCase(Selection))
  Selection.MoveRight 1
Wend
myStart = Selection.Start
Do
  Selection.MoveRight 1
Loop Until LCase(Selection) <> UCase(Selection) Or Asc(Selection) = 1
myEnd = Selection.Start
Set rng = ActiveDocument.Content
rng.End = myEnd
rng.Start = myStart
wasMiddle = rng
lastChar = Right(rng, 1)
If LCase(Selection) <> Selection Then
' It needs lowercasing
  Selection.Start = Selection.Start - 1
  preChar = Selection
  Selection.MoveStart 1
  Selection.MoveEnd 1
  newLetter = LCase(Selection)
  If InStr(myQuotes, preChar) > 0 Then
    Selection.Delete
    Selection.TypeText Text:=newLetter
    Selection.End = myEnd - 1
  Else
    newBit = newBit & newLetter
  End If
  Selection.Start = myStart
Else
  If lastChar = " " And Len(rng) > 1 Then newBit = Trim(newBit)
  Selection.MoveLeft 1
End If
Selection.Start = myStart
Selection.Delete
Selection.TypeText Text:=newBit
Selection.MoveRight Count:=2
End Sub

ET
Sub Colon()
' Version 06.04.13
' Make adjacent words into colon separated
' <Shift-Ctrl-Alt-;>

upperCaseAfter = False

newBit = ": "
myQuotes = Chr(34) & Chr(39) & ChrW(8220) & ChrW(8216)
Selection.Collapse wdCollapseEnd
While (LCase(Selection) <> UCase(Selection))
  Selection.MoveRight 1
Wend
myStart = Selection.Start
Do
  Selection.MoveRight 1
Loop Until LCase(Selection) <> UCase(Selection) Or Asc(Selection) = 1
myEnd = Selection.Start
Set rng = ActiveDocument.Content
rng.End = myEnd
rng.Start = myStart
wasMiddle = rng
lastChar = Right(rng, 1)

If upperCaseAfter = False Then
  If LCase(Selection) <> Selection Then
  ' It needs lowercasing
    Selection.Start = Selection.Start - 1
    preChar = Selection
    Selection.MoveStart 1
    Selection.MoveEnd 1
    newLetter = LCase(Selection)
    If InStr(myQuotes, preChar) > 0 Then
      Selection.Delete
      Selection.TypeText Text:=newLetter
      Selection.End = myEnd - 1
    Else
      newBit = newBit & newLetter
    End If
    Selection.Start = myStart
  Else
    If lastChar = " " And Len(rng) > 1 Then newBit = Trim(newBit)
    Selection.MoveLeft 1
  End If
Else
  If UCase(Selection) <> Selection Then
  ' It needs uppercasing
    Selection.Start = Selection.Start - 1
    preChar = Selection
    Selection.MoveStart 1
    Selection.MoveEnd 1
    newLetter = UCase(Selection)
    If InStr(myQuotes, preChar) > 0 Then
      Selection.Delete
      Selection.TypeText Text:=newLetter
      Selection.End = myEnd - 1
    Else
      newBit = newBit & newLetter
    End If
    Selection.Start = myStart
  Else
    If lastChar = " " And Len(rng) > 1 Then newBit = Trim(newBit)
    Selection.MoveLeft 1
  End If
End If
Selection.Start = myStart
Selection.Delete
Selection.TypeText Text:=newBit
Selection.MoveRight Count:=2
End Sub

ET
Sub Dash()
' Version 06.04.13
' Remove punctuation, add dash and lower case next char

EmDash = False

If EmDash = True Then
  myTrack = ActiveDocument.TrackRevisions
  ActiveDocument.TrackRevisions = False
End If
newBit = " " & ChrW(8211) & " "
myQuotes = Chr(34) & Chr(39) & ChrW(8220) & ChrW(8216)
Selection.Collapse wdCollapseEnd
While (LCase(Selection) <> UCase(Selection))
  Selection.MoveRight 1
Wend
myStart = Selection.Start
Do
  Selection.MoveRight 1
Loop Until LCase(Selection) <> UCase(Selection) Or Asc(Selection) = 1
myEnd = Selection.Start
Set rng = ActiveDocument.Content
rng.End = myEnd
rng.Start = myStart
wasMiddle = rng
lastChar = Right(rng, 1)
If LCase(Selection) <> Selection Then
' It needs lowercasing
  Selection.Start = Selection.Start - 1
  preChar = Selection
  Selection.MoveStart 1
  Selection.MoveEnd 1
  newLetter = LCase(Selection)
  If InStr(myQuotes, preChar) > 0 Then
    Selection.Delete
    Selection.TypeText Text:=newLetter
    Selection.End = myEnd - 1
  Else
    newBit = newBit & newLetter
  End If
  Selection.Start = myStart
Else
  If lastChar = " " And Len(rng) > 1 Then newBit = Left(newBit, 2)
  Selection.MoveLeft 1
End If
Selection.Start = myStart
Selection.Delete
If EmDash = True Then
  Selection.TypeText Text:=newBit
  Selection.Start = myStart
  Selection.End = myStart + 3
  Selection.Delete
  ActiveDocument.TrackRevisions = myTrack
  Selection.TypeText Text:=ChrW(8212)
Else
  Selection.TypeText Text:=newBit
End If
Selection.MoveRight Count:=2
End Sub


ET
Sub QuestionMark()
' Version 06.04.13
' Make adjacent words into sentence end
' <Ctrl-Alt-.>

newBit = "? "
myQuotes = Chr(34) & Chr(39) & ChrW(8220) & ChrW(8216)
Selection.Collapse wdCollapseEnd
While (LCase(Selection) <> UCase(Selection))
  Selection.MoveRight 1
Wend
myStart = Selection.Start
Do
  Selection.MoveRight 1
Loop Until LCase(Selection) <> UCase(Selection) Or Asc(Selection) = 1
myEnd = Selection.Start
If UCase(Selection) <> Selection Then
' It needs uppercasing
  Selection.Start = Selection.Start - 1
  preChar = Selection
  Selection.MoveStart 1
  Selection.MoveEnd 1
  newLetter = UCase(Selection)
  If InStr(myQuotes, preChar) > 0 Then
    Selection.Delete
    Selection.TypeText Text:=newLetter
    Selection.End = myEnd - 1
  Else
    newBit = newBit & newLetter
  End If
  Selection.Start = myStart
Else
  newBit = Trim(newBit)
  Selection.MoveLeft 1
End If
Selection.Start = myStart
If Right(Selection, 1) = " " And Len(Selection) > 1 Then
  Selection.End = Selection.End - 1
  newBit = Trim(newBit)
End If
If Selection.End <> myStart Then Selection.Delete
Selection.TypeText Text:=newBit
End Sub


ET
Sub ExclamationMark()
' Version 06.04.13
' Make adjacent words into sentence end
' <Ctrl-Alt-.>

newBit = "! "
myQuotes = Chr(34) & Chr(39) & ChrW(8220) & ChrW(8216)
Selection.Collapse wdCollapseEnd
While (LCase(Selection) <> UCase(Selection))
  Selection.MoveRight 1
Wend
myStart = Selection.Start
Do
  Selection.MoveRight 1
Loop Until LCase(Selection) <> UCase(Selection) Or Asc(Selection) = 1
myEnd = Selection.Start
If UCase(Selection) <> Selection Then
' It needs uppercasing
  Selection.Start = Selection.Start - 1
  preChar = Selection
  Selection.MoveStart 1
  Selection.MoveEnd 1
  newLetter = UCase(Selection)
  If InStr(myQuotes, preChar) > 0 Then
    Selection.Delete
    Selection.TypeText Text:=newLetter
    Selection.End = myEnd - 1
  Else
    newBit = newBit & newLetter
  End If
  Selection.Start = myStart
Else
  newBit = Trim(newBit)
  Selection.MoveLeft 1
End If
Selection.Start = myStart
If Right(Selection, 1) = " " And Len(Selection) > 1 Then
  Selection.End = Selection.End - 1
  newBit = Trim(newBit)
End If
If Selection.End <> myStart Then Selection.Delete
Selection.TypeText Text:=newBit
End Sub


ET
Sub FullPointInDialogue()
' Version 04.10.14
' Make adjacent words into sentence end


makeCaseChange = (Selection.Start = Selection.End)

Selection.Words(1).Select
Selection.MoveEnd wdCharacter, 1

wasStart = Selection.Start
wasEnd = Selection.End

newBit = ". "
oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text
Selection.End = Selection.Start
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Forward = True
  .Text = "?[.:,;\!\?" & ChrW(8211) & ChrW(8222) & ChrW(8212) & "]??"
  .Forward = True
  .Replacement.Text = ""
  .MatchWildcards = True
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute
End With

If Selection.Start > wasEnd Then
  Selection.Start = wasStart
  Selection.End = wasEnd
  spacePos = InStr(Selection, " ")
  Selection.Start = wasStart + spacePos - 1
  Selection.InsertBefore Trim(newBit)
  Selection.Start = wasStart
  Selection.Collapse wdCollapseStart
  Exit Sub
End If

preChar = Left(Selection, 1)
midChar = Mid(Selection, 3, 1)
postChar = Right(Selection, 1)

If midChar <> " " Then
  Selection.MoveEnd wdCharacter, -1
  postChar = Right(Selection, 1)
End If

If preChar <> " " Then Selection.MoveStart wdCharacter, 1

' If the middle char is a close quote go past it
If InStr(ChrW(8217) & ChrW(8221) & """'", midChar) > 0 Then
  Selection.MoveEnd wdCharacter, 2
  newBit = Replace(newBit, " ", postChar & " ")
  postChar = Right(Selection, 1)
End If

' If the next char is an open quote go past it
If InStr(ChrW(8216) & ChrW(8220) & """'", postChar) > 0 Then
  Selection.MoveEnd wdCharacter, 1
  newBit = newBit & postChar
  postChar = Right(Selection, 1)
End If

' If the case of the next letter needs changing
If UCase(postChar) <> postChar And makeCaseChange Then
  newBit = newBit & UCase(postChar)
Else
  Selection.MoveEnd wdCharacter, -1
End If

finish:
Selection.Delete
Selection.TypeText Text:=newBit
' Selection.MoveRight Unit:=wdCharacter, Count:=1


With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchWildcards = False
End With
End Sub

ET
Sub CommaInDialogue()
' Version 04.10.14
' Give adjacent words a comma link


makeCaseChange = (Selection.Start = Selection.End)

Selection.Words(1).Select
Selection.MoveEnd wdCharacter, 1

wasStart = Selection.Start
wasEnd = Selection.End

newBit = ", "
oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text
Selection.End = Selection.Start
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Forward = True
  .Text = "?[.:,;\!\?" & ChrW(8211) & ChrW(8222) & ChrW(8212) & "]??"
  .Forward = True
  .Replacement.Text = ""
  .MatchWildcards = True
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute
End With

If Selection.Start > wasEnd Then
  Selection.Start = wasStart
  Selection.End = wasEnd
  spacePos = InStr(Selection, " ")
  Selection.Start = wasStart + spacePos - 1
  Selection.InsertBefore Trim(newBit)
  Selection.Start = wasStart
  Selection.Collapse wdCollapseStart
  Exit Sub
End If

preChar = Left(Selection, 1)
midChar = Mid(Selection, 3, 1)
postChar = Right(Selection, 1)

If midChar <> " " Then
  Selection.MoveEnd wdCharacter, -1
  postChar = Right(Selection, 1)
End If

If preChar <> " " Then Selection.MoveStart wdCharacter, 1

' If the middle char is a close quote go past it
If InStr(ChrW(8217) & ChrW(8221) & """'", midChar) > 0 Then
  Selection.MoveEnd wdCharacter, 2
  newBit = Replace(newBit, " ", postChar & " ")
  postChar = Right(Selection, 1)
End If

' If the next char is an open quote go past it
If InStr(ChrW(8216) & ChrW(8220) & """'", postChar) > 0 Then
  Selection.MoveEnd wdCharacter, 1
  newBit = newBit & postChar
  postChar = Right(Selection, 1)
End If

' If the case of the next letter needs changing
If LCase(postChar) <> postChar And makeCaseChange Then
  newBit = newBit & LCase(postChar)
Else
  Selection.MoveEnd wdCharacter, -1
End If

finish:
Selection.Delete
Selection.TypeText Text:=newBit
' Selection.MoveRight Unit:=wdCharacter, Count:=1


With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchWildcards = False
End With
End Sub

ET
Sub NumberToText()
' Version 26.12.12
' Convert next number into text

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

' Find a number (six figures max)
Selection.End = Selection.Start
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[0-9]{1,6}"
  .Forward = True
  .MatchWildcards = True
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .MatchAllWordForms = False
  .Execute
End With

' Create a field containing the digits and a special format code
Selection.Fields.Add Range:=Selection.Range, _
   Type:=wdFieldEmpty, Text:="= " + Selection + " \* CardText", _
   PreserveFormatting:=True

' Select the field and copy it
Selection.MoveStart Unit:=wdCharacter, Count:=-1
Selection.Copy

' Paste the text as unformatted, replacing the field
Selection.PasteSpecial Link:=False, DataType:=wdPasteText, _
   Placement:=wdInLine, DisplayAsIcon:=False

With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchWildcards = False
End With
End Sub





ET
Sub NumberToText2()
' Version 30.05.10
' Convert next number into text

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

' Find a number (six figures max)
Selection.End = Selection.Start
With Selection.Find
  .ClearFormatting
  .Text = "[0-9]{1,6}"
  .MatchWildcards = True
  .Forward = True
  .Execute
End With

Selection.Collapse Direction:=wdCollapseStart

With Selection.Find
  .ClearFormatting
  .Text = "[0-9 ,^0160]{1,9}"
  .MatchWildcards = True
  .Execute
End With

myDigits = Replace(Selection, Chr(160), "")
myDigitsNow = Replace(myDigits, " ", "")
myDigitsFinal = Replace(myDigitsNow, ",", "")

' Create a field containing the digits and a special format code
Selection.Fields.Add Range:=Selection.Range, _
   Type:=wdFieldEmpty, Text:="= " + myDigitsFinal + " \* CardText", _
   PreserveFormatting:=True

' Select the field and copy it
Selection.MoveStart Unit:=wdCharacter, Count:=-1
Selection.Copy

' Paste the text unformatted, replacing the field
Selection.PasteSpecial Link:=False, DataType:=wdPasteText, _
   Placement:=wdInLine, DisplayAsIcon:=False

If Right(myDigitsNow, 1) = "," Then Selection.TypeText Text:=","
If Right(myDigits, 1) = " " Then Selection.TypeText Text:=" "

' Restore Find to original
With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchWildcards = False
End With
End Sub

ET
Sub NumberToTextUK()
' Version 14.09.11
' Convert next number into text

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

' Find a number (six figures max)
Selection.End = Selection.Start
With Selection.Find
  .ClearFormatting
  .Text = "[0-9]{1,6}"
  .Forward = True
  .MatchWildcards = True
  .Execute
End With

startHere = Selection.Start

' Create a field containing the digits and a special format code
Selection.Fields.Add Range:=Selection.Range, _
   Type:=wdFieldEmpty, Text:="= " + Selection + " \* CardText", _
   PreserveFormatting:=True

' Select the field and copy it
Selection.MoveStart Unit:=wdCharacter, Count:=-1
Selection.Copy

' Paste the text as unformatted, replacing the field
Selection.PasteSpecial Link:=False, DataType:=wdPasteText, _
   Placement:=wdInLine, DisplayAsIcon:=False

Selection.Start = startHere
numWords = Selection
If Right(numWords, 4) <> "dred" Then numWords = _
     Replace(numWords, "hundred", "hundred and")
If InStr(numWords, "hundred") > 0 Then
  numWords = Replace(numWords, "thousand", "thousand,")
Else
  If Right(numWords, 4) <> "sand" Then numWords = _
       Replace(numWords, "thousand", "thousand and")
End If
Selection.TypeText Text:=numWords

With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchWildcards = False
End With
End Sub


ET
Sub NumberToTextUK2()
' Version 14.09.11
' Convert next number into text
oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

' Find a number (six figures max)
Selection.End = Selection.Start
With Selection.Find
  .ClearFormatting
  .Text = "[0-9]{1,6}"
  .Forward = True
  .MatchWildcards = True
  .Execute
End With

Selection.Collapse Direction:=wdCollapseStart
startHere = Selection.Start

Selection.End = Selection.Start
With Selection.Find
  .ClearFormatting
  .Text = "[0-9 ,^0160]{1,9}"
  .MatchWildcards = True
  .Execute
End With

myDigits = Replace(Selection, Chr(160), "")
myDigitsNow = Replace(myDigits, " ", "")
myDigitsFinal = Replace(myDigitsNow, ",", "")

' Create a field containing the digits and a special format code
Selection.Fields.Add Range:=Selection.Range, _
   Type:=wdFieldEmpty, Text:="= " + myDigitsFinal + " \* CardText", _
   PreserveFormatting:=True

' Select the field and copy it
Selection.MoveStart Unit:=wdCharacter, Count:=-1
Selection.Copy

' Paste the text unformatted, replacing the field
Selection.PasteSpecial Link:=False, DataType:=wdPasteText, _
   Placement:=wdInLine, DisplayAsIcon:=False

Selection.Start = startHere
numWords = Selection
If Right(numWords, 4) <> "dred" Then numWords = _
     Replace(numWords, "hundred", "hundred and")
If InStr(numWords, "hundred") > 0 Then
  numWords = Replace(numWords, "thousand", "thousand,")
Else
  If Right(numWords, 4) <> "sand" Then numWords = _
       Replace(numWords, "thousand", "thousand and")
End If
Selection.TypeText Text:=numWords

If Right(myDigitsNow, 1) = "," Then Selection.TypeText Text:=","
If Right(myDigits, 1) = " " Then Selection.TypeText Text:=" "

' Restore Find to original
With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchWildcards = False
End With
End Sub



ET
Sub NumberIncrement()
' Version 08.03.13
' Add one to the following number

trackIt = True
jump = 1

Selection.End = Selection.Start
searchChars = "0123456789"
myTrack = ActiveDocument.TrackRevisions
If trackIt = False Then ActiveDocument.TrackRevisions = False

Selection.End = Selection.Start
While InStr(searchChars, Selection) = 0
  Selection.MoveRight 1
Wend

myStart = Selection.Start
While InStr(searchChars, Selection) > 0
  Selection.MoveRight 1
Wend

Selection.Start = myStart
Selection.TypeText Text:=Trim(Str(Val(Selection) + jump))
Selection.Start = myStart
Selection.End = Selection.Start
ActiveDocument.TrackRevisions = myTrack
End Sub


ET
Sub NumberDecrement()
' Version 08.03.13
' Subtract one to the following number

trackIt = True
jump = -1

Selection.End = Selection.Start
searchChars = "0123456789"
myTrack = ActiveDocument.TrackRevisions
If trackIt = False Then ActiveDocument.TrackRevisions = False

Selection.End = Selection.Start
While InStr(searchChars, Selection) = 0
  Selection.MoveRight 1
Wend

myStart = Selection.Start
While InStr(searchChars, Selection) > 0
  Selection.MoveRight 1
Wend

Selection.Start = myStart
Selection.TypeText Text:=Trim(Str(Val(Selection) + jump))
Selection.Start = myStart
Selection.End = Selection.Start
ActiveDocument.TrackRevisions = myTrack
End Sub


ET
Sub LetterIncrement()
' Version 07.03.13
' Pick up the current character and replace with the numerically next character
' (also useful for unicode Greek characters)

Selection.End = Selection.Start + 1
Selection.TypeText Text:=ChrW(AscW(Selection) + 1)
Selection.MoveLeft , 1
End Sub

ET
Sub LetterDecrement()
' Version 07.03.13
' Pick up the current character and replace with the numerically previous character
' (useful for unicode Greek characters)

Selection.End = Selection.Start + 1
Selection.TypeText Text:=ChrW(AscW(Selection) - 1)
Selection.MoveLeft , 1
End Sub


ET
Sub Ampersand()
' Version 25.02.13
' Change ampersand to 'and'

Do
  Selection.MoveRight Unit:=wdCharacter, Count:=1
Loop Until Selection = "&"
Selection.MoveEnd wdCharacter, 1
Selection.TypeText Text:="and"
End Sub


ET
Sub PerCent()
' Version 25.02.13
' Change percent symbol to words

Do
  Selection.MoveRight Unit:=wdCharacter, Count:=1
Loop Until Selection = "%"
Selection.MoveEnd wdCharacter, 1

If Selection.LanguageID = wdEnglishUK Then
  Selection.TypeText Text:=" per cent"
Else
  Selection.TypeText Text:=" percent"
End If
End Sub


ET
Sub AcuteAdd()
' Version 01.12.10
' Add an acute accent to an e
' Alt-E

Do
  Selection.MoveRight Unit:=wdCharacter, Count:=1
Loop Until Selection = "e"
Selection.MoveEnd wdCharacter, 1
Selection.TypeText Text:="�"
End Sub


ET
Sub ing()
' Version 23.05.13
' "splodge" -> "splodging" or vice versa

Selection.MoveStart , 1
Selection.Words(1).Select
Selection.MoveEndWhile cset:=ChrW(8217) & ChrW(39) & " ", Count:=wdBackward
If Right(Selection, 3) = "ing" Then
  Selection.Start = Selection.End - 3
  Selection.Delete
Else
  Selection.Collapse wdCollapseEnd
  Selection.MoveStart , -1
  If Selection = "e" Then Selection.Delete: GoTo ing
  Selection.MoveStart , -2
  If Selection = "ies" Then Selection.TypeText Text:="y": GoTo ing
  If Selection = "ion" Then GoTo ing
  Selection.MoveStart , 1
  If Selection = "es" Then Selection.Delete: GoTo ing
  Selection.MoveStart , 1
  If Selection = "s" Then Selection.Delete
  Selection.Collapse wdCollapseEnd
ing:
  Selection.TypeText Text:="ing"
End If

End Sub



ET
Sub SingleQuote()
' Version 07.03.13
' Change next quote mark to single curly

trackIt = False
makeItGrey = True
makeNotSubSuper = True

myChar = ChrW(8201)
myTrack = ActiveDocument.TrackRevisions
If trackIt = False Then ActiveDocument.TrackRevisions = False

searchChars = Chr(34) & Chr(39) & ChrW(8216) & ChrW(8217) _
       & ChrW(8220) & ChrW(8221) & ChrW(8249) & ChrW(8250) _
       & ChrW(8222) & ChrW(8218) & ChrW(171) & ChrW(187) & ChrW(96)
Selection.End = Selection.Start
Do
  Selection.MoveRight , 1
Loop Until InStr(searchChars, Selection) > 0
Selection.MoveEnd wdCharacter, 1

Set rng = ActiveDocument.Content
rng.Start = Selection.Start - 1
rng.End = Selection.Start
charBefore = rng.Text
Selection.TypeBackspace
If charBefore > "" Then C = AscW(charBefore) Else C = 0
If C = 13 Or C = 32 Or C = 40 Or C = 47 Or C = 8220 Or C = 91 Then
  Selection.TypeText Text:=ChrW(8216)
Else
  Selection.TypeText Text:=ChrW(8217)
End If
ActiveDocument.TrackRevisions = myTrack
End Sub

ET
Sub DoubleQuote()
' Version 07.03.13
' Change next quote mark to double

trackIt = False
makeItGrey = True
makeNotSubSuper = True

myChar = ChrW(8201)
myTrack = ActiveDocument.TrackRevisions
If trackIt = False Then ActiveDocument.TrackRevisions = False

searchChars = Chr(34) & Chr(39) & ChrW(8216) & ChrW(8217) _
       & ChrW(8220) & ChrW(8221) & ChrW(8249) & ChrW(8250) _
       & ChrW(8222) & ChrW(8218) & ChrW(171) & ChrW(187) & ChrW(96)
Selection.End = Selection.Start
Do
  Selection.MoveRight , 1
Loop Until InStr(searchChars, Selection) > 0
Selection.MoveEnd wdCharacter, 1

Set rng = ActiveDocument.Content
rng.Start = Selection.Start - 1
rng.End = Selection.Start
charBefore = rng.Text
Selection.TypeBackspace
If charBefore > "" Then C = AscW(charBefore) Else C = 0
If C = 13 Or C = 32 Or C = 40 Or C = 47 Or C = 8220 Or C = 91 Then
  Selection.TypeText Text:=ChrW(8220)
Else
  Selection.TypeText Text:=ChrW(8221)
End If
ActiveDocument.TrackRevisions = myTrack
End Sub


ET
Sub AddQuotes()
' Version 05.07.11
' Adds quotes round a phrase

myOpen = ChrW(8220)
myClose = ChrW(8221)

' Select first word
myEnd = Selection.End
Selection.End = Selection.Start
Selection.Words(1).Select

Selection.InsertBefore myOpen

' Select final word
Selection.Start = myEnd
Selection.Words(1).Select

' Don't include the space
Selection.Start = Selection.End - 1
If Asc(Selection) = 32 Then
  Selection.MoveEnd Count:=-1
Else
  Selection.MoveStart Count:=1
End If

Selection.TypeText Text:=myClose
End Sub


ET
Sub CommaAdd()
' Version 29.05.14
' Add a comma

spanishPunct = True

myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

Selection.Expand wdWord
Selection.MoveEndWhile cset:=ChrW(8217) & "' ", Count:=wdBackward
Selection.Collapse wdCollapseEnd
Selection.MoveStart , -1
italBefore = (Selection.Font.Italic = True)
boldBefore = (Selection.Font.Bold = True)
Selection.Collapse wdCollapseEnd

Selection.MoveEnd , 1
If InStr(";.:!", Selection) > 0 Then
  ActiveDocument.TrackRevisions = myTrack
  Selection.TypeBackspace
  ActiveDocument.TrackRevisions = False
  Selection.MoveEnd , 1
End If

If Selection = ")" Then
  Selection.MoveStart , 1
End If

' Check state of following character
italAfter = (Selection.Font.Italic = True)
boldAfter = (Selection.Font.Bold = True)

Selection.Collapse wdCollapseStart
ActiveDocument.TrackRevisions = myTrack
Selection.TypeText Text:=","
ActiveDocument.TrackRevisions = False

' Unitalicise/unbold the comma and space if necessary
If boldAfter = False And boldBefore = True Then
  If spanishPunct = False Then Selection.MoveStart , -1
  Selection.MoveEnd , 1
  Selection.Font.Bold = False
  Selection.Collapse wdCollapseEnd
  Selection.MoveLeft , 1
End If

If italAfter = False And italBefore = True Then
  If spanishPunct = False Then Selection.MoveStart , -1
  Selection.MoveEnd , 1
  Selection.Font.Italic = False
End If
If Selection.Start = Selection.End Then Selection.MoveStart , -1
If Selection.Font.Superscript = True Then Selection.Font.Superscript = False
If Selection.Font.Subscript = True Then Selection.Font.Subscript = False

Selection.MoveRight , 2
ActiveDocument.TrackRevisions = myTrack
End Sub

ET
Sub CommaAddUSUK()
' Version 28.11.10
' Add a comma

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

Selection.End = Selection.Start
With Selection.Find
  .ClearFormatting
  .Text = " "
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = False
  .Execute
End With

Selection.End = Selection.Start
If Selection.LanguageID = wdEnglishUS Then
  Selection.MoveEnd , -1
  If Selection <> ChrW(8217) And Selection <> ChrW(8221) Then
    Selection.MoveStart , 1
  End If
End If
Selection.TypeText Text:=","
Selection.MoveRight Unit:=wdCharacter, Count:=1

With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchWildcards = False
End With
End Sub


ET
Sub Hyphenate()
' Version 28.01.10
' Hyphenate two words

Selection.MoveRight Unit:=wdWord, Count:=1
Selection.TypeBackspace
Selection.TypeText Text:="-"
End Sub




ET
Sub PunctuationToSpace()
' Version 06.04.13
' Change the next punctuation item to a space

trackIt = True

myChar = " "
myTrack = ActiveDocument.TrackRevisions
If trackIt = False Then ActiveDocument.TrackRevisions = False

searchChars = "-" & ChrW(8201) & ChrW(160) & ChrW(8211) & ChrW(8212) _
     & ChrW(8722) & Chr(30)
Selection.End = Selection.Start
While InStr(searchChars, Selection) = 0
  Selection.MoveRight , 1
Wend
Selection.MoveEnd wdCharacter, 1
Selection.Delete
Selection.TypeText Text:=myChar
ActiveDocument.TrackRevisions = myTrack

End Sub

ET
Sub PunctuationToThinSpace()
' Version 06.04.13
' Change the next punctuation item to a thin space

trackIt = False
makeItGrey = True
makeNotSubSuper = True

myChar = ChrW(8201)
myTrack = ActiveDocument.TrackRevisions
If trackIt = False Then ActiveDocument.TrackRevisions = False

searchChars = " " & ChrW(160) & ChrW(8211) & ChrW(8212) & ChrW(8722) & Chr(30)
Selection.End = Selection.Start
While InStr(searchChars, Selection) = 0
  Selection.MoveRight , 1
Wend
Selection.MoveEnd wdCharacter, 1
Selection.Delete
Selection.TypeText Text:=myChar

Selection.MoveStart wdCharacter, -1
If makeItGrey = True Then Selection.Range.HighlightColorIndex = wdGray25
If makeNotSubSuper = True Then
  If Selection.Font.Subscript = True Then Selection.Font.Subscript = False
  If Selection.Font.Superscript = True Then Selection.Font.Superscript = False
End If
Selection.Collapse wdCollapseEnd
ActiveDocument.TrackRevisions = myTrack

End Sub

ET
Sub TypeThinSpace()
' Version 28.01.13
' Type and shade a thin space

trackIt = False
makeItGrey = False
makeNotSubSuper = True

myTrack = ActiveDocument.TrackRevisions
If trackIt = False Then ActiveDocument.TrackRevisions = False
Selection.TypeText Text:=ChrW(8201)
Selection.MoveStart wdCharacter, -1
If makeItGrey = True Then Selection.Range.HighlightColorIndex = wdGray25
If makeNotSubSuper = True Then
  If Selection.Font.Subscript = True Then Selection.Font.Subscript = False
  If Selection.Font.Superscript = True Then Selection.Font.Superscript = False
End If
Selection.Collapse wdCollapseEnd
ActiveDocument.TrackRevisions = myTrack
End Sub


ET
Sub PunctuationToHyphen()
' Version 14.05.13
' Change the word break punctuation to a hyphen

trackIt = True

myChar = "-"
myTrack = ActiveDocument.TrackRevisions
If trackIt = False Then ActiveDocument.TrackRevisions = False

searchChars = " " & ChrW(160) & ChrW(8211) & ChrW(8212) & ChrW(8722) & Chr(30)
'Selection.End = Selection.Start + 1

While InStr(searchChars, Selection) = 0
  Selection.MoveRight , 1
Wend
Selection.MoveRight wdCharacter, 1
Selection.TypeBackspace
Selection.TypeText Text:=myChar
ActiveDocument.TrackRevisions = myTrack
End Sub

ET
Sub PunctuationToDash()
' Version 06.04.13
' Change next hyphen/em/en dash to an em/en dash

myDash = ChrW(8211): ' en dash
' myDash = ChrW(8212): ' em dash

trackIt = False

myTrack = ActiveDocument.TrackRevisions
If trackIt = False Then ActiveDocument.TrackRevisions = False

searchChars = "- " & ChrW(8211) & ChrW(8212) & ChrW(8722) & Chr(30)
Selection.End = Selection.Start
While InStr(searchChars, Selection) = 0
  Selection.MoveRight , 1
Wend
Selection.MoveEnd wdCharacter, 1
Selection.Delete
Selection.TypeText Text:=myDash
ActiveDocument.TrackRevisions = myTrack

End Sub


ET
Sub PunctuationToMinus()
' Version 26.03.14
' Find punctuation and change to minus sign

trackIt = False

myChar = ChrW(8722)
myTrack = ActiveDocument.TrackRevisions
If trackIt = False Then ActiveDocument.TrackRevisions = False

searchChars = "-" & ChrW(8211) & ChrW(8212) & Chr(30)
Selection.End = Selection.Start
While InStr(searchChars, Selection) = 0
  Selection.MoveRight , 1
Wend
Selection.MoveEnd wdCharacter, 1
isSuper = (Selection.Font.Superscript)
Selection.Delete
Selection.TypeText Text:=myChar
If isSuper = True Then
  Selection.MoveStart , -1
  Selection.Font.Superscript = True
End If
ActiveDocument.TrackRevisions = myTrack
End Sub






ET
Sub PunctuationOff()
' Version 30.01.11
' Delete the next punctuation item
' Alt-Backspace
myChars = ""
myChars = myChars + ";:.,\!\?"
' Curly quotes
myChars = myChars + ChrW(8216) & ChrW(8217) & ChrW(8220) & ChrW(8221)
' Straight quotes
myChars = myChars + "'" & Chr(34)
' Brackets
myChars = myChars + "\(\)\[\]\{\}"

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

Selection.End = Selection.Start
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[" & myChars & "]"
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = True
  .Execute Replace:=wdReplaceOne
End With

With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchWildcards = False
End With
End Sub



ET
Sub PunctuationSwitcher()
' Version 04.10.12
' Change order of next two punctuation marks

myChars = ""
myChars = myChars + ";:.,\!\?"
' Curly quotes
myChars = myChars + ChrW(8216) & ChrW(8217) & ChrW(8220) & ChrW(8221)
' Straight quotes
myChars = myChars + "'" & Chr(34)
' Brackets
myChars = myChars + "\(\)\[\]\{\}"

oldFind = Selection.Find.Text

Selection.End = Selection.Start
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[" & myChars & "]{2}"
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = True
  .Execute
End With

myPair = Selection
pair = Right(myPair, 1) & Left(myPair, 1)
Selection.TypeText Text:=pair

With Selection.Find
  .Text = oldFind
  .MatchWildcards = False
End With
End Sub



ET
Sub zTOs()
' Version 24.07.10
' Change the next z to and s
' Alt-Z

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

Selection.End = Selection.Start
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "z"
  .Forward = True
  .MatchWildcards = False
  .Execute
End With

Selection.TypeText Text:="s"

With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
End With
End Sub



ET
Sub JoinTwoWords()
' Version 08.03.13
' Join two words

trackIt = True

searchChars = "- " & ChrW(8211) & ChrW(8212) & ChrW(8201) & Chr(30)
myTrack = ActiveDocument.TrackRevisions
If trackIt = False Then ActiveDocument.TrackRevisions = False
Selection.End = Selection.Start
Do
  Selection.MoveRight , 1
Loop Until InStr(searchChars, Selection) > 0
Selection.MoveEnd wdCharacter, 1
Selection.Delete
ActiveDocument.TrackRevisions = myTrack
End Sub


ET
Sub ThatWhich()
' Version 26.07.10
' Change that to which and vice versa
Selection.MoveLeft Unit:=wdWord, Count:=1
Selection.MoveRight Unit:=wdWord, Count:=1, Extend:=wdExtend
thisWord = Selection
If thisWord = "which " Then
  Selection.TypeText Text:="that "
  thisWord = ""
End If
If thisWord = "which" Then
  Selection.TypeText Text:="that"
  thisWord = ""
End If
If thisWord = "that " Then
  Selection.Start = Selection.Start - 1
  Selection.Delete
  Selection.TypeText Text:=", which "
  thisWord = ""
End If
If thisWord <> "" Then
  Selection.Start = Selection.End
  Selection.TypeText Text:="that "
End If
End Sub





ET
Sub MultiSwitch()
' Version 05.02.14
' Scripted word/phrase switching

maxWords = 4
abbrLength = 3
listName = "zzSwitchList"
myDir = "C:\Documents and Settings\Paul\My Documents\"

defaultList = myDir & listName

doaBeep = False
addaThat = False
specChar = "@"
doAnErrorBeep = False

On Error Resume Next
defaultLoaded = False
Set thisDoc = ActiveDocument
cursorPosn = Selection.Start
dirName = ActiveDocument.Path
' Go and look for the list file
gottaList = False
For i = 1 To Application.Windows.Count
  If InStr(Application.Windows(i).Document.Name, listName) > 0 Then
    Set listDoc = Application.Windows(i).Document
    gottaList = True
  End If
Next
If gottaList = False Then
  Documents.Open dirName & listName
  If Err.Number = 5174 Then
    Err.Clear
    Documents.Open defaultList
    If Err.Number = 5174 Then
      Err.Clear
      Documents.Open defaultList & ".docx"
      If Err.Number = 5174 Then
        Err.Clear
        defaultList = Replace(defaultList, ".", " [Compatibility Mode].")
        Application.Windows(defaultList).Activate
      End If
    End If
    If Err.Number = 5174 Then GoTo ReportIt
    If doAnErrorBeep = True Then Beep
    defaultLoaded = True
  Else
    If Err.Number > 0 Then GoTo ReportIt
  End If
Else
  listDoc.Activate
End If

carryOn:
' Create one long string of all the words
myWds = ""
For Each myPara In ActiveDocument.Paragraphs
  theLine = myPara
  If InStr(theLine, specChar) > 0 Then
    myWds = myWds & ";" & Replace(theLine, Chr(13), "")
' Special codes that you might want to use
    myWds = Replace(myWds, "^=", ChrW(8211))
    myWds = Replace(myWds, "^+", ChrW(8212))
    myWds = Replace(myWds, "^32", " ")
    myWds = Replace(myWds, "^t", Chr(9))
    myWds = Replace(myWds, "^pipe", ChrW(124))
    myWds = Replace(myWds, "^p", Chr(13))
    myWds = Replace(myWds, "^s", ChrW(160))
  End If
Next myPara
myWds = myWds & ";"

thisDoc.Activate
wNum = Application.Windows.Count
If wNum = 1 Then Exit Sub
wNum = Application.Windows.Count
pNum = ActiveDocument.ActiveWindow.Panes.Count
If Selection.Start <> cursorPosn And wNum > 1 Then _
     ActiveDocument.Windows(1).Activate
If Selection.Start <> cursorPosn And pNum > 1 Then _
     ActiveWindow.Panes(1).Activate

If defaultLoaded = True Then StatusBar = _
     ">>>>>>>>  Loaded from your default word list  <<<<<<<<"
    
' Use 'per cent' or 'percent' for UK/US spelling
If Selection.LanguageID = wdEnglishUS Then
  myWds = Replace(myWds, "%" & specChar & " per cent,", "%" & specChar & " percent,")
End If

' Move to the start of the word
Selection.Expand wdWord
aChar = Left(Selection, 1)
If UCase(aChar) = LCase(aChar) And Val(aChar) = 0 Then
  Selection.MoveLeft Unit:=wdCharacter, Count:=2
  Selection.Expand wdWord
End If
Selection.MoveEndWhile cset:=ChrW(8217) & "' ", Count:=wdBackward

' To trap i.e. and e.g.
If Len(Selection) = 1 Then
  Selection.MoveStartUntil cset:=" (", Count:=wdBackward
End If
startWas = Selection.Start
Set rng = ActiveDocument.Range
If Selection.Information(wdInEndnote) = True Then _
  Set rng = ActiveDocument.StoryRanges(wdEndnotesStory)
If Selection.Information(wdInFootnote) = True Then _
  Set rng = ActiveDocument.StoryRanges(wdFootnotesStory)
If Selection.Information(wdInCommentPane) = True Then
  Set rng = ActiveDocument.StoryRanges(wdCommentsStory)
End If
rng.Start = startWas
rng.End = startWas

i = 1
Do
' Add a word to the selection
  rng.MoveEnd wdWord, 1
  endWord = rng.End
  If Right(rng, 1) = " " Then
    gottaSpace = True
    rng.MoveEnd , -1
  End If
  If Right(rng, 1) = " " Then
    gottaSpace = True
    rng.MoveEnd , -1
  End If
  thiswd = rng
  ' Check in the list of words to see if it's there
  WordPos = InStr(myWds, ";" & thiswd & specChar)
  If WordPos > 0 Then
  ' If it's in the list, find the replacement word(s)
    myWds = Right(myWds, Len(myWds) - WordPos - 1 - Len(thiswd))
    newWd = Left(myWds, InStr(myWds, ";") - 1)
    ' If it starts with "!", delete it and delete
    ' the previous character in the text
    If Asc(newWd) = 33 Then
      newWd = Right(newWd, Len(newWd) - 1)
      rng.Start = rng.Start - 1
    End If
    rng.Select
    Selection.TypeText Text:=newWd
  ' If it's not an abbreviation expansion
  ' move cursor to beginning in case another
  ' change is wanted.
    If Len(thiswd) > abbrLength Then Selection.End = startWas
  ' We've found it, so finish
    Exit Sub
  Else
    If gottaSpace = True Then rng.MoveEnd , 1
  End If
  i = i + 1
Loop Until i > maxWords

' If no word found, assume that an added 'that' is needed.
' But first check if this is the switch list
' and, if so, don't do anything to it
thisDoc = ActiveDocument.Name
If InStr(thisDoc, "SwitchList") > 0 Then
  Selection.HomeKey Unit:=wdStory
  Exit Sub
End If
If doaBeep = True Then Beep
Selection.End = startWas

If addaThat = True Then
  Selection.MoveStart wdWord, 1

  nextChar = Selection
  If nextChar = ";" Then Selection.MoveStart wdWord, 1
  Selection.TypeText Text:="that "
End If
Exit Sub

ReportIt:
If Err.Number = 5174 Then
  MsgBox ("Couldn't find file: " & listName)
Else
  MsgBox Err.Description & "  Number: " & Err.Number, vbExclamation, "Error from Word"
End If
End Sub


ET
Sub WordSwitch()
' Version 07.03.12
' Scripted single-word switching

' Alt-1 - because I use it mainly for numbers
' to and from text
listName = "zzSwitchList"
myDir = "C:\Documents and Settings\Paul\My Documents\"
specChar = ">"


doaBeep = False
maxWords = 30

doAnErrorBeep = False

defaultList = myDir & listName
On Error GoTo ReportIt
defaultLoaded = False
Set thisDoc = ActiveDocument
cursorPosn = Selection.Start
dirName = Replace(ActiveDocument.FullName, ActiveDocument.Name, "")
' Go and look for the list file
gottaList = False
For i = 1 To Application.Windows.Count
  If InStr(Application.Windows(i).Document.Name, listName) > 0 Then
    Set listDoc = Application.Windows(i).Document
    gottaList = True
  End If
Next

If gottaList = False Then
  Documents.Open dirName & listName
Else
  listDoc.Activate
End If

carryOn:
' Create one long string of all the words
myWds = ""
For Each myPara In ActiveDocument.Paragraphs
  theLine = myPara
  If InStr(theLine, specChar) > 0 Then
    myWds = myWds & "|" & Replace(theLine, Chr(13), "")
    myWds = Replace(myWds, "^=", ChrW(8211))
    myWds = Replace(myWds, "^+", ChrW(8212))
    myWds = Replace(myWds, "^32", " ")
  End If
Next myPara
myWds = myWds & "|"

ActiveDocument.ActiveWindow.WindowState = wdWindowStateMinimize

thisDoc.Activate
wNum = Application.Windows.Count
pNum = Application.ActiveWindow.Panes.Count
If Selection.Start <> cursorPosn And wNum > 1 Then _
     Application.Windows(1).Activate
If Selection.Start <> cursorPosn And pNum > 1 Then _
     ActiveWindow.Panes(1).Activate


If defaultLoaded = True Then StatusBar = _
     ">>>>>>>>  Loaded from your default word list  <<<<<<<<"
If Selection.LanguageID = wdEnglishUS Then
  myWds = Replace(myWds, "%" & specChar & " per cent,", _
       "%" & specChar & " percent,")
End If

startWas = Selection.Start
' Set position at start of first word
Selection.Words(1).Select
Selection.End = Selection.Start

i = 1
Do
' Select the whole of the word
  Selection.MoveEnd wdWord, 1
  endWord = Selection.End
  If Right(Selection, 1) = " " Then Selection.MoveEnd , -1
  ' check if this "word" includes a close single curly quote
  If Right(Selection, 1) = ChrW(8217) Then Selection.MoveEnd , -1
  thiswd = Selection
  ' Check in the list of words to see if it's there
  WordPos = InStr(myWds, "|" & thiswd & specChar)
  If WordPos > 0 Then
  ' If it's in the list, find the replacement word(s)
    myWds = Right(myWds, Len(myWds) - WordPos - 1 - Len(thiswd))
    newWd = Left(myWds, InStr(myWds, "|") - 1)
    ' If it starts with "!", delete it and delete
    ' the previous character in the text
    If Len(newWd) > 0 Then
      If Asc(newWd) = 33 Then
        newWd = Right(newWd, Len(newWd) - 1)
        Selection.Start = Selection.Start - 1
      End If
      Selection.TypeText Text:=newWd
      Selection.Start = Selection.End
      If Len(newWd) > 1 Then Selection.MoveEnd , -1
    Else
      Selection.Delete
    End If
  ' We've finished, so stop
    Exit Sub
  Else
  ' get next char, to check for %
    thisStart = Selection.Start
    Selection.Start = endWord
    nextChar = Selection
    Selection.Start = thisStart

  ' Is it a numeral?
    If UCase(thiswd) = LCase(thiswd) And Asc(Left(thiswd, 1)) > 47 _
         And Asc(Left(thiswd, 1)) < 58 And nextChar <> "%" Then GoTo NumToWords
    Selection.Start = endWord
  End If
  i = i + 1
Loop Until i > maxWords

' If no word found beep for warning
If doaBeep = True Then Beep

Selection.End = startWas
Exit Sub

NumToWords:
startHere = Selection.Start
If Right(Selection, 1) = " " Then Selection.MoveEnd , -1
' check if this "word" includes a close single curly quote
If Right(Selection, 1) = ChrW(8217) Then Selection.MoveEnd , -1
' or a hard space
If Right(Selection, 1) = Chr(160) Then Selection.MoveEnd , -1

Set rng = ActiveDocument.Range
rng.Start = Selection.End
rng.End = Selection.End + 2
moreText = rng
i = Asc(Right(moreText, 1)) - 48
j = Val(Right(moreText, 1))
linkChar = Left(moreText, 1)
If (linkChar = "," Or linkChar = " " Or linkChar _
     = Chr(160)) And i = j Then
  sh = Selection.Start
  Selection.Start = Selection.End + 1
  Selection.TypeBackspace
  Selection.MoveEnd , 3
  Selection.Start = sh
End If
' Create a field containing the digits and a special format code
Selection.Fields.Add Range:=Selection.Range, _
   Type:=wdFieldEmpty, Text:="= " + Selection + " \* CardText", _
   PreserveFormatting:=True

' Select the field and copy it
Selection.MoveStart Unit:=wdCharacter, Count:=-1
Selection.Copy

' Paste the text unformatted, replacing the field
Selection.PasteSpecial Link:=False, DataType:=wdPasteText, _
   Placement:=wdInLine, DisplayAsIcon:=False

Selection.Start = startHere
numWords = Selection
If Right(numWords, 4) <> "dred" Then numWords = _
     Replace(numWords, "hundred", "hundred and")
If InStr(numWords, "hundred") > 0 Then
  numWords = Replace(numWords, "thousand", "thousand,")
Else
  If Right(numWords, 4) <> "sand" Then numWords = _
       Replace(numWords, "thousand", "thousand and")
End If
Selection.TypeText Text:=numWords

Selection.MoveRight Unit:=wdCharacter, Count:=1
Exit Sub

ReportIt:
If Err.Number = 5174 Then
  If doAnErrorBeep = True Then Beep
  defaultLoaded = True
  Documents.Open defaultList
  Resume carryOn
Else
  MsgBox Err.Description, vbExclamation, "Error from Word"
End If
End Sub


ET
Sub CharacterSwitch()
' Version 07.03.12
' Scripted character switching (was called 'QuickSwitchChar')

listName = "zzSwitchList"
myDir = "C:\Documents and Settings\Paul\My Documents\"
specChar = "_"

doaBeep = False
maxChars = 200
doAnErrorBeep = False

On Error GoTo ReportIt
defaultList = myDir & listName
defaultLoaded = False
Set thisDoc = ActiveDocument
cursorPosn = Selection.Start
dirName = Replace(ActiveDocument.FullName, ActiveDocument.Name, "")
' Go and look for the list file
gottaList = False
For i = 1 To Application.Windows.Count
  If InStr(Application.Windows(i).Document.Name, listName) > 0 Then
    Set listDoc = Application.Windows(i).Document
    gottaList = True
  End If
Next

If gottaList = False Then
  Documents.Open dirName & listName
Else
  listDoc.Activate
End If

carryOn:
' Create one long string of all the words
myWds = ""
For Each myPara In ActiveDocument.Paragraphs
  theLine = myPara
  If InStr(theLine, specChar) > 0 Then
    myWds = myWds & "|" & Replace(theLine, Chr(13), "")
    myWds = Replace(myWds, "^=", ChrW(8211))
    myWds = Replace(myWds, "^+", ChrW(8212))
    myWds = Replace(myWds, "^32", " ")
  End If
Next myPara
myWds = myWds & "|"

ActiveDocument.ActiveWindow.WindowState = wdWindowStateMinimize

thisDoc.Activate
wNum = Application.Windows.Count
pNum = ActiveDocument.ActiveWindow.Panes.Count
If Selection.Start <> cursorPosn And wNum > 1 Then _
     ActiveDocument.Windows(1).Activate
If Selection.Start <> cursorPosn And pNum > 1 Then _
     ActiveWindow.Panes(1).Activate


If defaultLoaded = True Then StatusBar = _
    ">>>>>>>>  Loaded from your default word list  <<<<<<<<"
If Selection.LanguageID = wdEnglishUS Then
  myWds = Replace(myWds, "%" & specChar & " per cent", _
       "%" & specChar & " percent")
End If


For i = 1 To maxChars
' Select the character
  Selection.End = Selection.Start + 1
  thisChar = Selection
  ' Check in the list of words to see if it's there
  WordPos = InStr(myWds, "|" & thisChar & specChar)
  If WordPos > 0 Then
  ' If it's in the list, find the replacement text
    myWds = Right(myWds, Len(myWds) - WordPos - 1 - Len(thisChar))
    newWd = Left(myWds, InStr(myWds, "|") - 1)
    Selection.TypeText Text:=newWd
    Selection.Start = Selection.End
  ' We've found it, so finish
    Exit Sub
  Else
    Selection.Start = Selection.End
  End If
Next

' If no character match found beep for warning
If doaBeep = True Then Beep

Selection.End = startWas
Exit Sub

ReportIt:
If Err.Number = 5174 Then
  If doAnErrorBeep = True Then Beep
  defaultLoaded = True
  Documents.Open defaultList
  Resume carryOn
Else
  MsgBox Err.Description, vbExclamation, "Error from Word"
End If
End Sub


ET
Sub Equation()
' Version 03.01.10
' Insert an equation

Selection.InlineShapes.AddOLEObject ClassType:="Equation.3"
End Sub


ET
Sub CentreText()
' Version 07.05.10
' Centre the text
' F6

If Selection.ParagraphFormat.Alignment = wdAlignParagraphCenter Then
  Selection.ParagraphFormat.Alignment = wdAlignParagraphLeft
Else
  Selection.ParagraphFormat.Alignment = wdAlignParagraphCenter
End If
End Sub


ET
Sub ListUcase()
' Version 07.01.10
' Upper case initial letter of list item
Selection.End = Selection.Start
With Selection.Find
  .Text = "^0149^t"
  .Replacement.Text = ""
  .Format = False
  .MatchCase = False
  .MatchWildcards = False
  .Execute
End With

Selection.MoveRight Unit:=wdCharacter, Count:=1, Extend:=wdExtend
Selection.Range.Case = wdUpperCase
' Now remove selection
Selection.Collapse wdCollapseEnd
End Sub


ET
Sub ListUcaseAll()
' Version 07.01.10
' Upper case initial letter of all list items
Selection.End = Selection.Start
With Selection.Find
  .Text = "^0149^t"
  .Replacement.Text = ""
  .Format = False
  .Wrap = False
'  .Wrap = wdFindContinue
  .MatchCase = False
  .MatchWildcards = False
End With

Do While Selection.Find.Execute
  Selection.MoveRight Unit:=wdCharacter, Count:=1, Extend:=wdExtend
  Selection.Range.Case = wdUpperCase
' Now move on past the selected character
  Selection.Collapse wdCollapseEnd
Loop
End Sub


ET
Sub AutoListLcaseAll()
' Version 30.01.12
' Lowercase initial letter of all auto-bulleted/numbered list items
If Selection.End - Selection.Start < 3 Then
  myResponse = MsgBox("Whole document?!", vbQuestion + vbYesNo, "Lowercase initial letters")
  If myResponse = vbNo Then Exit Sub
  Set rng = ActiveDocument.Content
  Else
  Set rng = Selection.Range
End If
For Each myPara In rng.Paragraphs
  If Len(myPara) > 1 Then
    Set myChar = myPara.Range
    myChar.End = myChar.Start + 1
    myChar.Case = wdLowerCase
  End If
Next myPara
End Sub


ET
Sub AutoListLcaseOne()
' Version 30.01.12
' Lowercase initial letter of one auto-bulleted/numbered list item

Selection.HomeKey Unit:=wdLine
Set rng = Selection.Range
rng.End = rng.Start + 1
rng.Case = wdLowerCase
Selection.MoveDown Unit:=wdLine, Count:=1
End Sub


ET
Sub TitleHeadingCapper()
' Version 11.08.14
' Uppercase initial letter of all major words (title case)

' If the headings are all in caps, say True
allIsInCaps = False

' Do you want an initial cap after a colon?
colonCap = True

' Do you want an initial cap after a hyphen?
hyphenCap = True

' List of lowercase words, each surrounded by spaces
lclist = " a an and as at by for from if in into with is of it "
lclist = lclist & " on or that the to "

If Selection.Start = Selection.End Then
  Selection.Expand wdParagraph
Else
  myEnd = Selection.End
  Selection.Collapse wdCollapseStart
  Selection.Expand wdWord
  myStart = Selection.Start
  Selection.Start = myEnd
  Selection.Expand wdWord
  Selection.Start = myStart
End If
Set rng = Selection.Range

'If a word is in an acronym, e.g. BBC, ignore it
If allIsInCaps = False Then
  For Each wd In rng.Words
    If UCase(wd) = wd And Len(Trim(wd)) > 1 Then wd.Font.Shadow = True
  Next
End If

' If there's a tag or a number, jump past it
Set rng2 = rng.Duplicate
charOne = Left(rng2, 1)
If charOne = "<" Then
  rng2.Start = rng2.Start + InStr(rng2, ">") + 1
  charOne = Left(rng2, 1)
End If
If LCase(charOne) = UCase(charOne) Then
  Do
    rng2.MoveStart , 1
    sdgfdf = rng2.Text
  Loop Until Asc(rng2) < 33
  rng2.MoveStart , 1
End If
rng2.End = rng2.Start + 1
rng2.Case = wdUpperCase
rng.Start = rng2.Start
endHere = rng.End
startHere = rng.Start
rng.Case = wdTitleWord

Set rng2 = rng.Duplicate
' Force lower case after hyphen?
If hyphenCap = False Then
  rng2.Start = startHere
  Do
' Capitalise after a colon if option set
    myText = rng2.Text
    hyphenPos = InStr(myText, "-")
    If hyphenPos > 0 Then
      rng2.MoveStart , hyphenPos
      rng2.End = rng2.Start + 1
      rng2.Case = wdLowerCase
      rng2.End = endHere
    End If
  Loop Until hyphenPos = 0
End If

For wrd = 2 To rng.Words.Count
  thisWord = Trim(rng.Words(wrd))
  
  thisWord = " " & LCase(thisWord) & " "
  If InStr(lclist, thisWord) Then
    rng.Words(wrd).Case = wdLowerCase
  End If
Next wrd

' Force upper case after colon?
If colonCap = True Then
  rng.Start = startHere
  Do
' Capitalise after a colon if option set
    myText = rng.Text
    colonPos = InStr(myText, ": ")
    If colonPos > 0 Then
      rng.MoveStart , colonPos + 1
      rng.End = rng.Start + 1
      rng.Case = wdUpperCase
      rng.End = endHere
    End If
  Loop Until colonPos = 0
End If

Set rng = Selection.Range

For Each wd In rng.Words
  If wd.Font.Shadow = True Then
    wd.Case = wdUpperCase
  End If
Next

Selection.Font.Shadow = False
Selection.Start = endHere
Selection.End = endHere
End Sub


ET
Sub TitleInQuotesCapper()
' Version 21.01.13
' Uppercase initial letter of all major words (title case)

' Do you want an initial cap after a colon?
colonCap = False
' Do you want an initial cap after a hyphen?
hyphenCap = False

' List of lowercase words, each surrounded by spaces
lclist = " a an and at by for from in into is it of "
lclist = lclist & " on or that the their they to "
lclist = lclist & " we with "

Set rng = Selection.Range
rng.MoveEndUntil cset:=ChrW(8221) & ChrW(8217) & "'"")", Count:=wdForward
Do
  Set rng2 = rng.Duplicate
  rng2.Start = rng2.End + 1
  rng2.End = rng2.End + 1
  keepGoing = False
  If UCase(rng2) <> LCase(rng2) Then
    rng.End = rng.End + 1
    rng.MoveEndUntil cset:=ChrW(8221) & ChrW(8217) & "'"")", Count:=wdForward
    keepGoing = True
  End If
Loop Until keepGoing = False
rng.MoveStartUntil cset:=ChrW(8220) & ChrW(8216) & "'""(", Count:=wdBackward

endHere = rng.End
startHere = rng.Start
rng.Case = wdTitleWord
rng.Select

For Each wd In rng.Words
  If UCase(wd) = wd And Len(Trim(wd)) > 1 Then wd.Font.Shadow = True
Next

Set rng2 = rng.Duplicate
' Force lower case after hyphen?
If hyphenCap = False Then
  rng2.Start = startHere
  Do
' Capitalise after a colon if option set
    myText = rng2.Text
    hyphenPos = InStr(myText, "-")
    If hyphenPos > 0 Then
      rng2.MoveStart , hyphenPos
      rng2.End = rng2.Start + 1
      rng2.Case = wdLowerCase
      rng2.End = endHere
    End If
  Loop Until hyphenPos = 0
End If

For wrd = 2 To rng.Words.Count
  thisWord = Trim(rng.Words(wrd))
 
  thisWord = " " & LCase(thisWord) & " "
  If InStr(lclist, thisWord) Then
    rng.Words(wrd).Case = wdLowerCase
  End If
Next wrd

' Force upper case after colon?
If colonCap = True Then
  rng.Start = startHere
  Do
' Capitalise after a colon if option set
    myText = rng.Text
    colonPos = InStr(myText, ": ")
    If colonPos > 0 Then
      rng.MoveStart , colonPos + 1
      rng.End = rng.Start + 1
      rng.Case = wdUpperCase
      rng.End = endHere
    End If
  Loop Until colonPos = 0
End If

Set rng = Selection.Range

For Each wd In rng.Words
  If wd.Font.Shadow = True Then
    wd.Case = wdUpperCase
  End If
Next

Selection.Font.Shadow = False
Selection.Start = endHere
Selection.End = endHere
End Sub



ET
Sub TitleUnCapper()
' Version 28.11.11
' Uppercase initial letter only on first word
' Alt-F6

moveOnAfter = True

nextFind = "\<[ABC]\>"
' Do you want an initial cap after a colon?
colonCap = False
Selection.MoveRight Unit:=wdCharacter, Count:=1
' Move to start of current word
Selection.Words(1).Select
Selection.End = Selection.Start
Selection.MoveStart wdCharacter, -1

' check the character before the first word
myChar = Selection
openChars = ChrW(8216) & ChrW(8220) & "("
If InStr(openChars & "(", myChar) > 0 Then
  allSentence = False
  quoteStart = Selection.Start + 1
Else
  allSentence = True
End If

If allSentence = True Then
  Selection.Sentences(1).Select
  myText = Selection
  firstChar = Chr(Asc(myText))
  If InStr(openChars, firstChar) > 0 Then Selection.MoveStart Count:=1
  If firstChar = "<" Then Selection.MoveStart Count:=InStr(myText, ">")
Else
  Selection.Sentences(1).Select
  Selection.Start = quoteStart
  myText = Selection
  i = Asc(myChar)
  ' Find the position of the corresponding close character
  lenTitle = InStr(myText, Chr(i + 1))
  Selection.End = Selection.Start + lenTitle
End If
endNow = Selection.End

' Just check the initial letter is uppercase
Selection.End = Selection.Start
Do
' select more until you meet an alpha character
  Selection.MoveEnd , 1
  firstBit = Selection
Loop Until UCase(firstBit) <> LCase(firstBit)
Selection.Range.Case = wdUpperCase
Selection.Start = Selection.End
Selection.End = endNow

' Lowercase the rest
Selection.Range.Case = wdLowerCase
If colonCap = True Then
' Capitalise after a colon if option set
  myText = Selection
  colonPos = InStr(myText, ": ")
  If colonPos > 0 Then
    Selection.Start = Selection.Start + colonPos + 1
    Selection.End = Selection.Start + 1
    Selection.Range.Case = wdUpperCase
  End If
End If
Selection.Start = endNow
If moveOnAfter = True Then
nextFind = "\<[ABC]\>"
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = nextFind
    .Wrap = False
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = True
    .MatchWholeWord = False
    .MatchSoundsLike = False
    .Execute
  End With
End If

End Sub



ET
Sub HeadingSentenceCase()
' Version 09.04.13
' Sentence case this selection or paragraph (but not acronyms)

allowAreaSelection = True

highlightChange = False
myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
If Selection.Start = Selection.End Or allowAreaSelection = False _
     Then Selection.Paragraphs(1).Range.Select
myEnd = Selection.End - 1
Selection.End = Selection.Start + 1
' Skip past <> codes
If Selection = "<" Then
  Do
    Selection.MoveRight , 1
  Loop Until Selection = ">"
  Selection.MoveRight , 1
End If
While LCase(Selection) = UCase(Selection)
  Selection.MoveRight Unit:=wdWord, Count:=1
  Selection.MoveEnd wdCharacter, 1
Wend
myStart = Selection.Start
Set rng = ActiveDocument.Content
Set rng1 = ActiveDocument.Content

For i = myStart + 1 To myEnd
  rng.Start = i
  rng.End = i + 1
  If Len(rng) > 0 Then
    a = Asc(rng)
  Else
    a = 0
  End If
  If a > 64 And a < 91 Then
    rng1.Start = i + 1
    rng1.End = i + 2
    a = Asc(rng1)
      If a > 96 And a < 123 Then
        rng1.Start = i - 2
        rng1.End = i - 1
        If Len(rng1) > 0 Then
          a = Asc(rng1)
        Else
          a = 0
        End If
        rng1.Start = i - 1
        rng1.End = i
        b = Asc(rng1)
        If Not (a = 46 Or a = 33 Or a = 63 Or b = 13 Or b = 9) Then
          rng.Case = wdLowerCase
          If highlightChange = True Then rng.HighlightColorIndex = wdGray25
        End If
      End If
  End If
Next i
ActiveDocument.TrackRevisions = myTrack
Selection.Start = myEnd + 1
End Sub


ET
Sub InitialCapItalicText()
' Version 03.12.11
' Initial caps on significant words in italic text in notes

For Each wd In ActiveDocument.Words
  If wd.Italic = True Then
    myWord = wd
    initChar = wd.Characters(1)
    If UCase(initChar) <> initChar Then
      If InStr(lclist, myWord) = 0 Then
        wd.Characters(1) = UCase(initChar)
        If doHighlight = True Then wd.HighlightColorIndex = myColour
      End If
    End If
  End If
Next wd
End Sub


ET
Sub InitialCapItalicInNotes()
' Version 03.12.11
' Initial caps on significant words in italic text in notes

' List of lowercase words, each surrounded by spaces
lclist = " a an and at by for from in into is it of "
lclist = lclist & " on or that the their they to "
lclist = lclist & " we with "

myColour = wdYellow
doHighlight = True

If ActiveDocument.Footnotes.Count > 0 Then
  For Each fn In ActiveDocument.Footnotes
    For Each wd In fn.Range.Words
      If wd.Italic = True Then
        myWord = wd
        initChar = wd.Characters(1)
        If UCase(initChar) <> initChar Then
          If InStr(lclist, myWord) = 0 Then
            wd.Characters(1) = UCase(initChar)
            If doHighlight = True Then wd.HighlightColorIndex = myColour
          End If
        End If
      End If
    Next wd
  Next
End If
End Sub


ET
Sub AllCapsToInitialCap()
' Version 14.12.11
' Initial cap any words in all caps

Set rng = ActiveDocument.Content
Do
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
' This is the line that sets the min no of letters
    .Text = "[A-Z]{3,}"
    .Wrap = False
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = True
    .Execute
  End With
  If rng.Find.Found = True Then
' This is a highlight line - delete if you don't want it
    rng.HighlightColorIndex = wdBrightGreen

    rng.Start = rng.Start + 1
    rng.Case = wdLowerCase
    rng.Start = rng.End
    stopNow = False
  Else
    stopNow = True
  End If
  Selection.Start = Selection.End
Loop Until stopNow = True
End Sub


ET
Sub SuperscriptNoteNumber()
' Version 11.08.11
' Superscript note number

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^p"
  .Replacement.Text = ""
  .Wrap = wdFindContinue
  .Execute
End With
startHere = Selection.End
Selection.Start = Selection.End
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = " "
  .Replacement.Text = ""
  .Forward = True
  .Wrap = wdFindContinue
  .Execute
End With
Selection.End = Selection.Start
Selection.Start = startHere
Selection.Font.Superscript = True
Selection.Start = Selection.End
End Sub





ET
Sub Italiciser()
' Version 22.04.12
' Toggle selected text italic
' <f9>
Dim charsItalic As Long
If Selection.End - Selection.Start < 2 Or _
     Selection.Information(wdInCommentPane) Then
  Selection.Font.Italic = Not (Selection.Font.Italic = True)
Else
' How many italic/roman chars?
  For i = Selection.Start To Selection.End
    If i < ActiveDocument.Range.End Then
      Set rng = ActiveDocument.Range(Start:=i, End:=i + 1)
      If rng.Font.Italic = True Then charsItalic = charsItalic + 1
    End If
  Next i
  charsRoman = Len(Selection) - charsItalic
' Change to italic or roman, accordingly
  If charsItalic > charsRoman Or charsRoman = 0 Then
    Selection.Font.Italic = False
  Else
  ' Don't italicise following space or quote
    Set rng = Selection
    rng.MoveEndWhile cset:=ChrW(8217) & "' ", Count:=wdBackward
    rng.Font.Italic = True
    rng.Select
  End If
End If
End Sub


ET
Sub Boldiser()
' Version 22.04.12
' Toggle selected text bold
' <f10>
Dim charsBold As Long

If Selection.End - Selection.Start < 2 Or _
     Selection.Information(wdInCommentPane) Then
  Selection.Font.Bold = Not (Selection.Font.Bold = True)
Else
' How many bold/roman chars?
  For i = Selection.Start To Selection.End
    If i < ActiveDocument.Range.End Then
      Set rng = ActiveDocument.Range(Start:=i, End:=i + 1)
      If rng.Font.Bold = True Then charsBold = charsBold + 1
    End If
  Next i
  charsRoman = Len(Selection) - charsBold
' Change to bold or roman, accordingly
  If charsBold > charsRoman Or charsRoman = 0 Then
    Selection.Font.Bold = False
  Else
  ' Don't boldise following space or quote
    Set rng = Selection
    rng.MoveEndWhile cset:=ChrW(8217) & "' ", Count:=wdBackward
    rng.Font.Bold = True
    rng.Select
  End If
End If
End Sub


ET
Sub BoldKill()
' Version 11.08.11
' Remove bold from selected text or current line
myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

If Selection.End = Selection.Start Then
  Selection.HomeKey Unit:=wdLine
  Selection.MoveDown Unit:=wdLine, Count:=1, Extend:=wdExtend
End If
Selection.Font.Bold = False
Selection.Start = Selection.End

ActiveDocument.TrackRevisions = myTrack
End Sub


ET
Sub ItalicKill()
' Version 11.08.11
' Remove italic from selected text or current line
myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

If Selection.End = Selection.Start Then
  Selection.HomeKey Unit:=wdLine
  Selection.MoveDown Unit:=wdLine, Count:=1, Extend:=wdExtend
End If
Selection.Font.Italic = False
Selection.Start = Selection.End

ActiveDocument.TrackRevisions = myTrack
End Sub


ETSub ItaliciseVariable()
' Version 24.05.14
' Run along to find alpha chars and italicise them

greekItalic = True
ucaseGreekItalic = False
avoidWords = ",to,and,or,at,"
doTrack = False

myTrack = ActiveDocument.TrackRevisions
If doTrack = False Then ActiveDocument.TrackRevisions = False
If Selection.Start = Selection.End Then
  Selection.MoveEnd , 1
  While LCase(Selection) = UCase(Selection) And _
       Not (Selection.Font.Name = "Symbol" And greekItalic)
    Selection.Collapse wdCollapseEnd
    Selection.MoveEnd , 1
  Wend
  myStart = Selection.Start
  isItalic = Selection.Font.Italic
  Do
    Selection.Collapse wdCollapseEnd
    Selection.MoveEnd , 1
  Loop Until LCase(Selection) = UCase(Selection) And _
       Not (Selection.Font.Name = "Symbol" And greekItalic)
  Selection.Collapse wdCollapseStart
  Selection.Start = myStart
  If InStr(avoidWords, "," & Selection & ",") = 0 Then
    numChars = Len(Selection)
    myStart = Selection.Start
    Set rng = Selection.Range
    For i = 1 To numChars
      gotVar = False
      rng.End = myStart + i
      rng.Start = rng.End - 1
      ch = rng.Text
      a = Asc(ch)
      u = AscW(ch)
      
      ' Ordinary letter
      If a = u Then gotVar = True
      If greekItalic = True Then
        If a = Asc("?") Then gotVar = True
        If u > 915 And u < 970 Then
          gotVar = True
          If ucaseGreekItalic = False And u < 945 Then gotVar = False
        End If
        If ucaseGreekItalic = False And u < -3999 Then gotVar = False
      End If
      If gotVar = True Then
        rng.Font.Italic = Not (isItalic)
      Else
        rng.Select
        Selection.Collapse wdCollapseEnd
        ActiveDocument.TrackRevisions = myTrack
        Exit Sub
      End If
    Next i
  End If
  Selection.Collapse wdCollapseEnd
Else
  If Selection <> " " Then Selection.Font.Italic = Not (Selection.Font.Italic)
  Selection.Collapse wdCollapseEnd
End If
ActiveDocument.TrackRevisions = myTrack
End Sub


ET
Su b SubscriptSwitch0()
' F4
Selection.Font.Subscript = Not Selection.Font.Subscript
End Sub

ET
Su b SuperscriptSwitch0()
' F5
Selection.Font.Superscript = Not Selection.Font.Superscript
End Sub

ET
Sub SubscriptSwitch()
' Version 05.09.11
' Toggle subscript
' F4
If Selection.Start = Selection.End Then
  Selection.MoveEnd wdCharacter, 1
  If Asc(Selection) > 48 Then
    Selection.Font.Subscript = Not Selection.Font.Subscript
    Selection.End = Selection.Start
  Else
    Selection.End = Selection.Start
    Selection.Font.Subscript = Not Selection.Font.Subscript
  End If
Else
  Selection.Font.Subscript = Not Selection.Font.Subscript
End If
End Sub


ET
Sub SuperscriptSwitch()
' Version 05.09.11
' Toggle superscript
' F5
If Selection.Start = Selection.End Then
  Selection.MoveEnd wdCharacter, 1
  If Asc(Selection) > 48 Then
    Selection.Font.Superscript = Not Selection.Font.Superscript
    Selection.End = Selection.Start
  Else
    Selection.End = Selection.Start
    Selection.Font.Superscript = Not Selection.Font.Superscript
  End If
Else
  Selection.Font.Superscript = Not Selection.Font.Superscript
End If
End Sub




ET
Sub ItalicQuoteToggle()
' Version 14.01.12
' Toggle between italic and single quote
hereNow = Selection.Start
If Selection.Font.Italic = True Then GoTo AddQuote
gotStart = False
Do
  Selection.MoveLeft Unit:=wdWord, Count:=1
  Selection.MoveStart Count:=-1
  If AscW(Selection) = 8216 Then
    Selection.Delete
    gotStart = True
  End If
Loop Until gotStart = True
startItal = Selection.Start
Do
  Selection.MoveRight Unit:=wdCharacter, Count:=1
Loop Until AscW(Selection) = 8217
Selection.Delete Unit:=wdCharacter, Count:=1
Selection.Start = startItal
Selection.Font.Italic = True
Selection.Start = hereNow - 1
Selection.End = Selection.Start
Exit Sub

AddQuote:
myStart = 0
Do
  Selection.MoveLeft 1
  If Selection.Font.Italic = False Then
    myStart = Selection.Start
    Selection.TypeText Text:=ChrW(8216)
  End If
Loop Until myStart > 0

Selection.Start = hereNow + 1
myEnd = 0
Do
  Selection.MoveRight 1
  If Selection.Font.Italic = False Then
    Selection.MoveLeft 1
    myEnd = Selection.Start
    Selection.MoveStart Count:=-1
    If Asc(Selection) = 32 Then
      Selection.MoveEnd Count:=-1
      myEnd = myEnd + 1
    Else
      Selection.MoveStart Count:=1
    End If
    Selection.TypeText Text:=ChrW(8217)
    Selection.End = myEnd + 1
    Selection.Start = myStart
    Selection.Font.Italic = False
  End If
Loop Until myEnd > 0
Selection.Start = hereNow + 1
Selection.End = Selection.Start
End Sub



ET
Sub ApplyAttribute()
' Version 19.10.13
' Apply attribute to current cell, row, column, paragraph, sentence, word or selection

' If the cursor is inside a table, select the cell (or column or row)
If Selection.Information(wdWithInTable) = True Then
  myCol = Selection.Information(wdStartOfRangeColumnNumber)
  myRow = Selection.Information(wdStartOfRangeRowNumber)
' Select current cell
  Selection.Tables(1).Columns(myCol).Cells(myRow).Range.Select
' ... or column
'  Selection.Tables(1).Columns(myCol).Select
' ... or row
'  Selection.Tables(1).Rows(myRow).Select
End If

' If no text is selected, select the paragraph (or sentence or word)
If Selection.End - Selection.Start = 0 Then
  Selection.Paragraphs(1).Range.Select
  ' or Selection.Sentences(1).Select
  ' or Selection.Words(1).Select
End If

' Apply attribute
Selection.Font.Underline = wdUnderlineDouble

' If in a table, replace the cursor where it was
If Selection.Information(wdWithInTable) = True Then
  Selection.Tables(1).Columns(myCol).Cells(myRow).Range.Select
End If
Selection.Collapse wdCollapseStart

End Sub


ET
Sub SelectHighlightedText()
' version 05.09.11
' Selects all text in a given highlight
nowTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

' Find beginning to the nearest word
nowCol = Selection.Range.HighlightColorIndex
wasEnd = Selection.End
Do
  Selection.MoveStart wdWord, -1
  Selection.MoveEnd wdWord, -1
Loop Until Selection.Range.HighlightColorIndex <> nowCol
Selection.MoveStart , -1

' Find beginning to the nearest character
Do
  Selection.MoveStart , 1
  Selection.MoveEnd , 1
Loop Until Selection.Range.HighlightColorIndex = nowCol
startHere = Selection.Start

' Find end to the nearest word
Selection.Start = wasEnd
Selection.End = wasEnd
Do
  Selection.MoveEnd wdWord, 1
  Selection.MoveStart wdWord, 1
Loop Until Selection.Range.HighlightColorIndex <> nowCol
Selection.MoveEnd , 1
' Find end to the nearest character
Do
  Selection.MoveStart , -1
  Selection.MoveEnd , -1
Loop Until Selection.Range.HighlightColorIndex = nowCol
Selection.Start = startHere

' Now do to the highlighted text whatever you want
' For example...

'Selection.Range.HighlightColorIndex = wdNoHighlight

'Selection.Font.Italic = wdToggle
'Selection.Start = Selection.End
ActiveDocument.TrackRevisions = nowTrack
End Sub


ET
Sub PasteUnformatted()
' Version 30.01.10
' Paste unformatted
Selection.PasteAndFormat (wdFormatPlainText)
End Sub


ET
Sub PasteWithEmphasis()
' Version 30.12.10
' Paste with emphasis

Selection.PasteAndFormat (wdFormatSurroundingFormattingWithEmphasis)
End Sub


ET
Sub ClipToText()
' Version 04.01.10
' Get pure text from pdfs and websites
Documents.Add
Selection.Paste
Selection.WholeStory
Selection.Cut
Selection.PasteAndFormat (wdFormatPlainText)
Selection.WholeStory
Selection.Copy
ActiveDocument.Close SaveChanges:=False
Selection.Paste
End Sub


ET
Sub ClipToTextWithEmphasis()
' Version 23.02.11
' Get pure text from pdfs and websites
Documents.Add
Selection.Paste
Selection.WholeStory
Selection.Cut
Selection.PasteAndFormat (wdFormatSurroundingFormattingWithEmphasis)
Selection.WholeStory
Selection.Copy
ActiveDocument.Close SaveChanges:=False
Selection.Paste
End Sub


ET
Sub StyleCopy()
' Version 04.01.11
' Copy style

Selection.CopyFormat
End Sub


ET
Sub StylePaste()
' Version 04.01.11
' Paste style

Selection.PasteFormat
End Sub


ET
Sub UnifyFormatBackwards()
' Version 14.03.14
' Make start of para (or selection) same format as the end

myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
If Selection.Start = Selection.End Then
  hereNow = Selection.Start
  Selection.Expand wdParagraph
  Selection.End = hereNow
End If
startHere = Selection.Start
Selection.Start = Selection.End - 1
Selection.Collapse wdCollapseEnd
Selection.MoveEnd , -1
Selection.CopyFormat
Selection.Start = startHere
Selection.PasteFormat
Selection.Collapse wdCollapseEnd
ActiveDocument.TrackRevisions = myTrack
End Sub


ET
Sub UnifyFormatForwards()
' Version 14.03.14
' Make end of para (or selection) same format as the start

myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
If Selection.Start = Selection.End Then
  hereNow = Selection.Start
  Selection.Expand wdParagraph
  Selection.Start = hereNow
End If
endHere = Selection.End
Selection.Collapse wdCollapseStart
Selection.MoveStart , 1
Selection.CopyFormat
Selection.End = endHere
Selection.PasteFormat
Selection.Collapse wdCollapseEnd
ActiveDocument.TrackRevisions = myTrack
End Sub



ET
Sub SpikeCopy()
' Version 26.02.11
' Copy to spike

NormalTemplate.AutoTextEntries.AppendToSpike Range:=Selection.Range
WordBasic.EditUndo
End Sub


ET
Sub SpikeCut()
' Version 26.02.11
' Cut to spike

NormalTemplate.AutoTextEntries.AppendToSpike Range:=Selection.Range
End Sub


ET
Sub SpikePaste()
' Version 26.02.11
' Paste the whole contents of the spike

gotOne = False
For Each myEntry In NormalTemplate.AutoTextEntries
    If myEntry.Name = "Spike" Then gotOne = True
Next myEntry
If gotOne = True Then
  With NormalTemplate.AutoTextEntries("Spike")
    .Insert Where:=Selection.Range, RichText:=True
    .Delete
  End With
End If
End Sub


ET
Sub TagA()
' Version 16.02.11
' Add a tag to the current paragraph
newLine = vbCrLf
startText = "<A>"
' endText = ""
endText = "</A>" & newLine
' endText = "</A>"
endTextOnSameLine = True

' Go to start of this line and add the tag
Selection.HomeKey Unit:=wdLine
Selection.TypeText Text:=startText
' Add close tag, if there is one
If endText > "" Then
  Selection.Paragraphs(1).Range.Select
  Selection.Start = Selection.End
' Tag on this line or next?
  If endTextOnSameLine = True Then
    Selection.MoveLeft Unit:=wdCharacter, Count:=1
  End If
  Selection.TypeText Text:=endText
End If
End Sub


ET
Sub TagList()
' Version 19.01.13
' Add tags to a numbered or bulleted list

newLine = vbCrLf
startTextBullet = "<BL>"
' endTextBullet = "</BL>" & newLine
endTextBullet = "</BL>"

startTextNum = "<NL>"
' endTextNum = "</NL>" & newLine
endTextNum = "</NL>"

startTextLttr = "<Box>"
'startTextLttr = "<LL>"
' endTextLttr = "</LL>" & newLine
'endTextLttr = "</LL>"
endTextLttr = "</Box>"

endTextOnSameLine = True

numList = False
bulletList = False
lttrList = False
startText = startTextBullet
endText = endTextBullet
myStyle = ""

myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
localFont = Selection.Font.Name
' Go to start of this line and add the tag
Selection.Paragraphs(1).Range.Select
i = Asc(Selection)
If i > 47 And i < 59 Then numList = True
If i = 149 Then bulletList = True
thisStyle = Selection.Range.Style
If InStr(thisStyle, "umber") > 0 Then numList = True: myStyle = thisStyle
If InStr(thisStyle, "ullet") > 0 Then bulletList = True: myStyle = thisStyle
myListString = Selection.FormattedText.ListFormat.ListString
If myListString > "" Then
  If UCase(myListString) <> LCase(myListString) Then
    lttrList = True
  Else
    numList = True
  End If
If Selection.FormattedText.ListFormat.ListValue > 3 Then: numList = False
End If
Selection.End = Selection.Start + 1
myFont = ""
thisFont = Selection.Font.Name
If thisFont = "Symbol" Or thisFont = "Wingdings" Then
  bulletList = True
  myFont = thisFont
End If
If (numList = False And bulletList = False) Then
  lttrList = True
  startText = startTextLttr
  endText = endTextLttr
End If
If (numList = True And bulletList = True) Then
  myResponse = MsgBox("Bullets?", vbQuestion + vbYesNo)
  If myResponse = vbNo Then
    numList = True: bulletList = False
  Else
    numList = False: bulletList = True
  End If
End If

If numList = True Then
  startText = startTextNum
  endText = endTextNum
Else
End If
Selection.InsertBefore Text:=startText
Selection.MoveEnd wdCharacter, -1
Selection.Font.Name = localFont
Selection.Start = Selection.End

' Find the final item
Selection.Paragraphs(1).Range.Select
myListValue = Selection.FormattedText.ListFormat.ListValue
Selection.Start = Selection.End
If myStyle > "" Then
  Do
    Selection.Paragraphs(1).Range.Select
    thisStyle = Selection.Style
    Selection.Start = Selection.End
  Loop Until thisStyle <> myStyle
Else
  If numList = True Then
    If myListString = "" Then
      Do
        Selection.Paragraphs(1).Range.Select
        i = Asc(Selection)
        Selection.Start = Selection.End
      Loop Until i < 48 Or i > 58
    Else
      Do
        Selection.Paragraphs(1).Range.Select
        thisListString = Selection.FormattedText.ListFormat.ListString
        Selection.Start = Selection.End
      Loop Until thisListString = ""
    End If
  Else
  ' It's either a bullet list or a letter list
    If bulletList = True Then
      If myFont > "" Then
    ' Find the end by a font change
        Do
          Selection.End = Selection.Start + 1
          thisFont = Selection.Font.Name
          Selection.Paragraphs(1).Range.Select
          Selection.Start = Selection.End
        Loop Until thisFont <> myFont
      Else
    ' Find the end by bullet ASCII codes
        Do
          Selection.Paragraphs(1).Range.Select
          i = Asc(Selection)
          Selection.Start = Selection.End
        Loop Until i <> 149
      End If
    Else
    ' It's a lettered list
      If myListString = "" Then
        Do
          Selection.End = Selection.Start + 6
          thisBit = Selection
          Selection.Paragraphs(1).Range.Select
          Selection.Start = Selection.End
        Loop Until InStr(thisBit, Chr(9)) = 0
      Else
        Do
          Selection.Paragraphs(1).Range.Select
          thisListString = Selection.FormattedText.ListFormat.ListString
          Selection.Start = Selection.End
        Loop Until thisListString = ""
      End If
    End If
  End If
End If
Selection.MoveUp Unit:=wdParagraph, Count:=1
Selection.Paragraphs(1).Range.Select
Selection.End = Selection.Start

' Tag on this line or next?
If endTextOnSameLine = True Then
  Selection.MoveLeft Unit:=wdCharacter, Count:=1
End If
Selection.InsertBefore Text:=endText
Selection.Style = wdStyleNormal
ActiveDocument.TrackRevisions = myTrack
End Sub


ET
Sub BorderParaOff()
' Version 06.08.11
' Remove the borders applied to the paragraph
myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
Selection.Borders(wdBorderTop).LineStyle = wdLineStyleNone
Selection.Borders(wdBorderLeft).LineStyle = wdLineStyleNone
Selection.Borders(wdBorderBottom).LineStyle = wdLineStyleNone
Selection.Borders(wdBorderRight).LineStyle = wdLineStyleNone
ActiveDocument.TrackRevisions = myTrack
End Sub


ET
Sub FandRapostrophe()
' Version 22.04.11
' F&R text that contains curly quotes/apostrophe

Options.AutoFormatReplaceQuotes = False
Options.AutoFormatAsYouTypeReplaceQuotes = False

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^0145t Hooft"
  .Replacement.Text = "^0146t Hooft"
  .Wrap = wdFindContinue
  .Forward = True
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

Options.AutoFormatReplaceQuotes = True
Options.AutoFormatAsYouTypeReplaceQuotes = True
End Sub


ET
Sub AccentPicker()
' Version 10.05.11
' Call up the Insert Symbol window set to accented characters

Selection.TypeText Text:=Chr(224)
Selection.MoveStart wdCharacter, -1
Application.Run MacroName:="InsertSymbol"
End Sub


ET
Sub GreekPicker()
' Version 20.06.14
' Call up the Insert Symbol window set to Greek characters
' Ctrl-Alt-Shift-G
If Selection.Start = Selection.End Then
  Selection.TypeText Text:=ChrW(937)
  Selection.MoveStart wdCharacter, -1
  Application.Run MacroName:="InsertSymbol"
Else
  myLen = Len(Selection)
  Selection.Collapse wdCollapseStart
  Selection.TypeText Text:=ChrW(937)
  Selection.MoveStart wdCharacter, -1
  Selection.MoveRight , 1
  Selection.MoveEnd , myLen
  Selection.Delete
End If
End Sub


ET
Sub SciMarkPicker()
' Version 10.05.11
' Call up the Insert Symbol window set to Greek characters

Selection.TypeText Text:=ChrW(8242)
Selection.MoveStart wdCharacter, -1
Application.Run MacroName:="InsertSymbol"
End Sub


ET
Sub BlankLineRemover()
' Version 28.10.11
' Remove blank line from double-spaced paragraphs

Selection.Font.Shadow = True
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^p^p"
  .Font.Shadow = True
  .Wrap = wdFindContinue
  .Replacement.Text = "^p"
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With
Selection.Font.Shadow = False
' Tidy up the F&R
Selection.Find.ClearFormatting
End Sub



ET
Sub ReferenceDateShift()
' Version 13.04.11
' Move date from end of reference to after author

' Assumes reference has ", dddd." at the end.
' Places date, in parens, before first full point in ref.

' myHLcolour = wdYellow
myHLcolour = 0
myColour = wdColorRed
' myColour = 0

Do
  Selection.Paragraphs(1).Range.Select
  endPara = Selection.End
  Selection.End = Selection.End - 2
  Selection.Start = Selection.End - 5
  myDate = Selection
  myDate = Right(myDate, 4)
  If Val(myDate) > 999 Then
    Selection.Delete
    Selection.Paragraphs(1).Range.Select
    myRef = Selection
    myDotPos = InStr(myRef, ".") - 1
    Selection.MoveStart wdCharacter, myDotPos - 2
    myChar = Asc(Selection)
    If myChar <> 32 Then
    ' We've got an author's forename
      Selection.MoveStart wdCharacter, 2
     
    Else
    ' We've got an initial
      Selection.MoveStart , -1
   
      Do
        Selection.MoveStart , 3
        myChar = Asc(Selection)
      Loop Until myChar <> Asc(".")
      Selection.MoveStart , -3
      Selection.InsertBefore Text:="."
      Selection.MoveStart , 1
    End If
    Selection.End = Selection.Start
    Selection.InsertAfter Text:=" (" & myDate & ")"
    Selection.Paragraphs(1).Range.Select
    If myColour > 0 Then Selection.Range.Font.Color = myColour
    If myHLcolour > 0 Then Selection.Range.HighlightColorIndex = myHLcolour
  Else
    Selection.Start = endPara
  End If
  Selection.Start = Selection.End
  Selection.Paragraphs(1).Range.Select
Loop Until endPara = ActiveDocument.Range.End
Selection.Start = Selection.End
End Sub




ET
Sub ReferenceNameFinder()
' Version 08.04.11
' Get date from reference and add after author citation

myHLcolour = wdYellow
' myHLcolour = 0
' myColour = wdColorRed
myColour = 0

Do
  Selection.Paragraphs(1).Range.Select
  endPara = Selection.End
  startPara = Selection.Start
  myRef = Selection
  myDatePos = InStr(myRef, "(")
  myNamePos = InStr(myRef, ",") - 1
  If myDatePos > 0 And myNamePos > -1 Then
    Selection.End = startPara + myNamePos
    myName = Selection
    Selection.Start = startPara + myDatePos
    Selection.End = Selection.Start + 4
    myDate = Selection
    Selection.HomeKey Unit:=wdStory
    With Selection.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .MatchWildcards = True
      .Text = "<" & myName & ">"
      .Replacement.Text = ""
      .Execute
    End With
    If Selection.Find.Found And Selection.Start < startPara Then
      startName = Selection.Start
      endName = Selection.End
      Selection.End = Selection.Start
      Selection.MoveStart wdCharacter, -25
      myBit = Selection
      Selection.Start = endName
      Selection.End = endName + 2
      valNext = Val(Selection)
      Selection.End = Selection.Start
      If InStr(myBit, "(") > 0 Then
         If valNext > 0 Then myDate = myDate & ":"
      Else
        myDate = "(" & myDate & ")"
      End If
      Selection.InsertAfter Text:=" " & myDate
      Selection.Start = startName
      If myColour > 0 Then Selection.Range.Font.Color = myColour
      If myHLcolour > 0 Then Selection.Range.HighlightColorIndex = myHLcolour
    End If
  Else
    Selection.Start = endPara
  End If
  Selection.Start = endPara + Len(myDate) + 1
Loop Until endPara = ActiveDocument.Range.End
Selection.Start = Selection.End
End Sub



ET
Sub ShortTitleLister()
' Version 19.05.13
' Create a list of the named references in the notes

' myPattern1 = "<[A-Z][a-zA-Z]{1,} van [A-Z.]{1,}[ ,]"
' myPattern2 = "<[A-Z][a-zA-Z]{1,} [A-Z.]{1,}[ ,]"

' Smith J P[ ,] or Smith J. P.[ ,]
' myPattern = "<[A-Z][a-zA-Z]{1,}[ ,]{1,2}[A-Z. ]{1,}[ ,]"

' J P Smith[ ,] or J. P. Smith[ ,]
' myPattern = "<[A-Z. ]{1,} [A-Z][a-zA-Z]{1,}[ ,]"

' Smith, JP=[ ,] or Smith, J.P.[ ,]
' myPattern = "<[A-Z][a-zA-Z]{1,}[ ,]{1,2}[A-Z.]{1,}[ ,]"

' JP Smith[ ,] or J.P. Smith[ ,]
myPattern = "<[A-Z.]{1,} [A-Z][a-zA-Z]{1,}[ ,]"

myInitials = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"


refLength = 40

myResponse = MsgBox("Test run first?", vbQuestion _
        + vbYesNoCancel, "Short Reference Lister")
If myResponse = vbCancel Then Exit Sub
doTest = (myResponse = vbYes)
If ActiveDocument.Footnotes.Count > 0 Then
  ActiveDocument.StoryRanges(wdFootnotesStory).Copy
Else
  If ActiveDocument.Endnotes.Count = 0 Then
    Beep
    myResponse = MsgBox("Can't find any notes. Is this the correct file?", _
        vbQuestion + vbYesNoCancel, "Short Reference Lister")
    Exit Sub
  Else
    ActiveDocument.StoryRanges(wdEndnotesStory).Copy
  End If
End If
Documents.Add
Selection.Paste

myPattern1 = Replace(myPattern, "[A-Z][a-zA-Z]", "[derVDivan ]{2,8}[A-Z][a-zA-Z]")

' Highlight pattern1 for van/de/di etc
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = myPattern1
  .Highlight = False
  .Wrap = False
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = True
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute
End With
myCount = 0
Do While rng.Find.Found = True
  rng.MoveStartWhile cset:=myInitials & " .", Count:=wdBackward
  rng.MoveStart , 1
  If Asc(rng.Text) = Asc(".") Then rng.MoveStart , 1
  If Asc(rng.Text) = Asc(" ") Then rng.MoveStart , 1
  rng.End = rng.End + refLength
    dhfjk = rng.Font.Color
    rng.Select
  If rng.Font.Color = wdColorAutomatic Then
    rng.HighlightColorIndex = wdGray25
  End If
  rng.Collapse wdCollapseEnd
  rng.Find.Execute
Loop

' Main highlight pattern
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = myPattern
  .Highlight = False
  .Wrap = False
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = True
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute
End With
myCount = 0
Do While rng.Find.Found = True
  rng.MoveStartWhile cset:=myInitials & " .", Count:=wdBackward
  rng.MoveStart , 1
  If Asc(rng.Text) = Asc(".") Then rng.MoveStart , 1
  If Asc(rng.Text) = Asc(" ") Then rng.MoveStart , 1
  rng.End = rng.End + refLength
  crPos = InStr(rng.Text, vbCr)
  If crPos > 0 Then rng.End = rng.Start + crPos - 1
  If rng.Font.Color = wdColorAutomatic Then
    rng.HighlightColorIndex = wdGray25
  End If
  rng.Collapse wdCollapseEnd
  rng.Find.Execute
Loop

If doTest = True Then Exit Sub

' Split file at the beginning of each highlight
Set rng = ActiveDocument.Content
With rng.Find
  .Wrap = wdFindContinue
  .Text = ""
  .Highlight = True
  .Replacement.Text = "^p^&"
  .Replacement.Highlight = False
  .Forward = True
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Wrap = wdFindContinue
  .Text = Chr(2) & " "
  .Replacement.Text = "zzzz"
  .Forward = True
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

Selection.HomeKey Unit:=wdStory
Selection.TypeText Text:="zzzz!!!!" & vbCr
Selection.WholeStory
Selection.Sort
Selection.HomeKey Unit:=wdStory

Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Wrap = wdFindContinue
  .Text = "zzzz" & "^p"
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Wrap = wdFindContinue
  .Text = "zzzz"
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With
ln = "~~~~~~~~~~~~~~~~~~~~"
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Wrap = wdFindContinue
  .Text = "!!!!"
  .Replacement.Text = "^p" & ln & ln & ln
  .Forward = True
  .MatchWildcards = False
  .Execute Replace:=wdReplaceOne
End With

With rng.ParagraphFormat
  .LeftIndent = CentimetersToPoints(4)
  .SpaceBeforeAuto = False
  .SpaceAfterAuto = False
  .SpaceBeforeAuto = False
  .SpaceAfterAuto = False
  .FirstLineIndent = CentimetersToPoints(-4)
End With
Selection.HomeKey Unit:=wdStory
Do While Selection = Chr(13)
  Selection.MoveEnd , 1
  Selection.Delete
Loop
Selection.HomeKey Unit:=wdStory
End Sub


ET
Sub AuthorCaseChange()
' Version 09.11.13
' lowercase author surname (e.g. SMITH, J. to Smith, J.) in references list

Selection.Collapse wdCollapseEnd
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "([A-Z]{3,}), ([A-Z])."
  .Wrap = False
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = True
  .Execute
End With

myCount = 0
Do While Selection.Find.Found = True
  Selection.MoveStart , 1
  Selection.MoveEnd , -4
  Selection.Range.Case = wdLowerCase
  Selection.Collapse wdCollapseEnd
  Selection.Find.Execute
Loop
End Sub


ET
Sub AuthorDateFormatter()
' Version 25.06.14
' Check/correct author/date formatting of reference list

' Is the date with the names? (i.e. for Vancouver date near end use False)
dateWithNames = True
addVancouverDate = False

' Create a separate list of all the changes?
createChangesList = True

' Is the list double-spaced?
listIsDoubleSpaced = False

' Highlight colour on query
myColour = wdYellow

' List of words likely to occur in multi-word surnames
avoidWords = "de la le ten den der du van von et al"

' Highlight references with 5+ authors
manyAuthorHighlight = True
manyAuthorColour = wdGray25


' Choose your set of options
optionSet = 1

Select Case optionSet
   
Case 1: ' First set of options

    ' Spaced initials?
    spaceInits = False
    ' Add a full point after each initial?
    fullPtInits = True
    
    ' Comma after surname?
    commaAfterSurname = True

    ' Use parentheses on the date?
    parensOnDate = True
    
    ' Punctuation after the date?
    punctuationAfterDate = ""
    ' punctuationAfterDate = "."
    ' punctuationAfterDate = ","
    
    ' Punctuation before the date?
    punctuationB4Date = ""
    ' punctuationB4Date = "."
    ' punctuationB4Date = ","

    ' Initials before or after name on second and succeeding names?
    initsBeforeName = False

    ' Use serial comma?
    serialComma = False

    ' Use 'and' with two authors? If so, which format?
    myFirstAnd = " and "
     myFirstAnd = " & "
    ' myFirstAnd = " "

    ' Use 'and' with three or more authors? If so, which format?
    myFinalAnd = " and "
     myFinalAnd = " & "
    ' myFinalAnd = " "

    ' Format for saying "Editors"
    edText = "(Editor)"
    edsText = "(Editors)"

    ' If needed, et al.
    etAlText = ", et al. "

Case 2: ' Second set of options

    ' Spaced initials?
    spaceInits = True
    ' Add a full point after each initial?
    fullPtInits = True

    ' Comma after surname?
    commaAfterSurname = True

    ' Use parentheses on the date?
    parensOnDate = True
    
    ' Any punctuation after the date?
    ' punctuationAfterDate = ""
    ' punctuationAfterDate = "."
    punctuationAfterDate = ","
    
    ' Any punctuation before the date?
    punctuationB4Date = ""
    'punctuationB4Date = "."
    ' punctuationB4Date = ","

    ' Initials before or after name on second and succeeding names?
    initsBeforeName = True

    ' Use serial comma?
    serialComma = False

    ' Allow for the list to be double-spaced
    listIsDoubleSpaced = False

    ' Use 'and' with two authors? If so, which format?
    myFirstAnd = " and "
    ' myFirstAnd = " & "
    ' myFirstAnd = " "

    ' Use 'and' with three or more authors? If so, which format?
    myFinalAnd = " and "
    ' myFinalAnd = " & "
    ' myFinalAnd = " "

    ' Format for saying "Editors"
    edText = "(ed.)"
    edsText = "(eds)"

    ' If needed, et al.
    etAlText = ", et al. "
End Select

' create list of avoidWords
allAvoidWords = avoidWords
avoidWords = LCase(Trim(avoidWords)) & " "
numNoWds = Len(avoidWords) - Len(Replace(avoidWords, " ", ""))
ReDim notWords(numNoWds) As String
For i = 1 To numNoWds
  spPos = InStr(avoidWords, " ")
  notWords(i) = Left(avoidWords, spPos - 1) & " "
  avoidWords = Mid(avoidWords, spPos + 1)
Next i

' Start of main program
CR = vbCr: CR2 = CR & CR
If serialComma = True And myFinalAnd > " " Then
  myFinalAnd = "," & myFinalAnd
End If

If createChangesList = True Then
  gottaList = False
  Set mainList = ActiveDocument
  For Each myWnd In Application.Windows
    Set myDoc = myWnd.Document
    myDoc.Activate
      Set rng = ActiveDocument.Content
    rng.End = rng.Start + 15
    If InStr(rng.Text, "Changes list") > 0 Then
      gottaList = True
      Exit For
    End If
  Next myWnd
  If gottaList = False Then
    Documents.Add
    Selection.TypeText Text:="Changes list" & CR2
  End If
  Selection.TypeText Text:="------------------------------" & CR
  Set changeList = ActiveDocument
  mainList.Activate
End If

initsDelim = "": If fullPtInits = True Then initsDelim = "."
initsSpace = "": If spaceInits = True Then initsSpace = " "

maxAuNums = 20
Selection.Collapse wdCollapseStart
Selection.Expand wdParagraph
lenPara = Len(Selection)

' Change square brackets to parentheses
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "\[([0-9]{4})\]"
  .Wrap = wdFindContinue
  .Replacement.Text = "\1"
  .Forward = True
  .MatchWildcards = True
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "\[([0-9]{4}[a-z])\]"
  .Wrap = wdFindContinue
  .Replacement.Text = "\1"
  .Forward = True
  .MatchWildcards = True
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute Replace:=wdReplaceAll
End With

hereNow = Selection.Start
Selection.EndKey Unit:=wdStory
Selection.MoveStart , -1
If Asc(Selection) <> 13 Then
  Selection.Collapse wdCollapseEnd
  Selection.TypeParagraph
End If

Selection.Start = hereNow
Selection.Collapse wdCollapseStart

Do While lenPara > 5
  ReDim myWd(maxAuNums * 2) As String
  ReDim extraWord(maxAuNums * 2) As String
  ReDim auName(maxAuNums) As String
  ReDim auInits(maxAuNums) As String
  badData0 = False
  badData1 = False
  badData2 = False
  badData3 = False
  badData4 = False
  badData5 = False
  badData = False
  theFinalAnd = myFinalAnd
  gotEtAl = False
  paraStart = Selection.Start
  With Selection.Find
    .Text = "[\(\)0-9]{4,}"
    .Wrap = False
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = True
    .Execute
  End With
  
  If Selection.Find.Found = False Then
    Selection.MoveLeft 1
    Selection.Expand wdParagraph
    myResponse = MsgBox("No date." & CR2 & "Click 'Yes' to continue; " & _
         "'No' to highlight; 'Cancel' to exit.", _
         vbQuestion + vbYesNoCancel, "AuthorDateFormatter")
    If myResponse = vbYes Then GoTo nextItem
    If myResponse = vbNo Then
      Selection.Expand wdParagraph
      Selection.Range.HighlightColorIndex = myColour
      GoTo nextItem
    End If
    If myResponse = vbCancel Then GoTo cleanEnd
  End If
  'Put date into correct format
  Selection.MoveEndWhile cset:=",.) abcdefg", Count:=wdForward
  If Right(Selection, 1) = " " Then
    Selection.MoveEnd , -1
  Else
    Set rng = ActiveDocument.Content
    rng.End = Selection.End
    rng.InsertAfter " "
  End If
  myOldDate = Selection

  myDate = Replace(Trim(myOldDate), "(", "")
  myDate = Replace(myDate, ")", "")
  myDate = Replace(myDate, ",", "")
  myDate = Replace(myDate, ".", "")
  If parensOnDate = True Then myDate = "(" & myDate & ")"
  myDate = " " & punctuationB4Date & myDate & punctuationAfterDate
  
  ' If no date
  Selection.Start = paraStart
  If InStr(Selection, Chr(13)) > 0 Then
    Selection.Collapse wdCollapseStart
    With Selection.Find
      .Text = "\(*\)"
      .Execute
    End With
  
  ' Put date into correct format
    myDate = Selection

    If InStr(Selection, Chr(13)) > 0 Then
      Selection.Collapse wdCollapseStart
      GoTo stopIT
    End If
  End If

  Selection.Collapse wdCollapseEnd
  Do While Asc(Selection) <> 32
   Selection.MoveRight , 1
  Loop
  Selection.Start = paraStart

  If dateWithNames = False Then
    loopStop = False
    Do
      Selection.MoveStart wdWord, 1
      myTestWd = Trim(Selection.Words(2))
      If myTestWd = "-" Then myTestWd = Trim(Selection.Words(3))
      If (LCase(myTestWd) = myTestWd) And (UCase(myTestWd) <> myTestWd) Then
      loopStop = True
      End If
      If InStr(allAvoidWords, myTestWd) > 0 Then loopStop = False
    Loop Until loopStop = True
    Selection.Collapse wdCollapseStart
    Selection.Start = paraStart
  End If


  ' Move past the initial number (for Vancouver)
  Do While Asc(Selection) < 65 Or Asc(Selection) = 91 Or Asc(Selection) = 93
    Selection.MoveStart , 1
  Loop
  paraStart = Selection.Start
  
  myText = Selection
  originalText = myText
  If InStr(myText, "et al") > 0 Then
    theEtAlText = etAlText
    myText = Replace(myText, "et al. ", "")
    myText = Replace(myText, "et al ", "")
    theFinalAnd = ", "
  Else
    theEtAlText = ""
    theFinalAnd = myFinalAnd
  End If
  
  For i = 1 To numNoWds
    If LCase(Selection.Range.Words(1)) = notWords(i) Then
      spPos = Len(notWords(i))
      myText = Left(myText, spPos - 1) & ChrW(124) & Mid(myText, spPos + 1)
      i = numNoWds
    End If
  Next i
  
  ' Check for II III and IV
  If InStr(myText, " II") > 0 Or InStr(myText, " II") > 0 Then
    badData0 = True
    GoTo badDataHere
  End If

  ' Check for "(editors)" etc
  justNames = Replace(myText, myOldDate, "")
  parenPosn = InStr(justNames, "(")
  If parenPosn > 0 Then
    parenText = Trim(Mid(justNames, parenPosn))
    justNames = Replace(justNames, parenText, "")
    If InStr(LCase(parenText), "ed") > 0 Then
      If InStr(LCase(parenText), "s") > 0 Then
        parenText = " " & edsText
      Else
        parenText = " " & edText
      End If
    End If
    myDate = parenText & myDate
  End If

' remove "and"
  myText = Replace(justNames, " and ", ", ")
  myText = Replace(myText, " & ", ", ")
  
' Sort out and switch the first surname and initials
  i = 1
  noSpace = False
  Do
    i = i + 1
    x = Mid(myText, i, 1)
    gotOne = (x = " ")
    ' check if you've fallen off the end
    noSpace = (i > Len(myText))
  Loop Until gotOne Or noSpace = True
  
  If noSpace = True Then GoTo badDataHere

  name1 = Trim(Left(myText, i - 1)) & " "
  myText = Mid(myText, i + 1)

  ' Is this a name or initials
  If UCase(name1) = name1 Then
  ' Initials before name
    inits1 = name1
    myText = Trim(myText) & " "
    i = 1
    Do
      i = i + 1
      x = Mid(myText, i, 1)
      ' Check for final lowercase
    Loop Until UCase(x) = x
    name1 = Trim(Left(myText, i)) & " "
  Else
  ' Name first, then initials
    i = 1
    endInits = False
    Do
      i = i + 1
      x = Mid(myText, i, 1)
      ' Check for a lowercase char in the inits
      If LCase(x) = x And UCase(x) <> x Then badData5 = True
      ' Check for a comma or an and or
      If x = "&" Or x = "," Or Mid(myText, i, 4) = " and" Then
        endInits = True
      Else
        xy = Mid(myText, i, 3)
        If UCase(xy) <> xy And LCase(xy) <> xy Then endInits = True
      End If
    Loop Until endInits = True Or i >= Len(myText)
    inits1 = Trim(Left(myText, i)) & " "
  End If
  myText = inits1 & name1 & Trim(Mid(myText, i)) & " "


  ' count the number of spaces = number of "words"
  myText = Replace(myText, ".-", "-")
  myText = Replace(myText, ",", " ")
  myText = Replace(myText, ".", " ")
  myText = Replace(myText, "  ", " ")
  myText = Replace(myText, "  ", " ")
  myText = Replace(myText, "  ", " ")
  numWds = Len(myText) - Len(Replace(myText, " ", ""))
  spPos = 0
  isInst = True
  j = 1
  For i = 1 To numWds
    myText = Mid(myText, spPos + 1)
    spPos = InStr(myText, " ")
    aBit = Left(myText, spPos - 1)
    lenBit = Len(aBit)
    If UCase(aBit) = aBit Then
      isInst = False: ' We've got at least one initial
      For q = 1 To lenBit
        myWd(j) = Mid(aBit, q, 1)
        j = j + 1
      Next q
    Else
      myWd(j) = aBit
      j = j + 1
    End If
  Next i
  numWds = j - 1
  doCheckData = True
  If isInst = True Then
    myResponse = MsgBox("I can't work this one out, sorry" & CR2 _
         & "Click 'Yes' to continue; " & _
         "'No' to highlight; 'Cancel' to exit.", _
         vbQuestion + vbYesNoCancel, "AuthorDateFormatter")
    Selection.Expand wdParagraph
    If myResponse = vbNo Then
      Selection.Range.HighlightColorIndex = myColour
      Selection.Collapse wdCollapseEnd
      GoTo nextItem
    End If
    If myResponse = vbCancel Then GoTo cleanEnd
    If myResponse = vbYes Then GoTo nextItem
  End If

' Divide data into sets of initials or names
  i = 1
  j = 1
  setOfInits = ""

  Do While i <= numWds
    If Len(myWd(i)) = 1 Then
      setOfInits = setOfInits & myWd(i)
      If myWd(i) <> "-" Then setOfInits = setOfInits & _
           initsDelim & initsSpace
    Else
      If setOfInits > "" Then
        extraWord(j) = Replace(setOfInits, " -", "-")
        j = j + 1
        extraWord(j) = myWd(i)
        j = j + 1
        setOfInits = ""
      Else
        extraWord(j) = myWd(i)
        j = j + 1
      End If
    End If
    i = i + 1
  Loop
  If setOfInits > "" Then
    extraWord(j) = Replace(setOfInits, " -", "-")
    j = j + 1
  End If

' Check for non-paired items
  numWds = j - 1
  If numWds / 2 > Int(numWds / 2) Then badData1 = True

' Check for "editors" without parens
  If badData1 = True Then
    If Left(LCase(extraWord(numWds)), 2) = "ed" Then
      If InStr(extraWord(numWds), "s") Then
        myDate = " " & edsText & myDate
      Else
        myDate = " " & edText & myDate
      End If
      badData1 = False
      numWds = numWds - 1
    End If
  End If

  myTest = LCase(" " & myTest)
  myTest = Replace(myTest, ",", " ")
  myTest = Replace(myTest, ".", " ")
  
' Check for multi-word surnames
  For i = 1 To numNoWds
    If InStr(testBit, notWords(i)) Then badData2 = True
  Next i

' Put the names in one array and the initials in another
  auInits(1) = Trim(extraWord(1))
  auName(1) = extraWord(2)
  auNum = 2
  i = 3
  mismatchedData = False
  Do While i <= numWds
    wordOne = extraWord(i)
    wordTwo = extraWord(i + 1)
    i = i + 2
    initsOne = (UCase(wordOne) = wordOne)
    initsTwo = (UCase(wordTwo) = wordTwo)
    If ((initsOne = True And initsTwo = True) Or (initsOne = False _
          And initsTwo = False)) Then mismatchedData = True
    If initsOne = True Then
      auInits(auNum) = wordOne
      auName(auNum) = wordTwo
    Else
      auInits(auNum) = wordTwo
      auName(auNum) = wordOne
    End If
    auNum = auNum + 1
  Loop
  If mismatchedData = True Then badData3 = True

' Check for the word "ed(itors)" NOT in parentheses
  totAuNum = auNum - 1
  If badData3 = True Then
    If Left(LCase(auName(auNum)), 2) = "ed" Then
      totAuNum = auNum - 1
      If InStr(auName(auNum), "s") > 0 Then
        myDate = " " & edsText & myDate
      Else
        myDate = " " & edText & myDate
      End If
      badData3 = False
    End If
  End If

' Make up the new data as a string of names/inits
  surnameSpacer = " "
  If commaAfterSurname = True Then surnameSpacer = ", "


  If UCase(auName(1)) = auName(1) Then
    initsOne = auName(1)
    auName(1) = auInits(1)
    auInits(1) = initsOne
  End If
  newText = auName(1) & surnameSpacer & auInits(1)
  auNum = 2
  Do While auNum < totAuNum
    If initsBeforeName = True Then
      newText = newText & ", " & Trim(auInits(auNum)) & " " _
           & auName(auNum)
    Else
      newText = newText & ", " & auName(auNum) & surnameSpacer _
           & Trim(auInits(auNum))
    End If
    auNum = auNum + 1
  Loop

' Add the second or final name and initials
  Select Case totAuNum
    Case 1: ' do nothing
    Case 2:
    If initsBeforeName = True Then
      newText = newText & myFirstAnd & Trim(auInits(2)) & " " & auName(2)
    Else
      newText = newText & myFirstAnd & auName(2) & surnameSpacer & Trim(auInits(2))
    End If
  Case Else
    ' add final author at end of string
    If initsBeforeName = True Then
      newText = newText & theFinalAnd & Trim(auInits(totAuNum)) _
           & " " & auName(totAuNum)
    Else
      newText = newText & theFinalAnd & auName(totAuNum) & surnameSpacer _
           & Trim(auInits(totAuNum))
    End If
  End Select
  If auInits(totAuNum) = "" Then badData4 = True
  
  If dateWithNames = False And addVancouverDate = False Then myDate = " "
  
' Tidy up and add date
  newText = Replace(newText, "  ", " ") & theEtAlText & myDate & " "
  newText = Replace(newText, "  ", " ")
  newText = Replace(newText, " ,", ",")
  newText = Replace(newText, "..", ".")
  newText = Replace(newText, ChrW(124), " ")
  
badDataHere:
  badData = noSpace Or badData0 Or badData1 Or badData2 Or _
       badData3 Or badData4 Or badData5
  If newText <> originalText Then
    If badData = False Then
      Selection.TypeText Text:=newText
      If createChangesList = True Then
        changeList.Activate
        start1 = Selection.Start
        Selection.TypeText Text:=originalText & CR
        end1 = Selection.Start
        start2 = Selection.Start
        Selection.TypeText Text:=newText & CR
        end2 = Selection.Start
        len1 = Len(originalText)
        len2 = Len(newText)
        maxCheck = len1
        If len1 > len2 Then maxCheck = len2

        For i = 1 To maxCheck
          If Mid(originalText, i, 1) <> Mid(newText, i, 1) _
               Then Exit For
        Next i
        firstDiff = i
        For i = 0 To maxCheck - 1
          If Mid(originalText, len1 - i, 1) <> Mid(newText, len2 - i, 1) _
               Then Exit For
        Next i
        lastDiff = i
        Selection.Start = start1 + firstDiff - 1
        Selection.End = start2 - lastDiff - 1
        Selection.Range.HighlightColorIndex = myColour
        Selection.Start = start2 + firstDiff - 1
        Selection.End = end2 - lastDiff - 1
        Selection.Range.HighlightColorIndex = myColour
        Selection.Start = end2
        Selection.TypeParagraph
        mainList.Activate
      End If
    End If
  End If
  If badData = True Then
    Selection.Start = paraStart
    myResponse = MsgBox("I can't work this one out, sorry." & CR2 _
         & "Click 'Yes' to continue; " & _
         "'No' to highlight; 'Cancel' to exit.", vbQuestion _
         + vbYesNoCancel, "AuthorDateFormatter")
    If myResponse = vbCancel Then GoTo cleanEnd
    If myResponse = vbNo Then
      Selection.Expand wdParagraph
      Selection.Range.HighlightColorIndex = myColour
    End If
  End If
  
  Selection.Start = Selection.End - 15
  etAlPos = InStr(Selection, "et al")
  If etAlPos > 0 Then
    Selection.MoveStart , etAlPos - 1
    Selection.End = Selection.Start + 5
    Selection.Font.Italic = True
    etAlPos = 0
  End If
  Selection.Expand wdParagraph
  If manyAuthorHighlight = True And totAuNum > 4 Then _
       Selection.Range.HighlightColorIndex = manyAuthorColour
nextItem:
  Selection.Collapse wdCollapseEnd
  Selection.Expand wdParagraph
  lenPara = Len(Selection)
  If listIsDoubleSpaced = True Then
    If lenPara > 5 Then GoTo stopIT
    Selection.Collapse wdCollapseEnd
    Selection.Expand wdParagraph
    lenPara = Len(Selection)
  End If
Loop

Beep
cleanEnd:
Selection.Collapse wdCollapseEnd
Exit Sub

stopIT:
If listIsDoubleSpaced = True Then
  myResponse = MsgBox("I was expecting double-spaced references" _
       & CR2 & "Set listIsDoubleSpaced = False for single-spaced" _
       , , "AuthorDateFormatter")
Else
  Beep
End If
myTime = Timer
Do
Loop Until Timer > myTime + 0.1
Beep
Selection.Collapse wdCollapseStart
End Sub

ET
Sub AuthorNameSwap()
' Version 01.10.14
' Change author surname and initials/given name

avoidWords = "the "


' create list of avoidWords
avoidWords = LCase(Trim(avoidWords)) & " "
numNoWds = Len(avoidWords) - Len(Replace(avoidWords, " ", ""))
ReDim notWords(numNoWds) As String
For i = 1 To numNoWds
  spPos = InStr(avoidWords, " ")
  notWords(i) = Left(avoidWords, spPos - 1) & " "
  avoidWords = Mid(avoidWords, spPos + 1)
Next i
maxAuNums = 4

Selection.Collapse wdCollapseStart
Selection.Expand wdParagraph
lenPara = Len(Selection)

Do Until 0
  If lenPara < 2 Then
    Selection.Collapse wdCollapseEnd
    Selection.Expand wdParagraph
    lenPara = Len(Selection)
    If lenPara < 2 Then
      Selection.Collapse wdCollapseEnd
      Beep
      Exit Sub
    End If
  End If
  ReDim myWd(maxAuNums * 2) As String
  ReDim extraWord(maxAuNums * 2) As String
  ReDim auName(maxAuNums) As String
  ReDim auInits(maxAuNums) As String
  paraStart = Selection.Start
  With Selection.Find
    .Text = "[.,\(]"
    .Wrap = False
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = True
    .Execute
  End With
  Selection.Collapse wdCollapseStart
  Selection.Start = paraStart
  
  tryThisOne = True
  allNames = Selection.Text
  If Selection.Font.Italic = True Then tryThisOne = False
  If InStr(allNames, Chr(34)) > 0 Then tryThisOne = False
  If InStr(allNames, "HYPERLINK") > 0 Then tryThisOne = False
  
  If tryThisOne = True Then
    maxWds = Selection.Words.Count
    longWds = 0
    For i = 1 To maxWds
      myWd(i) = Selection.Words(i)
      If Len(myWd(i)) > 2 Then longWds = longWds + 1
    Next i
    If maxWds < 5 And InStr(allNames, ",") > 0 Then
      forename = myWd(maxWds)
      surname = myWd(maxWds - 2)
      Selection.Words(maxWds) = surname
      Selection.Words(maxWds - 2) = forename
      Selection.Words(maxWds - 1) = " "
    End If
  End If


nextItem:
  Selection.Expand wdParagraph
  Selection.Collapse wdCollapseEnd
  Selection.Expand wdParagraph
  lenPara = Len(Selection)
Loop

Beep
cleanEnd:
Selection.Collapse wdCollapseEnd
Exit Sub

stopIT:
If listIsDoubleSpaced = True Then
  myResponse = MsgBox("I was expecting double-spaced references" _
       & CR2 & "Set listIsDoubleSpaced = False for single-spaced" _
       , , "AuthorDateFormatter")
Else
  Beep
End If
myTime = Timer
Do
Loop Until Timer > myTime + 0.1
Beep
Selection.Collapse wdCollapseStart
End Sub

ET
Sub SwapNames()
' Version 06.10.14
' Swap adjacent names

addComma = True
fpOnInitials = True
minLen = 50

startAgain:
startPoint = Selection.Start
Selection.Expand wdParagraph
If Len(Selection) < minLen Then GoTo cantDoIt
myText = Replace(Selection, ".", "")
myText = Replace(myText, ",", "")

wd = Split(myText, " ")


startHere = Selection.Start

If addComma = True Then w2 = w2 & ","

' This is for Fred Bloggs
If Len(wd(0)) > 1 And Len(wd(1)) > 1 Then
  secondNameEnd = InStr(Selection, wd(1)) + Len(wd(1))
  If addComma = True Then wd(1) = wd(1) & ","
  Selection.End = startHere + secondNameEnd - 1
  Selection.TypeText Text:=wd(1) & " " & wd(0)
  GoTo getNext
End If

' This is for F. Bloggs
If Len(wd(0)) = 1 And Len(wd(1)) > 1 Then
  secondNameEnd = InStr(Selection, wd(1)) + Len(wd(1))
  If addComma = True Then wd(1) = wd(1) & ","
  Selection.End = startHere + secondNameEnd - 1
  If fpOnInitials = True Then
    wd(0) = wd(0) & "."
  End If
  Selection.TypeText Text:=wd(1) & " " & wd(0)
  GoTo getNext
End If

' If none of the above!
cantDoIt:
Beep
Exit Sub

getNext:
Selection.Expand wdParagraph
lenPara = Len(Selection)
Selection.Collapse wdCollapseEnd
endPoint = Selection.Start
If lenPara > minLen And endPoint <> startPoint Then GoTo startAgain
Beep
End Sub

ET
Sub YearMoveToEnd()
' Version 10.05.14
' Move the year to end of reference

' delete this many characters before the year
cutBefore = 2
' delete this many characters after the year
cutAfter = 1

' add this text before the year
textBefore = ", ("
' add this text after the year
textAfter = ")"

Set rng = ActiveDocument.Content
rng.Start = Selection.Start
Do
  rng.Expand wdParagraph
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "^#^#^#^#"
    .MatchWildcards = False
    .Execute
  End With
  If rng.Find.Found = False Then rng.Select: Beep:  Exit Sub
  rng.Copy
  rng.MoveEnd , cutAfter
  rng.MoveStart , -cutBefore
  rng.Delete
  rng.Expand wdParagraph
  rng.MoveEnd , -2
  rng.Collapse wdCollapseEnd
  rng.Select
  Selection.TypeText textBefore
  Selection.Paste
  Selection.TypeText textAfter
  rng.Expand wdParagraph
  rng.Collapse wdCollapseEnd
Loop Until rng.End = ActiveDocument.Range.End - 1
Beep
End Sub

ET
Sub Yesterdate()
' Version 09.01.12
' Type yesterday's date into the text

Selection.TypeText Text:=Format(Date - 1, "d mmmm yyyy")
End Sub



ET
Sub CompareTexts()
' Version 07.12.13
' Select non-identical text (was called IdenticalTextCheck)

myColour = wdGray25
myText = Selection
Selection.Range.HighlightColorIndex = wdNoHighlight

myStart = Selection.Start
myEnd = Selection.End
Selection.End = Selection.Start
Selection.Paste
Selection.Start = myStart
myOtherText = Selection
WordBasic.EditUndo

If myText = myOtherText Then
  Beep
Else
  For myCount = 1 To Len(myText)
    If Left(myText, myCount) <> Left(myOtherText, myCount) Then Exit For
  Next myCount
  myChangedStart = myCount - 1
 
  For myCount = 1 To Len(myText)
    If Right(myText, myCount) <> Right(myOtherText, myCount) Then Exit For
  Next myCount
  myChangedEnd = myCount - 1
 
  Selection.Start = myEnd - myChangedEnd
  Selection.End = myEnd
  Selection.Range.HighlightColorIndex = myColour
 
  Selection.Start = myStart
  Selection.End = myStart + myChangedStart
  Selection.Range.HighlightColorIndex = myColour

  Selection.Collapse wdCollapseEnd
End If
End Sub


ET
Sub SpaceBeforeSelector()
' Version 18.02.13
' Increase and decrease space before paragraph

myValues = "0,3,6,9,12,0"
' myValues = "0,12,9,6,3,0"
' myValues = "0,6,12,0"
' myValues = "6,9,6"

spNow = Trim(Str(Selection.ParagraphFormat.SpaceBefore))
myValues = "," & myValues & ","
myStart = InStr(myValues, "," & spNow & ",")
If myStart = 0 Then myStart = 0: spNow = ""
spNext = Mid(myValues, myStart + Len(spNow) + 2)
spNext = Left(spNext, InStr(spNext, ",") - 1)
Selection.ParagraphFormat.SpaceBefore = Val(spNext)
End Sub


ET
Sub SpaceAfterSelector()
' Version 18.02.13
' Increase and decrease space after paragraph

myValues = "0,3,6,9,12,0"
' myValues = "0,12,9,6,3,0"
' myValues = "0,6,12,0"
' myValues = "6,9,6"

spNow = Trim(Str(Selection.ParagraphFormat.SpaceAfter))
myValues = "," & myValues & ","
myStart = InStr(myValues, "," & spNow & ",")
If myStart = 0 Then myStart = 0: spNow = ""
spNext = Mid(myValues, myStart + Len(spNow) + 2)
spNext = Left(spNext, InStr(spNext, ",") - 1)
Selection.ParagraphFormat.SpaceAfter = Val(spNext)
End Sub


ET
Sub IndentsMeasure()
' Version 01.02.13
' Measure and display current indent settings

lft = PointsToCentimeters(Selection.ParagraphFormat.LeftIndent)
fst = PointsToCentimeters(Selection.ParagraphFormat.FirstLineIndent)
rt = PointsToCentimeters(Selection.ParagraphFormat.RightIndent)
lft = Int(lft * 100 + 0.5) / 100
fst = Int(fst * 100 + 0.5) / 100
rt = Int(rt * 100 + 0.5) / 100
MsgBox ("Left = " & lft & "    First = " & fst & "    Right = " & rt)
End Sub

ET
Sub IndentsSet()
' Version 01.02.13
' Set indent settings

Selection.ParagraphFormat.LeftIndent = CentimetersToPoints(1.27)
Selection.ParagraphFormat.FirstLineIndent = CentimetersToPoints(-1.27)
Selection.ParagraphFormat.RightIndent = CentimetersToPoints(0)
End Sub




ET
Sub IndentLeftSelector()
' Version 18.02.13
' Set left indent settings to specific values

myValues = "0,0.5,1,-1,-0.5,0"
' myValues = "0,-1,1,0"
' myValues = "0,1.27,-1.27"
 myValues = "1"

Dim myValue(10)
myValues = myValues & ","
myValues = Replace(myValues, ",,", ",")
numItems = Len(myValues) - Len(Replace(myValues, ",", ""))
myValues = myValues & Left(myValues, InStr(myValues, ",") - 1) & ","

For i = 0 To numItems
  leftText = Left(myValues, InStr(myValues, ",") - 1)
  myValue(i) = Val(leftText)
  myValues = Mid(myValues, Len(leftText) + 2)
Next i
myValue(i + 1) = myValue(0)
indentNow = PointsToCentimeters(Selection.ParagraphFormat.LeftIndent)
indentNow = Int(indentNow * 100 + 0.5) / 100

For i = 0 To numItems
  If myValue(i) = indentNow Then Exit For
Next i
Selection.ParagraphFormat.LeftIndent = CentimetersToPoints(myValue(i + 1))
End Sub

ET
Sub IndentFirstSelector()
' Version 18.02.13
' Set left indent settings to specific values

myValues = "0,0.5,1,-1,-0.5,0"
' myValues = "0,-1,1,0"
' myValues = "0,1.27,-1.27,0"
' myValues = "1"

Dim myValue(10)
myValues = myValues & ","
myValues = Replace(myValues, ",,", ",")
numItems = Len(myValues) - Len(Replace(myValues, ",", ""))
myValues = myValues & Left(myValues, InStr(myValues, ",") - 1) & ","

For i = 0 To numItems
  leftText = Left(myValues, InStr(myValues, ",") - 1)
  myValue(i) = Val(leftText)
  myValues = Mid(myValues, Len(leftText) + 2)
Next i
myValue(i + 1) = myValue(0)
indentNow = PointsToCentimeters(Selection.ParagraphFormat.FirstLineIndent)
indentNow = Int(indentNow * 100 + 0.5) / 100

For i = 0 To numItems
  If myValue(i) = indentNow Then Exit For
Next i
Selection.ParagraphFormat.FirstLineIndent = CentimetersToPoints(myValue(i + 1))
End Sub

ET
Sub IndentRightSelector()
' Version 18.02.13
' Set left indent settings to specific values

myValues = "0,2,4,6"
' myValues = "0,1,2,0"
' myValues = "0,0.5,1,0"

Dim myValue(10)
myValues = myValues & ","
myValues = Replace(myValues, ",,", ",")
numItems = Len(myValues) - Len(Replace(myValues, ",", ""))
myValues = myValues & Left(myValues, InStr(myValues, ",") - 1) & ","

For i = 0 To numItems
  leftText = Left(myValues, InStr(myValues, ",") - 1)
  myValue(i) = Val(leftText)
  myValues = Mid(myValues, Len(leftText) + 2)
Next i
myValue(i + 1) = myValue(0)
indentNow = PointsToCentimeters(Selection.ParagraphFormat.RightIndent)
indentNow = Int(indentNow * 100 + 0.5) / 100

For i = 0 To numItems
  If myValue(i) = indentNow Then Exit For
Next i
Selection.ParagraphFormat.RightIndent = CentimetersToPoints(myValue(i + 1))
End Sub





EI
Sub WhatChar()
' Version 26.10.13
' Show ASCII and unicode and character names

' Decide where to show results
showOnStatusbar = True
showInMessageBox = True
' If you aslo want to know what the hex of the ANSI code is:
showHexANSI = True

If Selection.Start = Selection.End Then Selection.MoveEnd wdCharacter, 1
ansicode = Asc(Selection)
ucodeOLD = AscW(Selection)
uCode = Val(Dialogs(wdDialogInsertSymbol).charnum)
myStyleish = Selection.Font.Name
If Asc(ansicode) = 63 And uCode <> 63 Then
  ansiBit = "ANSI: ???"
Else
  ansiBit = ">>>>>>  ANSI: " & Str(ansicode)
  If showHexANSI = True Then ansiBit = ansiBit & _
       " (Hex " & Hex(ansicode) & ")"
End If
ucodeBit = "Unicode: " & Str(uCode) & "   (Hex " & _
     Replace(Hex(uCode), "FFFF", "") & ")"
fontBit = "Font: " & Selection.Font.Name
fntSize = Selection.Font.Size
normalSize = ActiveDocument.Styles(wdStyleNormal).Font.Size
If normalSize <> fntSize Then _
     fontBit = fontBit & "   FONT SIZE: " & Str(fntSize)

' To correct for Mac codes
Select Case ansicode
  Case 2: extrabit = "foot/endnote marker"
  Case 32: extrabit = "ordinary space"
  Case 34: extrabit = "straight double quote"
  Case 39: extrabit = "straight apostrophe"
  Case 40: If Selection.Font.Name = "Symbol" Then extrabit = "Funny Symbol character!"
  Case 45: extrabit = "ordinary hyphen"
  Case 46: extrabit = "full point"
  Case 48: extrabit = "number 0"
  Case 49: extrabit = "number 1"
  Case 50: extrabit = "number 2"
  Case 51: extrabit = "number 3"
  Case 63: extrabit = "question mark"
  Case 73: extrabit = "capital I(eye)"
  Case 76: extrabit = "capital L"
  Case 79: extrabit = "capital O"
  Case 88: extrabit = "upper case X"
  Case 96: extrabit = "back tick"
  Case 105: extrabit = "lowercase i (eye)"
  Case 108: extrabit = "lowercase l (el)"
  Case 111: extrabit = "lowercase o"
  Case 120: extrabit = "lower case x"

  Case Else: extrabit = ""
End Select

' The unicode information will overwrite the ANSI
Select Case uCode
  Case 9: extrabit = "tab character"
  Case 11: extrabit = "manual line break"
  Case 13: extrabit = "just an ordinary paragraph end"
  Case 30: extrabit = "non-breaking hyphen"
  Case 31: extrabit = "optional hyphen"
  Case 124: extrabit = "pad character (vertical bar)"
  Case 160: extrabit = "non-breaking space"
  Case 176: extrabit = "proper degree symbol"
  Case 178: extrabit = "dodgy squared symbol"
  Case 179: extrabit = "dodgy cubed symbol"
  Case 186: extrabit = "masculine ordinal"
  Case 215: extrabit = "proper multiply symbol"
  Case 937: extrabit = "omega"
  Case 956: extrabit = "mu = micro"
  Case 8194: extrabit = "en space"
  Case 8195: extrabit = "em space"
  Case 8201: extrabit = "thin space"
  Case 8216: extrabit = "single open curly quote"
  Case 8217: extrabit = "single close curly quote = apostrophe"
  Case 8211: extrabit = "en dash"
  Case 8212: extrabit = "em dash"
  Case 8220: extrabit = "double open curly quote"
  Case 8221: extrabit = "double close curly quote"
  Case 8222: extrabit = "German open curly quote"
  Case 8226: extrabit = "ordinary bullet"
  Case 8230: extrabit = "ellipsis"
  Case 8242: extrabit = "unicode: single prime"
  Case 8243: extrabit = "unicode: double prime"
  Case 8249: extrabit = "French open quote"
  Case 8250: extrabit = "French close quote"
  Case 8722: extrabit = "proper minus sign"
End Select

If extrabit > "" Then extrabit = "      >>>>  " & extrabit

If Selection.Font.Superscript = True Then
  extrabit = extrabit & "      >>>>  superscripted"
End If
If Selection.Font.Subscript = True Then
  extrabit = extrabit & "      >>>>  subscripted"
End If
If Selection.Font.Italic = True Then
  extrabit = extrabit & "      >>>>  italic"
End If

' Show on the status bar
S = "                   "
If showOnStatusbar = True Then
  StatusBar = S & S & S & S & ansiBit & "    " & extrabit _
       & "    " & ucodeBit & "    " & fontBit
End If
' Display in a message box
If showInMessageBox = True Then
  MsgBox ansiBit & extrabit & vbCrLf & ucodeBit & vbCrLf & fontBit
End If
Selection.End = Selection.Start

End Sub


EI
Sub TextProbe()
' Version 19.12.10
' Find funny character codes
showUnicode = True: notShowDashes = True
showTab = False: ' code = 9
showReturn = False: ' code = 13
showTableCellMarker = True: ' code = 7
showPictureMarker = True: ' code = 1
showNewPage = True: ' code = 12
showNoteMarker = True: ' code = 2
showSoftReturn = True: ' code = 11


Set rng = ActiveDocument.Range
theEnd = rng.End
Selection.MoveEnd wdWord, -1
rng.Start = Selection.Start
rng.End = Selection.Start
Do
  rng.MoveEnd wdWord, 1
  ' Check if any character < 31 or any unicode
  myWord = rng
  showIt = False
  For myChar = 1 To Len(myWord)
    i = Asc(Mid(myWord, myChar, 1))
    If i < 32 Then
      showCode = True
      If showReturn = False And i = 13 Then showCode = False
      If showTab = False And i = 9 Then showCode = False
      If showNewPage = False And i = 12 Then showCode = False
      If showNoteMarker = False And i = 2 Then showCode = False
      If showSoftReturn = False And i = 11 Then showCode = False
      If showTableCellMarker = False And i = 7 Then showCode = False
      If showPictureMarker = False And i = 1 Then showCode = False
      If showCode = True Then showIt = True
    End If
    If showUnicode = True Then
      i = AscW(Mid(myWord, myChar, 1))
      If i = 8211 Or i = 8212 Then
        If notShowDashes = False Then showIt = True
      Else
        If i > 255 Then showIt = True
      End If
    End If
  Next myChar
 
  If showIt = True Then
    ' Now make up string of text + codes
    rng.Select
    foundThis = ""
    For i = 1 To Len(myWord)
      myChar = Mid(myWord, i, 1)
      If Asc(myChar) > 31 Then
       
        uni = AscW(myChar)
        If showUnicode = True And uni > 255 Then
          foundThis = foundThis & "{" & myChar & " = " & Trim(Str(uni)) & "}"
        Else
          foundThis = foundThis & myChar
        End If
      Else
        foundThis = foundThis & "[" & Trim(Str(Asc(myChar))) & "]"
      End If
    Next i
    myResponse = MsgBox(foundThis & vbCrLf & vbCrLf _
         & "Continue?", vbQuestion + vbYesNo)
    If myResponse = vbNo Then Exit Sub
  End If
  rng.MoveStart wdWord, 1
Loop Until rng.End = theEnd
rng.Select
Selection.Start = Selection.End
Beep
End Sub


EI
Sub SpellcheckWordUK()
' Version 25.08.11
' Spellcheck single word UK
' Alt-`
Selection.Words(1).Select
If Application.CheckSpelling(Selection,
     _MainDictionary:=Languages(wdEnglishUK).NameLocal) = False Then
  ActiveDocument.CheckSpelling
Else
  Selection.Start = Selection.End
  Beep
End If
End Sub


EI
Sub SpellcheckWordUS()
' Version 25.08.11
' Spellcheck single word US

Selection.Words(1).Select
If Application.CheckSpelling(Selection, _
     MainDictionary:=Languages(wdEnglishUS).NameLocal) = False Then
  ActiveDocument.CheckSpelling
Else
  Selection.Start = Selection.End
  Beep
End If
End Sub


EI
Sub SpellcheckWordCurrent()
' Version 10.12.11
' Spellcheck single word in current language
' Alt-`
Selection.Words(1).Select
If Application.CheckSpelling(Selection, _
     MainDictionary:=Languages(Selection.LanguageID).NameLocal) = False Then
  ActiveDocument.CheckSpelling
Else
  Selection.End = Selection.Start
  Beep
End If
End Sub


EI
Sub SpellcheckWordUSUK()
' Version 19.12.11
' Spellcheck single word in NOT the current language

Selection.Words(1).Select
If Selection.LanguageID = wdEnglishUK Then
  checkLang = wdEnglishUS
Else
  checkLang = wdEnglishUK
End If

If Application.CheckSpelling(Selection, _
       MainDictionary:=Languages(checkLang).NameLocal) = False Then
  ActiveDocument.CheckSpelling
Else
  Selection.Start = Selection.End
  Beep
End If
End Sub


EI
Sub Spellcheck()
' Version 06.12.10
' Run a spellcheck
' F7
If Selection.LanguageID = wdEnglishUS Then Beep
ActiveDocument.CheckSpelling
End Sub


EI
Sub SpellcheckWarn()
' Version 30.04.11
' Run a spellcheck, but beep if not UK or langauges are mixed
' F7
langHere = Selection.LanguageID
If langHere <> wdEnglishUK Then Beep

Set rng = ActiveDocument.Content
langMix = rng.LanguageID

If langMix > 9999 Then MsgBox _
    ("Beware: multiple languages within document")
ActiveDocument.CheckSpelling
End Sub




EI
Sub SpellWordChange()
' Version 02.01.13
' Accept Word's spelling suggestion for current word

' Select the word, and nothing but the word
Selection.Words(1).Select
Selection.MoveEndWhile cset:=ChrW(8217) & "' ", Count:=wdBackward
langText = Languages(Selection.LanguageID).NameLocal

' Make erWord and actual Word 'word'
Set erWord = Selection.Words.First

' If it's a spelling error...
If Application.CheckSpelling(erWord, MainDictionary:=langText) = False Then
  Set suggList = erWord.GetSpellingSuggestions(MainDictionary:=langText)
  If suggList.Count > 0 Then
  ' and if an alternative is available, type it
    Selection.TypeText Text:=suggList.Item(1).Name
  Else
  ' otherwise just colour it
    Selection.Range.HighlightColorIndex = wdPink
  End If
  ' Beep twice
  Beep
  myTime = Timer
  Do
  Loop Until Timer > myTime + 0.1
  Beep
Else
' But if correctly spelt, beep once and move on
  Beep
End If
Selection.Start = Selection.End + 1
End Sub


EI
Sub CountPhrase()
' Version 20.02.14
' Count this word or phrase

doFormatCount = True
doCountWhole = True
limitTimeSpent = True
maxTime = 10

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text
myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

If limitTimeSpent = True Then
  myTime = Timer
Else
  myTime = Timer + 999
End If

' If nothing is selected, select the current word
If Selection.Start = Selection.End Then
  Selection.Expand wdWord
  Selection.MoveEndWhile cset:=ChrW(8217) & "' ", Count:=wdBackward
End If

oldStart = Selection.Start
oldEnd = Selection.End

thisBit = Trim(Selection)
thisBit = Replace(thisBit, "^", "^^")
thisBit = Replace(thisBit, Chr(13), "^p")
If Right(thisBit, 1) = ChrW(8217) Then thisBit _
     = Left(thisBit, Len(thisBit) - 1)
CR = vbCrLf: CR2 = CR & CR

' Find whether we're in a footnote
InANote = Selection.Information(wdInFootnote)

If InANote = True Then
  lineJump = 0
  Do
    Selection.MoveUp Unit:=wdLine, Count:=1
    lineJump = lineJump + 1
  Loop Until Selection.Information(wdInFootnote) = False
  oldStart = Selection.Start
  oldEnd = Selection.Start
End If
Selection.HomeKey Unit:=wdStory
Set rng = ActiveDocument.Content
myTot = ActiveDocument.Range.End
' Count all occurences
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = thisBit
  .MatchCase = False
  .Replacement.Text = "^& "
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute Replace:=wdReplaceAll
End With
allCount = ActiveDocument.Range.End - myTot
If allCount > 0 Then WordBasic.EditUndo

nts = ""
' Are there any footnotes?
If ActiveDocument.Footnotes.Count > 0 Then
  nts = ActiveDocument.StoryRanges(wdFootnotesStory)
End If
If ActiveDocument.Endnotes.Count > 0 Then
  nts = nts & ActiveDocument.StoryRanges(wdEndnotesStory)
  nts = Replace(nts, Chr(2), "")
End If

If Len(nts) > 0 Then
  lcNts = LCase(nts)
  lcBit = LCase(thisBit)
  lnWas = Len(nts)
  lnNow = Len(Replace(lcNts, lcBit, lcBit & "!"))
  allCount = allCount + lnNow - lnWas
End If
myText = "All:" & Str(allCount) & CR

If Timer > myTime + maxTime Then
  myText = myText & CR & "Time out!"
  GoTo printResult
End If

' Count case sensitively
With rng.Find
  .MatchCase = True
  .Execute Replace:=wdReplaceAll
End With
caseCount = ActiveDocument.Range.End - myTot
If caseCount > 0 Then WordBasic.EditUndo

' Now notes, if any
If Len(nts) > 0 Then
  lnNow = Len(Replace(nts, thisBit, thisBit & "!"))
  caseCount = caseCount + lnNow - lnWas
End If

myText = myText & "Case sensitive:" & Str(caseCount) & CR

If Timer > myTime + maxTime Then
  myText = myText & CR & "Time out!"
  GoTo printResult
End If

If doFormatCount = True Then
  ' Count bold italic
  With rng.Find
    .ClearFormatting
    .MatchCase = False
    .Font.Italic = True
    .Font.Bold = True
    .Execute Replace:=wdReplaceAll
  End With

  biCount = ActiveDocument.Range.End - myTot
  If biCount > 0 Then
    WordBasic.EditUndo
    myText = myText & "Bold italic (main text only):" & Str(biCount) & CR
  End If

  ' Count italic
  With rng.Find
    .ClearFormatting
    .MatchCase = False
    .Font.Italic = True
    .Execute Replace:=wdReplaceAll
  End With
  iCount = ActiveDocument.Range.End - myTot
  If iCount > 0 Then
    WordBasic.EditUndo
    myText = myText & "Italic (main text only):" & Str(iCount - biCount) & CR
  End If

  ' Count bold
  With rng.Find
    .ClearFormatting
    .Font.Bold = True
    .Execute Replace:=wdReplaceAll
  End With
  bCount = ActiveDocument.Range.End - myTot
  If bCount > 0 Then
    WordBasic.EditUndo
    myText = myText & "Bold (main text only):" & Str(bCount - biCount) & CR
  End If
End If

If Timer > myTime + maxTime Then
  myText = myText & CR & "Time out!"
  GoTo printResult
End If

If doCountWhole = True Then
  ' Count as whole words (case sensitive)
  thisBit = Replace(thisBit, "(", "\(")
  thisBit = Replace(thisBit, ")", "\)")
  thisBit = Replace(thisBit, "^p", "^13")
 
  ' Add text of the notes at the end of main text
  If Len(nts) > 0 Then
    Selection.EndKey Unit:=wdStory
    Selection.TypeText Text:="fzcq " & nts & " fzcq"
  End If
 
  Set rng = ActiveDocument.Content
  myTot = ActiveDocument.Range.End
  With rng.Find
    .ClearFormatting
    .Text = "[!a-zA-Z]" & thisBit & "[!a-zA-Z]"
    .Replacement.Text = "^& "
    .MatchWholeWord = False
    .MatchCase = True
    .MatchWildcards = True
    .Execute Replace:=wdReplaceAll
  End With
  wholeWdCaseCount = ActiveDocument.Range.End - myTot
  If wholeWdCaseCount > 0 Then
    WordBasic.EditUndo
    myText = myText & "Whole words (case sensitive):" & _
         Str(wholeWdCaseCount) & CR
  End If
  With rng.Find
    .ClearFormatting
    .Text = "fzcq*fzcq"
    .Replacement.Text = ""
    .MatchWholeWord = False
    .MatchWildcards = True
    .Execute Replace:=wdReplaceAll
  End With
End If

printResult:
Selection.End = oldStart
Selection.MoveRight Unit:=wdCharacter, Count:=1
Selection.Start = oldStart
Selection.End = oldEnd

If InANote = True Then
  Selection.MoveDown Unit:=wdLine, Count:=lineJump
End If

MsgBox myText

With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchWildcards = False
End With
ActiveDocument.TrackRevisions = myTrack
End Sub


EI
Sub HyphenSpaceWordCount()
' Version 03.08.11
' Count hyphenated word forms

startHere = Selection.Start
endHere = Selection.End
myText = Selection
hyphenPos = InStr(myText, "-")
spacePos = InStr(myText, " ")
markerPos = hyphenPos + spacePos
If Len(myText) < 2 Or markerPos = 0 Then
  MsgBox ("Select two words separated by a hyphen or a space")
  Exit Sub
End If

fstWd = Left(myText, markerPos - 1)
scndWd = Mid(myText, markerPos + 1)

myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
Set rng = ActiveDocument.Content
myTot = rng.End

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = fstWd & " " & scndWd
  .Replacement.Text = "^&!"
  .MatchWildcards = False
  .MatchCase = False
  .Execute Replace:=wdReplaceAll
End With
spaceCount = ActiveDocument.Range.End - myTot
If spaceCount > 0 Then WordBasic.EditUndo

With rng.Find
  .Replacement.ClearFormatting
  .Text = fstWd & "-" & scndWd
  .Replacement.Text = "^&!"
  .Execute Replace:=wdReplaceAll
End With
HyphenCount = ActiveDocument.Range.End - myTot
If HyphenCount > 0 Then WordBasic.EditUndo

With rng.Find
  .Text = fstWd & scndWd
  .Replacement.Text = "^&!"
  .Execute Replace:=wdReplaceAll
End With
oneWordCount = ActiveDocument.Range.End - myTot
If oneWordCount > 0 Then WordBasic.EditUndo

myResult = fstWd & " " & scndWd & ":   " & Str(spaceCount) & vbCrLf
myResult = myResult & fstWd & "-" & scndWd & ":   " & Str(HyphenCount) & vbCrLf
myResult = myResult & fstWd & scndWd & ":   " & Str(oneWordCount) & vbCrLf

MsgBox (myResult)

Selection.Start = startHere
Selection.End = endHere
ActiveDocument.TrackRevisions = myTrack
End Sub


EI
Sub CountRemainderSimple()
' Version 01.01.10
' Count words below the cursor
  Set rng = ActiveDocument.Content
  rng.Start = Selection.Start
  MsgBox Str(rng.Words.Count) & " words left, out of " _
      & Str(ActiveDocument.Content.Words.Count)
End Sub



EI
Sub CountRemainder()
' Version 13.03.13
' Count words below the cursor
' <shift-ctrl-alt-W>

myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

Set rng = ActiveDocument.Content
rng.LanguageID = Selection.LanguageID

' Main macro starts here
Dim wordsTrue As Long, wordsCount As Long
' Find actual total wordcount
wordsTrue = ActiveDocument.Content.ReadabilityStatistics(1).Value
' Turn to a string with single decimal place
num = Str(wordsTrue \ 100)
totWords = Trim(Left(num, Len(num) - 1) & "." & Right(num, 1) & ".")

' Find apparent total word count
wordsCount = ActiveDocument.Content.Words.Count
correctionFactor = wordsTrue / wordsCount

' Find apparent remaining word count
Set rng = ActiveDocument.Content
rng.Start = Selection.Start
wordsleft = rng.Words.Count
wordsDone = wordsCount - wordsleft
num = Trim(Str(wordsDone * correctionFactor \ 100))
wordsRead = Left(num, Len(num) - 1) & "." & Right(num, 1)
If wordsDone < 1000 Then wordsRead = "0" & wordsRead

' Calculate words to go as a proportion of actual total
num = Trim(Str(wordsleft * correctionFactor \ 100))
wordsToGo = Left(num, Len(num) - 1) & "." & Right(num, 1)
If wordsleft < 1000 Then wordsToGo = "0" & wordsToGo

ActiveDocument.TrackRevisions = myTrack
MsgBox Trim(wordsToGo) & " left, out of " & Trim(totWords) & vbCrLf & vbCrLf _
      & wordsRead & " done."
End Sub


EI
Sub CountAll()
' Version 12.03.13
' Count text inc footnotes, endnotes and textboxes

myResponse = MsgBox("Count all text?", vbQuestion + vbYesNo)
If myResponse = vbNo Then Exit Sub

myLanguage = "UK"

Set rng = ActiveDocument.Content
rng.LanguageID = Switch(myLanguage = "UK", wdEnglishUK, _
                        myLanguage = "US", wdEnglishUS)
rng.NoProofing = False
Application.CheckLanguage = False

wordsMain = ActiveDocument.Content.ReadabilityStatistics(1).Value
Selection.EndKey Unit:=wdStory
theEnd = Selection.Start

' copy all the footnotes to the end of the text
If ActiveDocument.Footnotes.Count > 0 Then
  For Each fn In ActiveDocument.Footnotes
    fn.Range.Copy
    Selection.Paste
  Next
End If

' copy all the endnotes to the end of the text
If ActiveDocument.Endnotes.Count > 0 Then
  For Each fn In ActiveDocument.Endnotes
    fn.Range.Copy
    Selection.Paste
  Next
End If

' copy all the textboxes to the end of the text
Set rng = ActiveDocument.Range
rng.Start = rng.End
If ActiveDocument.Shapes.Count > 0 Then
  For Each shp In ActiveDocument.Shapes
'    someText = shp.TextFrame.HasText
 '   If someText Then
    If shp.TextFrame.HasText Then
      Set rng2 = shp.TextFrame.TextRange
      rng2.Copy
      rng.Paste
      rng.Start = rng.End
    End If
  Next
End If

Set rng = ActiveDocument.Content
If myLanguage = "UK" Then
  rng.LanguageID = wdEnglishUK
Else
  rng.LanguageID = wdEnglishUS
End If
rng.NoProofing = False
Application.CheckLanguage = False

wordsAll = ActiveDocument.Content.ReadabilityStatistics(1).Value

Selection.EndKey Unit:=wdStory
Selection.Start = theEnd
Selection.Delete
Selection.HomeKey Unit:=wdStory

MsgBox ("Main text: " & wordsMain & vbCrLf & vbCrLf _
     & "Total: " & wordsAll & vbCrLf & vbCrLf _
     & "(Extras: " & wordsAll - wordsMain & ")")
End Sub


EI
Sub ItalicCount()
' Version 22.07.10
' Count the number of words that are initalic

Selection.EndKey Unit:=wdStory
theEnd = Selection.Start

' copy all the footnotes to the end of the text
If ActiveDocument.Footnotes.Count > 0 Then
  For Each fn In ActiveDocument.Footnotes
    fn.Range.Copy
    Selection.Paste
  Next
End If

' copy all the endnotes to the end of the text
If ActiveDocument.Endnotes.Count > 0 Then
  For Each fn In ActiveDocument.Endnotes
    fn.Range.Copy
    Selection.Paste
  Next
End If

' copy all the textboxes to the end of the text
Set rng = ActiveDocument.Range
rng.Start = rng.End
If ActiveDocument.Shapes.Count > 0 Then
  For Each shp In ActiveDocument.Shapes
    If shp.TextFrame.HasText Then
      Set rng2 = shp.TextFrame.TextRange
      rng2.Copy
      rng.Paste
      rng.Start = rng.End
    End If
  Next
End If

Selection.HomeKey Unit:=wdStory
totItalic = 0
totRoman = 0
totChars = ActiveDocument.Characters.Count
For Each myChar In ActiveDocument.Characters
  If myChar.Font.Italic = True Then
    totItalic = totItalic + 1
  Else
    totRoman = totRoman + 1
  End If
If totItalic Mod 100 = 0 Then StatusBar = _
     "    Press Ctrl-Break to stop.      " _
     & "Remaining:  " & Int((totChars - totItalic - totRoman) / 100)
Next

' Delete all the added text
Selection.EndKey Unit:=wdStory
Selection.Start = theEnd
Selection.Delete
Selection.HomeKey Unit:=wdStory

StatusBar = ""
MsgBox ("Italic: " & totItalic & vbCrLf & vbCrLf _
     & "Roman: " & totRoman)

End Sub


EI
Sub ItalicWordList()
' Version 03.09.10
' Create a list of all words in italic
Selection.EndKey Unit:=wdStory
theEnd = Selection.Start

' copy all the footnotes to the end of the text
If ActiveDocument.Footnotes.Count > 0 Then
  For Each fn In ActiveDocument.Footnotes
    fn.Range.Copy
    Selection.Paste
  Next
End If

' copy all the endnotes to the end of the text
If ActiveDocument.Endnotes.Count > 0 Then
  For Each fn In ActiveDocument.Endnotes
    fn.Range.Copy
    Selection.Paste
  Next
End If

' copy all the textboxes to the end of the text
Set rng = ActiveDocument.Range
rng.Start = rng.End
If ActiveDocument.Shapes.Count > 0 Then
  For Each shp In ActiveDocument.Shapes
    If shp.TextFrame.HasText Then
      Set rng2 = shp.TextFrame.TextRange
      rng2.Copy
      rng.Paste
      rng.Start = rng.End
    End If
  Next
End If

Selection.WholeStory
Selection.Copy
' Delete all the added text
Selection.Start = theEnd
Selection.Delete

Documents.Add
Selection.Paste

Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ""
  .Font.Italic = False
  .Replacement.Text = "^p"
  .Forward = True
  .Wrap = wdFindContinue
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With
End Sub








EI
Sub MultiFileCount()
' Version 19.03.12
' Count words in a group of files

useStats = True
useBoth = True

Dim FilesTotal As Integer, myCountMain As Long, myCountAll As Long
Dim StatWordsMain As Long, StatWordsAll As Long, myErr As Long
Dim myCountMainTot As Long, myCountAllTot As Long
Dim StatWordsMainTot As Long, StatWordsAllTot As Long
Dim allMyFiles(200) As String

Set rng = ActiveDocument.Content
myExtent = 250
If rng.End - rng.Start > myExtent Then rng.End = rng.Start + myExtent

If InStr(rng.Text, ".doc") = 0 And InStr(rng.Text, ".rtf") = 0 Then
' If not a file list then open a file in the relevant folder
  docCount = Documents.Count
  Dialogs(wdDialogFileOpen).Show
  If docCount = Documents.Count Then Exit Sub
  dirPath = Replace(ActiveDocument.Path, ":" & ActiveDocument.Name, "")
  ActiveDocument.Close
' Read the names of all the files in this directory
  myFile = Dir("")
  Documents.Add
  numFiles = 0
  Do While myFile <> ""
    If InStr(myFile, ".doc") > 0 Or InStr(myFile, ".rtf") > 0 Then
      Selection.TypeText myFile & vbCrLf
      numFiles = numFiles + 1
    End If
    myFile = Dir()
  Loop

' Now sort the file list (only actually needed for Macs)
  Selection.WholeStory
  Selection.Sort
  Selection.EndKey Unit:=wdStory
  Selection.TypeParagraph
  Selection.HomeKey Unit:=wdStory
  Selection.TypeText dirPath
' Go back until you hit myDelimiter
  Selection.MoveStartUntil cset:=":\", Count:=wdBackward
  dirName = Selection
  Selection.HomeKey Unit:=wdStory

  myResponse = MsgBox("Count ALL the files in directory:" & dirName _
       & " ?", vbQuestion + vbYesNoCancel, "Multifile Wordcount")
  If myResponse <> vbYes Then Exit Sub
Else
  myResponse = MsgBox("Count just the files listed here?", _
       vbQuestion + vbYesNoCancel, "Multifile Wordcount")
  If myResponse <> vbYes Then Exit Sub
End If

' Pick up the folder name and the filenames from the file list
numFiles = 0
myFolder = ""
For Each myPara In ActiveDocument.Paragraphs
  myPara.Range.Select
  Selection.MoveEnd , -1
  lineText = Selection
  If myFolder = "" Then
    myFolder = lineText
    Selection.Collapse wdCollapseEnd
    Selection.MoveStartUntil cset:=":\", Count:=wdBackward
    Selection.MoveStart , -1
    myDelimiter = Left(Selection, 1)
  Else
    thisFile = lineText
    If Len(thisFile) > 2 Then
      If Left(thisFile, 1) <> "|" Then
        numFiles = numFiles + 1
        allMyFiles(numFiles) = thisFile
      End If
    End If
  End If
Next myPara

Documents.Add
Set myResults = ActiveDocument

For i = 1 To numFiles
  thisFile = myFolder & myDelimiter & allMyFiles(i)
  Set myDoc = Application.Documents.Open(fileName:=thisFile, ReadOnly:=True)
  StatusBar = allMyFiles(i)

  ' Clean up the document
  ActiveDocument.ConvertNumbersToText
  Set rng = ActiveDocument.Range
  rng.Fields.Unlink
  If ActiveDocument.Comments.Count > 0 Then ActiveDocument.DeleteAllComments
  ActiveDocument.TrackRevisions = False
  Selection.Range.Revisions.AcceptAll

  ' Mark the end of the main text
  Selection.EndKey Unit:=wdStory
  Selection.TypeText Text:="zczczczc" & vbCrLf & vbCrLf
  Selection.EndKey Unit:=wdStory

  ' copy all the footnotes to the end of the text
  If ActiveDocument.Footnotes.Count > 0 Then
    For Each fn In ActiveDocument.Footnotes
      fn.Range.Copy
      Selection.Paste
      Selection.TypeText Text:=" "
    Next
  End If

  ' copy all the endnotes to the end of the text
  If ActiveDocument.Endnotes.Count > 0 Then
    For Each EN In ActiveDocument.Endnotes
      EN.Range.Copy
      Selection.Paste
      Selection.TypeText Text:=" "
    Next
  End If

  ' copy all the textboxes to the end of the text
  Set rng = ActiveDocument.Range
  rng.Start = rng.End
  If ActiveDocument.Shapes.Count > 0 Then
    For Each shp In ActiveDocument.Shapes
      If shp.TextFrame.HasText Then
        Set rng2 = shp.TextFrame.TextRange
        rng2.Cut
        rng.Paste
        rng.Start = rng.End
      End If
    Next
  End If

' Delete all notes
  Set rng = ActiveDocument.Range
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .MatchWildcards = False
    .Text = "^2"
    .Replacement.Text = " "
    .Execute Replace:=wdReplaceAll
  End With

  Set rng = ActiveDocument.Range
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .MatchWildcards = True
    .Text = "[0-9].[0-9]"
    .Replacement.Text = "111"
    .Execute Replace:=wdReplaceAll
  End With

  Set rng = ActiveDocument.Range
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .MatchWildcards = True
    .Text = "[0-9].[0-9]"
    .Replacement.Text = "111"
    .Execute Replace:=wdReplaceAll
  End With

  If useStats = True Or useBoth = True Then
  ' Ensure only a single language
    Set rng = ActiveDocument.Content
    rng.LanguageID = wdEnglishUK

    ' Count the whole of the text
    Selection.EndKey Unit:=wdStory
    StatWordsAll = ActiveDocument.Content.ReadabilityStatistics(1).Value - 1
    StatWordsAllTot = StatWordsAllTot + StatWordsAll

    ' Now delete the extra text ready for a recount
    Set rng = ActiveDocument.Range
    With rng.Find
      .MatchWildcards = False
      .Text = "zczczczc"
      .Replacement.Text = " "
      .Execute
    End With
    rng.End = ActiveDocument.Range.End
    rng.Cut

    ' Count just the main text
    Selection.HomeKey Unit:=wdStory
    Selection.EndKey Unit:=wdStory
    StatWordsMain = ActiveDocument.Content.ReadabilityStatistics(1).Value
    If StatWordsAll - StatWordsMain = -1 Then StatWordsMain = StatWordsAll
    StatWordsMainTot = StatWordsMainTot + StatWordsMain
    ' Put the extra text back in
    Selection.Paste
  End If

  If useStats = False Or useBoth = True Then
  ' Deal with hyphens and apostrophes (e.g. half-hearted and can't)
  ' treating them as single words
    Set rng = ActiveDocument.Range
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .MatchWildcards = True
      .Text = "([a-zA-Z])[-'\&" & ChrW(8217) & "]([a-zA-Z])"
      .Replacement.Text = "\1x\2"
      .Execute Replace:=wdReplaceAll
    End With

    ' Change '/' into a space for "this/that" type occurences;
    ' also punctuation, brackets etc.
    Set rng = ActiveDocument.Range
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .MatchWildcards = True
      .Text = "[_/\(\)\<\>\&\@\[\]""'\!\?\,\.+\-;:�^l^m^t^13" & ChrW(8220) _
             & ChrW(8221) & ChrW(8216) & ChrW(8217) & ChrW(8226) & "]"
      .Replacement.Text = " "
      .Execute Replace:=wdReplaceAll
    End With

    Set rng = ActiveDocument.Range
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .MatchWildcards = True
      .Text = " [!a-zA-Z] "
      .Replacement.Text = "   "
      .Execute Replace:=wdReplaceAll
    End With
      
    ' Count all the text -1 for zczc, and -1 because .Count adds one!
    tot = ActiveDocument.Content.Words.Count - 2
    myCountAll = tot
    myCountAllTot = myCountAllTot + myCountAll

    ' Now delete the extra text ready for a recount
    Set rng = ActiveDocument.Range
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .MatchWildcards = True
      .Text = "zczczczc"
      .Replacement.Text = " "
      .Execute
    End With
    rng.End = ActiveDocument.Range.End
    rng.Cut
    myCountMain = ActiveDocument.Content.Words.Count - 1
    If myCountAll - myCountMain = -1 Then myCountMain = myCountAll
    myCountMainTot = myCountMainTot + myCountMain
  End If

  myDoc.Close SaveChanges:=wdDoNotSaveChanges
  FilesTotal = FilesTotal + 1
  myFileName = Left(allMyFiles(i), InStr(allMyFiles(i), ".") - 1)
  Selection.TypeText myFileName

  If useStats = True Or useBoth = True Then _
       Selection.TypeText vbTab & Str(StatWordsMain) & _
       vbTab & Str(StatWordsAll - StatWordsMain) & vbTab & Str(StatWordsAll)

  If useStats = False Or useBoth = True Then _
       Selection.TypeText vbTab & Str(myCountMain) & vbTab & _
       Str(myCountAll - myCountMain) & vbTab & Str(myCountAll)
  If useBoth = True Then
    myErr = myCountAll - StatWordsAll
    myErrPC = Int(2000 * myErr / (myCountAll + StatWordsAll)) / 10
    If myErr = 0 Then
      myPC = ChrW(8212)
    Else
      myPC = Trim(Str(Abs(myErrPC)))
      If InStr(myPC, ".") = 0 Then myPC = myPC & ".0"
      If Abs(myErrPC) < 1 Then myPC = "0" & myPC
      myPC = myPC & "%"
      If myErr > 0 Then
        myPC = "+" & myPC
      Else
        myPC = ChrW(8722) & myPC
      End If
    End If
    Selection.TypeText vbTab & myPC
  End If
  Selection.TypeText vbCrLf
Next i

Selection.TypeText vbCrLf
Selection.TypeText "Totals (" & Trim(Str(FilesTotal)) & " files)"
If useStats = True Or useBoth = True Then _
     Selection.TypeText vbTab & Str(StatWordsMainTot) & vbTab & _
     Str(StatWordsAllTot - StatWordsMainTot) & vbTab & Str(StatWordsAllTot)
If useStats = False Or useBoth = True Then _
     Selection.TypeText vbTab & Str(myCountMainTot) & vbTab & _
     Str(myCountAllTot - myCountMainTot) & vbTab & Str(myCountAllTot)

' Add title line and make it bold
Selection.HomeKey Unit:=wdStory
Selection.TypeText Text:="Filename"
If useStats = True Or useBoth = True Then _
     Selection.TypeText vbTab & "Main (stats)" & vbTab & _
     "Extras" & vbTab & "All"
If useStats = False Or useBoth = True Then _
     Selection.TypeText vbTab & "Main (count)" & vbTab & _
     "Extras" & vbTab & "All"
If useBoth = True Then Selection.TypeText vbTab & "Difference"
Selection.TypeText vbCrLf

Selection.MoveUp Unit:=wdParagraph, Count:=1, Extend:=wdExtend
Selection.Start = 0
Selection.Font.Bold = True

' Make the totals line bold
Selection.EndKey Unit:=wdStory
Selection.MoveUp Unit:=wdParagraph, Count:=1, Extend:=wdExtend
Selection.Font.Bold = True

Selection.WholeStory
Set tbl = Selection.ConvertToTable(Separator:=wdSeparateByTabs)
tbl.Columns.AutoFit
Selection.HomeKey Unit:=wdStory
End Sub


EI
Sub WordTotaller()
' Version 21.01.11
' Adds up word numbers in selected texts
addPageNum = True
wordsQuote = 3

myTotal = Selection.Words.Count
ss = Selection.Start
se = Selection.End
Selection.End = ss
Selection.MoveEnd wdWord, wordsQuote
myQuote = Selection
If addPageNum = True Then
  Selection.Start = se
  Selection.Fields.Add Range:=Selection.Range, Type:=wdFieldEmpty, Text:= _
      "PAGE  \* Arabic ", PreserveFormatting:=True
  Selection.MoveLeft Unit:=wdCharacter, Count:=1, Extend:=wdExtend
  pageNum = Selection
  Selection.Delete
  Selection.Start = ss
  Selection.End = se
End If
Set thisDoc = ActiveDocument
' Find the doc with the totals
For Each myDoc In Documents
  myDoc.Activate
  Set rng = ActiveDocument.Content
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .MatchWildcards = False
    .Text = "====="
    .Replacement.Text = ""
    .Execute
  End With
  If rng.Find.Found Then
    gotOne = True
    Exit For
  Else
    gotOne = False
  End If
Next myDoc
' If no totals file, create one
If gotOne = False Then
  Documents.Add
  Selection.TypeText Text:="=====" & vbCrLf & "0" & vbCrLf & vbCrLf
  Selection.HomeKey Unit:=wdStory
  Selection.MoveDown Unit:=wdLine, Count:=1, Extend:=wdExtend
Else
  rng.End = rng.End + 1
  rng.Select
End If
' Add current word number to total
Selection.InsertBefore Text:=Str(myTotal) & Chr(9) & "p." & _
     pageNum & " � " & myQuote & vbCrLf
Selection.Start = Selection.End
Selection.MoveDown Unit:=wdLine, Count:=1, Extend:=wdExtend
myTotal = myTotal + Val(Selection)
Selection.TypeText Text:=Str(myTotal)
Selection.MoveRight Unit:=wdCharacter, Count:=1
thisDoc.Activate
End Sub


EI
Sub ColumnTotal()
' Version 25.02.11
' Check the column total
allowErrorPercent = 0.01
okChars = "0123456789.,-" & ChrW(8211) & ChrW(8212)
myTotal = 0
On Error GoTo ReportIt
Do
  gogo = True
  Selection.MoveLeft Unit:=wdCell, Count:=1
  Selection.MoveRight Unit:=wdCell, Count:=1
  myText = Selection
  ' Change sign if it starts with a hyphen or en dash
  signBit = 1
  aBit = Left(myText, 1)
  ' A hyphen or en or em dash means either minus
  ' or nothing (zero); this will cope with either
  If aBit = "-" Or aBit = ChrW(8211) Or aBit = ChrW(8212) Then
    myText = Right(myText, Len(myText) - 1)
    signBit = -1
  End If

' Remove any commas and find the value
  previousNumber = thisNumber
  thisNumber = signBit * Val(Replace(myText, ",", ""))
  If thisNumber = 0 And InStr(okChars, aBit) = 0 Then gogo = False
  myTotal = myTotal + thisNumber
  Selection.MoveDown Unit:=wdLine, Count:=1
' Go down a line and check for a characterless cell
  Do
    myNext = Asc(Selection)
    If myNext <> 13 Then
      Selection.MoveRight Unit:=wdCharacter, Count:=1
      myNext = Asc(Selection)
    End If
  Loop Until myNext = 13
' Keep going until you drop out of the table
Loop Until Asc(Right(Selection, 1)) <> 7 Or gogo = False
If thisNumber = 0 Then thisNumber = previousNumber
' At this point, the total of the column should be twice
' the final figure, i.e. the total plus the total
myDiff = myTotal - 2 * thisNumber
' Make the difference a positive number
If myDiff < 0 Then myDiff = -myDiff

' Check how small the difference is compared to the total
If myDiff / myTotal < allowErrorPercent / 100 Then
  Beep
Else
  MsgBox ("I make the total: " & myTotal - thisNumber)
End If
Exit Sub

ReportIt:
MsgBox ("Please ensure that the cursor is the a cell containing a number")
End Sub


EI
Sub WhatDirectory()
' Version 25.01.12
' Insert the directory name at the cursor
dirName = ActiveDocument.Path
Selection.InsertBefore Text:=dirName
End Sub


EI
Sub GoogleFetch()
' Version 08.05.12
' Launch selected text on Google

useExplorer = False

runBrowser = "C:\Program Files\Mozilla Firefox\Firefox"

mySite = "http://www.google.co.uk/search?q="

If Len(Selection) = 1 Then Selection.Words(1).Select
mySubject = Trim(Selection)
mySubject = Replace(mySubject, "&", "%26")
mySubject = Replace(mySubject, " ", "+")

If useExplorer = True Then
  Set objIE = CreateObject("InternetExplorer.application")
  objIE.Visible = True
  objIE.navigate mySite & mySubject
  Set objIE = Nothing
Else
  Shell (runBrowser & " " & mySite & mySubject)
End If
End Sub


EI
Sub GoogleFetchQuotes()
' Version 08.05.12
' Launch selected text - with quotes - on Google

useExplorer = False

runBrowser = "C:\Program Files\Mozilla Firefox\Firefox"

mySite = "http://www.google.co.uk/search?q="

If Len(Selection) = 1 Then Selection.Words(1).Select
mySubject = Trim(Selection)
mySubject = Replace(mySubject, " ", "+")
mySubject = Replace(mySubject, "&", "%26")
mySubject = "%22" & mySubject & "%22"

If useExplorer = True Then
  Set objIE = CreateObject("InternetExplorer.application")
  objIE.Visible = True
  objIE.navigate mySite & mySubject
  Set objIE = Nothing
Else
  Shell (runBrowser & " " & mySite & mySubject)
End If
End Sub


EI
Sub WikiFetch()
' Version 02.12.11
' Launch selected text on Wikipedia

useExplorer = False

runBrowser = "C:\Program Files\Mozilla Firefox\Firefox"

mySite = "http://en.wikipedia.org/wiki/Special:Search?search="

If Len(Selection) = 1 Then Selection.Words(1).Select
mySubject = Trim(Selection)
mySubject = Replace(mySubject, " ", "+")

If useExplorer = True Then
  Set objIE = CreateObject("InternetExplorer.application")
  objIE.Visible = True
  objIE.navigate mySite & mySubject
  Set objIE = Nothing
Else
  Shell (runBrowser & " " & mySite & mySubject)
End If
End Sub


EI
Sub OUPFetch()
' Version 06.02.14
' Launch selected text to OUP

useExplorer = False

' runBrowser = "C:\Program Files\Mozilla Firefox\Firefox"
' runBrowser = "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"
runBrowser = "C:\Program Files\Google\Chrome\Application\Chrome"

mySite = "http://oxforddictionaries.com"

If Len(Selection) < 3 Then Selection.Expand wdWord
Selection.MoveEndWhile cset:=" " & ChrW(8217), Count:=wdBackward
Selection.MoveStartWhile cset:=" ", Count:=wdForward

mySubject = Replace(Selection, " ", "+")

' In case the word is not listed specifically
Selection.Copy

'  getThis = mySite
getThis = mySite & "/definition/english/" & mySubject

If useExplorer = True Then
  Set objIE = CreateObject("InternetExplorer.application")
  objIE.Visible = True
  objIE.navigate mySite & getThis
  Set objIE = Nothing
Else
  Shell (runBrowser & " " & getThis)
End If
End Sub


EI
Sub GoogleTranslate()
' Version 16.10.13
' Launch selected text to GoogleTranslate

myLanguage = "fr"
' myLanguage = "sp"

useExplorer = False

runBrowser = "C:\Program Files\Mozilla Firefox\Firefox"

mySite = "translate.google.co.uk/translate_t?oe=utf-8&hl=en"

If Len(Selection) = 1 Then
  Selection.Paragraphs(1).Range.Select
  Selection.MoveEnd , -1
End If
wd = Selection.Words(1)
myLang = "&fl=en&tl=" & myLanguage
If Application.CheckSpelling(wd, MainDictionary:=Languages(wdEnglishUK).NameLocal) _
     = False And Application.CheckSpelling(wd, MainDictionary:=Languages(wdEnglishUS).NameLocal) _
     = False Then myLang = "&fl=" & myLanguage & "&tl=en"
mySubject = Trim(Selection)
mySubject = Replace(mySubject, " ", "+")
mySubject = myLang & "&text=" & mySubject

If useExplorer = True Then
  Set objIE = CreateObject("InternetExplorer.application")
  objIE.Visible = True
  objIE.navigate mySite & mySubject
  Set objIE = Nothing
Else
  Shell (runBrowser & " " & mySite & mySubject)
End If

End Sub



EI
Sub ThesaurusFetch()
' Version 11.11.13
' Launch selected text to thesaurus.com

useExplorer = False

runBrowser = "C:\Program Files\Mozilla Firefox\Firefox"

mySite = "http://thesaurus.com"

If Len(Selection) < 3 Then Selection.Words(1).Select
Selection.MoveEndWhile cset:=" " & ChrW(8217), Count:=wdBackward
Selection.MoveStartWhile cset:=" ", Count:=wdForward

mySubject = Replace(Selection, " ", "+")

getThis = mySite & "/browse/" & mySubject

If useExplorer = True Then
  Set objIE = CreateObject("InternetExplorer.application")
  objIE.Visible = True
  objIE.navigate mySite & getThis
  Set objIE = Nothing
Else
  Shell (runBrowser & " " & getThis)
End If
End Sub

EI
Sub DictionaryFetch()
' Version 11.11.13
' Launch selected text to dictionary.com

useExplorer = False

runBrowser = "C:\Program Files\Mozilla Firefox\Firefox"

mySite = "http://dictionary.com"

If Len(Selection) < 3 Then Selection.Words(1).Select
Selection.MoveEndWhile cset:=" " & ChrW(8217), Count:=wdBackward
Selection.MoveStartWhile cset:=" ", Count:=wdForward

mySubject = Replace(Selection, " ", "+")

getThis = mySite & "/browse/" & mySubject

If useExplorer = True Then
  Set objIE = CreateObject("InternetExplorer.application")
  objIE.Visible = True
  objIE.navigate mySite & getThis
  Set objIE = Nothing
Else
  Shell (runBrowser & " " & getThis)
End If
End Sub



EI
Sub PubMedFetch()
' Version 06.12.13
' Launch selected text to OUP

useExplorer = False

runBrowser = "C:\Users\Carlotta\AppData\Local\Google\Chrome\Application\Chrome.exe"
runBrowser = "C:\Program Files\Mozilla Firefox\Firefox"

mySite = "http://www.ncbi.nlm.nih.gov/pubmed/?term="

If Len(Selection) < 3 Then Selection.Words(1).Select
Selection.MoveEndWhile cset:=" " & ChrW(8217), Count:=wdBackward
Selection.MoveStartWhile cset:=" ", Count:=wdForward

mySubject = Replace(Selection, " ", "+")

getThis = mySite & mySubject

If useExplorer = True Then
  Set objIE = CreateObject("InternetExplorer.application")
  objIE.Visible = True
  objIE.navigate mySite & getThis
  Set objIE = Nothing
Else
  Shell (runBrowser & " " & getThis)
End If

End Sub



EI
Sub URLlauncher()
' Version 20.03.12
' Launch successive URLs from the text

defaultNum = 10
useExplorer = False

highlightURL = True
myHighlight = wdPink

runBrowser = "C:\Program Files\Mozilla Firefox\Firefox"

' Extra characters at the ends of a URL
' that are not to be included
extraneousChars = ".,)(;:[]" & ChrW(8211) & ChrW(8212)

nowColour = Options.DefaultHighlightColorIndex
Options.DefaultHighlightColorIndex = myHighlight

expandURL = True
If Selection.Start = Selection.End Then
  numURLs = 1
Else
  If Left(Selection, 4) = "http" Or Left(Selection, 4) = "www." Then
    highlightURL = False
    expandURL = False
    numURLs = 1
  Else
    myInput = InputBox("How many?", "URL launcher", Trim(Str(defaultNum)))
    numURLs = Val(myInput)
    If numURLs = 0 Then Exit Sub
  End If
End If

For i = 1 To numURLs
  If expandURL = True Then
    Set rng = ActiveDocument.Content
    rng.End = Selection.End
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = "[^13 " & ChrW(8212) & "]"
      .Wrap = wdFindContinue
      .Replacement.Text = ""
      .Forward = False
      .MatchWildcards = True
      .Execute
    End With
    Selection.Start = rng.Start + 1
    With rng.Find
      .Text = "[^13 ]"
      .Wrap = wdFindContinue
      .Forward = True
      .MatchWildcards = True
      .Replacement.Text = ""
      .Execute
    End With
    Selection.End = rng.Start
    Selection.MoveEndWhile cset:=extraneousChars, Count:=wdBackward
    Selection.MoveStartWhile cset:=extraneousChars, Count:=wdForward
  End If

  mySubject = Trim(Selection)
  If highlightURL = True Then
    Set rng = ActiveDocument.Content
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = mySubject
      .Wrap = wdFindContinue
      .Replacement.Text = ""
      .Forward = True
      .Replacement.Highlight = True
      .MatchWildcards = False
      .Execute Replace:=wdReplaceAll
    End With
  End If

  If useExplorer = True Then
    Set objIE = CreateObject("InternetExplorer.application")
    objIE.Visible = True
    objIE.navigate mySubject
    Set objIE = Nothing
  Else
    Shell (runBrowser & " " & mySubject)
  End If
  Selection.Collapse wdCollapseEnd

  With Selection.Find
    .Text = "[wh][wt][wt][p.]*."
    .Replacement.Text = ""
    .Wrap = wdFindContinue
    If highlightURL = True Then .Highlight = False
    .Forward = True
    .MatchWildcards = True
  End With
  If numURLs > 1 Then
    If i < numURLs Then
      Selection.Find.Execute
    Else
      Selection.MoveStart , -10
    End If
  End If
Next
Options.DefaultHighlightColorIndex = nowColour
End Sub



EH
Sub HiLightON()
' Version 01.01.10
' Add highlight in currently selected colour
   Selection.Range.HighlightColorIndex = Options.DefaultHighlightColorIndex
   Selection.Start = Selection.End
End Sub


EH
Sub HiLightTurquoise()
' Version 01.01.10
' Add highlight in turquoise
   Selection.Range.HighlightColorIndex = wdTurquoise
   Selection.Start = Selection.End
End Sub


EH
Sub HiLightOFF()
' Version 01.01.10
' Remove highlight (text colour) from selected text
   Selection.Range.HighlightColorIndex = wdNoHighlight
'  Selection.Range.Font.Color = wdColorAutomatic
   Selection.Start = Selection.End
End Sub


EH
Sub HiLightOffALL()
' Version 01.01.10
' Remove ALL highlights (text colour) from whole text
   Selection.WholeStory
   Selection.Range.HighlightColorIndex = wdNoHighlight
'  Selection.Range.Font.Color = wdColorAutomatic
   Selection.Start = Selection.End
End Sub


EH
Sub HiLightOffCurrentLine()
' Version 22.01.10
' Remove highlight (text colour) from selected text or current line

' Switch off track changes
myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

' If nothing selected, select the whole line
If Selection.Start = Selection.End Then
  Selection.HomeKey Unit:=wdLine
  Selection.MoveDown Unit:=wdLine, Count:=1, Extend:=wdExtend
End If
' Remove highlighting
Selection.Range.HighlightColorIndex = wdNoHighlight
' Remove text colouring
' Selection.Range.Font.Color = wdColorAutomatic
Selection.Collapse wdCollapseEnd

ActiveDocument.TrackRevisions = myTrack
End Sub


EH
Sub Colourtoggle()
' Version 05.05.10
' Red text on/off
' F11

nowColour = Selection.Font.Color

' If it�s red or a mix of red and black, switch to black
If nowColour = wdColorRed Or nowColour > 100 Then
   Selection.Font.Color = wdColorAutomatic
Else
   Selection.Font.Color = wdColorRed
End If
End Sub


EH
Sub HighlightPlus()
' Version 30.07.10
' Add highlight in a choice of colours

Dim myCol(20)
myCol(0) = wdNoHighlight
myCol(1) = wdYellow
myCol(2) = wdTurquoise
myCol(3) = wdBrightGreen
myCol(4) = wdPink
myCol(5) = wdRed
myCol(6) = wdGreen
myCol(7) = wdDarkYellow
myCol(8) = wdGray25
myCol(9) = wdGray50
myColTotal = 9

' Remember track changes status, then switch it off
myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

Dim v As Variable: Dim nowColour As Long
varsExist = False
For Each v In ActiveDocument.Variables
  If v.Name = "selStart" Then varsExist = True: Exit For
Next v

If varsExist = False Then
  ActiveDocument.Variables.Add "selStart", 0
  ActiveDocument.Variables.Add "selEnd", 0
  ActiveDocument.Variables.Add "colNum", 0
End If

wasStart = ActiveDocument.Variables("selStart")
wasEnd = ActiveDocument.Variables("selEnd")
wasCol = ActiveDocument.Variables("colNum")

' If no text is selected ...
If Selection.Start = Selection.End Then
  ' If the cursor is outside the area, give up
  If Selection.Start < wasStart Or Selection.End > wasEnd Then GoTo theEnd
  ' Otherwise check the highlight colour
  Selection.Start = wasStart
  Selection.End = wasStart + 1
  nowColour = Selection.Range.HighlightColorIndex
  If nowColour <> myCol(wasCol) Then
    ' colour has changed, so go back to colour 1
    nowCol = 1
  Else
    ' go to next colour
    nowCol = wasCol + 1
    If nowCol > myColTotal Then nowCol = 0
  End If
  Selection.End = wasEnd
Else:  ' if some text is selected ...
  ' Record current selection range
  ActiveDocument.Variables("selStart") = Selection.Start
  ActiveDocument.Variables("selEnd") = Selection.End
  nowCol = 1
End If
ActiveDocument.Variables("colNum") = nowCol
Selection.Range.HighlightColorIndex = myCol(nowCol)
Selection.End = Selection.Start

theEnd:
ActiveDocument.TrackRevisions = myTrack
End Sub



EH
Sub HighLightMinus()
' Version 07.01.12
' Remove or add highlight in a choice of colours

Dim myCol(20)
myCol(0) = wdNoHighlight
myCol(1) = wdYellow
myCol(2) = wdTurquoise
myCol(3) = wdBrightGreen
myCol(4) = wdPink
myCol(5) = wdRed
myCol(6) = wdGreen
myCol(7) = wdDarkYellow
myCol(8) = wdGray25
myCol(9) = wdGray50
myColTotal = 9

' Remember track changes status, then switch it off
myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

Dim v As Variable: Dim nowColour As Long
varsExist = False
For Each v In ActiveDocument.Variables
  If v.Name = "colNum" Then varsExist = True: Exit For
Next v

If varsExist = False Then
  ActiveDocument.Variables.Add "selStart", 0
  ActiveDocument.Variables.Add "selEnd", 0
  ActiveDocument.Variables.Add "colNum", 0
End If

wasStart = ActiveDocument.Variables("selStart")
wasEnd = ActiveDocument.Variables("selEnd")
wasCol = ActiveDocument.Variables("colNum")

' If no text is selected ...
If Selection.Start = Selection.End Then
  ' If the cursor is outside the area, give up
  If Selection.Start < wasStart Or Selection.End _
       > wasEnd - 2 Then GoTo LineClear
  ' Otherwise check the highlight colour
  Selection.Start = wasStart
  Selection.End = wasStart + 1
  nowColour = Selection.Range.HighlightColorIndex
  If nowColour <> myCol(wasCol) Then
    ' colour has changed, so go back to colour 1
    nowCol = 0
  Else
    ' go to next colour
    nowCol = wasCol - 1
    If nowCol < 0 Then nowCol = myColTotal
  End If
  Selection.End = wasEnd
Else
  ' if some text is selected ...
  ' As long as it's not the whole text that's been selected,
  ' record the current selection range
  If Not (Selection.Start = 0 And Selection.End = _
       ActiveDocument.Range.End) Then
    ActiveDocument.Variables("selStart") = Selection.Start
    ActiveDocument.Variables("selEnd") = Selection.End
  End If
  nowCol = 0
End If
ActiveDocument.Variables("colNum") = nowCol
Selection.Range.HighlightColorIndex = myCol(nowCol)
Selection.End = Selection.Start
ActiveDocument.TrackRevisions = myTrack
Exit Sub

LineClear:

Selection.HomeKey Unit:=wdLine
clearStart = Selection.Start
Selection.MoveDown Unit:=wdLine, Count:=1
Selection.HomeKey Unit:=wdLine
Set rng = Selection.Range
rng.Start = clearStart
ActiveDocument.Variables("selStart") = clearStart
ActiveDocument.Variables("selEnd") = Selection.End
'                         This next line solves the problem where text
'                         is highlighted, yet its colour is apparently 0(!)
'                         so force it to turquoise, then back to 0.
'                         rng.HighlightColorIndex = wdTurquoise
rng.HighlightColorIndex = wdNoHighlight
ActiveDocument.TrackRevisions = myTrack
End Sub



EH
Sub ColourPlus()
' Version 30.07.10
' Add font colour in a choice of colours

Dim myCol(20)
myCol(0) = wdColorAutomatic
myCol(1) = wdColorRed
myCol(2) = wdColorOrange
myCol(3) = wdColorGreen
myCol(4) = wdColorBlue
myCol(5) = wdColorLime
myCol(6) = wdColorPlum
myCol(7) = wdColorSkyBlue
myCol(8) = wdColorPink
myCol(9) = wdColorBrightGreen
myCol(10) = wdColorGray25
myCol(11) = wdColorGray50
myColTotal = 11

' OldColour = Options.DefaultHighlightColorIndex
myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

Dim v As Variable, nowColour As Long
varsExist = False
For Each v In ActiveDocument.Variables
  If v.Name = "selStart" Then varsExist = True: Exit For
Next v

If varsExist = False Then
  ActiveDocument.Variables.Add "selStart", 0
  ActiveDocument.Variables.Add "selEnd", 0
  ActiveDocument.Variables.Add "colNum", 0
End If

wasStart = ActiveDocument.Variables("selStart")
wasEnd = ActiveDocument.Variables("selEnd")
wasCol = ActiveDocument.Variables("colNum")

' If no text is selected ...
If Selection.Start = Selection.End Then
  ' If the cursor is outside the area, give up
  If Selection.Start < wasStart Or Selection.End > wasEnd Then GoTo theEnd
  ' Otherwise check the font colour
  Selection.Start = wasStart
  Selection.End = wasStart + 1
  nowColour = Selection.Font.Color
  If nowColour <> myCol(wasCol) Then
    ' colour has changed, so go back to first colour
    nowCol = 1
  Else
    ' go to next colour
    nowCol = wasCol + 1
    If nowCol > myColTotal Then nowCol = 0
  End If
  Selection.End = wasEnd
Else:  ' if some text is selected ...
  ' Record current selection range
  ActiveDocument.Variables("selStart") = Selection.Start
  ActiveDocument.Variables("selEnd") = Selection.End
  nowCol = 1
End If
ActiveDocument.Variables("colNum") = nowCol
Selection.Font.Color = myCol(nowCol)
Selection.End = Selection.Start

theEnd:
ActiveDocument.TrackRevisions = myTrack
End Sub


EH
Sub ColourMinus()
' Version 07.01.12
' Remove or add font colour in a choice of colours

Dim myCol(20)
myCol(0) = wdColorAutomatic
myCol(1) = wdColorRed
myCol(2) = wdColorBlue
myCol(3) = wdColorPink
myCol(4) = wdColorBrightGreen
myColTotal = 4

' OldColour = Options.DefaultHighlightColorIndex
myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

Dim v As Variable: Dim nowColour As Long
varsExist = False
For Each v In ActiveDocument.Variables
  If v.Name = "selStart" Then varsExist = True: Exit For
Next v

If varsExist = False Then
  ActiveDocument.Variables.Add "selStart", 0
  ActiveDocument.Variables.Add "selEnd", 0
  ActiveDocument.Variables.Add "colNum", 0
End If

wasStart = ActiveDocument.Variables("selStart")
wasEnd = ActiveDocument.Variables("selEnd")
wasCol = ActiveDocument.Variables("colNum")

' If no text is selected ...
If Selection.Start = Selection.End Then
  ' If the cursor is outside the area, give up
  If Selection.Start < wasStart Or Selection.End > wasEnd Then GoTo LineClear
  ' Otherwise check the font colour
  Selection.Start = wasStart
  Selection.End = wasStart + 1
  nowColour = Selection.Font.Color
  If nowColour <> myCol(wasCol) Then
    ' colour changed, so go to last colour on list
    nowCol = 0
  Else
    ' go to next colour
    nowCol = wasCol - 1
    If nowCol < 0 Then nowCol = myColTotal
  End If
  Selection.End = wasEnd
Else:  ' if some text is selected ...
  ' Record current selection range
  ActiveDocument.Variables("selStart") = Selection.Start
  ActiveDocument.Variables("selEnd") = Selection.End
  If Selection.Start = 0 Then ActiveDocument.Variables("selStart") _
       = Selection.End
  ' go to last colour on list
  nowCol = 0
End If
ActiveDocument.Variables("colNum") = nowCol
Selection.Font.Color = myCol(nowCol)
Selection.End = Selection.Start
ActiveDocument.TrackRevisions = myTrack
Exit Sub

LineClear:
Selection.HomeKey Unit:=wdLine
clearStart = Selection.Start
Selection.MoveDown Unit:=wdLine, Count:=1
Set rng = Selection.Range
rng.Start = clearStart
rng.Font.Color = wdNoHighlight
ActiveDocument.TrackRevisions = myTrack
End Sub


EH
Sub UnHighlight()
' Version 07.01.10
' Remove highlight of this colour
selColour = Selection.Range.HighlightColorIndex

Set rng = ActiveDocument.Content
gotOne = False
rng.End = 0
myCount = 0
Do
  With rng.Find
   .ClearFormatting
   .Replacement.ClearFormatting
   .Text = ""
   .Highlight = True
   .Wrap = False
   .Replacement.Text = ""
   .Forward = True
   .MatchWildcards = False
   .Execute
  End With
  gotOne = rng.Find.Found

  foundColour = rng.HighlightColorIndex
  If foundColour = selColour Then rng.HighlightColorIndex = 0

  If foundColour > 99 Then
    findLength = rng.End - rng.Start
    rng.End = rng.Start + 1
    For i = 1 To findLength
      foundColour = rng.HighlightColorIndex
      If foundColour = selColour Then rng.HighlightColorIndex = 0
      rng.Start = rng.Start + 1
      rng.End = rng.End + 1
    Next i
  End If
  StatusBar = "On large files, this may take some time.  " _
       & Str(myCount)
  myCount = myCount + 1
  rng.Start = rng.End
Loop Until gotOne = False
StatusBar = "                     Finished!!!!!!"

End Sub


EH
Sub unTextColour()
' Version 07.01.10
' Remove font colour of this colour
selColour = Selection.Range.Font.Color

Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ""
  .Font.Color = selColour
  .Replacement.Font.Color = wdColorAutomatic
  .Wrap = False
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With
End Sub


EH
Sub Un()
' Version 07.01.10
' Remove highlight and font colour of this colour

nowTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

nowColour = Selection.Range.Font.Color
If nowColour <> wdBlack And nowColour <> wdColorAutomatic Then
  Call unTextColour
End If

nowColour = Selection.Range.HighlightColorIndex
If nowColour > 0 Then
  Call UnHighlight
End If
ActiveDocument.TrackRevisions = nowTrack
End Sub




EH
Sub HighlightAllOff()
' Version 02.04.13
' Remove all highlighting, including in boxes

Set rng = ActiveDocument.Content
rng.HighlightColorIndex = 0
If ActiveDocument.Shapes.Count > 0 Then
  For Each shp In ActiveDocument.Shapes
    If shp.TextFrame.HasText Then
      shp.TextFrame.TextRange.HighlightColorIndex = 0
    End If
  Next
  Beep
End If
End Sub


EH
Sub unHighlightExcept()
' Version 11.01.11
' Remove all highlights except one/two chosen colours
keepColour1 = wdYellow
' And another colour as well?
' keepColour2 = wdBrightGreen
keepColour2 = wdTurquoise
' keepColour2 = wdRed
' keepColour2 = 0

Set rng = ActiveDocument.Content
theEnd = rng.End
gotOne = False
nowTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
rng.End = 0
Do
  With rng.Find
   .ClearFormatting
   .Replacement.ClearFormatting
   .Text = ""
   .Highlight = True
   .Wrap = False
   .Replacement.Text = ""
   .Forward = True
   .MatchWildcards = False
   .Execute
  End With
  gotOne = rng.Find.Found
  If gotOne = True Then
    foundColour = rng.HighlightColorIndex
    If foundColour > 99 Then
    ' Mixed colours of highlighting
      colEnd = rng.End
      Do
        rng.End = rng.Start + 1
        foundColour = rng.HighlightColorIndex
        If (foundColour <> keepColour1) And (foundColour <> keepColour2) Then
          rng.HighlightColorIndex = 0
        End If
        If foundColour > 99 Then rng.HighlightColorIndex = 0
        rng.Start = rng.End
      Loop Until rng.End = colEnd
    Else
      If (foundColour <> keepColour1) And (foundColour <> keepColour2) Then
        rng.HighlightColorIndex = 0
      End If
    End If
    StatusBar = "On large files, this may take some time.  " _
         & Str(theEnd - rng.End)
    rng.Start = rng.End
  End If
Loop Until gotOne = False

' Now remove all the 'Not formatted' track revisions
theEnd = ActiveDocument.Content.End
started = False
For Each rev In ActiveDocument.Range.Revisions
  Set rng = rev.Range
  myType = rev.FormatDescription
  If myType = "Formatted: Not Highlight" Then rng.Revisions.AcceptAll
  StatusBar = "Getting rid of 'Formatted: Not Highlight'..." _
       & Str(theEnd - rng.End)
Next rev
ActiveDocument.TrackRevisions = nowTrack
StatusBar = "               Finished!!!!!!!!!!!!!!!"
Selection.HomeKey Unit:=wdStory
End Sub


EH
Sub ShowFixedSpaces()
' Version 09.02.10
' Fixed spaces visible

myColour = wdGray25

Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Text = "^s"
  .Highlight = True
  .Wrap = wdFindContinue
  .Execute
End With
If rng.Find.Found = False Then
  oldColour = Options.DefaultHighlightColorIndex
  Options.DefaultHighlightColorIndex = myColour
  Set rng = ActiveDocument.Range
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "^s"
    .Replacement.Text = "^s"
    .Replacement.Highlight = True
    .Wrap = wdFindContinue
    .Execute Replace:=wdReplaceAll
  End With
  Options.DefaultHighlightColorIndex = oldColour
Else
  Set rng = ActiveDocument.Range
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "^s"
    .Replacement.Text = "^s"
    .Replacement.Highlight = False
    .Wrap = wdFindContinue
    .Execute Replace:=wdReplaceAll
  End With
End If
End Sub


EH
Sub ShowTabs()
' Version 09.02.10
' Make tabs visible

myColour = wdTurquoise

Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^t"
  .Highlight = True
  .Wrap = wdFindContinue
  .Execute
End With
If rng.Find.Found = False Then
  oldColour = Options.DefaultHighlightColorIndex
  Options.DefaultHighlightColorIndex = myColour
  Set rng = ActiveDocument.Range
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "^t"
    .Replacement.Text = "^t"
    .Replacement.Highlight = True
    .Wrap = wdFindContinue
    .Execute Replace:=wdReplaceAll
  End With
  Options.DefaultHighlightColorIndex = oldColour
Else
  Set rng = ActiveDocument.Range
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "^t"
    .Replacement.Text = "^t"
    .Replacement.Highlight = False
    .Wrap = wdFindContinue
    .Execute Replace:=wdReplaceAll
  End With
End If
End Sub


EH
Sub HighlightSame()
' Version 22.07.11
' Highlight this text in this colour

myHiColour = wdGray25
Dim myCol(20)
myCol(0) = wdNoHighlight
myCol(1) = wdYellow
myCol(2) = wdTurquoise
myCol(3) = wdBrightGreen
myCol(4) = wdPink
myCol(5) = wdRed
myCol(6) = wdGreen
myCol(7) = wdDarkYellow
myCol(8) = wdGray25
myCol(9) = wdGray50

' Preserve TC status and existing highlight colour
oldColour = Options.DefaultHighlightColorIndex
oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text
nowTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

Dim v As Variable: Dim nowColour As Long
varsExist = False
For Each v In ActiveDocument.Variables
  If v.Name = "colNum" Then varsExist = True: Exit For
Next v

If varsExist = False Then
  If Selection.Start = Selection.End Then
    Selection.End = Selection.Start + 1
    findText = Selection
    nowColour = Selection.Range.HighlightColorIndex
    If nowColour = wdNoHighlight Then nowColour = myHiColour _
         Else nowColour = wdNoHighlight
    Options.DefaultHighlightColorIndex = nowColour
  Else
    findText = Selection
    Selection.End = Selection.Start + 1
    nowColour = Selection.Range.HighlightColorIndex
    If nowColour = 0 Then nowColour = myHiColour
    Options.DefaultHighlightColorIndex = nowColour
    highlightYes = True
  End If
Else
  If Selection.Start = Selection.End Then
    ' If nothing selected, assume range as set
    ' by stored variables.
      wasStart = ActiveDocument.Variables("selStart")
      wasEnd = ActiveDocument.Variables("selEnd")
      wasCol = ActiveDocument.Variables("colNum")
    If Selection.Start < wasStart Or Selection.End > wasEnd Then
      ' If outside H_Plus_Minus range, just change highlight
      ' of single character
      Selection.End = Selection.Start + 1
      findText = Selection
      nowColour = Selection.Range.HighlightColorIndex
      If nowColour = wdNoHighlight Then nowColour = myHiColour _
           Else nowColour = wdNoHighlight
      Options.DefaultHighlightColorIndex = nowColour
    Else
    ' Select the text range as coloured by
    ' HighlightPlus or HighlightMinus
      Selection.End = wasEnd
      findText = Selection
      Options.DefaultHighlightColorIndex = myCol(wasCol)
    End If
  Else
  ' if some text is selected ...
    findText = Selection
    Selection.End = Selection.Start + 1
    nowColour = Selection.Range.HighlightColorIndex
    Options.DefaultHighlightColorIndex = nowColour
  ' If the selection has no colour, use the default colour
    If Selection.Range.HighlightColorIndex = 0 Then _
         Options.DefaultHighlightColorIndex = myHiColour
  End If
End If
Selection.End = Selection.Start
' Now F&R all occurrences of that text into the same
' highlight colour
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = findText
  .MatchCase = True
  .Replacement.Text = "^&"
  .Replacement.Highlight = True
  .Wrap = wdFindContinue
  .Execute Replace:=wdReplaceAll
End With

' Restore to original state
Options.DefaultHighlightColorIndex = oldColour
ActiveDocument.TrackRevisions = nowTrack
With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchCase = False
  .Replacement.Highlight = False
End With
End Sub


EH
Sub HighlightLister()
' Version 26.05.11
' List all the highlight colours used
allHighs = ""
Selection.WholeStory
Selection.Copy
Documents.Add
Selection.Paste
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Highlight = False
  .Text = ""
  .Replacement.Text = ""
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With
wasCol = 0
For Each ch In ActiveDocument.Characters
  thisCol = ch.HighlightColorIndex
  If thisCol <> wasCol Then
    Select Case thisCol
      Case 0: 'Do nowt
      Case wdYellow: col = "Yellow"
      Case wdBrightGreen: col = "BrightGreen"
      Case wdGreen: col = " Green"
      Case wdPink: col = "Pink"
      Case wdRed: col = "Red"
      Case wdBlue: col = "Blue"
      Case wdGray25: col = "Gray25"
      Case wdGray50: col = "Gray50"
      Case wdTurquoise: col = "Turquoise"
      Case wdTeal: col = "Teal"
      Case wdDarkBlue: col = "DarkBlue"
      Case wdDarkYellow: col = "DarkYellow"
      Case wdDarkRed: col = "DarkRed"
      Case wdViolet: col = "Violet"
    Case Else
      ch.Select
      col = "A colour not on the list!"
    End Select
    If InStr(allHighs, col) = 0 Then allHighs = allHighs & col & ","
  End If
  wasCol = thisCol
  i = i + 1
  If i Mod 50 = 0 Then StatusBar = "Checked: " & Str(i)
Next ch

ActiveDocument.Close SaveChanges:=False

Selection.HomeKey Unit:=wdStory
Selection.TypeText Text:=Replace(allHighs, ",", vbCrLf)
Selection.Start = 0
Selection.Style = wdStyleNormal
Selection.Sort
Selection.End = 0
End Sub


EH
Sub HighlightListerDeLuxe()
' Version 27.05.11
' List all the highlight colours used

Dim gotCol(16) As Boolean
mixCol = 9999999
' Copy the whole text
Selection.WholeStory
Selection.Copy
Documents.Add
Selection.Paste
' Delete unhighlighted text
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Highlight = False
  .Text = ""
  .Replacement.Text = ""
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

' Find which colours are used
For Each wd In rng.Words
  thisCol = wd.HighlightColorIndex
  If thisCol < mixCol Then
    gotCol(thisCol) = True
  Else
    For Each ch In wd.Characters
      thisCol = ch.HighlightColorIndex
      gotCol(thisCol) = True
    Next ch
  End If
Next wd
ActiveDocument.Close SaveChanges:=False

' List colours used and not used
' avoiding 1 = white and 8 = black
Selection.HomeKey Unit:=wdStory
For i = 2 To 16
' Make the used colour stand out
  If gotCol(i) = True Then Selection.TypeText Text:=vbTab & vbTab & "aaa"
  Select Case i
    Case wdWhite: col = "White"
    Case wdBlack: col = "Black"
    Case wdYellow: col = "Yellow"
    Case wdBrightGreen: col = "BrightGreen"
    Case wdGreen: col = "Green"
    Case wdPink: col = "Pink"
    Case wdRed: col = "Red"
    Case wdBlue: col = "Blue"
    Case wdGray25: col = "Gray25"
    Case wdGray50: col = "Gray50"
    Case wdTurquoise: col = "Turquoise"
    Case wdTeal: col = "Teal"
    Case wdDarkBlue: col = "DarkBlue"
    Case wdDarkYellow: col = "DarkYellow"
    Case wdDarkRed: col = "DarkRed"
    Case wdViolet: col = "Violet"
  End Select
  If i <> 8 Then Selection.InsertBefore Text:=col & vbCrLf
  Selection.Range.HighlightColorIndex = i
  Selection.Start = Selection.End
Next i

Selection.Start = 0
Selection.Style = wdStyleNormal
Selection.Sort
' Remove the 'used' markers
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Highlight = False
  .Text = "aaa"
  .Replacement.Text = ""
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

Selection.HomeKey Unit:=wdStory
End Sub


EH
Sub BackgroundColourOff()
' Version 20.10.11
' Remove background colour

If InStr(Selection, Chr(13)) > 0 Then
  For Each para In Selection.Paragraphs
    Set rng = para.Range
    rng.End = rng.End - 1
    rng.Shading.BackgroundPatternColor = wdColorAutomatic
  Next para
Else
  Selection.Shading.BackgroundPatternColor = wdColorAutomatic
End If
End Sub



EN
Sub InstantFindDown()
' Version 07.03.13
' Find selected text downwards

If Selection.Start = Selection.End Then Selection.Words(1).Select
thisBit = Selection
If Asc(thisBit) <> 32 Then thisBit = Trim(thisBit)
thisBit = Replace(thisBit, "^", "^^")

Selection.Start = Selection.End
hereNow = Selection.Start
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Wrap = False
  .Text = thisBit
  .Replacement.Text = thisBit
  .MatchWildcards = False
  .MatchCase = False
  .MatchWholeWord = False
  .Forward = True
  .Execute
End With
If Selection.Start = hereNow Then
  Beep
Else
' Move the screen display down a couple of lines
  Selection.MoveUp Unit:=wdLine, Count:=2
  Selection.MoveDown Unit:=wdLine, Count:=2
  Selection.Find.Execute
End If
' Leaves F&R dialogue in a sensible state
Selection.Find.Wrap = wdFindContinue
End Sub


EN
Sub InstantFindDownWild()
' Version 07.03.13
' Find selected text downwards wildcards set

If Selection.Start = Selection.End Then Selection.Words(1).Select
thisBit = Selection
If Asc(thisBit) <> 32 Then thisBit = Trim(thisBit)
thisBit = Replace(thisBit, "^", "^^")

Selection.Start = Selection.End
hereNow = Selection.Start
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Wrap = False
  .Text = thisBit
  .Replacement.Text = thisBit
  .MatchWildcards = True
  .MatchWholeWord = False
  .MatchCase = False
  .Forward = True
  .Execute
End With
If Selection.Start = hereNow Then
  Beep
Else
' Move the screen display down a couple of lines
  Selection.MoveUp Unit:=wdLine, Count:=2
  Selection.MoveDown Unit:=wdLine, Count:=2
  Selection.Find.Execute
End If
' Leaves F&R dialogue in a sensible state
Selection.Find.Wrap = wdFindContinue
End Sub


EN
Sub InstantFindUp()
' Version 07.03.13
' Find selected text upwards

If Selection.Start = Selection.End Then Selection.Words(1).Select
thisBit = Selection
If Asc(thisBit) <> 32 Then thisBit = Trim(thisBit)
thisBit = Replace(thisBit, "^", "^^")

Selection.End = Selection.Start
hereNow = Selection.Start
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Wrap = False
  .Forward = False
  .Text = thisBit
  .Replacement.Text = thisBit
  .MatchCase = False
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute
End With
' Leave F&R dialogue in a sensible state
Selection.Find.Forward = True
Selection.Find.Wrap = wdFindContinue

If Selection.Start = hereNow Then
  Beep
Else
' Move the screen display down a couple of lines
  Selection.MoveUp Unit:=wdLine, Count:=2
  Selection.MoveDown Unit:=wdLine, Count:=2
  Selection.Find.Execute
End If
End Sub


EN
Sub InstantFindTop()
' Version 18.11.10
' Find this from the top

If Selection.Start = Selection.End Then
  Selection.MoveRight Unit:=wdWord, Count:=1
  Selection.MoveLeft Unit:=wdWord, Count:=1, Extend:=wdExtend
End If

thisBit = Trim(Selection)
Selection.Start = Selection.End
Selection.HomeKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Wrap = False
  .Text = thisBit
  .Replacement.Text = thisBit
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchCase = False
  .Forward = True
  .Execute
End With
' Move the screen display down a couple of lines
Selection.MoveUp Unit:=wdLine, Count:=2
Selection.MoveDown Unit:=wdLine, Count:=2
Selection.Find.Execute
' Leaves F&R dialogue in a sensible state
Selection.Find.Wrap = wdFindContinue
End Sub


EN
Sub InstantFindBottom()
' Version 25.02.11
' I couldn't possibly comment!

If Selection.Start = Selection.End Then
  Selection.MoveRight Unit:=wdWord, Count:=1
  Selection.MoveLeft Unit:=wdWord, Count:=1, Extend:=wdExtend
End If

thisBit = Trim(Selection)
Selection.Start = Selection.End
Selection.EndKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Wrap = False
  .Text = thisBit
  .Replacement.Text = thisBit
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchCase = False
  .Forward = False
  .Execute
End With
' Move the screen display down a couple of lines
Selection.MoveUp Unit:=wdLine, Count:=2
Selection.MoveDown Unit:=wdLine, Count:=2
Selection.Find.Execute
' Leaves F&R dialogue in a sensible state
With Selection.Find
  .Wrap = wdFindContinue
  .Forward = True
End With
End Sub


EN
Sub FindFwd()
' Version 12.11.10
' Next find forwards
' Alt-Right
Selection.Start = Selection.End
hereNow = Selection.End
With Selection.Find
  .Wrap = False
  .Forward = True
  .MatchCase = False
  .Execute
End With
If Selection.Start = hereNow And Selection.End = hereNow Then
  Beep
Else
' Move the screen display down a couple of lines
  Selection.MoveUp Unit:=wdLine, Count:=2
  Selection.MoveDown Unit:=wdLine, Count:=2
  Selection.Find.Execute
End If
' Leave F&R dialogue in a sensible state
Selection.Find.Wrap = wdFindContinue

End Sub


EN
Sub FindBack()
' Version 12.11.10
' Next find backwards
' Alt-Left
Selection.End = Selection.Start
hereNow = Selection.End
With Selection.Find
  .Wrap = False
  .Forward = False
  .MatchCase = False
  .Execute
End With

If Selection.Start = hereNow And Selection.End = hereNow Then
  Beep
Else
' Move the screen display down a couple of lines
  hereNow = Selection.Start
  Selection.MoveUp Unit:=wdLine, Count:=2
  Selection.MoveDown Unit:=wdLine, Count:=2
  With Selection.Find
    .Forward = True
    .Execute
  End With
  If Selection.Start > hereNow Then
    With Selection.Find
      .Forward = False
      .Execute
    End With
  End If
End If
' Leave F&R dialogue in a sensible state
Selection.Find.Forward = True
Selection.Find.Wrap = wdFindContinue
End Sub


EN
Sub FindFwdCase()
' Version 30.11.10
' Next case sensitive find forwards

Selection.Start = Selection.End
hereNow = Selection.End
With Selection.Find
  .Wrap = False
  .Forward = True
  .MatchCase = True
  .Execute
End With
If Selection.Start = hereNow And Selection.End = hereNow Then
  Beep
Else
' Move the screen display down a couple of lines
  hereNow = Selection.Start
  Selection.MoveUp Unit:=wdLine, Count:=2
  Selection.MoveDown Unit:=wdLine, Count:=2
  With Selection.Find
    .Forward = True
    .Execute
  End With
  If Selection.Start > hereNow Then
    With Selection.Find
      .Forward = False
      .Execute
    End With
  End If
End If
' Now leave F&R dialogue in a sensible state
With Selection.Find
  .Forward = True
  .Execute
End With
End Sub


EN
Sub FindBackCase()
' Version 30.11.10
' Next case sensitive find backwards

Selection.End = Selection.Start
hereNow = Selection.Start
With Selection.Find
  .Wrap = False
  .Forward = False
  .MatchCase = True
  .Execute
End With

If Selection.Start = hereNow And Selection.End = hereNow Then
  Beep
Else
' Move the screen display down a couple of lines
  hereNow = Selection.Start
  Selection.MoveUp Unit:=wdLine, Count:=2
  Selection.MoveDown Unit:=wdLine, Count:=2
  With Selection.Find
    .Forward = True
    .Execute
  End With
  If Selection.Start > hereNow Then
    With Selection.Find
      .Forward = False
      .Execute
    End With
  End If
End If
' Leave F&R dialogue in a sensible state
With Selection.Find
  .Forward = True
  .Wrap = wdFindContinue
End With
End Sub


EN
Sub FindReplaceGo()
' Version 05.02.11
' Find, replace and move to next

hereNow = Selection.End
If Selection.End = Selection.Start Then
  With Selection.Find
    .Wrap = False
    .Forward = True
    .Execute
  End With
Else
  Selection.End = Selection.Start
  With Selection.Find
    .Execute Replace:=wdReplaceOne
    .Wrap = False
    .Forward = True
  End With
  Selection.Start = Selection.End
  Selection.Find.Execute
End If

If Selection.Start = hereNow And Selection.End = hereNow Then
  Beep
Else
' Move the screen display down a couple of lines
  Selection.MoveUp Unit:=wdLine, Count:=2
  Selection.MoveDown Unit:=wdLine, Count:=2
  Selection.Find.Execute
End If
Selection.Find.Wrap = wdFindContinue
End Sub


EN
Sub FindReplaceStay()
' Version 06.02.10
' Find and replace but don't move to next

Selection.End = Selection.Start
With Selection.Find
  .Execute Replace:=wdReplaceOne
End With
End Sub


EN
Sub FindThisFwdMark()
' Version 17.03.11
' Instant find, but leave marker

If Selection.Start = Selection.End Then
  Selection.MoveRight Unit:=wdWord, Count:=1
  Selection.MoveLeft Unit:=wdWord, Count:=1, Extend:=wdExtend
End If
thisBit = Trim(Selection)
Selection.Start = Selection.End
Selection.TypeText ("[[[")
hereNow = Selection.Start
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Wrap = False
  .Forward = True
  .Text = thisBit
  .Replacement.Text = thisBit
  .MatchCase = False
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute
End With
If Selection.Start = hereNow And Selection.End = hereNow Then
  Beep
Else
' Move the screen display down a couple of lines
  Selection.MoveUp Unit:=wdLine, Count:=2
  Selection.MoveDown Unit:=wdLine, Count:=2
  Selection.Find.Execute
End If
' Leave F&R dialogue in a sensible state
Selection.Find.Wrap = wdFindContinue
End Sub


EN
Sub FindThisBackMark()
' Version 12.11.10
' Instant find, but leave marker

If Selection.Start = Selection.End Then
  Selection.MoveRight Unit:=wdWord, Count:=1
  Selection.MoveLeft Unit:=wdWord, Count:=1, Extend:=wdExtend
End If
thisBit = Trim(Selection)
Selection.End = Selection.Start
Selection.TypeText ("[[[")
hereNow = Selection.Start
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Wrap = False
  .Forward = False
  .Text = thisBit
  .Replacement.Text = thisBit
  .MatchCase = False
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute
End With
' Leave F&R dialogue in a sensible state
Selection.Find.Forward = True
Selection.Find.Wrap = wdFindContinue

If Selection.Start = hereNow And Selection.End = hereNow Then
  Beep
Else
' Move the screen display down a couple of lines
  Selection.MoveUp Unit:=wdLine, Count:=2
  Selection.MoveDown Unit:=wdLine, Count:=2
  Selection.Find.Execute
End If
End Sub


EN
Sub FindMyMarker()
' Version 15.02.11
' Find my marker
oldFind = Selection.Find.Text

Selection.HomeKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Text = "[[["
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute
End With

If Selection.Start = 0 Then
  Beep
Else
  Selection.MoveUp Unit:=wdLine, Count:=2
  Selection.MoveDown Unit:=wdLine, Count:=2
  Selection.Find.Execute
End If

Selection.Find.Text = oldFind
End Sub



EN
Sub FindClip()
' Version 24.05.13
' Find whatever is in the clipboard

' Move up this many lines first, before starting the search
moveLinesUp = 20

Application.ScreenUpdating = False
If moveLinesUp > 0 Then Selection.MoveUp , moveLinesUp
hereNow = Selection.Start
Selection.Paste
Selection.Start = hereNow
strClip = Trim(Selection)
WordBasic.EditUndo
Application.ScreenUpdating = True

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Wrap = False
  .Forward = True
  .MatchCase = False
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Text = strClip
  .Execute
End With
If Selection.Start = hereNow Then
  Beep
Else
' Move the screen display down a couple of lines
  Selection.MoveUp Unit:=wdLine, Count:=2
  Selection.MoveDown Unit:=wdLine, Count:=2
  Selection.Find.Execute
End If
'Add this to leave F&R dialogue in a sensible state
Selection.Find.Wrap = wdFindContinue
End Sub


EN
Sub FindClipTop()
' Version 14.05.13
' From the top, find whatever is in the clipboard

Beep
myTime = Timer
Do
Loop Until Timer > myTime + 0.1
Beep
Application.ScreenUpdating = False
Selection.HomeKey Unit:=wdStory
Selection.Paste
Selection.Start = 0
strClip = Trim(Selection)
WordBasic.EditUndo
Application.ScreenUpdating = True
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Wrap = False
  .Forward = True
  .Text = strClip
  .MatchCase = False
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute
End With
' Move the screen display down a couple of lines
If Selection.Start = 0 Then
  Beep
Else
  Selection.MoveUp Unit:=wdLine, Count:=2
  Selection.MoveDown Unit:=wdLine, Count:=2
  Selection.Find.Execute
End If
Selection.Find.Wrap = wdFindContinue
End Sub


EN
Sub InstantFindFormatDown()
' Version 10.09.10
' Find format similar to this

hereNow = Selection.Start
isSuper = Selection.Font.Superscript
thisBit = Trim(Selection)
If Selection.End = Selection.Start Then
  thisBit = ""
  Selection.MoveEnd , 1
End If
isSuper = Selection.Font.Superscript
isSub = Selection.Font.Subscript
isItalic = Selection.Font.Italic
isBold = Selection.Font.Bold
Selection.MoveStart , 10
Selection.End = Selection.Start
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Wrap = False
  If isSuper Then .Font.Superscript = True
  If isSub Then .Font.Subscript = True
  If isItalic Then .Font.Italic = True
  If isBold Then .Font.Bold = True
  .Text = thisBit
  .Replacement.Text = thisBit
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .MatchCase = False
  .Forward = True
  .Execute
End With
If Selection.End = hereNow Then Beep
If Selection.Start = hereNow + 10 Then
  Beep
  Selection.Start = hereNow
  Selection.End = hereNow
End If

' Leave F&R dialogue in a sensible state
Selection.Find.Wrap = wdFindContinue
End Sub


EN
Sub InstantFindFormatUp()
' Version 10.09.10
' Find format similar to this

hereNow = Selection.Start
thisBit = Trim(Selection)
If Selection.End = Selection.Start Then
  thisBit = ""
  Selection.MoveEnd , 1
End If
isSuper = Selection.Font.Superscript
isSub = Selection.Font.Subscript
isItalic = Selection.Font.Italic
isBold = Selection.Font.Bold
Selection.MoveStart , -10
Selection.End = Selection.Start
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Wrap = False
  .Forward = False
  If isSuper Then .Font.Superscript = True
  If isSub Then .Font.Subscript = True
  If isItalic Then .Font.Italic = True
  If isBold Then .Font.Bold = True
  .Text = thisBit
  .Replacement.Text = thisBit
  .MatchCase = False
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute
End With
If Selection.Start = hereNow Then Beep
If Selection.Start = hereNow - 10 Then
  Beep
  Selection.Start = hereNow
  Selection.End = hereNow
End If

'Add these two to leave F&R dialogue in a sensible state
Selection.Find.Forward = True
Selection.Find.Wrap = wdFindContinue
End Sub


EN
Sub FindSamePlace()
' Version 17.12.12
' Find the same place in another file

myStep = 10
minLength = 15
If Selection.Start = Selection.End Then
  hereNow = Selection.Start
  Selection.HomeKey Unit:=wdLine
  cursorPos = hereNow - Selection.Start
  Selection.MoveDown Unit:=wdLine, Count:=1, Extend:=wdExtend
  wildSearch = (Len(Selection) > 2 * myStep)
End If
mySearch = Trim(Replace(Selection, Chr(13), ""))

Set thisDoc = ActiveDocument
For Each myWnd In Application.Windows
  Set myDoc = myWnd.Document
  If myDoc.FullName <> thisDoc.FullName Then
    myDoc.Activate
    Selection.HomeKey Unit:=wdStory
    With Selection.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .MatchCase = False
      .MatchWildcards = False
      .MatchWholeWord = False
      .MatchSoundsLike = False
      .Text = mySearch
      .Forward = True
      .Execute
    End With
    If Selection.Find.Found = True Then
      If myWnd.WindowState = 2 Then myWnd.WindowState = wdWindowStateNormal
      Exit Sub
    End If
  End If
Next myWnd
If wildSearch = True Then
  Do
    If Len(mySearch) > 2 * cursorPos Then
      mySearch = Left(mySearch, Len(mySearch) - myStep)
    Else
      mySearch = Right(mySearch, Len(mySearch) - myStep)
      cursorPos = cursorPos - myStep
    End If
    For Each myWnd In Application.Windows
      Set myDoc = myWnd.Document
      If myDoc.FullName <> thisDoc.FullName Then
        myDoc.Activate
        Selection.HomeKey Unit:=wdStory
        With Selection.Find
          .ClearFormatting
          .Replacement.ClearFormatting
          .MatchCase = False
          .MatchWildcards = False
          .Text = mySearch
          .Execute
        End With
        If Selection.Find.Found = True Then
          If myWnd.WindowState = 2 Then myWnd.WindowState = wdWindowStateNormal
          Exit Sub
        End If
      End If
    Next myWnd
  Loop Until Len(mySearch) < minLength
End If
Beep
thisDoc.Activate
End Sub


EN
Sub FindInContext()
' Version 28.01.13
' Find certain words within a given word range

mainWord = "fish"
NearWord1 = "chips"
NearWord2 = "peas"
distance = 50

' Check file for alternative variable settings
myFileName = "zzSwitchList"
Set nowDoc = ActiveDocument
For Each myWnd In Application.Windows
  If InStr(myWnd.Document.Name, myFileName) Then
    myWnd.Activate
    allText = ActiveDocument.Content
  ' First variable
    myVariable = "mainword = "
    myPos = InStr(LCase(allText), myVariable)
    If myPos > 0 Then
      myData = Mid(allText, myPos + Len(myVariable) + 1)
      endPos = InStr(myData, Chr(34)) - 1
      If endPos >= 0 Then myData = Left(myData, endPos)
      mainWord = myData
    End If
  ' Second variable
    myVariable = "nearword1 = "
    myPos = InStr(LCase(allText), myVariable)
    If myPos > 0 Then
      myData = Mid(LCase(allText), myPos + Len(myVariable) + 1)
      endPos = InStr(myData, Chr(34)) - 1
      If endPos >= 0 Then myData = Left(myData, endPos)
      NearWord1 = myData
    End If
  ' Third variable
    myVariable = "nearword2 = "
    myPos = InStr(LCase(allText), myVariable)
    If myPos > 0 Then
      myData = Mid(allText, myPos + Len(myVariable) + 1)
      endPos = InStr(myData, Chr(34)) - 1
      If endPos >= 0 Then myData = Left(myData, endPos)
      NearWord2 = myData
    End If
  ' Fourth (number) variable
    myVariable = "distance = "
    myPos = InStr(allText, myVariable)
    If myPos > 0 Then distance = Val(Mid(allText, myPos + Len(myVariable)))
  End If
Next myWnd

nowDoc.Activate
Set rng = ActiveDocument.Content
Set rng2 = ActiveDocument.Content
rng.Start = Selection.End
' Go and find the first occurrence
With rng.Find
  .Text = mainWord
  .Wrap = False
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  If LCase(mainWord) = mainWord Then
    .MatchCase = False
  Else
    .MatchCase = True
  End If
  .Execute
End With

Do While rng.Find.Found = True
  rng2.Start = rng.Start
  rng2.End = rng.End
  rng2.MoveEnd wdWord, distance
  rng2.MoveStart wdWord, -distance
  If LCase(NearWord1) = NearWord1 Then
    wordPos1 = InStr(LCase(rng2), NearWord1)
  Else
    wordPos1 = InStr(rng2, NearWord1)
  End If
  If LCase(NearWord2) = NearWord2 Then
    wordPos2 = InStr(LCase(rng2), NearWord2)
  Else
    wordPos2 = InStr(rng2, NearWord2)
  End If
  found1 = (wordPos1 > 0) Or (NearWord1 = "")
  found2 = (wordPos2 > 0) Or (NearWord2 = "")
' If found then select it...
' ...but if either word is null, don't alter the selection
  If NearWord1 = "" Then wordPos1 = rng.Start - rng2.Start
  If NearWord2 = "" Then wordPos2 = rng.Start - rng2.Start
  If found1 And found2 Then
    nearWord1Start = rng2.Start + wordPos1
    nearWord2Start = rng2.Start + wordPos2
    If nearWord1Start < rng.Start Then rng.Start = nearWord1Start - 1
    If nearWord2Start < rng.Start Then rng.Start = nearWord2Start - 1
    nearWord1End = rng2.Start + wordPos1 + Len(NearWord1)
    nearWord2End = rng2.Start + wordPos2 + Len(NearWord2)
    If nearWord1End > rng.End Then rng.End = nearWord1End - 1
    If nearWord2End > rng.End Then rng.End = nearWord2End - 1
    rng.Select
    Exit Sub
  End If
  rng.Find.Execute
Loop
Beep
rng.Select
End Sub

EN
Sub FindThisOrThat()
' Version 24.02.14
' Find the next occurrence of certain specific words

Dim v As Variable
varsExist = False
For Each v In ActiveDocument.Variables
  If v.Name = "findText" Then varsExist = True: Exit For
Next v

If varsExist = False Then
  Call FindThisOrThatSetUp
  Exit Sub
End If

listWds = Trim(ActiveDocument.Variables("findText"))
If InStr(listWds, "|") > 0 Then
  de = "|"
Else
  de = ","
End If

numWds = Len(listWds) - Len(Replace(listWds, de, "")) + 1

ReDim findWds(numWds) As String
findWds = Split(listWds, de)

Selection.Collapse wdCollapseEnd
Set rng = ActiveDocument.Content
hereStart = Selection.Start

' Find the nearest of the required words
nearest = ActiveDocument.Range.End
wdNum = -1
For i = 0 To numWds - 1
  rng.Start = hereStart
  rng.End = nearest
  w = findWds(i)
  If Left(w, 1) = "~" Then
    w = Mid(w, 2)
    WC = True
  Else
    WC = False
  End If
  If Left(w, 1) = "$" Then
    w = Mid(w, 2)
    sens = True
  Else
    sens = False
  End If
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = w
    .Wrap = False
    .Replacement.Text = ""
    .Forward = True
    .MatchCase = sens
    .MatchWildcards = WC
    .MatchWholeWord = False
    .MatchSoundsLike = False
    .Execute
  End With
  wdPos = rng.Start
  If wdPos < nearest And wdPos <> hereStart Then
    nearest = wdPos
    wdNum = i
  End If
Next i

If wdNum >= 0 Then
  w = findWds(wdNum)
  If Left(w, 1) = "~" Then
    w = Mid(w, 2)
    WC = True
  Else
    WC = False
  End If
  If Left(w, 1) = "$" Then
    w = Mid(w, 2)
    sens = True
  Else
    sens = False
  End If
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = w
    .Wrap = False
    .Replacement.Text = ""
    .MatchCase = sens
    .MatchWildcards = WC
    .Forward = True
    .Execute
  End With
Else
  Beep
End If
End Sub


EN
Sub FindThisOrThatSetUp()
' Version 22.02.14
' Set-up for finding the next occurrence of certain specific words

Dim v As Variable
varsExist = False
For Each v In ActiveDocument.Variables
  If v.Name = "findText" Then varsExist = True: Exit For
Next v

If varsExist = False Then
  ActiveDocument.Variables.Add "findText", " "
  textNow = ""
Else
  textNow = ActiveDocument.Variables("findText")
End If

t = InputBox("Search for?", "Find This Or That", textNow)
If t = "" Then Exit Sub
ActiveDocument.Variables("findText") = t
 
Call FindThisOrThat

End Sub

EN
Sub PrepareToReplaceDown()
' Version 09.12.10
' Copy text into the F&R box

myText = Selection
If Asc(myText) <> 32 Then myText = Trim(myText)

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = False
  .MatchWholeWord = False
  .Text = myText
  .Replacement.Text = myText
  .MatchCase = True
End With
Selection.End = Selection.Start
CommandBars("Menu Bar").Controls("Edit").Controls("Replace...").Execute
End Sub



EN
Sub PrepareToReplaceDownTCoff()
' Version 03.08.11
' Copy text into the F&R box

ActiveDocument.TrackRevisions = False
myText = Selection
If Asc(myText) <> 32 Then myText = Trim(myText)

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = False
  .MatchWholeWord = False
  .Forward = True
  .Text = myText
  .Replacement.Text = myText
  .MatchCase = True
End With
Selection.End = Selection.Start
'This first command doesn't work for some reason!
' Application.Run MacroName:="EditReplace"
CommandBars("Menu Bar").Controls("Edit").Controls("Replace...").Execute
End Sub


EN
Sub PrepareToReplaceFromTop()
' Version 09.12.10
' Copy text into the F&R box from top

myText = Selection
If Asc(myText) <> 32 Then myText = Trim(myText)
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = myText
  .Replacement.Text = myText
  .MatchWildcards = False
  .MatchWholeWord = False
End With
Selection.HomeKey Unit:=wdStory
CommandBars("Menu Bar").Controls("Edit").Controls("Replace...").Execute
End Sub


EN
Sub PrepareToReplaceWithMarker()
' Version 09.12.10
' Copy text into the F&R box from top leaving marker

myText = Selection
If Asc(myText) <> 32 Then myText = Trim(myText)
Selection.Start = Selection.End
Selection.TypeText Text:="[[["


With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = myText
  .Replacement.Text = myText
  .MatchWildcards = False
  .MatchWholeWord = False
End With
Selection.HomeKey Unit:=wdStory
CommandBars("Menu Bar").Controls("Edit").Controls("Replace...").Execute
End Sub


EN
Sub FindAnything()
' Version 12.03.13
' Just find this text/note/page/etc NOW!
' Alt-F

myMarker = "[[["
myMarkerCode = "["

existingFind = Selection.Find.Text
startMyText = Selection.Start
endMyText = Selection.End
SelText = Selection
' If nothing selected, pick up the current word
noTextSelected = (Selection.End = Selection.Start)
If noTextSelected Then
  cursorPosn = Selection.Start
  Selection.Expand wdWord
  Selection.MoveEndWhile cset:=ChrW(8217) & "' ", Count:=wdBackward
 
  SelText = ""
  If Len(SelText) > 1 Then SelText = Selection
' Ends of the current word
  startMyText = Selection.Start
  endMyText = Selection.End
' Put the cursor back where it was
  Selection.End = cursorPosn
  Selection.Start = cursorPosn
End If

SelText = Trim(SelText)

' For safety, first clear all 'funny' finds down
With Selection.Find
  .Wrap = False
  .Forward = True
  .MatchAllWordForms = False
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
End With

inputCommand:
myText = InputBox("Find?", "Smart finder", SelText)
If Len(myText) = 0 Then Exit Sub
myText = Trim(Replace(myText, Chr(13), ""))

' Check the final character
markerAdded = False
If Right(myText, 1) = myMarkerCode Then
  If Len(myText) > 1 Then
' If it's on the end of some text, snip it off, type in
'  the marker and go to parsing stage
    Selection.TypeText Text:=myMarker
    myText = Replace(myText, myMarkerCode, "")
    markerAdded = True
  Else
' If it's just the marker code, go find the special marker
    Selection.HomeKey Unit:=wdStory
    With Selection.Find
      .ClearFormatting
      .Text = myMarker
      .MatchWildcards = False
      .Forward = True
      .Execute
    End With

    If Selection.Start = 0 Then
      Beep
      Selection.Start = startMyText
    Else
      Selection.MoveUp Unit:=wdLine, Count:=2
      Selection.MoveDown Unit:=wdLine, Count:=2
      Selection.Find.Execute
    End If
    Exit Sub
  End If
End If

' Default settings for all searches
mvStart = 0
mvEnd = 0
myReplace = ""
myStyle = ""
isBold = False
isItalic = False
isSuper = False
isSub = False
isSmalls = False
thisSize = 0
nmlSize = 0
thisFont = ""
nmlFont = ""
thisColour = 0
nmlColour = 0
sndsLike = False
wholeWds = False
allForms = False

If Len(myText) > 1 Then GoTo moreText
' Single-letter commands
WC = True
myReplace = ""
Select Case myText
  Case "a": myFind = "[A-Z]{2,}": ' acronyms (see also x and X)
  Case "B": myFind = "[ABC][DCE]": ' BC/AD/CE/BCE
  Case "b": myFind = "[abc][dce]": ' BC/AD/CE/BCE in small caps
    wasSmalls = 0: isSmalls = True
  Case "d": myFind = "[123][0-9] [A-O][a-z]@ [0-9]{2,4}": ' dates
  Case "D": myFind = "[0-9]{1,2}.[0-9]{1,2}.[0-9]{2,4}": ' dates
  Case "e": ' Do nowt and then later jump back to input function
  ' f is for font attributes - see below
  ' h is for headings - see below
  Case "E": myFind = "[A-Z][ -,][0-9]{4}"
  Case "i": myFind = "[A-Z.]{2} [A-Z][a-z]": ' People's initials dotted
  Case "I": myFind = "[A-Z]{2} [A-Z][a-z]": ' People's initials, no dots
  Case "m": myFind = "[ .][a-zA-Z]@\@[a-zA-Z]@.[a-zA-Z]{1,}": ' emails
  ' M is macro select, below
  Case "s": myFind = "^13[0-9]{1,}.[0-9]{1,}": mvStart = 1: ' section numbers
  Case "p": myFind = "[A-Z]{1,2}[0-9]{1,2} [0-9][A-Z]{2}": ' postcodes
  Case "u": myFind = "[0-9 ^0160][kcmM][NJAVmg]>": ' units
  Case "w": myFind = "[wt]{2}[wp][.:][a-z/]{2,}": ' web addresses
  ' x is expand acronym below
  ' X is expand acronym, case insensitively below
  Case "y": myFind = "[12][0-9]{3}[!0-9]": mvEnd = -1: ' years
  Case "z": myFind = "<[0-9]{5}>"
  Case "Z": myFind = "[A-Z][0-9][A-Z] [0-9][A-Z][0-9]>"
  Case "<": myFind = "^13\<[A-Za-z]{1,3}\>"
    mvStart = 1: ' <A>, <Cap>
  Case "(": myFind = "\([0-9.]@\)": ' Number in brackets: (3.16), (12.2)
  Case ")": myFind = "^13([0-9]{1,2}). ": myReplace = "^p\1^t"
 
  Case "-":
    hyphenPos = InStr(SelText, "-")
    spacePos = InStr(SelText, " ")
    markerPos = hyphenPos + spacePos
    If Len(SelText) < 2 Or markerPos = 0 Then
      MsgBox ("Select two words separated by a hyphen or a space")
      Exit Sub
    End If
    endOne = Mid(SelText, markerPos - 1, 1)
    startTwo = Mid(SelText, markerPos + 1, 1)
    fstWd = Left(SelText, markerPos - 2)
    scndWd = Mid(SelText, markerPos + 2)
    myFind = fstWd & "[ " & endOne & startTwo & "^=-]{2,4}" & scndWd
    ' Full point off note number
 
  Case "A": myFind = Trim(Replace(SelText, " ", ""))
    allForms = True: WC = False
  Case "S": myFind = Trim(SelText): sndsLike = True: WC = False
  Case "W": myFind = Trim(SelText): wholeWds = True: WC = False
     
  Case "x": ' Xpand, i.e. look for acronym expansion
    If markerAdded = False Then
      Selection.InsertAfter Text:="[[["
      Selection.Start = endMyText
      Selection.End = endMyText
    End If
    myFind = "<"
    abbrLength = Len(SelText)
    For i = 1 To abbrLength
      myFind = myFind & Mid(SelText, i, 1) & "[a-z]{1,}^32"
    Next i
    myFind = Left(myFind, Len(myFind) - 3)

  Case "X": ' Xpand, i.e. look for acronym expansion, either case
    If markerAdded = False Then
      Selection.InsertAfter Text:="[[["
      Selection.Start = endMyText
      Selection.End = endMyText
    End If
    myFind = "<"
    abbrLength = Len(SelText)
    For i = 1 To abbrLength
      If i = 4 Then Exit For
      myChar = Mid(SelText, i, 1)
      myChar = "[" & LCase(myChar) & UCase(myChar) & "]"
      myFind = myFind & myChar & "[a-z]{1,}^32"
    Next i
    myFind = Left(myFind, Len(myFind) - 3)

  Case "h": WC = False: myFind = "": ' A heading
    myStyle = Selection.Range.Style
    If myStyle = wdStyleNormal Then
      myStyle = ""
      Selection.End = Selection.Start + 1
      Selection.Font.Reset
      nmlSize = Selection.Font.Size
      nmlFont = Selection.Font.Name
      nmlColour = Selection.Font.Color
      wasBold = Selection.Font.Bold
      wasItalic = Selection.Font.Italic
    ' undo the change, i.e. restore the text's original attributes
      WordBasic.EditUndo
    ' Check the text's emphasis now
      isBold = Selection.Font.Bold
      isItalic = Selection.Font.Italic
      thisSize = Selection.Font.Size
      thisFont = Selection.Font.Name
      thisColour = Selection.Font.Color
    End If
    Selection.Expand wdParagraph
    If myStyle = "" And wasBold = isBold And wasItalic = isItalic _
         And thisSize = nmlSize And thisFont = nmlFont _
         And thisColour = nmlColour Then
      myFind = Left(Selection, 1)
    End If
    Selection.Start = Selection.End
 
  Case "f": WC = False: myFind = "": ' Font attributes
    If Selection.End = Selection.Start Then Selection.MoveEnd wdCharacter, 1
    Selection.Font.Reset
    nmlSize = Selection.Font.Size
    nmlFont = Selection.Font.Name
    nmlColour = Selection.Font.Color
    wasBold = Selection.Font.Bold
    wasItalic = Selection.Font.Italic
    wasSuper = Selection.Font.Superscript
    wasSub = Selection.Font.Subscript
    wasSmalls = Selection.Font.SmallCaps

  ' undo the change, i.e. restore the text's original attributes
    WordBasic.EditUndo
  ' Check the text's emphasis now
    isBold = Selection.Font.Bold
    isItalic = Selection.Font.Italic
    isSuper = Selection.Font.Superscript
    isSub = Selection.Font.Subscript
    isSmalls = Selection.Font.SmallCaps
    isStrike = Selection.Font.StrikeThrough
    isDStrike = Selection.Font.DoubleStrikeThrough
    isUnderline = Selection.Font.Underline
    thisSize = Selection.Font.Size
    thisFont = Selection.Font.Name
    thisColour = Selection.Font.Color

  Case "3"
  ' A caption/heading with the same first three characters
    Selection.HomeKey Unit:=wdLine
    Selection.MoveEnd wdCharacter, 3
    myFind = "^p" & Selection
    mvStart = 1
    WC = False
  Case "4"
  ' A caption/heading with the same first four characters
    Selection.HomeKey Unit:=wdLine
    Selection.MoveEnd wdCharacter, 4
    myFind = "^p" & Selection
    mvStart = 1
    WC = False
Case Else
    myFind = myText: WC = False
End Select

If myText = "e" Then SelText = Trim(existingFind): GoTo inputCommand

GoTo goFind

' See if it's a page, fnote, enote or section number
moreText:
startChar = Left(LCase(myText), 1)
lastChar = Asc(Right(myText, 1))
If lastChar > 47 And lastChar < 58 And Len(myText) <= 4 Then
' The final chacter is a number
  isACode = True
  Select Case startChar
    Case "p"
      Selection.GoTo What:=wdGoToPage, Count:=Mid(myText, 2)
    Case "n"
      Selection.GoTo What:=wdGoToFootnote, Count:=Mid(myText, 2)
    Case "f"
      Selection.GoTo What:=wdGoToFootnote, Count:=Mid(myText, 2)
    Case "e"
      Selection.GoTo What:=wdGoToEndnote, Count:=Mid(myText, 2)
    Case "c"
      Selection.GoTo What:=wdGoToComment, Count:=Mid(myText, 2)
    Case "s"
      Set rng = ActiveDocument.Content
      With rng.Find
        .ClearFormatting
        .Replacement.ClearFormatting
        .Text = "[^13\>^t]" & Mid(myText, 2)
        .MatchCase = False
        .MatchWildcards = True
        .MatchWholeWord = False
        .MatchSoundsLike = False
        .Wrap = False
        .Forward = True
        .Execute
      End With
      If rng.Find.Found = True Then
        rng.Select
        Selection.MoveStart wdCharacter, 1
      Else
        Beep
        Selection.Start = endMyText
      End If
    Case Else
      isACode = False
  End Select
  If isACode = True Then Exit Sub
End If

' If not a special command, then it's ordinary text to find
myFind = myText
If myReplace > "" Then myReplace = myText
WC = False

goFind:
Selection.Start = Selection.End
' Go and look for the word
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = myFind
  If Len(myStyle) > 0 And myStyle <> "Normal" Then .Style = myStyle
  .Replacement.Text = myReplace
  .Forward = True
  .MatchCase = False
  .MatchPhrase = mtchPhr
  .MatchWildcards = WC
  .MatchWholeWord = wholeWds
  .MatchSoundsLike = sndsLike
  .MatchAllWordForms = allForms
  If isBold Then .Font.Bold = True
  If isItalic Then .Font.Italic = True
  If isSuper Then .Font.Superscript = True
  If isSub Then .Font.Subscript = True
  If isSmalls Then .Font.SmallCaps = True
  If isStrike Then .Font.StrikeThrough = True
  If isDStrike Then .Font.DoubleStrikeThrough = True
  If isUnderline Then .Font.Underline = True
  If thisSize <> nmlSize Then .Font.Size = thisSize
  If thisFont <> nmlFont Then .Font.Name = thisFont
  If thisColour <> nmlColour Then .Font.Color = thisColour
  .Wrap = False
  .Forward = True
  .Execute
End With
If (Selection.Start = endMyText Or Selection.End = cursorPosn) And WC = False Then
  Beep
  Selection.End = startMyText
  With Selection.Find
    .Forward = False
    .Execute
  End With
Else
' Move the screen display down a couple of lines
  hereNow = Selection.Start
  Selection.MoveUp Unit:=wdLine, Count:=2
  Selection.MoveDown Unit:=wdLine, Count:=2
  With Selection.Find
    .Forward = True
    .Execute
  End With
  If Selection.Start > hereNow Then
    With Selection.Find
      .Forward = False
      .Execute
    End With
  End If
End If
Selection.MoveStart wdCharacter, mvStart
Selection.MoveEnd wdCharacter, mvEnd
With Selection.Find
  .Forward = True
  .Wrap = wdFindContinue
End With
End Sub



EN
Sub FindAdvanced1()
' Version 22.02.11
' Call up Advanced Find dialog box

With Dialogs(wdDialogEditFind)
  .MatchCase = True
  .Show
End With
End Sub


EN
Sub FindAdvanced2()
' Version 22.02.11
' Call up Advanced Find dialog box

Dialogs(wdDialogEditFind).Show
End Sub


EN
Sub FindReplaceAdvanced()
' Version 04.06.11
' Call up Advanced Find & Replace dialog box

Dialogs(wdDialogEditReplace).Show
End Sub


EN
Sub FindAdvanced3()
' Version 01.06.11
' Call up Advanced Find dialog box

With Selection.Find
  .MatchFuzzy = False
  .MatchPhrase = False
  .MatchAllWordForms = False
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .MatchCase = False
End With

Dialogs(wdDialogEditFind).Show
End Sub


EN
Sub FindHighlight()
' Version 04.02.12
' Find next highlighted text
' Alt-PageDown

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

'Clear down any funny finds
With Selection.Find
  .MatchCase = False
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
End With

mixedColour = 9999999
Dim thisColour, searchColour As Long
varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "hColour" Then varExists = True: Exit For
Next v

If varExists = False Then ActiveDocument.Variables.Add "hColour", 0
searchColour = ActiveDocument.Variables("hColour")

' If no text is selected, search for next highlight
If Selection.Start = Selection.End Then GoTo FindNext

' If some text is selected, see what colour it is;
' then go find more text of that colour.
selColour = Selection.Range.HighlightColorIndex
If selColour = mixedColour Then
  Selection.Start = Selection.End
  Beep
  Exit Sub
End If
If selColour > 0 Then
  searchColour = selColour
  ActiveDocument.Variables("hColour") = searchColour
  GoTo FindNext
End If

Selection.Start = Selection.End
Do
' Find some highlighted text
  With Selection.Find
   .ClearFormatting
   .Replacement.ClearFormatting
   .Text = ""
   .Highlight = True
   .Wrap = False
   .Replacement.Text = ""
   .Forward = True
   .MatchWildcards = False
   .Execute
  End With
 
  wasEnd = Selection.End
  wasStart = Selection.Start
  Selection.End = Selection.Start
  myResponse = MsgBox("This colour? (Cancel = any colour)", _
       vbQuestion + vbYesNoCancel)
  If myResponse = vbCancel Then
    ActiveDocument.Variables("hColour") = 0
    GoTo finish
  End If
  If Selection.Find.Found = False Then GoTo finish
  Selection.Start = wasEnd
Loop Until myResponse = vbYes
searchColour = Selection.Range.HighlightColorIndex
ActiveDocument.Variables("hColour") = searchColour

If myResponse = vbYes Then
  Selection.End = wasStart
  Selection.End = wasStart
  GoTo finish
End If
Selection.Start = wasEnd

FindNext:
' Find higlight of this specific colour
Selection.MoveEnd , 1
nowCol = Selection.Range.HighlightColorIndex
If nowCol = mixedColour Then
  Selection.End = Selection.Start + 1
  nowCol = Selection.Range.HighlightColorIndex
End If
If nowCol > 0 Then
' Find end to the nearest word
  Do
    hereNow = Selection.Start
    Selection.MoveStart wdWord, 1
    Selection.End = Selection.Start + 1
    hereAfter = Selection.Start
  Loop Until Selection.Range.HighlightColorIndex <> _
       nowCol Or Selection.End = ActiveDocument.Range.End _
       Or hereNow = hereAfter
End If

Selection.Start = Selection.End
' If we've got stuck in a field
If hereNow = hereAfter Then Selection.Start = Selection.Start + 1

If searchColour = 0 Then
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = ""
    .Highlight = True
    .Wrap = False
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = False
    .Execute
  End With
  foundSomething = Selection.Find.Found
Else
  Selection.Start = Selection.End
  foundHlight = False
  Do
    Do
      With Selection.Find
        .ClearFormatting
        .Replacement.ClearFormatting
        .Text = ""
        .Highlight = True
        .Wrap = False
        .Replacement.Text = ""
        .Forward = True
        .MatchWildcards = False
        .Execute
      End With
      foundSomething = Selection.Find.Found
      thisColour = Selection.Range.HighlightColorIndex
      If thisColour = searchColour Or thisColour > 100 Then
        foundHlight = True
      End If
    Loop Until foundHlight = True Or foundSomething = False
    If thisColour > 100 Then
      ' We've found a mixed-colour section
      foundHlight = False
      stopHere = Selection.End
      Selection.End = Selection.Start + 1
      firstCol = Selection.Range.HighlightColorIndex
      Do
      ' if the col of 1st char not the search col
        If firstCol <> searchColour Then
          Selection.End = Selection.Start
          Do
            Selection.MoveStart wdWord, 1
            Selection.MoveEnd 1
            thisColour = Selection.Range.HighlightColorIndex
          Loop Until thisColour <> firstCol
          Do
            hereNow = Selection.Start
            Selection.MoveEnd , -1
            hereAfter = Selection.Start
            thisColour = Selection.Range.HighlightColorIndex
          Loop Until thisColour = firstCol Or hereNow = hereAfter
          Selection.Start = Selection.End
          Selection.End = Selection.Start + 1
          firstCol = Selection.Range.HighlightColorIndex
        End If
      Loop Until firstCol = searchColour Or Selection.End > stopHere
      If firstCol = searchColour Then
        Selection.MoveEnd , -1
        foundHlight = True
      Else
        Selection.Start = Selection.End
      End If
    End If
  Loop Until foundHlight = True Or foundSomething = False
End If
If foundSomething = False Then Beep

finish:
Selection.End = Selection.Start - 1
Selection.MoveRight Count:=1
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = oldFind
  .Replacement.Text = oldReplace
  .Wrap = wdFindContinue
End With
End Sub


EN
Sub FindHighlightUp()
' Version 04.02.12
' Find highlighted text upwards
' Alt-PageUp
oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

'Clear down any funny finds
With Selection.Find
  .MatchCase = False
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
End With

mixedColour = 9999999
Dim v As Variable: Dim thisColour As Long: Dim searchColour As Long
varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "hColour" Then varExists = True: Exit For
Next v

If varExists = False Then ActiveDocument.Variables.Add "hColour", 0
searchColour = ActiveDocument.Variables("hColour")

' If no text is selected, search for next highlight
If Selection.Start = Selection.End Then GoTo FindNext

' If some text is selected, see what colour it is;
' then go find more text of that colour.
selColour = Selection.Range.HighlightColorIndex
If selColour = mixedColour Then
  Selection.Start = Selection.End
  Beep
  Exit Sub
End If
If selColour > 0 Then
  searchColour = selColour
  ActiveDocument.Variables("hColour") = searchColour
  GoTo FindNext
End If

Selection.Start = Selection.End
Do
  With Selection.Find
   .ClearFormatting
   .Replacement.ClearFormatting
   .Text = ""
   .Highlight = True
   .Wrap = False
   .Replacement.Text = ""
   .Forward = False
   .MatchWildcards = False
   .Execute
  End With
 
  Selection.End = Selection.Start
  myResponse = MsgBox("This colour? (Cancel = any colour)", vbQuestion + vbYesNoCancel)
  If myResponse = vbCancel Then
    ActiveDocument.Variables("hColour") = 0
    GoTo finish
  End If
  If Selection.Find.Found = False Then GoTo finish
Loop Until myResponse = vbYes
Selection.MoveEnd , 1
searchColour = Selection.Range.HighlightColorIndex
Selection.MoveEnd , -1
ActiveDocument.Variables("hColour") = searchColour
If myResponse = vbYes Then GoTo finish

FindNext:
If searchColour = 0 Then
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = ""
    .Highlight = True
    .Wrap = False
    .Replacement.Text = ""
    .Forward = False
    .MatchWildcards = False
    .Execute
  End With
  foundSomething = Selection.Find.Found
Else
  Selection.End = Selection.Start
  foundHlight = False
  Do
    Do
      With Selection.Find
        .ClearFormatting
        .Replacement.ClearFormatting
        .Text = ""
        .Highlight = True
        .Wrap = False
        .Replacement.Text = ""
        .Forward = False
        .MatchWildcards = False
        .Execute
      End With
      foundSomething = Selection.Find.Found
      thisColour = Selection.Range.HighlightColorIndex
      If thisColour = searchColour Or thisColour > 100 Then
        foundHlight = True
      End If
    Loop Until foundHlight = True Or foundSomething = False
    If thisColour > 100 Then
      ' We've found a mixed-colour section
      foundHlight = False
      stopHere = Selection.Start
      Selection.Start = Selection.End - 1
      firstCol = Selection.Range.HighlightColorIndex
      Do
      ' for each colour block
        Do
          Selection.MoveStart wdWord, -1
          thisColour = Selection.Range.HighlightColorIndex
        Loop Until thisColour = mixedColour
        Do
          wasLen = Selection.End - Selection.Start
          Selection.MoveStart , 1
          nowLen = Selection.End - Selection.Start
          thisColour = Selection.Range.HighlightColorIndex
        Loop Until thisColour <> mixedColour Or nowLen = wasLen
        Selection.End = Selection.Start
        If nowLen = wasLen Then
          Selection.MoveLeft Unit:=wdCharacter, Count:=1
        End If
      Loop Until thisColour = searchColour Or Selection.End = stopHere _
           Or nowLen = wasLen
           ' This bit above is to catch mixed colours inside field codes
      If thisColour = searchColour Then foundHlight = True
    End If
  Loop Until foundHlight = True Or foundSomething = False
End If
Selection.End = Selection.Start
If foundSomething = False Then Beep

finish:
Selection.End = Selection.Start - 1
Selection.MoveRight Count:=1
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = oldFind
  .Replacement.Text = oldReplace
  .Wrap = wdFindContinue
  .Forward = True
End With
End Sub



EN
Sub FindColouredText()
' Version 28.02.12
' Find coloured text
' <Ctrl-Shift-PageDown>
Dim v As Variable: Dim rng As Range
' Check what the background (black) colour is
Set rng = ActiveDocument.Content
rng.Collapse wdCollapseEnd
myBlack = rng.Font.Color

' Check for search colour variable
varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "tColour" Then varExists = True: Exit For
Next v

If varExists = False Then ActiveDocument.Variables.Add "tColour", 0
searchColour = ActiveDocument.Variables("tColour")

initialPosition = Selection.End
' If no text is selected, search for next coloured bit
If Selection.Start = Selection.End Then GoTo FindNext

' If some text is selected, see what colour it is;
' then go find more text of that colour.

searchColour = Selection.Font.Color
If searchColour < 0 Then searchColour = 0
ActiveDocument.Variables("tColour") = searchColour
If searchColour > 0 Then GoTo FindNext

Selection.Start = Selection.End

' Go and find the next non-black colour
Set rng = ActiveDocument.Content
theEnd = rng.End
Do
  Set rng = ActiveDocument.Content
  Set rng2 = ActiveDocument.Content
  rng.Start = Selection.End
  With rng.Find
   .ClearFormatting
   .Replacement.ClearFormatting
   .Text = ""
   .Font.Color = myBlack
   .Wrap = False
   .Replacement.Text = ""
   .Forward = True
   .MatchWildcards = False
   .Execute
  End With
' Examine the character after the end of the find
  rng2.Start = rng.End
  rng2.End = rng.End + 1
' If the next bit is still black, find the end of that
  Do While rng2.Font.Color = myBlack
    rng.Collapse wdCollapseEnd
    rng.Find.Execute
    rng2.Start = rng.End
    rng2.End = rng.End + 1
  Loop
  rng.Collapse wdCollapseEnd
  rng.Find.Execute
  rng.Collapse wdCollapseStart
  rng.Start = rng.Start - 1
  colourHere = rng.Font.Color
  rng.Select
  myResponse = MsgBox("This colour? (Cancel = any colour)", vbQuestion + vbYesNoCancel)
  If myResponse = vbCancel Then
    Selection.Collapse wdCollapseEnd
    ActiveDocument.Variables("tColour") = 0
    Exit Sub
  End If
  If rng.End = theEnd Then Exit Sub
Loop Until myResponse = vbYes
ActiveDocument.Variables("tColour") = colourHere
GoTo finish

FindNext:
If searchColour = 0 Then
  Set rng = ActiveDocument.Content
  Set rng2 = ActiveDocument.Content
  rng.Start = Selection.End
  With rng.Find
   .ClearFormatting
   .Replacement.ClearFormatting
   .Text = ""
   .Font.Color = myBlack
   .Wrap = False
   .Replacement.Text = ""
   .Forward = True
   .MatchWildcards = False
   .Execute
  End With
  If rng.Find.Found = True Then
  ' Examine the character after the end of the find
    rng2.Start = rng.End
    rng2.End = rng.End + 1
  ' If the next bit is still black, find the end of that
    Do While rng2.Font.Color = myBlack
      rng.Collapse wdCollapseEnd
      rng.Find.Execute
      rng2.Start = rng.End
      rng2.End = rng.End + 1
    Loop
    rng.Collapse wdCollapseEnd
    rng.Find.Execute
    rng.Collapse wdCollapseStart
  End If
  GoTo finish
Else
  Set rng = ActiveDocument.Content
  rng.Start = Selection.End
  foundHlight = False
  With rng.Find
   .ClearFormatting
   .Replacement.ClearFormatting
   .Text = ""
   .Font.Color = searchColour
   .Wrap = False
   .Replacement.Text = ""
   .Forward = True
   .MatchWildcards = False
   .Execute
  End With
End If

finish:
rng.Start = rng.End
rng.Select
If Selection.End = initialPosition Then Beep
End Sub

EN
Sub FindColouredTextUp()
' Version 28.02.12
' Find coloured text
' <Ctrl-Shift-PageUp>
Dim v As Variable: Dim rng As Range
' Check what the background (black) colour is
Set rng = ActiveDocument.Content
rng.Collapse wdCollapseEnd
myBlack = rng.Font.Color

' Check for search colour variable
varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "tColour" Then varExists = True: Exit For
Next v

If varExists = False Then ActiveDocument.Variables.Add "tColour", 0
searchColour = ActiveDocument.Variables("tColour")

initialPosition = Selection.Start
' If no text is selected, search for next coloured bit
If Selection.Start = Selection.End Then GoTo FindNext

' If some text is selected, see what colour it is;
' then go find more text of that colour.

searchColour = Selection.Font.Color
If searchColour < 0 Then searchColour = 0
ActiveDocument.Variables("tColour") = searchColour
If searchColour > 0 Then GoTo FindNext

Selection.Start = Selection.End
' Go and find the previous non-black colour
Set rng = ActiveDocument.Content
theEnd = rng.End
Do
  Set rng = ActiveDocument.Content
  Set rng2 = ActiveDocument.Content
  rng.End = Selection.End
  With rng.Find
   .ClearFormatting
   .Replacement.ClearFormatting
   .Text = ""
   .Font.Color = myBlack
   .Wrap = False
   .Replacement.Text = ""
   .Forward = False
   .MatchWildcards = False
   .Execute
  End With
' Examine the character before the beginning of the find
  rng2.Start = rng.Start - 1
  rng2.End = rng.Start
' If the next bit is still black, find the end of that
  Do While rng2.Font.Color = myBlack
    rng.Collapse wdCollapseStart
    rng.Find.Execute
    rng2.Start = rng.Start - 1
    rng2.End = rng.Start
  Loop
  rng.Collapse wdCollapseStart
  rng.Find.Execute
  rng.Collapse wdCollapseEnd
  rng.End = rng.Start + 1
  colourHere = rng.Font.Color
  rng.Select
  myResponse = MsgBox("This colour? (Cancel = any colour)", vbQuestion + vbYesNoCancel)
  Selection.Collapse wdCollapseStart
  If myResponse = vbCancel Then
    ActiveDocument.Variables("tColour") = 0
    Exit Sub
  End If
  If rng.End = theEnd Then Exit Sub
Loop Until myResponse = vbYes
ActiveDocument.Variables("tColour") = colourHere
GoTo finish

FindNext:
If searchColour = 0 Then
  Set rng = ActiveDocument.Content
  Set rng2 = ActiveDocument.Content
  rng.End = Selection.End
  With rng.Find
   .ClearFormatting
   .Replacement.ClearFormatting
   .Text = ""
   .Font.Color = myBlack
   .Wrap = False
   .Replacement.Text = ""
   .Forward = False
   .MatchWildcards = False
   .Execute
  End With
  If rng.Find.Found = True Then
  ' Examine the character after the end of the find
    rng2.Start = rng.Start - 1
    rng2.End = rng.Start
  ' If the next bit is still black, find the end of that
    Do While rng2.Font.Color = myBlack
      rng.Collapse wdCollapseStart
      rng.Find.Execute
      rng2.Start = rng.Start - 1
      rng2.End = rng.Start
    Loop
    rng.Collapse wdCollapseStart
    rng.Find.Execute
    rng.Collapse wdCollapseEnd
  End If
  GoTo finish
Else
  Set rng = ActiveDocument.Content
  rng.End = Selection.End
  foundHlight = False
  With rng.Find
   .ClearFormatting
   .Replacement.ClearFormatting
   .Text = ""
   .Font.Color = searchColour
   .Wrap = False
   .Replacement.Text = ""
   .Forward = False
   .MatchWildcards = False
   .Execute
  End With
End If

finish:
rng.Collapse wdCollapseStart
rng.Select
If Selection.Start = initialPosition Then Beep
End Sub


EN
Sub FindStyleOld()
' Version 17.11.10
' Find text in this style
' Alt-O

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

targetStyle = Selection.Range.Style
Do
  Selection.MoveDown Unit:=wdParagraph, Count:=1
  If Selection.Start = 0 Then Beep: Exit Sub
  thisStyle = Selection.Range.Style
Loop Until thisStyle <> targetStyle

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = False
  .Wrap = False
  .Text = ""
  .Replacement.Text = ""
  .Style = targetStyle
  .Execute
End With

Selection.Start = Selection.End
Selection.MoveLeft Unit:=wdCharacter, Count:=1

Selection.MoveUp Unit:=wdLine, Count:=3
Selection.MoveDown Unit:=wdLine, Count:=3

With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .Wrap = wdFindContinue
End With
End Sub


EN
Sub FindStyleOldUp()
' Version 16.11.10
' Find text in this style
' Alt-I

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

targetStyle = Selection.Range.Style
Do
  Selection.MoveUp Unit:=wdParagraph, Count:=1
  thisStyle = Selection.Range.Style
Loop Until thisStyle <> targetStyle

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = False
  .Text = ""
  .Forward = False
  .Wrap = False
  .Replacement.Text = ""
  .Style = targetStyle
  .Execute
End With

Selection.Start = Selection.End
Selection.MoveLeft Unit:=wdCharacter, Count:=1
Selection.MoveUp Unit:=wdLine, Count:=3
Selection.MoveDown Unit:=wdLine, Count:=3

With Selection.Find
  .Text = oldFind
  .Wrap = wdFindContinue
  .Replacement.Text = oldReplace
  .Forward = True
End With
End Sub


EN
Sub FindStyle()
' Version 30.12.10
' Find text in this style
' Called from FindOnly

' Turn the selected text into Normal style
Selection.Style = wdStyleNormal
' and records its parameters
nmlSize = Selection.Font.Size
nmlFont = Selection.Font.Name
nmlColour = Selection.Font.Color
' Undo the change, i.e. restore the text's original style
WordBasic.EditUndo

' Check the text's emphasis
isBold = Selection.Font.Bold
isItalic = Selection.Font.Italic
isSuper = Selection.Font.Superscript
isSub = Selection.Font.Subscript
isSmalls = Selection.Font.SmallCaps
thisSize = Selection.Font.Size
thisFont = Selection.Font.Name
thisColour = Selection.Font.Color
' First find all the existing style
endWas = Selection.End
Selection.Start = Selection.End
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ""
  .Replacement.Text = ""
  .MatchCase = False
  .MatchWildcards = False
  .MatchWholeWord = False
  If isBold = True Then .Font.Bold = True
  If isItalic = True Then .Font.Italic = True
  If isSuper = True Then .Font.Superscript = True
  If isSub = True Then .Font.Subscript = True
  If isSmalls = True Then .Font.SmallCaps = True
  If thisSize <> nmlSize Then .Font.Size = thisSize
  If thisFont <> nmlFont Then .Font.Name = thisFont
  If thisColour <> nmlColour Then .Font.Color = thisColour
  .Wrap = False
  .Forward = True
  .Execute
End With
If Selection.Start = endWas Then
  Selection.Start = Selection.End
Else
  Selection.End = Selection.Start
End If
' Now go and find the next one
hereNow = Selection.End
With Selection.Find
  .Forward = False
  .Execute
End With

If Selection.Start = hereNow And Selection.End = hereNow Then
  Beep
Else
' Move the screen display down a couple of lines
  hereNow = Selection.Start
  Selection.MoveUp Unit:=wdLine, Count:=2
  Selection.MoveDown Unit:=wdLine, Count:=2
  With Selection.Find
    .Forward = True
    .Execute
  End With
  If Selection.Start > hereNow Then
    With Selection.Find
      .Forward = False
      .Execute
    End With
  End If
End If
End Sub


EN
Sub ToCback()
' Version 18.07.10
' Jump back to table of contents

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

Selection.HomeKey Unit:=wdLine
Selection.EndKey Unit:=wdLine, Extend:=wdExtend
thisLine = Selection

Selection.HomeKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = False
  .Text = thisLine
  .Replacement.Text = ""
  .Execute
End With
Selection.MoveUp Unit:=wdLine, Count:=3
Selection.MoveDown Unit:=wdLine, Count:=3
Selection.MoveRight Unit:=wdCharacter, Count:=1, Extend:=wdExtend

With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
End With

End Sub



EN
Sub CommentNext()
' Version 15.03.14
' Check/correct author/date formatting of reference list

hereNow = Selection.Start

WordBasic.GoToNextComment
If Selection.Start = hereNow Then
  Beep
End If
End Sub

EN
Sub CommentPrevious()
' Version 15.03.14
' Check/correct author/date formatting of reference list

hereNow = Selection.Start

WordBasic.GoToPreviousComment
If Selection.Start = hereNow Then
  Beep
End If

End Sub


EN
Sub NextChange()
' Version 23.10.13
' Find next change (not! comment)
On Error GoTo theEnd
Do
  Application.Run MacroName:="NextChangeOrComment"
Loop While Selection.Information(wdInCommentPane)
Exit Sub

theEnd:
Selection.HomeKey Unit:=wdStory
Beep
End Sub

EN
Sub PreviousChange()
' Version 23.10.13
' Find next change (not! comment)
On Error GoTo theEnd
Do
  Application.Run MacroName:="PreviousChangeOrComment"
Loop While Selection.Information(wdInCommentPane)
Exit Sub

theEnd:
Selection.HomeKey Unit:=wdStory
Beep
End Sub





EN
Sub FootnoteNext()
' Version 13.10.10
' Jump to next footnote
Selection.GoTo What:=wdGoToFootnote, Which:=wdGoToNext, _
     Count:=1, Name:=""
End Sub


EN
Sub FootnoteNextUp()
' Version 13.10.10
' Jump to previous footnote
Selection.GoTo What:=wdGoToFootnote, Which:=wdGoToPrevious, _
     Count:=1, Name:=""
End Sub


EN
Sub NoteJumper()
' Version 21.05.12
' Jump back and forth between notes and main text

myStoryType = Selection.StoryType
If (myStoryType = wdFootnotesStory Or myStoryType = wdEndnotesStory) Then
   ActiveWindow.View.SeekView = wdSeekMainDocument
Else
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "^e"
    .Wrap = wdFindContinue
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = False
    .Execute
  End With
  If ActiveDocument.Footnotes.Count >= 1 Then _
      ActiveDocument.ActiveWindow.View.SeekView = wdSeekFootnotes
  If ActiveDocument.Endnotes.Count >= 1 Then _
      ActiveDocument.ActiveWindow.View.SeekView = wdSeekEndnotes
End If
End Sub

EN
Sub NextNumberPlus()
' Version 11.10.10
' Find next section number
allowedChars = "0123456789."
theNumber = ""
Selection.End = Selection.Start
startPos = Selection.Start
Selection.Start = startPos - 4
leftBit = Selection
Selection.Start = startPos - 1
If Asc(Selection) < 32 Then leftBit = ""
Selection.Start = startPos
pos = 1
dotPos = 0
Do
  thisChar = Selection
  theNumber = theNumber + thisChar
  If thisChar = "." Then
    prevDotPos = dotPos
    dotPos = pos
  End If
  Selection.MoveRight Unit:=wdCharacter, Count:=1
  pos = pos + 1
Loop Until InStr(allowedChars, thisChar) = 0
If dotPos > 0 Then
  lastNumber = Mid(theNumber, dotPos + 1, pos - dotPos - 2)
  If Val(lastNumber) = 0 Then
    dotPos = prevDotPos
    lastNumber = Mid(theNumber, dotPos + 1, pos - dotPos - 2)
  End If
  newNumber = Left(theNumber, dotPos) + Trim(Str(Val(lastNumber) + 1))
Else
  lastNumber = Left(theNumber, pos - 2)
  newNumber = Trim(Str(Val(lastNumber) + 1))
End If
hereNow = Selection.End
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  If leftBit > "" Then
    .Text = leftBit & newNumber
  Else
    .Text = "^p" & newNumber
  End If
  .Replacement.Text = ""
  .Wrap = False
  .Forward = True
  .MatchWildcards = False
  .Execute
End With
Selection.End = Selection.Start
If Selection.End = hereNow Then Beep
If leftBit > "" Then
  Selection.MoveRight Unit:=wdCharacter, Count:=4
Else
  Selection.MoveRight Unit:=wdCharacter, Count:=1
End If

'Add this to leave F&R dialogue in a sensible state
With Selection.Find
  .Wrap = wdFindContinue
End With
Selection.End = Selection.Start
End Sub


EN
Sub NextNumberPlusUp()
' Version 11.10.10
' Find previous section number
allowedChars = "0123456789."
theNumber = ""
Selection.End = Selection.Start
startPos = Selection.Start
Selection.Start = startPos - 4
leftBit = Selection
Selection.Start = startPos - 1
If Asc(Selection) < 32 Then leftBit = ""
Selection.Start = startPos
pos = 1
dotPos = 0
Do
  thisChar = Selection
  theNumber = theNumber + thisChar
  If thisChar = "." Then
    prevDotPos = dotPos
    dotPos = pos
  End If
  Selection.MoveRight Unit:=wdCharacter, Count:=1
  pos = pos + 1
Loop Until InStr(allowedChars, thisChar) = 0
If dotPos > 0 Then
  lastNumber = Mid(theNumber, dotPos + 1, pos - dotPos - 2)
  If Val(lastNumber) = 0 Then
    dotPos = prevDotPos
    lastNumber = Mid(theNumber, dotPos + 1, pos - dotPos - 2)
  End If
  newNumber = Left(theNumber, dotPos) + Trim(Str(Val(lastNumber) - 1))
Else
  lastNumber = Left(theNumber, pos - 2)
  newNumber = Trim(Str(Val(lastNumber) - 1))
End If
hereNow = Selection.End
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  If leftBit > "" Then
    .Text = leftBit & newNumber
  Else
    .Text = "^p" & newNumber
  End If
  .Replacement.Text = ""
  .Wrap = False
  .Forward = False
  .MatchWildcards = False
  .Execute
End With
Selection.End = Selection.Start
If Selection.End = hereNow Then Beep
If leftBit > "" Then
  Selection.MoveRight Unit:=wdCharacter, Count:=4
Else
  Selection.MoveRight Unit:=wdCharacter, Count:=1
End If

'Add this to leave F&R dialogue in a sensible state
With Selection.Find
  .Wrap = wdFindContinue
  .Forward = True
End With
Selection.End = Selection.Start
End Sub


EN
Sub NextNumber()
' Version 11.10.10
' Find next section number
allowedChars = "0123456789."
theNumber = ""
Selection.End = Selection.Start
startPos = Selection.Start
Selection.Start = startPos - 4
leftBit = Selection
Selection.Start = startPos
pos = 1
dotPos = 0
Do
  thisChar = Selection
  theNumber = theNumber + thisChar
  If thisChar = "." Then
    prevDotPos = dotPos
    dotPos = pos
  End If
  Selection.MoveRight Unit:=wdCharacter, Count:=1
  pos = pos + 1
Loop Until InStr(allowedChars, thisChar) = 0
If dotPos > 0 Then
  lastNumber = Mid(theNumber, dotPos + 1, pos - dotPos - 2)
  If Val(lastNumber) = 0 Then
    dotPos = prevDotPos
    lastNumber = Mid(theNumber, dotPos + 1, pos - dotPos - 2)
  End If
  newNumber = Left(theNumber, dotPos) + Trim(Str(Val(lastNumber) + 1))
Else
  lastNumber = Left(theNumber, pos - 2)
  newNumber = Trim(Str(Val(lastNumber) + 1))
End If
hereNow = Selection.End
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = newNumber
  .Replacement.Text = ""
  .Wrap = False
  .Forward = True
  .MatchWildcards = False
  .Execute
End With
If Selection.End = hereNow Then
  Beep
  Selection.End = startPos
End If
'Add this to leave F&R dialogue in a sensible state
With Selection.Find
  .Wrap = wdFindContinue
End With
Selection.End = Selection.Start
End Sub


EN
Sub NextNumberUp()
' Version 11.10.10
' Find previous section number
allowedChars = "0123456789."
theNumber = ""
Selection.End = Selection.Start
startPos = Selection.Start
Selection.Start = startPos - 4
leftBit = Selection
Selection.Start = startPos
pos = 1
dotPos = 0
Do
  thisChar = Selection
  theNumber = theNumber + thisChar
  If thisChar = "." Then
    prevDotPos = dotPos
    dotPos = pos
  End If
  Selection.MoveRight Unit:=wdCharacter, Count:=1
  pos = pos + 1
Loop Until InStr(allowedChars, thisChar) = 0
If dotPos > 0 Then
  lastNumber = Mid(theNumber, dotPos + 1, pos - dotPos - 2)
  If Val(lastNumber) = 0 Then
    dotPos = prevDotPos
    lastNumber = Mid(theNumber, dotPos + 1, pos - dotPos - 2)
  End If
  newNumber = Left(theNumber, dotPos) + Trim(Str(Val(lastNumber) - 1))
Else
  lastNumber = Left(theNumber, pos - 2)
  newNumber = Trim(Str(Val(lastNumber) - 1))
End If
hereNow = Selection.End
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = newNumber
  .Replacement.Text = ""
  .Wrap = False
  .Forward = False
  .MatchWildcards = False
  .Execute
End With
If Selection.End = hereNow Then
  Beep
  Selection.End = startPos
End If
'Add this to leave F&R dialogue in a sensible state
With Selection.Find
  .Wrap = wdFindContinue
  .Forward = True
End With
Selection.End = Selection.Start
End Sub


EN
Sub FindNextNumber()
' Version 09.01.13
' Jump from one number to the next - section, fig, table, etc

Selection.Words(1).Select
If Selection.Start = 0 Then
  Selection.InsertBefore Text:=Chr(13)
  Selection.MoveStart wdCharacter, 1
End If
wd = Selection
If InStr(wd, "Box") + InStr(wd, "Fig") + InStr(wd, "Tab") > 0 Then
  Selection.Collapse wdCollapseEnd
Else
  Selection.Collapse wdCollapseStart
End If
Selection.MoveStartUntil cset:=Chr(13) & " " & Chr(9), Count:=wdBackward
Selection.MoveEndUntil cset:=Chr(13) & " :" & Chr(9), Count:=wdForward

myDelay = 0.1
Set rng = ActiveDocument.Content
rng.Start = Selection.Start - 1
rng.End = Selection.Start
b4Text = rng
If b4Text <> Chr(13) Then
  ' If the first char isn't a return, is there one further back?
  gotCR = False
  For j = 1 To 10
    rng.MoveStart wdCharacter, -1
    b4Text = rng
    If Asc(rng) = 13 Then
      rng.MoveStart wdCharacter, 1
      gotCR = True
      Exit For
    End If
  Next j
  If gotCR = True Then
    b4Text = "^p" & rng.Text
  Else
    rng.MoveStart wdCharacter, 5
    b4Text = rng
    If InStr(b4Text, "Fig.") > 0 Then b4Text = "^p" & "Fig. "
  End If
Else
  rng.MoveStart wdCharacter, 1
  b4Text = "^p" & rng.Text
End If
' At this point b4Txt has the preceding text, and
' the number is selected
allText = Selection
txtStart = Selection.Start
myLen = Len(allText)

' See if the next level section number is there
Selection.Collapse wdCollapseEnd
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = b4Text & allText & ".1"
  .Wrap = False
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = False
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute
End With

If Selection.Find.Found = True Then GoTo theEnd

' So you've not found the ".1" just find the next heading
gotNumber = False
Do
  'go find the next number of this level of heading
  If InStr(allText, ".") = 0 Then
    getNumber = Trim(Str(Val(allText) + 1)) & ".1"
    i = 1
  Else
    For i = 1 To myLen
      myText = Right(allText, i)
      If gotNumber = True Then
        If Asc(myText) < 48 Or Asc(myText) > 57 Then
          theNumber = Val(Right(allText, i - 1))
          Exit For
        End If
      End If
      If Asc(myText) > 47 And Asc(myText) < 58 Then gotNumber = True
    Next i
    thisNumber = Val(theNumber)
    getNumber = Left(allText, myLen - i + 1) & Trim(Str(thisNumber + 1))
  End If
  Selection.Collapse wdCollapseEnd
  With Selection.Find
    .Text = b4Text & getNumber
    .Execute
  End With
  gotOne = Selection.Find.Found
  If gotOne = True Then GoTo theEnd

  If Len(b4Text) > 2 Then
  ' Where a table has got in the way, search again w/o CR
    b4Text2 = Replace(b4Text, "^p", "")
    With Selection.Find
      .Text = b4Text2 & getNumber
      .Execute
    End With
    gotOne = Selection.Find.Found
    If gotOne = True Then
      Beep
      GoTo theEnd
    End If
  End If
  ' With section numbering, check for the next number in the sequence
  getWas = getNumber
  getNumber = Left(allText, myLen - i + 1) & Trim(Str(thisNumber + 2))
  With Selection.Find
    .Text = b4Text & getNumber
    .Execute
  End With
  If Selection.Find.Found Then
    Selection.Start = txtStart - 1
    Selection.End = txtStart - 1
    Selection.MoveRight Count:=1
    b4Text = Replace(b4Text, "^p", "")
    If Len(b4Text) = 0 Then b4Text = "Section "
    MsgBox (b4Text & getWas & " missing!")
    Exit Sub
  Else
    getWas2 = getNumber
    getNumber = Left(allText, myLen - i + 1) & Trim(Str(thisNumber + 3))
    With Selection.Find
      .Text = b4Text & getNumber
      .Execute
    End With
    If Selection.Find.Found Then
      Selection.Start = txtStart - 1
      Selection.End = txtStart - 1
      Selection.MoveRight Count:=1
      b4Text = Replace(b4Text, "^p", "")
      If Len(b4Text) = 0 Then b4Text = "Section "
      MsgBox (b4Text & getWas & " missing, AND " & getWas2 & "!")
      Exit Sub
    End If
  End If

  myLen = myLen - i
  If myLen < 1 Then
    GoTo theEnd
  End If
  allText = Left(allText, myLen)
  myTime = Timer
  Do
  Loop Until Timer > myTime + myDelay
  Beep
Loop Until gotOne = True

theEnd:
Selection.Collapse wdCollapseEnd
varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "whereIwas" Then varExists = True: Exit For
Next v
If varExists = False Then
  ActiveDocument.Variables.Add "whereIwas", txtStart
Else
  ActiveDocument.Variables("whereIwas") = txtStart
End If

End Sub


EN
Sub FindPreviousNumber()
' Version 08.01.13
' Jump back to the previous number

varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "whereIwas" Then varExists = True: Exit For
Next v
If varExists = False Then
  ActiveDocument.Variables.Add "whereIwas", 0
Else
  Selection.Start = ActiveDocument.Variables("whereIwas")
  Selection.Collapse wdCollapseStart
End If

' Use jumpScroll to set cursor to top of page
startHere = Selection.Start
Application.ScreenUpdating = False
Selection.EndKey Unit:=wdLine
lineEnd = Selection.Start

If Selection.Information(wdInEndnote) = True _
     Then inEndNotes = True

Selection.MoveDown Unit:=wdScreen, Count:=2

' Make sure that you've not dropped into a footnote ...
Do While Selection.Information(wdInFootnote) = True
  Selection.MoveDown Unit:=wdLine, Count:=2
Loop
If inEndNotes = False Then
' Make sure that you've not dropped into an endnote ...
  Do While Selection.Information(wdInEndnote) = True
    Selection.MoveUp Unit:=wdLine, Count:=2
  Loop
End If
Selection.End = lineEnd
Selection.MoveRight , 1
Selection.MoveUp , 1
Selection.End = startHere
Selection.Start = startHere
If lineEnd = startHere Then Selection.MoveLeft , 1
Application.ScreenUpdating = True
End Sub


EN
Sub ListItemFinder()
' Version 28.08.14
' Jump to an auto-list number

myOffset = 0

If Selection.Start = Selection.End Then Selection.Expand wdWord
mySearchText = Trim(Selection)
mySearchNumber = Val(mySearchText)
If mySearchNumber = 0 Then MsgBox "Place the cursor in the citation number": Exit Sub
myDocName = Left(ActiveDocument.Name, InStr(ActiveDocument.Name, ".") - 1)
nowSelection = Selection
Windows(myDocName & ":1").Activate
If Selection = nowSelection Then Windows(myDocName & ":2").Activate

For i = myOffset + mySearchNumber To ActiveDocument.ListParagraphs.Count
  num = ActiveDocument.ListParagraphs(i).Range.ListFormat.ListString
  If Val(num) = mySearchNumber Then
    ActiveDocument.ListParagraphs(i).Range.Select
  Exit Sub
  End If
Next i
MsgBox "Sorry, I can't find an item of that number."
End Sub

EN
Sub ListCheckStart()
' Version 28.08.14
' Jump to an auto-list number

hereNow = Selection.Start
For i = 1 To ActiveDocument.ListParagraphs.Count
  itemEnd = ActiveDocument.ListParagraphs(i).Range.End
  If itemEnd > hereNow Then
    MsgBox "Use: myOffset = " & Str(i - 1)
  Exit Sub
  End If
Next i
End Sub

EN
Sub FigTableBoxLister()
' Version 28.05.13
' Find figure/table/box elements and their citations

myLetters = "a-h"

Set mainDoc = ActiveDocument
CR = vbCrLf
CR2 = CR & CR
TB = Chr(9)
Selection.HomeKey Unit:=wdStory
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "Fig[ure. ]{1,4}[1-9]"
  .Replacement.Text = ""
  .Forward = True
  .MatchCase = True
  .MatchWildcards = True
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute
End With
isFigure = rng.Find.Found

Set rng = ActiveDocument.Content
With rng.Find
  .Text = "Table [1-9]"
  .MatchWildcards = True
  .Execute
End With
isTable = rng.Find.Found

Set rng = ActiveDocument.Content
With rng.Find
  .Text = "Box [1-9]"
  .MatchWildcards = True
  .Execute
End With
isBox = rng.Find.Found

If isFigure Then
  foundList = CR2
  Set rng = ActiveDocument.Content
  ' Go and find the first occurrence
  With rng.Find
    .Text = "Fig[ure. ]{1,4}[0-9.:" & myLetters & "]{1,}"
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = True
    .Execute
  End With
 
  Do While rng.Find.Found = True
    Set rng2 = rng.Paragraphs(1).Range
    pa = rng2.Text
    If InStr(pa, ">") > 0 Then
      pa = Mid(pa, InStr(pa, ">") + 1)
    End If
    wd = rng.Text
    If InStr(pa, wd) > 2 Then
      foundList = foundList & TB
    Else
      wdLess = wd
      If Right(wd, 1) = "." Or Right(wd, 1) = ":" Then wdLess = _
           Left(wd, Len(wd) - 1)
      If InStr(foundList, CR & wd & CR) > 0 Or InStr(foundList, CR & _
           wd & ":" & CR) > 0 Or InStr(foundList, CR & wd & "." & CR) _
           > 0 Or InStr(foundList, CR & wdLess & CR) > 0 Then foundList _
           = foundList & TB & TB
    End If
    foundList = foundList & rng & CR
    rng.Collapse wdCollapseEnd
  ' Go and find the next occurence (if there is one)
    rng.Find.Execute
  Loop
  Documents.Add
  Set rng = ActiveDocument.Content
  rng.InsertAfter Text:=foundList
  For Each para In ActiveDocument.Paragraphs
    pa = Replace(para.Range.Text, Chr(13), "")
    If Right(pa, 1) = "." Or Right(pa, 1) = ":" Then pa = _
           Left(pa, Len(pa) - 1)
    If InStr(foundList, TB & pa) = 0 And InStr(pa, TB) = 0 Then _
         para.Range.HighlightColorIndex = wdYellow
    If InStr(foundList, CR & Replace(pa, Chr(9), "")) = 0 And InStr(pa, TB) _
         > 0 Then para.Range.HighlightColorIndex = wdYellow
  Next
  Set figDoc = ActiveDocument
  mainDoc.Activate
End If


If isTable Then
  foundList = CR2
  Set rng = ActiveDocument.Content
  ' Go and find the first occurrence
  With rng.Find
    .Text = "Table [0-9.:" & myLetters & "]{1,}"
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = True
    .Execute
  End With
  Do While rng.Find.Found = True
    Set rng2 = rng.Paragraphs(1).Range
    pa = rng2.Text
    If InStr(pa, ">") > 0 Then
      pa = Mid(pa, InStr(pa, ">") + 1)
    End If
    wd = rng.Text
    If InStr(pa, wd) > 2 Then
      foundList = foundList & TB
    Else
      wdLess = wd
      If Right(wd, 1) = "." Or Right(wd, 1) = ":" Then wdLess = _
           Left(wd, Len(wd) - 1)
      If InStr(foundList, CR & wd & CR) > 0 Or InStr(foundList, CR & _
           wd & ":" & CR) > 0 Or InStr(foundList, CR & wd & "." & CR) _
           > 0 Or InStr(foundList, CR & wdLess & CR) > 0 Then foundList _
           = foundList & TB & TB
    End If
    foundList = foundList & rng & CR
    rng.Collapse wdCollapseEnd
  ' Go and find the next occurence (if there is one)
    rng.Find.Execute
  Loop
  Documents.Add
  Selection.TypeText Text:=foundList
  For Each para In ActiveDocument.Paragraphs
    pa = Replace(para.Range.Text, Chr(13), "")
    If Right(pa, 1) = "." Or Right(pa, 1) = ":" Then pa = _
           Left(pa, Len(pa) - 1)
    If InStr(foundList, TB & pa) = 0 And InStr(pa, TB) = 0 Then _
         para.Range.HighlightColorIndex = wdYellow
    If InStr(foundList, CR & Replace(pa, Chr(9), "")) = 0 And InStr(pa, TB) _
         > 0 Then para.Range.HighlightColorIndex = wdYellow
  Next
  Set tableDoc = ActiveDocument
  mainDoc.Activate
End If

If isBox Then
  foundList = CR2
  Set rng = ActiveDocument.Content
  ' Go and find the first occurrence
  With rng.Find
    .Text = "Box [0-9.:" & myLetters & "]{1,}"
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = True
    .Execute
  End With
 
  Do While rng.Find.Found = True
    Set rng2 = rng.Paragraphs(1).Range
    pa = rng2.Text
    If InStr(pa, ">") > 0 Then
      pa = Mid(pa, InStr(pa, ">") + 1)
    End If
    wd = rng.Text
    If InStr(pa, wd) > 2 Then
      foundList = foundList & TB
    Else
      wdLess = wd
      If Right(wd, 1) = "." Or Right(wd, 1) = ":" Then wdLess = _
           Left(wd, Len(wd) - 1)
      If InStr(foundList, CR & wd & CR) > 0 Or InStr(foundList, CR & _
           wd & ":" & CR) > 0 Or InStr(foundList, CR & wd & "." & CR) _
           > 0 Or InStr(foundList, CR & wdLess & CR) > 0 Then foundList _
           = foundList & TB & TB
    End If
    foundList = foundList & rng & CR
    rng.Collapse wdCollapseEnd
  ' Go and find the next occurence (if there is one)
    rng.Find.Execute
  Loop
  Documents.Add
  Selection.TypeText Text:=foundList
  For Each para In ActiveDocument.Paragraphs
    pa = Replace(para.Range.Text, Chr(13), "")
    If Right(pa, 1) = "." Or Right(pa, 1) = ":" Then pa = _
           Left(pa, Len(pa) - 1)
    If InStr(foundList, TB & pa) = 0 And InStr(pa, TB) = 0 Then _
         para.Range.HighlightColorIndex = wdYellow
    If InStr(foundList, CR & Replace(pa, Chr(9), "")) = 0 And InStr(pa, TB) _
         > 0 Then para.Range.HighlightColorIndex = wdYellow
  Next
End If

If isTable Then tableDoc.Activate
If isFigure Then figDoc.Activate
Beep
End Sub


EN
Sub ParaShort()
' Version 06.11.10
' Finds medium-length paragraphs
minLen = 20
maxLen = 150
Do
  Selection.MoveDown Unit:=wdParagraph, Count:=1, Extend:=wdExtend
  myText = Selection
  Selection.Start = Selection.End
Loop Until Len(myText) < 150 And Len(myText) > minLen
Selection.MoveUp Unit:=wdParagraph, Count:=1
startHere = Selection.Start - 1
Selection.MoveDown Unit:=wdScreen, Count:=1
' Make sure that you've not dropped into a footnote ...
Do
  Selection.MoveDown Unit:=wdScreen, Count:=1
Loop Until Selection.Start > startHere
Selection.End = startHere
Selection.MoveRight Unit:=wdCharacter, Count:=1
Selection.MoveDown Unit:=wdParagraph, Count:=1
End Sub


EN
Sub ListHighlighter()
' Version 19.10.10
' This highlights all 'short' paragraphs in a text

minLength = 10
MaxLength = 150
myColour = wdBrightGreen

oldColour = Options.DefaultHighlightColorIndex
Options.DefaultHighlightColorIndex = myColour

For Each myPara In ActiveDocument.Paragraphs
  paraWords = myPara
  lineLen = Len(paraWords)
  If lineLen < MaxLength And lineLen > minLength Then
    myPara.Range.HighlightColorIndex = myColour
  End If
Next
Options.DefaultHighlightColorIndex = oldColour
Selection.HomeKey Unit:=wdStory
End Sub


EN
Sub PageHopper()
' Version 08.12.10
' Go to page x
' Alt-G
myPage = InputBox("Go to page?", "Page Hopper")
Selection.GoTo What:=wdGoToPage, Count:=myPage
End Sub




EN
Sub PageHopper2()
' Version 18.12.10
' Go to page x
' Alt-G
myPage = InputBox("Go to page?", "Page Hopper")
Selection.GoTo What:=wdGoToPage, Count:=myPage
startHere = Selection.Start - 1
Selection.MoveDown Unit:=wdScreen, Count:=1
' Make sure that you've not dropped into a footnote ...
Do
  Selection.MoveDown Unit:=wdScreen, Count:=1
Loop Until Selection.Start > startHere
Selection.End = startHere
Selection.MoveRight Unit:=wdCharacter, Count:=1
End Sub


EN
Sub SelectCurrentPage()
' Version 27.01.11
' Select the current page
Selection.GoTo What:=wdGoToPage, Which:=wdGoToNext, Count:=1
endHere = Selection.Start
Selection.GoTo What:=wdGoToPage, Which:=wdGoToPrevious, Count:=1
Selection.End = endHere
End Sub




ENSub JumpScroll()
' Version 20.02.14
' Scroll this line to top of page
' Supplied by Howard Silcock
' Alt-Enter

Set wasSelected = Selection.Range
Application.ScreenUpdating = False

Selection.EndKey Unit:=wdStory
wasSelected.Select
Application.ScreenUpdating = True

ActiveDocument.ActiveWindow.SmallScroll down:=1
End Sub


EN
Sub ListOfFound()
' Version 11.07.11
' List all occurrences of a word/phrase
' F2

charsBefore = 20
charsAfter = 40

soundsLike = False
caseMatch = False

myDummy = "Dummy First Line"

Dim v As Variable, doc As Document
Dim cutOff As Integer, bothNumbers As Single
Dim findLine, fText, lineText As String, startHere As Long

myCharsBefore = charsBefore
myCharsAfter = charsAfter
' Are we in the List of Found? If so go to FoldBack
Set rng = ActiveDocument.Paragraphs(1).Range
fText = rng
If InStr(fText, "List of found") > 0 Then GoTo FoldBack

' We are in the text, not the list
findThis = Trim(Selection)
findThis = Replace(findThis, Chr(9), "#t")
' Does the text have a find-word stored in the LoFtext variable?
varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "LoFtext" Then varExists = True
Next v

findWas = ""
startHere = Selection.Start
' If no find-word stored, store the current word
If varExists = True Then
  findWas = ActiveDocument.Variables("LoFtext")
Else
' Otherwise add the variable
  ActiveDocument.Variables.Add "LoFtext", findThis
End If

' If no text selected, go back to the previous
If Selection.Start = Selection.End Then findThis = findWas

' Check to find a List of Found file
nameNow = ActiveDocument.Name
If LCase(findWas) = LCase(findThis) Then
  For Each doc In Documents
    If doc.Name <> nameNow And doc.Name <> "Normal.dot" _
         And gottadoc = False Then
      doc.Activate
      Set rng = ActiveDocument.Paragraphs(1).Range
      fText = rng
      ' If there is an LoF file exit the macro
      If Left(fText, 13) = "List of found" Then Exit Sub
    End If
  Next
End If

If findThis = "" Then MsgBox "Select text to find": Exit Sub

' Store the current search word in a variable in the text file
ActiveDocument.Variables("LoFtext") = findThis

MakeList:
' Record the first line
Set rng = ActiveDocument.Paragraphs(1).Range
rng.End = rng.End - 1
myFirstLine = rng
If Len(myFirstLine) < 3 Then
  Selection.HomeKey Unit:=wdStory
  Selection.InsertAfter Text:=myDummy
  myFirstLine = myDummy
End If
If Len(myFirstLine) > 20 Then
  myFirstLine = Left(myFirstLine, 20)
End If

'Shorten it if it contains a newline character
returnPos = InStr(myFirstLine, Chr(13))
If returnPos > 0 Then myFirstLine = Left(myFirstLine, returnPos - 2)

Dim foundArray(500)
Set rng = ActiveDocument.Content

' Are we asking to search for anything non-alpha?
checkThis = LCase(findThis)
For i = 1 To Len(checkThis)
  If Asc(checkThis) < 97 Or Asc(checkThis) > 122 Then Exit For
  checkThis = Right(checkThis, Len(checkThis) - 1)
Next i
' An empty string means all characters were alphabetic

' Collect all the occurrences of the search text
i = 0
gogo = True
Do While gogo = True And i < 500
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = Replace(findThis, "#", "^")
    If soundsLike = True And checkThis = "" Then _
         .MatchSoundsLike = True
    If caseMatch = True Then .MatchCase = True
    .Wrap = False
    .Format = True
    .Execute
  End With
' Select a bit of text before and after
  gogo = rng.Find.Found
  If gogo Then
    rng.Start = rng.Start - myCharsBefore
    rng.End = rng.End + myCharsAfter
    lineText = rng
    lineText = Replace(lineText, vbTab, "#t")
    lineText = Replace(lineText, vbCr, "#p")
    lineText = Replace(lineText, Chr$(12), "?")
    i = i + 1
    rng.Start = rng.End
    foundArray(i) = lineText
  End If
Loop

iMax = i

' Find a List of found document ...
nameNow = ActiveDocument.Name
gottadoc = False
For Each doc In Documents
  If doc.Name <> nameNow And doc.Name <> "Normal.dot" _
       And gottadoc = False Then
    doc.Activate
    Set rng = ActiveDocument.Paragraphs(1).Range
    fText = rng
    If InStr(fText, "List of found") Then
      ActiveDocument.Select
      Selection.Delete
      Set docNew = doc
      gottadoc = True
    End If
  End If
Next

' ... or open a new one
If caseMatch Then matchState = "MatchCase = True" Else _
     matchState = "MatchCase = False"
If soundsLike Then soundState = "SoundsLike = True" Else _
     soundState = "SoundsLike = False"
If gottadoc = False Then Set docNew = Documents.Add
Selection.TypeText Text:="List of found: " & findThis _
     & vbCrLf & myFirstLine & vbCrLf & Str(myCharsBefore) _
     & vbCrLf & Str(myCharsAfter) & vbCrLf & soundState _
     & vbCrLf & matchState & vbCrLf & vbCrLf

' Type in the lines of found text
For i = 1 To iMax
  Selection.TypeText Text:=foundArray(i)
  Selection.TypeParagraph
Next

' Embolden the search-for text
totFinds = -2
Set rng = ActiveDocument.Content
Do
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .MatchCase = False
    .MatchWildcards = False
    .Format = True
    .Text = findThis
    If caseMatch = True Then .MatchCase = True
    .MatchSoundsLike = False
    .Replacement.Text = "^&"
    .Replacement.Font.Bold = True
    .Execute Replace:=wdReplaceOne
  End With
  rng.Start = rng.End
  totFinds = totFinds + 1
Loop Until Not rng.Find.Found

Selection.EndKey Unit:=wdStory
Selection.TypeParagraph
Selection.TypeParagraph
If iMax = 500 Then
  Selection.TypeText _
       Text:="There are more, but I gave up!"
Else
  Selection.TypeText Text:=Str(totFinds) & _
       " occurrences on these " & Str(iMax) & " lines"
End If
Selection.HomeKey Unit:=wdLine, Extend:=wdExtend
Selection.Font.Bold = True
Selection.End = Selection.Start
ActiveDocument.Variables("LoFstart") = startHere
Exit Sub

FoldBack:
'fText holds first line; find the word
theWord = Mid(fText, 16, Len(fText) - 16)
lofFile = ActiveDocument.Name

varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "LoFnumbers" Then varExists = True
Next v

' If no start/end numbers stored, store the current numbers
If varExists = True Then
  bothNumbers = ActiveDocument.Variables("LoFnumbers")
  myCharsBefore = Int(bothNumbers / 1000)
  myCharsAfter = Int(bothNumbers Mod 1000)
  myState = bothNumbers * 10 Mod 10
  soundsLike = (myState Mod 2 = 1)
  caseMatch = (Int(myState / 2) Mod 2 = 1)
Else
' Otherwise add the variable
  addBit = 0
  If soundsLike = True Then addBit = 0.1
  If caseMatch = True Then addBit = addBit + 0.2
  bothNumbers = 1000 * myCharsBefore + myCharsAfter + addBit
  ActiveDocument.Variables.Add "LoFnumbers", bothNumbers
End If

' Select the cursor line
Selection.HomeKey Unit:=wdLine
Selection.EndKey Unit:=wdLine, Extend:=wdExtend

' Find out if the variables at the top are different
' from the stored variables, so we know whether to rerun to list.
reRun = False
Set rng = ActiveDocument.Paragraphs(3).Range
myLine = rng
beforeLen = Val(myLine)
If beforeLen <> myCharsBefore Then
  myCharsBefore = beforeLen
  reRun = True
End If

Set rng = ActiveDocument.Paragraphs(4).Range
myLine = rng
afterLen = Val(myLine)
If afterLen <> myCharsAfter Then
  myCharsAfter = afterLen
  reRun = True
End If

Set rng = ActiveDocument.Paragraphs(5).Range
myLine = rng
soundState = (Left(Right(myLine, 4), 3) = "rue")
If soundsLike <> soundState Then
  reRun = True
  soundsLike = soundState
End If


Set rng = ActiveDocument.Paragraphs(6).Range
myLine = rng
matchState = (Left(Right(myLine, 4), 3) = "rue")
If caseMatch <> matchState Then
  reRun = True
  caseMatch = matchState
End If

' Where do the prelims end?
prelimsEnd = rng.End

If reRun = True Then
' One or more variables have changed, therefore update them
  addBit = 0
  If soundsLike = True Then addBit = 0.1
  If caseMatch = True Then addBit = addBit + 0.2
  bothNumbers = 1000 * myCharsBefore + myCharsAfter + addBit
  ActiveDocument.Variables("LoFnumbers") = bothNumbers
End If

findLine = Selection
If Selection.End > ActiveDocument.Range.End - 3 Or _
     Selection.Start < prelimsEnd Then
  If reRun = False Then findLine = ""
  Selection.Start = Selection.End
Else
  findLine = Replace(findLine, "#p", "^p")
  findLine = Replace(findLine, "#t", "^t")
  findLine = Left(findLine, Len(findLine) - 1)
End If

' What is the first line of the text file?
Set rng = ActiveDocument.Paragraphs(2).Range
myFirstLine = rng
If Right(myFirstLine, 1) = Chr(13) Then
  myFirstLine = Left(myFirstLine, Len(myFirstLine) - 1)
End If
startHere = ActiveDocument.Variables("LoFstart")


gottadoc = False
For Each doc In Documents
  If doc.Name <> "Normal.dot" Then
    doc.Activate
    Set rng = ActiveDocument.Paragraphs(1).Range
    testText = rng
    If InStr(testText, myFirstLine) > 0 Then gottadoc = True: Exit For
  End If
Next

If gottadoc = False Then
  MsgBox ("Can't find the right file")
  Exit Sub
End If

If reRun = True Then findThis = theWord: GoTo MakeList

' Select the text
If Len(findLine) > 2 Then
  cutOff = myCharsAfter
  For myTry = 1 To 9
  ' If it couldn't find it the first time, chop off the LH end
  ' of the find string
    If myTry = 2 Then
      wordPosn = InStr(LCase(findLine), LCase(theWord)) - 1
      findLine = Right(findLine, Len(findLine) - wordPosn)
    End If
  ' Try to find the searched-for line
    Selection.HomeKey Unit:=wdStory
    With Selection.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .MatchWildcards = False
      .MatchSoundsLike = False
      .Text = findLine
      .Execute
    End With
    gottit = Selection.Find.Found
    If gottit = True Then
      myTry = 10
    Else
    ' cut down the findLine a bit
      If myTry > 1 Then
        cutOff = cutOff / 2
        findLine = Left(findLine, Len(findLine) - cutOff)
      End If
    End If
  Next myTry
  ' select the searched-for word
  If gottit = True Then
    With Selection.Find
      .ClearFormatting
      .MatchSoundsLike = False
      .Replacement.ClearFormatting
      .Text = Replace(theWord, "#", "^")
      .Execute
    End With
    If Not Selection.Find.Found Then
      Selection.Start = Selection.Start + charsBefore
      Selection.End = Selection.Start
    End If
  startHere = Selection.Start - 1
  Selection.MoveDown Unit:=wdScreen, Count:=1
' Make sure that you've not dropped into a footnote
  Do
    Selection.MoveDown Unit:=wdScreen, Count:=1
  Loop Until Selection.Start > startHere
  Selection.End = startHere
  Selection.MoveRight Unit:=wdCharacter, Count:=1
  Selection.Find.Execute
    If gottit = True Then Exit Sub
  End If
Else
  ' Reselect the exact piece of text we started from
  Selection.Start = startHere - 20
  Selection.End = Selection.Start
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .MatchWildcards = False
    .MatchSoundsLike = False
    .Text = theWord
    .Execute
  End With
  startHere = Selection.Start - 1
  Exit Sub
End If

' Somehow we missed it!
Windows(lofFile).Activate
MsgBox ("Sorry, I can't find that line.")
End Sub


EN
Sub ListofHeadings()
' Version 10.02.10
' Create a list of all headings

Dim myStyle(10)
' How many styles
numStyles = 3
' What are their names
myStyle(1) = "Heading 1"
myStyle(2) = "Heading 2"
myStyle(3) = "Heading 3"

' minimum heading length
minLength = 8

Dim v As Variable, listDoc, textDoc As Document
Dim fText, lineText As String
Dim textArray(500), styleArray(500)
nameNow = ""

' First check if there are any styles specified
endPara = ActiveDocument.Paragraphs.Count
myStyles = 0
For i = 0 To 5
  Set rng = ActiveDocument.Paragraphs(endPara - i).Range
  myTestPara = rng
  myTestPara = Left(myTestPara, Len(myTestPara) - 1)
  If myTestPara = "Heading" Then
  ' As long as it's a heading type, record it
    myStyles = myStyles + 1
    thisStyle = rng.Style
    myStyle(myStyles) = thisStyle
  ' and remember where it starts
    headingFirst = rng.Start
  End If
Next i
If myStyles > 0 Then numStyles = myStyles

' Are we in the List of headings? If so go to FoldBack
Set rng = ActiveDocument.Paragraphs(1).Range
fText = rng
If InStr(fText, "List of headings") > 0 Then GoTo FoldBack

' We are in the text, not the list
findThis = Selection
newList = False
If Selection.Start <> Selection.End Then
' If some test is selected, offer to create a new list
  myResponse = MsgBox("Create new list?", vbQuestion + vbYesNo)
  If myResponse = vbNo Then Exit Sub
  newList = True
End If
Set textDoc = ActiveDocument
foundList = False
For Each doc In Documents
  If aDoc <> "Normal.dot" Then
    Application.Windows(doc.Name).Activate
    Set rng = ActiveDocument.Paragraphs(1).Range
    fText = rng
    If InStr(fText, "List of headings") Then
      If newList = False Then
        Exit Sub
      Else
        Set listDoc = doc
        foundList = True
      End If
    End If
  End If
Next doc
  ' Go back to text file
Application.Windows(textDoc).Activate
ActiveDocument.Range.Copy

'Create a dummy file to avoid changing the original document
Documents.Add
ActiveDocument.Range.Paste

' Collect all the headings
Set rng = ActiveDocument.Content
For n = 1 To numStyles
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = ""
    .Replacement.Text = "abbb^&zxzx^p"
    .MatchWildcards = False
    .Style = myStyle(n)
    .Wrap = False
    .Format = True
    .Execute Replace:=wdReplaceAll
  End With
Next n

i = 0
Do
  Set rng = ActiveDocument.Content
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "abbb(*)zxzx"
    .Replacement.Text = "\1"
    .MatchWildcards = True
    .Wrap = False
    .Format = True
    .Execute Replace:=wdReplaceOne
  End With
  gotOne = (rng.Find.Found = True)
  If gotOne = True Then
    i = i + 1
    textArray(i) = rng
    styleArray(i) = rng.Style
  End If
Loop Until gotOne = False
iMax = i

' Get rid of the temporary document
ActiveDocument.Close SaveChanges:=wdDoNotSaveChanges

' Find a List of headings document ...
gottadoc = False
For Each doc In Documents
  Application.Windows(doc.Name).Activate
  Set rng = ActiveDocument.Paragraphs(1).Range
  fText = rng
  If InStr(fText, "List of headings") Then
    gottadoc = True: Exit For
  End If
Next

' ... or open a new one
If foundList = True Then
  listDoc.Activate
  Selection.WholeStory
  Selection.Delete
Else
  Documents.Add
  listDoc = ActiveDocument.Name
  Application.Windows(textDoc).Activate
  Set rng = ActiveDocument.Content
  rng.Start = headingFirst
  rng.Select
  Selection.Copy
  Selection.HomeKey Unit:=wdStory
  listDoc.Activate
  Selection.Paste
End If

Selection.HomeKey Unit:=wdStory
Selection.TypeText Text:="List of headings" & vbCrLf
Selection.MoveUp Unit:=wdLine, Extend:=wdExtend
Selection.Style = wdStyleNormal
Selection.Start = Selection.End

' Type in the lines of headings text
For i = 1 To iMax
  If Len(textArray(i)) > minLength Then
    Selection.TypeText Text:=textArray(i)
    Selection.MoveUp Unit:=wdLine, Extend:=wdExtend
    Selection.Style = styleArray(i)
    Selection.Collapse wdCollapseEnd
  End If
Next

Selection.HomeKey Unit:=wdStory

Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^12"
  .Replacement.Text = "^p"
  .MatchWildcards = False
  .Wrap = False
  .Format = True
  .Execute Replace:=wdReplaceAll
End With
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^p^p"
  .Replacement.Text = "^p"
  .MatchWildcards = False
  .Wrap = False
  .Format = True
  .Execute Replace:=wdReplaceAll
End With
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "Heading^p"
  .Replacement.Text = ""
  .MatchWildcards = False
  .Wrap = False
  .Format = True
  .Execute Replace:=wdReplaceAll
End With
With ActiveDocument.Styles(wdStyleNormal).ParagraphFormat
  .KeepWithNext = True
End With
Exit Sub


FoldBack:
' Select the cursor line
Selection.HomeKey Unit:=wdLine
Selection.EndKey Unit:=wdLine, Extend:=wdExtend
Selection.MoveLeft Unit:=wdCharacter, Count:=1, Extend:=wdExtend
fText = Selection
fStyle = Selection.Style

' Find the right document
Set rng = ActiveDocument.Paragraphs(2).Range
firstHeading = rng
DocName = ""
For Each doc In Documents
  If doc <> "Normal.dot" Then
    Application.Windows(doc.Name).Activate
    Set rng = ActiveDocument.Paragraphs(1).Range
    topText = rng
    If topText = firstHeading Then
      DocName = ActiveDocument.Name
    End If
    If DocName > "" Then Exit For
  End If
Next
If DocName = "" Then
  MsgBox ("Can't find file: " & firstHeading)
  Exit Sub
End If

' Try to find the searched-for line
Selection.HomeKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = fText
  .Replacement.Text = ""
  .Style = fStyle
  .MatchCase = False
  .MatchWildcards = False
  .Execute
End With
' select the searched-for heading
If Selection.Find.Found Then
   Selection.EndKey Unit:=wdLine
Else
' Somehow we missed it!
  If nameNow > "" Then
    listDoc.Activate
    MsgBox ("For some reason I can't find this heading.")
  End If
End If

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = fText
  .MatchCase = False
End With
End Sub




EC
Sub CommentAdd()
' Version 01.06.13
' Add a comment

attrib1 = "PB: "
attrib2 = "PB: "
attrib1 = ""
attrib2 = ""
postText = ""
keepPaneOpen = False
addPageNum1 = False
addLineNum1 = False
addPageNum2 = False
addLineNum2 = False
highlightTheText = False
textHighlightColour = wdYellow
colourTheText = False
textColour = wdColorBlue
removeHighlight = True

myStart = Selection.Start
wasEnd = Selection.End
Set rng = Selection.Range
rng.Collapse wdCollapseEnd
rng.MoveEnd , 1
pageNum = rng.Information(wdActiveEndAdjustedPageNumber)
lineNum = rng.Information(wdFirstCharacterLineNumber)
If myStart <> wasEnd Then
  If Right(Selection, 1) = Chr(32) Then
    Selection.MoveEnd wdCharacter, -1
    wasEnd = wasEnd - 1
  End If
  myTrack = ActiveDocument.TrackRevisions
  ActiveDocument.TrackRevisions = False
' Either highlight it ...
  If highlightTheText = True Then Selection.Range.HighlightColorIndex _
       = textHighlightColour
' And/or change the text colour to red
  If colourTheText = True Then Selection.Font.Color = textColour
' Now create the comment
  Selection.Copy
  Selection.Collapse wdCollapseEnd
  If addPageNum1 = True Then attrib1 = attrib1 & "(p. " & _
       pageNum & ") "
  If addLineNum1 = True Then attrib1 = attrib1 & "(line " & _
       lineNum & ") "
  Selection.TypeText Text:=attrib1 & ChrW(8216) & ChrW(8217)
' Move back to between the close and open quotes
  Selection.MoveLeft 1
' Paste in a copy of the selected text
  Selection.Paste
' Move back past the close quote
  Selection.MoveRight Count:=1
  If postText > "" Then
    Selection.TypeText Text:=postText
  Else
    Selection.TypeText Text:=" " & ChrW(8211) & " "
  End If
  Selection.Start = wasEnd
  Selection.Range.Revisions.AcceptAll
' If wanted, unhighlight the text clear
  If removeHighlight = True Then Selection.Range.HighlightColorIndex = wdAuto
  Selection.Cut
  If Asc(Selection) = 13 Then Selection.MoveEnd 1: Selection.Delete
  Selection.Comments.Add Range:=Selection.Range
  Selection.Paste
  ActiveDocument.TrackRevisions = myTrack
Else
  cmntText = attrib2
  If addPageNum2 = True Then cmntText = cmntText & _
       "(p. " & pageNum & ") "
  If addLineNum2 = True Then cmntText = cmntText & _
       "(line " & lineNum & ") "
  Selection.MoveEnd , 1
  Selection.Comments.Add Range:=Selection.Range
  Selection.TypeText cmntText
  Selection.HomeKey Unit:=wdLine
  Selection.Fields.Unlink
End If
If keepPaneOpen = False Then ActiveWindow.ActivePane.Close
End Sub




EC
Sub CommentAdd2()
' Version 01.06.13
' Add a comment

attrib1 = "PB: "
' attrib2 = "T/S: "
' postText = " is not on the refs list."
 postText = " Will the readers know this acronym?"
' postText = " Undefined acronym " & Chrw(8217) & " "
' postText = " � reference?"

addPageNum1 = True
addLineNum1 = True
addPageNum2 = True
addLineNum2 = True
highlightTheText = False
textHighlightColour = wdYellow
colourTheText = False
textColour = wdColorBlue
keepPaneOpen = False
myStart = Selection.Start
wasEnd = Selection.End
Set rng = Selection.Range
rng.Collapse wdCollapseEnd
rng.MoveEnd , 1
pageNum = rng.Information(wdActiveEndAdjustedPageNumber)
lineNum = rng.Information(wdFirstCharacterLineNumber)
If myStart <> wasEnd Then
  If Right(Selection, 1) = Chr(32) Then
    Selection.MoveEnd wdCharacter, -1
    wasEnd = wasEnd - 1
  End If
  myTrack = ActiveDocument.TrackRevisions
  ActiveDocument.TrackRevisions = False
' Either highlight it ...
  If highlightTheText = True Then Selection.Range.HighlightColorIndex _
       = textHighlightColour
' And/or change the text colour to red
  If colourTheText = True Then Selection.Font.Color = textColour
' Now create the comment
  Selection.Copy
  Selection.Collapse wdCollapseEnd
  If addPageNum1 = True Then attrib1 = attrib1 & "(p. " & _
       pageNum & ") "
  If addLineNum1 = True Then attrib1 = attrib1 & "(line " & _
       lineNum & ") "
  Selection.TypeText Text:=attrib1 & ChrW(8216) & ChrW(8217)
' Move back to between the close and open quotes
  Selection.MoveLeft 1
' Paste in a copy of the selected text
  Selection.Paste
' Move back past the close quote
  Selection.MoveRight Count:=1
  If postText > "" Then
    Selection.TypeText Text:=postText
  Else
    Selection.TypeText Text:=" " & ChrW(8211) & " "
  End If
  Selection.Start = wasEnd
  Selection.Range.Revisions.AcceptAll
' If wanted, unhighlight the text clear
  If removeHighlight = True Then Selection.Range.HighlightColorIndex = wdAuto
  Selection.Cut
  If Asc(Selection) = 13 Then Selection.MoveEnd 1: Selection.Delete
  Selection.Comments.Add Range:=Selection.Range
  Selection.Paste
  ActiveDocument.TrackRevisions = myTrack
Else
  cmntText = attrib2
  If addPageNum2 = True Then cmntText = cmntText & _
       "(p. " & pageNum & ") "
  If addLineNum2 = True Then cmntText = cmntText & _
       "(line " & lineNum & ") "
  Selection.MoveEnd , 1
  Selection.Comments.Add Range:=Selection.Range
  Selection.TypeText cmntText
  Selection.HomeKey Unit:=wdLine
  Selection.Fields.Unlink
End If
If keepPaneOpen = False Then ActiveWindow.ActivePane.Close
End Sub


EC
Sub CommentAddMenu()
' Version 28.04.14
' Add a comment off a menu

' Shift-Alt-#
' attrib1 = "PB: "
' attrib2 = "T/S: "
attrib1 = ""
attrib2 = ""

copyTheText = False
copyTheText = True


addPageNum1 = True
addLineNum1 = False
addPageNum2 = False
addLineNum2 = False
highlightTheText = False
textHighlightColour = wdYellow
colourTheText = False
textColour = wdColorBlue
keepPaneOpen = False
removeHighlight = True


c0 = "0 = no comment - just quote" & vbCr
cdot = ". = no comment - no quote" & vbCr
CR = "r = Not on refs list" & vbCr
cn = "n = Not cited" & vbCr
ca = "a = acronym unknown?" & vbCr
ci = "i = I can't work out..." & vbCr
cu = "u = URL is broke" & vbCr
cw = "w = Will the readers...?!" & vbCr
ch = "h = Have I caught...?" & vbCr
cf = "f = Figure not cited...?" & vbCr
co = "o = This OR that"


myResponse = InputBox(c0 & cdot & CR & cn & ca & ci & cu & cw & ch & cf & co, _
     "Comment Add Extra")
If Left(myResponse, 1) = "." Then
  noQuote = True
  myResponse = Right(myResponse, 1)
Else
  noQuote = False
End If

If copyTheText = False Then noQuote = True

Select Case myResponse
  Case "r": postText = "Not on the refs list."
  Case "n": postText = "Not cited in the text? Do you want to cite it somewhere?"
  Case "c": postText = "Not cited in the text? Do you want to cite it somewhere?"
  Case "a": postText = "Will the readers know this acronym? (It" & ChrW(8217) _
            & "s not defined anywhere that I can see.)"
  Case "i": postText = "I can" & ChrW(8217) & "t work out the intended meaning" & _
            " here, sorry. Is it something like "
  Case "w": postText = "Will the readers know what this means? If so, fine."
  Case "h": postText = "Have I caught the intended meaning?"
  Case "u": postText = "This URL does not seem to work."
  Case "f": postText = "This is not cited in the text. Where should I cite it?"
  Case "o": postText = " (as elsewhere)?"
  Case "0": postText = ""
  Case ".": Selection.Comments.Add Range:=Selection.Range
            If keepPaneOpen = False Then ActiveWindow.ActivePane.Close
            Exit Sub

  Case Else: Exit Sub
End Select


myStart = Selection.Start
wasEnd = Selection.End
Set rng = Selection.Range
rng.Collapse wdCollapseEnd
rng.MoveEnd , 1
pageNum = rng.Information(wdActiveEndAdjustedPageNumber)
lineNum = rng.Information(wdFirstCharacterLineNumber)
If Selection.End <> Selection.Start Then
  If Right(Selection, 1) = Chr(32) Or Right(Selection, 1) = Chr(13) Then
    Selection.MoveEnd wdCharacter, -1
    wasEnd = wasEnd - 1
  End If
' Either highlight it ...
  myTrack = ActiveDocument.TrackRevisions
  ActiveDocument.TrackRevisions = False
  If highlightTheText = True Then Selection.Range.HighlightColorIndex _
       = textHighlightColour
' And/or change the text colour to red
  If colourTheText = True Then Selection.Font.Color = textColour
' Now create the comment
  Selection.Copy
  Selection.Collapse wdCollapseEnd
  Set rng = Selection.Range
  rng.MoveEndUntil cset:=Chr(13), Count:=wdForward
  Selection.End = rng.End
  Selection.Collapse wdCollapseEnd
  extraBitStart = Selection.Start
  If addPageNum1 = True Then attrib1 = attrib1 & "(p. " & _
       pageNum & ") "
  If addLineNum1 = True Then attrib1 = attrib1 & "(line " & _
       lineNum & ") "
  Selection.TypeText Text:=attrib1

  If noQuote = False Then
    Selection.TypeText Text:=ChrW(8216)
  ' Paste in a copy of the selected text
    Selection.Paste
    If myResponse <> "o" Then
      Selection.TypeText Text:=ChrW(8217) & " " & ChrW(8211) & " "
    Else
      Selection.TypeText Text:=ChrW(8217) & " or " & ChrW(8216)
      Selection.Paste
      Selection.TypeText Text:=ChrW(8217)
    End If
  End If
  If postText > "" Then Selection.TypeText Text:=postText
  Selection.Start = extraBitStart
' If wanted, unhighlight the text clear
  If removeHighlight = True Then Selection.Range.HighlightColorIndex = wdAuto
  Selection.Range.Revisions.AcceptAll
  lenBit = Len(Selection)
  If lenBit > 1 Then Selection.Cut
'  If Asc(Selection) = 13 Then Selection.MoveEnd 1: Selection.Delete
  Selection.Start = myStart
  Selection.End = wasEnd
  Selection.Comments.Add Range:=Selection.Range
  If lenBit > 1 Then Selection.Paste
  ActiveDocument.TrackRevisions = myTrack
Else
  cmntText = attrib2
  If addPageNum2 = True Then cmntText = cmntText & _
       "(p. " & pageNum & ") "
  If addLineNum2 = True Then cmntText = cmntText & _
       "(line " & lineNum & ") "
  Selection.MoveEnd , 1
  Selection.Comments.Add Range:=Selection.Range
  Selection.TypeText cmntText
  Selection.HomeKey Unit:=wdLine
  Selection.Fields.Unlink
End If
If keepPaneOpen = False Then ActiveWindow.ActivePane.Close
End Sub



EC
Sub CommentsPane()
' Version 06.01.12
' Open the comments pane

If (ActiveDocument.Comments.Count > 0) Then
  ActiveDocument.ActiveWindow.View.SplitSpecial = wdPaneComments
End If
End Sub




EC
Sub CommentCopier()
' Version 18.01.12
' Create an author query list

addAnswerLine = True

If ActiveDocument.Comments.Count >= 1 Then
  ActiveDocument.StoryRanges(wdCommentsStory).Copy
End If

Documents.Add
Selection.Paste
Selection.TypeBackspace
If addAnswerLine = True Then
  Set rng = ActiveDocument.Range
 
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "^p"
    .Forward = True
    .Replacement.Text = "^p^pAnswer: ^p^p"
    .Execute Replace:=wdReplaceAll
  End With
 
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "Answer: ^p"
    .Replacement.Text = "Answer: ^p"
    .Replacement.Font.Color = wdColorRed
    .Execute Replace:=wdReplaceAll
  End With
 
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "T/S*AQ"
    .Replacement.Text = "AQ"
    .MatchWildcards = True
    .Execute Replace:=wdReplaceAll
  End With
End If

Selection.WholeStory
Selection.Style = wdStyleNormal
' Remove hidden page references
Selection.Fields.Unlink
Selection.HomeKey Unit:=wdStory

Selection.TypeText Text:="Author queries " & ChrW(8211) & " Chapter " & vbCrLf & vbCrLf
ActiveDocument.Paragraphs(1).Range.Style = "Heading 2"
End Sub


EC
Sub CopyComments()
' Version 01.06.10
' Create a list of all the comments
If ActiveDocument.Comments.Count >= 1 Then
  ActiveDocument.StoryRanges(wdCommentsStory).Copy
End If

Documents.Add
Selection.Paste
End Sub


EC
Sub CopyComments2()
' Version 01.06.10
' Create a list of all the comments
DocName = ActiveDocument.FullName
NewDocName = Replace(DocName, ".doc", "_Cmnts.doc")

If ActiveDocument.Comments.Count >= 1 Then
  ActiveDocument.StoryRanges(wdCommentsStory).Copy
End If

Documents.Add
Selection.Paste
ActiveDocument.SaveAs fileName:=NewDocName
End Sub


EC
Sub CommentListNumbered()
' Version 15.03.14
' List all comments in file with index numbers

addAnswerLine = True

If ActiveDocument.Comments.Count = 0 Then
  MsgBox "No comments in this file."
  Exit Sub
End If

Dim cmnt As Word.Comment
totCmnts = ActiveDocument.Comments.Count
ReDim cmText(totCmnts) As String
ReDim cmInits(totCmnts) As String
CR = vbCr: CR2 = CR & CR

' Collect comments, including formatting
If ActiveDocument.Comments.Count >= 1 Then
  ActiveDocument.StoryRanges(wdCommentsStory).Copy
End If

' Collect initials and text of comments
For i = 1 To totCmnts
  Set cmnt = ActiveDocument.Comments(i)
  cmInits(i) = cmnt.Initial
  cmText(i) = cmnt.Range
Next i


Documents.Add
Selection.Paste
Set rng = ActiveDocument.Content

For i = 1 To totCmnts
  lenCmnt = Len(cmText(i))
  If lenCmnt > 10 Then
    thisText = Left(cmText(i), 10) & "*^13"
    thisText = Replace(thisText, "\", "\\")
    thisText = Replace(thisText, "{", "\{")
    thisText = Replace(thisText, "}", "\}")
    thisText = Replace(thisText, "(", "\(")
    thisText = Replace(thisText, ")", "\)")
    thisText = Replace(thisText, "[", "\[")
    thisText = Replace(thisText, "]", "\]")
  Else
    thisText = cmText(i) & "^13"
  End If
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = thisText
    .Wrap = False
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = True
    .Execute
  End With
 
  If i = 1 Then
    rng.InsertBefore Text:="[" & cmInits(i) & Trim(Str(i)) & "]: "
  Else
    rng.InsertBefore Text:=CR & "[" & cmInits(i) & Trim(Str(i)) & "]: "
  End If
  rng.Collapse wdCollapseEnd
Next i
Selection.EndKey Unit:=wdStory
Selection.TypeText Text:=CR & "["

If addAnswerLine = True Then
  Set rng = ActiveDocument.Content
  rng.Font.Color = wdColorBlue

  Set rng = ActiveDocument.Content
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "^p["
    .Replacement.Text = "^pAnswer:^p^p["
    .Replacement.Font.Color = wdColorBlack
    .Execute Replace:=wdReplaceAll
  End With
End If

Set rng = ActiveDocument.Content
rng.Font.Size = ActiveDocument.Styles(wdStyleNormal).Font.Size

Selection.EndKey Unit:=wdStory
Selection.MoveStart , -1
Selection.Delete

' Delete all page number fields
For Each fld In ActiveDocument.Fields
  If fld.Type = 33 Then fld.Delete
Next fld

Selection.HomeKey Unit:=wdStory
Selection.TypeText Text:="Author queries " & ChrW(8211) & " Chapter " & vbCrLf & vbCrLf
ActiveDocument.Paragraphs(1).Range.Style = "Heading 2"
ActiveDocument.Paragraphs(1).Range.Font.ColorIndex = wdColorBlack
Selection.MoveLeft , 2
End Sub


EC
Sub CommentCollectTabulated()
' Version 17.12.13
' Collect comments into a table

authorComment = "Please insert response in this column."
sortOnColumn = 4
splitComment = False

ActiveDocument.TrackRevisions = False
totCmnts = ActiveDocument.Comments.Count
Selection.HomeKey Unit:=wdStory
If totCmnts = 0 Then
  MsgBox "There are no comments in this file!"
  Exit Sub
End If

Selection.WholeStory
Selection.Copy
Selection.HomeKey Unit:=wdStory
Documents.Add
Selection.Paste

Dim cmnt As Word.Comment
ReDim lineNum(totCmnts) As Integer
ReDim pageNum(totCmnts) As Integer

For i = 1 To totCmnts
  Selection.GoTo What:=wdGoToComment, Count:=i
  pn = Trim(Str(Selection.Range.Information(wdActiveEndAdjustedPageNumber)))
  ln = Trim(Str(Selection.Range.Information(wdFirstCharacterLineNumber)))
  Set cmnt = ActiveDocument.Comments(i)
  inits = cmnt.Initial
  scp = cmnt.Scope
  itemNo = Trim(Str(i))
  If splitComment = True And InStr(cmnt.Range, "|") = False Then
    cmnt.Range.InsertBefore Text:="|"
  End If



' BEFORE TEXT HERE <<<<<<<<<<<<<<<<<<<<<<<<
' Full version
cmnt.Range.InsertBefore Text:=inits & itemNo & "|" & "p." & pn & " l." & ln & "|" & scp & "|"

' Paul's own short version
'  cmnt.Range.InsertBefore Text:=inits & itemNo & "|"



' AFTER TEXT HERE <<<<<<<<<<<<<<<<<<<<<<<<<
  cmnt.Range.InsertAfter Text:="|"


Next i
ActiveDocument.StoryRanges(wdCommentsStory).Copy

' Replace whole text with just the (augmented) comments
Selection.WholeStory
Selection.Paste

' Remove hidden page references
Selection.WholeStory
Selection.Fields.Unlink
Selection.HomeKey Unit:=wdStory


' HEADING LINE HERE<<<<<<<<<<<<<<<<<<<<<<<<
' Full version
Selection.TypeText Text:="||Existing text|Who|Comment/query|Response" & vbCr

' Paul's own short version
' Selection.TypeText Text:="|||Comment/query|Response" & vbCr



ActiveDocument.Paragraphs(1).Range.Bold = True
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "|"
  .Wrap = wdFindContinue
  .Replacement.Text = "^t"
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With

Selection.EndKey Unit:=wdStory
Selection.TypeBackspace
Selection.HomeKey Unit:=wdStory
Selection.TypeText Text:="Author queries " & ChrW(8211) & " Chapter " & vbCr & vbCr
ActiveDocument.Paragraphs(1).Range.Style = "Heading 2"

ActiveDocument.Paragraphs(3).Range.Select
Selection.Collapse wdCollapseEnd
Selection.End = ActiveDocument.Range.End

If sortOnColumn > 0 Then Selection.Sort ExcludeHeader:=False, _
     FieldNumber:="Column " & Trim(Str(sortOnColumn)), _
     SortFieldType:=wdSortFieldAlphanumeric, SortOrder:=wdSortOrderAscending, _
     Separator:=wdSortSeparateByTabs, SortColumn:=False, CaseSensitive:=True
ActiveDocument.Paragraphs(4).Range.Select
Selection.MoveEnd , -1
Selection.InsertAfter authorComment
ActiveDocument.Paragraphs(2).Range.Select
Selection.Collapse wdCollapseEnd
Selection.End = ActiveDocument.Range.End

Selection.ConvertToTable Separator:=wdSeparateByTabs
Selection.Tables(1).Style = "Table Grid"
Selection.Tables(1).AutoFitBehavior (wdAutoFitContent)
Selection.HomeKey Unit:=wdStory
End Sub


EC
Sub MultiFileComment()
' Version 19.03.12
' List all comments in a set of files

addAnswers = True
myFileTitleSize = 14

CR = vbCrLf
CR2 = CR & CR
Dim cmnt As Word.Comment

Dim allMyFiles(200) As String
Set rng = ActiveDocument.Content
myExtent = 250
If rng.End - rng.Start > myExtent Then rng.End = rng.Start + myExtent

If InStr(rng.Text, ".doc") = 0 And InStr(rng.Text, ".rtf") = 0 Then
' If not a file list then open a file in the relevant folder
  docCount = Documents.Count
  Dialogs(wdDialogFileOpen).Show
  If docCount = Documents.Count Then Exit Sub
  dirPath = Replace(ActiveDocument.Path, ":" & ActiveDocument.Name, "")
  ActiveDocument.Close
' Read the names of all the files in this directory
  myFile = Dir("")
  Documents.Add
  numFiles = 0
  Do While myFile <> ""
    If InStr(myFile, ".doc") > 0 Or InStr(myFile, ".rtf") > 0 Then
      Selection.TypeText myFile & vbCrLf
      numFiles = numFiles + 1
    End If
    myFile = Dir()
  Loop

' Now sort the file list (only actually needed for Macs)
  Selection.WholeStory
  Selection.Sort
  Selection.EndKey Unit:=wdStory
  Selection.TypeParagraph
  Selection.HomeKey Unit:=wdStory
  Selection.TypeText dirPath
' Go back until you hit myDelimiter
  Selection.MoveStartUntil cset:=":\", Count:=wdBackward
  dirName = Selection
  Selection.HomeKey Unit:=wdStory

  myResponse = MsgBox("Collect comments from ALL the files in" & _
       " directory:" & dirName & " ?", vbQuestion + vbYesNoCancel, _
       "Multifile Comment Collection")
  If myResponse <> vbYes Then Exit Sub
Else
  myResponse = MsgBox("Collect comments from the files listed here?", _
  vbQuestion + vbYesNoCancel, "Multifile Comment Collection")
  If myResponse <> vbYes Then Exit Sub
End If

' Pick up the folder name and the filenames from the file list
numFiles = 0
myFolder = ""
For Each myPara In ActiveDocument.Paragraphs
  myPara.Range.Select
  Selection.MoveEnd , -1
  lineText = Selection
  If myFolder = "" Then
    myFolder = lineText
    Selection.Collapse wdCollapseEnd
    Selection.MoveStartUntil cset:=":\", Count:=wdBackward
    Selection.MoveStart , -1
    myDelimiter = Left(Selection, 1)
  Else
    thisFile = lineText
    If Len(thisFile) > 2 Then
      If Left(thisFile, 1) <> "|" Then
        numFiles = numFiles + 1
        allMyFiles(numFiles) = thisFile
      End If
    End If
  End If
Next myPara

Documents.Add
Set myList = ActiveDocument
For j = 1 To numFiles
  thisFile = myFolder & myDelimiter & allMyFiles(j)
  Set myDoc = Application.Documents.Open(fileName:=thisFile, ReadOnly:=True)
  myDocName = myDoc.Name
  myFileType = Mid(myDocName, InStr(myDocName, "."))
  myDocName = Replace(myDocName, myFileType, "")

' Collect all the comments
  totCmnts = ActiveDocument.Comments.Count
  If totCmnts > 0 Then
    ReDim cmText(totCmnts) As String
    For i = 1 To totCmnts
      Set cmnt = ActiveDocument.Comments(i)
      inits = cmnt.Initial
      cmText(i) = cmnt.Range
      If Left(cmText(i), Len(inits)) <> inits Then _
           cmText(i) = inits & ": " & cmText(i)
      If addAnswers = True Then cmText(i) = cmText(i) & CR2 _
           & "Answer: " & CR
      cmText(i) = cmText(i) & CR
    Next i
    cmText(i - 1) = cmText(i - 1) & CR
  Else
    totCmnts = 1
    ReDim cmText(totCmnts) As String
    cmText(1) = "zxzx"
  End If
' Type the comments into the list
  myList.Activate
' First type the filename as a heading
  Selection.TypeText Text:=myDocName & vbCrLf
  Selection.MoveUp Unit:=wdLine, Count:=1, Extend:=wdExtend
  Selection.Font.Bold = True
  Selection.Font.Size = myFileTitleSize
  Selection.EndKey Unit:=wdStory
  Selection.TypeText CR
' Then type each of the comments
  For i = 1 To totCmnts
    startHere = Selection.Start
    Selection.TypeText Text:="[" & cmText(i) & CR
    endHere = Selection.Start
    nowLen = Selection.Start - startHere
    Selection.End = startHere + InStr(cmText(i), ":")
    extrabit = Trim(Str(i)) & "]"
    Selection.TypeText Text:=extrabit
    Selection.Start = endHere + Len(extrabit)
    Selection.End = Selection.Start
  Next i
  myDoc.Close SaveChanges:=wdDoNotSaveChanges
Next j

' Colour the Answer lines
myList.Activate
If addAnswers = True Then
  Set rng = ActiveDocument.Content
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "Answer: ^p"
    .Replacement.Text = "^&"
    .MatchWildcards = False
    .Replacement.Font.Color = wdColorRed
    .Execute Replace:=wdReplaceAll
  End With
End If
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "1][zxzx"
  .Replacement.Text = "No comments^p^p"
  .Execute Replace:=wdReplaceAll
End With

' Remove hidden page references
Set rng = ActiveDocument.Content
rng.Fields.Unlink

Selection.HomeKey Unit:=wdStory
Beep
End Sub



EC
Sub CommentNumbering()
' Version 30.12.11
' Add or remove fixed comment initials and numbers

myText = ActiveDocument.Comments(1).Range
' If the first comment has a [ ] in it...
If InStr(myText, "[") > 0 Then
' Delete a [ ] item from each comment
  For i = 1 To ActiveDocument.Comments.Count
    Set rng = ActiveDocument.Comments(i).Range
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = "\[*\]: "
      .Wrap = wdFindContinue
      .Replacement.Text = ""
      .Forward = True
      .MatchWildcards = True
      .Execute Replace:=wdReplaceOne
    End With
  Next i
Else
' Add initials + index number to each comment
  For i = 1 To ActiveDocument.Comments.Count
    myInits = "[" & ActiveDocument.Comments(i).Initial & _
         Trim(Str(i)) & "]: "
    ActiveDocument.Comments(i).Range.InsertBefore myInits
  Next i
End If
End Sub


EC
Sub AddCommentMarkersInText()
' Version 30.12.11
' Add comment initials and numbers to text
For k = 1 To ActiveDocument.Comments.Count
  myInits = "[" & ActiveDocument.Comments(k).Initial & Trim(Str(k)) & "]"
  ActiveDocument.Comments(k).Reference.InsertAfter myInits
  Set rng = ActiveDocument.Comments(k).Reference
  rng.End = rng.End + Len(myInits)
  rng.HighlightColorIndex = wdBrightGreen
Next k
End Sub


EC
Sub DeleteAllComments()
' Version 21.02.12
' Delete all comments
numberCmnts = ActiveDocument.Comments.Count
If numberCmnts > 0 Then ActiveDocument.DeleteAllComments
MsgBox ("Comments deleted: " & Str(numberCmnts))
End Sub


EC
Sub CommentPicker()
' Version 09.11.13
' Copy a comment out of a list of comments

myCode = InputBox("WhichComment number?", "CommentPicker")

For Each pa In ActiveDocument.Paragraphs
  myText = pa.Range.Text

  If Len(myText) < 4 And Asc(myText) = Asc(myCode) Then
    pa.Range.Select
    Selection.Collapse wdCollapseEnd
    Selection.Paragraphs(1).Range.Select
    Selection.MoveEnd , -1
    Selection.Copy
    Exit Sub
  End If
Next
MsgBox ("Can't find comment >> " & myCode & " <<")
End Sub


EC
Sub CommentPickerInserter()
' Version 14.11.13
' Copy a comment from a list and paste into text

commentDoc = "CommentList"

Set mainDoc = ActiveDocument
Documents(commentDoc).Activate
myCode = InputBox("WhichComment number?", "CommentPicker")

For Each pa In ActiveDocument.Paragraphs
  myText = pa.Range.Text

  If Len(myText) < 4 And Asc(myText) = Asc(myCode) Then
    pa.Range.Select
    Selection.Collapse wdCollapseEnd
    Selection.Paragraphs(1).Range.Select
    Selection.MoveEnd , -1
    Selection.Copy
    theText = Selection.Text
    mainDoc.Activate
    Selection.Collapse wdCollapseEnd
    theStart = Selection.Start
    Selection.Paste
    cursorPosn = InStr(theText, "><")
    If cursorPosn > 0 Then
      Selection.Start = theStart + cursorPosn - 1
      Selection.End = theStart + cursorPosn + 1
      Selection.Delete
    End If
    Exit Sub
  End If
Next
MsgBox ("Can't find comment >> " & myCode & " <<")
End Sub


EK
Sub TrackOnOff()
' Version 01.01.10
' Toggle track changes on and off
  ActiveDocument.TrackRevisions = Not ActiveDocument.TrackRevisions
End Sub


EK
Sub TrackOnOffAudible()
' Version 26.02.11
' Track on/off with audio feedback
' <Ctrl-alt-num-/>

ActiveDocument.TrackRevisions = Not ActiveDocument.TrackRevisions
Beep
myTime = Timer
If ActiveDocument.TrackRevisions = True Then
  Do
  Loop Until Timer > myTime + 0.1
  Beep
End If
End Sub


EK
Sub ChangeNotTracked()
' Version 03.11.11
' Change (or add) some text, but don't track the change
myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

If Selection.Start <> Selection.End Then Selection.Delete
newText = InputBox("Change to?")
Selection.TypeText Text:=newText

ActiveDocument.TrackRevisions = myTrack

End Sub


EK
Sub TrackChangeAccept()
' Version 23.11.11
' Accept the track changes on the current line
If Selection.Start = Selection.End Then
  Selection.HomeKey Unit:=wdLine
  Selection.MoveDown Unit:=wdLine, Count:=1, Extend:=wdExtend
  Selection.Range.Revisions.AcceptAll
  Selection.Start = Selection.End
Else
  Selection.Range.Revisions.AcceptAll
End If
End Sub


EK
Sub TrackChangeReject()
' Version 23.11.11
' Accept the track changes on the current line
If Selection.Start = Selection.End Then
  Selection.HomeKey Unit:=wdLine
  Selection.MoveDown Unit:=wdLine, Count:=1, Extend:=wdExtend
  Selection.Range.Revisions.RejectAll
  Selection.Start = Selection.End
Else
  Selection.Range.Revisions.RejectAll
End If
End Sub


EK
Sub ShowHideAllTracking()
' Version 01.01.10
' Toggle showing track changes and comments on and off
ActiveWindow.View.ShowRevisionsAndComments = Not _
     ActiveWindow.View.ShowRevisionsAndComments
End Sub


EK
Sub ShowHideTracksOnly()
' Version 01.01.10
' Toggle showing just track changes on and off
ActiveWindow.View.ShowInsertionsAndDeletions = Not _
     ActiveWindow.View.ShowInsertionsAndDeletions
ActiveWindow.View.ShowFormatChanges = Not _
     ActiveWindow.View.ShowFormatChanges
End Sub


EK
Sub TrackingShowSwitcher()
' Version 23.02.11
' Toggle visibility of track changes

comm = ActiveWindow.View.ShowComments
inDel = ActiveDocument.ActiveWindow.View.ShowInsertionsAndDeletions

If comm = True And inDel = True Then
  ActiveDocument.ActiveWindow.View.ShowFormatChanges = False
  ActiveDocument.ActiveWindow.View.ShowInsertionsAndDeletions = False
  Exit Sub
End If
If inDel = False And comm = True Then
  ActiveDocument.ActiveWindow.View.ShowFormatChanges = False
  ActiveDocument.ActiveWindow.View.ShowInsertionsAndDeletions = False
  ActiveWindow.View.ShowComments = False
  Exit Sub
End If
ActiveDocument.ActiveWindow.View.ShowFormatChanges = True
ActiveDocument.ActiveWindow.View.ShowInsertionsAndDeletions = True
ActiveWindow.View.ShowComments = True
End Sub


EK
Sub TrackingShowSwitcherExtreme()
' Version 23.02.11
' Toggle visibility of track changes

If ActiveDocument.ActiveWindow.View.ShowFormatChanges = True Then
  ActiveDocument.ActiveWindow.View.ShowFormatChanges = False
  Exit Sub
End If
If ActiveDocument.ActiveWindow.View.ShowInsertionsAndDeletions = True Then
  ActiveDocument.ActiveWindow.View.ShowInsertionsAndDeletions = False
  Exit Sub
End If
If ActiveDocument.ActiveWindow.View.ShowComments = True Then
  ActiveDocument.ActiveWindow.View.ShowComments = False
  Exit Sub
End If
ActiveDocument.ActiveWindow.View.ShowFormatChanges = True
ActiveDocument.ActiveWindow.View.ShowInsertionsAndDeletions = True
ActiveWindow.View.ShowComments = True
End Sub


EK
Sub TrackChangeShowHide()
' Version 24.02.11
' Set up track changes to taste

With ActiveWindow.View
' Do you want comments showing?
  .ShowComments = True
' Do you want insertions and deletions showing?
  .ShowInsertionsAndDeletions = True
' Do you want format changes showing?
  .ShowFormatChanges = False

' Now switch the track changes on/off
  .ShowRevisionsAndComments = Not .ShowRevisionsAndComments
  .RevisionsView = wdRevisionsViewFinal
End With
End Sub


EK
Sub VisibleTrackOff()
' Version 01.01.10
' Visible reminder that track changes is off
If ActiveDocument.TrackRevisions = True Then
  ActiveDocument.TrackRevisions = False
  ActiveDocument.Range.Font.Emboss = True
Else
  ActiveDocument.Range.Font.Emboss = False
  ActiveDocument.TrackRevisions = True
End If
End Sub




EK
Sub VisibleTrackOff2()
' Version 01.01.10
' Visible reminder that track changes is off
If ActiveDocument.TrackRevisions = True Then
  ActiveDocument.TrackRevisions = False
  ActiveDocument.Range.Font.Underline = wdUnderlineDotted
  ActiveDocument.Range.Font.UnderlineColor = wdColorBlue
Else
  ActiveDocument.Range.Font.Underline = False
  ActiveDocument.TrackRevisions = True
End If
End Sub


EK
Sub VisibleTrackOff3()
' Version 24.09.11
' Visible trackchange reminder - using wiggly lines!
' Written by Thiers Halliwell

Set myRng = ActiveDocument.Range.Borders(wdBorderRight)

If ActiveDocument.TrackRevisions = False Then
    myRng.LineStyle = wdLineStyleNone
  ActiveDocument.TrackRevisions = True
Else
  ActiveDocument.TrackRevisions = False
    myRng.LineStyle = wdLineStyleSingleWavy
End If
End Sub


EK
Sub AcceptFormatting()
' Version 20.08.11
' Accept just the formatting track changes
' Supplied by Jessica Weissman

' Save the current state of things
comshow = ActiveWindow.View.ShowComments
inkshow = ActiveWindow.View.ShowInkAnnotations
indelshow = ActiveWindow.View.ShowInsertionsAndDeletions
formshow = ActiveWindow.View.ShowFormatChanges

' Hide all except formatting TCs
With ActiveWindow.View
  .ShowComments = False
  .ShowInkAnnotations = False
  .ShowInsertionsAndDeletions = False
  .ShowFormatChanges = True
End With
ActiveDocument.AcceptAllRevisionsShown

' Set things back as they were
With ActiveWindow.View
  .ShowComments = comshow
  .ShowInkAnnotations = inkshow
  .ShowInsertionsAndDeletions = indelshow
  .ShowFormatChanges = formshow
End With
End Sub


EK
Sub AcceptSpecificTrackChange()
' Version 02.03.11
' Accept all occurences of one specific track change
' Alt-Ctrl-Shift-f10

myType = Selection.Range.Revisions.Item(1).FormatDescription

theEnd = ActiveDocument.Content.End
i = 0
For Each rev In ActiveDocument.Range.Revisions
  Set rng = rev.Range
  thisType = rev.FormatDescription
  If thisType = myType Then
  i = i + 1
  rng.Revisions.AcceptAll
  End If
  StatusBar = "Accepting track changes...  " _
       & Str(theEnd - rng.End)
Next rev
StatusBar = "Accepted track changes:  " & Str(i)
Beep
End Sub


EK
Sub ConsolidateTracking()
' Version 12.11.14
' Turn split tracking into a single change

If Selection.Start = Selection.End Then
  Selection.Words(1).Select
  Selection.MoveEnd , -1
End If
Selection.Cut
Selection.Paste
End Sub


OT
Sub PageNumberChecker()
' Version 09.02.12
' Check that the text page number = Word's page number

' Is page number at the bottom of the page?
pageNumberAtBottom = False
 
Application.Browser.Target = wdBrowsePage
pageNow = Selection.Information(wdActiveEndPageNumber)
Do
  pageWas = pageNow
  Application.Browser.Next
' Have we moved at all from the line where we started
  If pageNumberAtBottom = True Then
    Selection.MoveStart Unit:=wdLine, Count:=-1
  Else
    Selection.MoveEnd Unit:=wdLine, Count:=1
  End If
  ' move past the non-numeric characters
  Selection.MoveEndUntil cset:="0123456789", Count:=wdBackward
  Selection.MoveStartUntil cset:="0123456789", Count:=wdForward
  myNumber = Val(Selection)
  pageNow = Selection.Information(wdActiveEndPageNumber)
Loop Until pageNow <> myNumber Or pageNow = pageWas
Beep
If pageNow = pageWas Then
' Second beep to show end
  myTime = Timer: Do: Loop Until Timer > myTime + 0.2: Beep
End If
End Sub


OT
Sub ContentsListChecker()
' Version 09.02.12
' Confirm the page numbers in the contents list

Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Text = "[[["
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = False
  .Execute Replace:=wdReplaceOne
End With

If rng.Find.Found = False Then
' We are in the contents list, so get the page number
  Selection.EndKey Unit:=wdLine
  Selection.MoveStartWhile cset:="0123456789", Count:=wdBackward
  findPage = Val(Selection)
  Selection.Collapse wdCollapseEnd
  Selection.InsertAfter Text:="[[["
  Selection.GoTo What:=wdGoToPage, Count:=findPage
Else
' We must now be back in the contents list
  rng.Select
  startHere = Selection.End + 1
  Selection.MoveDown Unit:=wdScreen, Count:=2
  Selection.End = startHere
  Selection.EndKey Unit:=wdLine
End If
End Sub


OT
Sub IndexChecker()
' Version 09.02.12
' List all pages on which this word(s) occur(s)

' Either zero or the first page number of the index
indexPageStart = 0

myColour = wdYellow
doWholeWordsOnly = False

If Selection.Start = Selection.End Then Selection.Words(1).Select
Selection.MoveEndWhile cset:=ChrW(8217) & "' ", Count:=wdBackward
myWord = Selection
indexPage = Selection.Information(wdActiveEndAdjustedPageNumber)

Set rng = ActiveDocument.Content
If indexPageStart = 0 Then indexPageStart = _
     rng.Information(wdActiveEndAdjustedPageNumber)
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = myWord
  .Wrap = False
  .Replacement.Text = ""
  .Forward = True
  .MatchCase = False
  .Execute
  .IgnoreSpace = True
  .IgnorePunct = True
End With
wasNum = 0

myFinds = ""
stopNow = False
Do While rng.Find.Found And stopNow = False
  pageNum = rng.Information(wdActiveEndAdjustedPageNumber)
  gotOne = True
  If doWholeWordsOnly = True Then
    rng.MoveEnd wdCharacter, 1
    rng.MoveStart wdCharacter, -1
    preChar = Left(rng, 1)
    postChar = Right(rng, 1)
    If UCase(preChar) <> LCase(preChar) Then gotOne = False
    If UCase(postChar) <> LCase(postChar) Then gotOne = False
  End If
' Got this page already?
  If wasNum = pageNum Then gotOne = False
' This is the page in the index where the word occurs!
  If pageNum = indexPage Then gotOne = False
' We've arrived at the index, so give up looking
  If pageNum >= indexPageStart Then gotOne = False: stopNow = True
' Add it to the list of finds
  If gotOne = True Then myFinds = myFinds & ", " & Trim(Str(pageNum))
  wasNum = pageNum
  rng.Collapse wdCollapseEnd
  rng.Find.Execute
Loop

With Selection.Find
  .IgnoreSpace = False
  .IgnorePunct = False
End With

myFinds = Mid(myFinds, 3)
myResponse = MsgBox(myFinds & vbCr & vbCr & "OK?", vbQuestion + vbYesNo)
If myResponse = vbYes Then
  Selection.Collapse wdCollapseStart
  Selection.MoveDown Unit:=wdLine, Count:=1
Else
  Selection.EndKey Unit:=wdLine
  Selection.InsertAfter Text:=" >>>>>>> " & myFinds
  Selection.Range.HighlightColorIndex = myColour
  Selection.Range.Font.Bold = True
  Selection.Collapse wdCollapseEnd
End If
End Sub




OT
Sub CiteFileLink()
' Version 18.04.12
' Link text and references files for CiteCheck
Set textDoc = ActiveDocument

Application.GoBack
Set refsDoc = ActiveDocument

' We are now in the references
varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "textDocName" Then varExists = True
  If v.Name = "refsDocName" Then v.Delete
Next v
If varExists = False Then
  ActiveDocument.Variables.Add "textDocName", textDoc.Name
Else
  ActiveDocument.Variables("textDocName") = textDoc.Name
End If

' Now go to the text file
textDoc.Activate
varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "refsDocName" Then varExists = True
  If v.Name = "textDocName" Then v.Delete
Next v
If varExists = False Then
  ActiveDocument.Variables.Add "refsDocName", refsDoc.Name
Else
  ActiveDocument.Variables("refsDocName") = refsDoc.Name
End If

myResponse = MsgBox("Text = " & textDoc.Name & "    References = " & refsDoc.Name, _
     vbQuestion + vbYesNoCancel)
If myResponse = vbCancel Then Exit Sub
If myResponse = vbNo Then
  myResponse = MsgBox("Text = " & refsDoc.Name & "    References = " & textDoc.Name, _
       vbQuestion + vbYesNoCancel)
  If myResponse = vbCancel Then Exit Sub
  If myResponse = vbYes Then
    textDoc.Activate
    ' We are now in the references
    varExists = False
    For Each v In ActiveDocument.Variables
      If v.Name = "textDocName" Then varExists = True
      If v.Name = "refsDocName" Then v.Delete
    Next v
    If varExists = False Then
      ActiveDocument.Variables.Add "textDocName", refsDoc.Name
    Else
      ActiveDocument.Variables("textDocName") = refsDoc.Name
    End If
    refsDoc.Activate
    ' We are now in the text
    varExists = False
    For Each v In ActiveDocument.Variables
      If v.Name = "refsDocName" Then varExists = True
      If v.Name = "textDocName" Then v.Delete
    Next v
    If varExists = False Then
      ActiveDocument.Variables.Add "refsDocName", textDoc.Name
    Else
      ActiveDocument.Variables("refsDocName") = textDoc.Name
    End If
  End If
End If
' Prepare the Find to look for dates
With Selection.Find
  .ClearFormatting
  .Highlight = False
  .Replacement.ClearFormatting
  .Text = "[0-9]{4}[a-k\);:,]"
  .Replacement.Text = ""
  .Forward = True
  .Wrap = False
  .MatchWildcards = True
  .Execute
End With
End Sub




OT
Sub CiteFileLink2()
' Version 25.06.12
' Link text and references files for CiteCheck

notTheseFiles = "zzSwitchList,zzFReditList,AllTextWord"

gottaRefs = False
gottaText = False
Do Until gottaRefs = True And gottaText = True
  For Each myWnd In Application.Windows
    myWnd.Document.Activate
    thisFile = Replace(myWnd.Document.Name, ".docx", "")
    thisFile = Replace(myWnd.Document.Name, ".doc", "")
    If InStr(notTheseFiles, thisFile) = 0 Then
      thisOne = InputBox("Text or Refs?", "CiteFileLink")
      If Len(thisOne) > 0 Then
        If Asc(LCase(thisOne)) = Asc("r") Then
          Set refsDoc = myWnd.Document
          gottaRefs = True
        End If
        If Asc(LCase(thisOne)) = Asc("t") Then
          Set textDoc = myWnd.Document
          gottaText = True
        End If
      End If
    End If
    If gottaRefs = True And gottaText = True Then Exit For
  Next myWnd
Loop

refsDoc.Activate
varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "textDocName" Then varExists = True
  If v.Name = "refsDocName" Then v.Delete
Next v
If varExists = False Then
  ActiveDocument.Variables.Add "textDocName", textDoc.Name
Else
  ActiveDocument.Variables("textDocName") = textDoc.Name
End If

textDoc.Activate
varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "refsDocName" Then varExists = True
  If v.Name = "textDocName" Then v.Delete
Next v
If varExists = False Then
  ActiveDocument.Variables.Add "refsDocName", refsDoc.Name
Else
  ActiveDocument.Variables("refsDocName") = refsDoc.Name
End If

' Prepare the Find to look for dates
With Selection.Find
  .ClearFormatting
  .Highlight = False
  .Replacement.ClearFormatting
  .Text = "[0-9]{4}[a-k\);:,]"
  .Replacement.Text = ""
  .Forward = True
  .Wrap = False
  .MatchWildcards = True
  .Execute
End With
Selection.HomeKey Unit:=wdStory
Beep
End Sub


OT
Sub CiteFindName()
' Version 21.10.11
' Jump back from date to name

beepIFand = True

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[A-Z]"
  .Wrap = wdFindContinue
  .Replacement.Text = ""
  .Forward = False
  .MatchWildcards = True
  .Execute
End With

backHere = Selection.Start
Selection.MoveLeft Unit:=wdCharacter, Count:=3
Selection.Words(1).Select
If Selection = "and " Then
  Selection.MoveLeft Unit:=wdCharacter, Count:=3
  Selection.Words(1).Select
  Selection.End = Selection.Start
  If beepIFand = True Then Beep
Else
  Selection.Start = backHere
  Selection.Words(1).Select
  Selection.End = Selection.Start
End If

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = oldFind
  .Forward = True
  .Highlight = False
  .Replacement.Text = oldReplace
  .MatchWildcards = True
  .Wrap = wdFindContinue
End With

End Sub


OT
Sub CiteCheck()
' Version 29.11.11
' Check reference citations

moveOnAfterOK = True
citeCheckBackwards = False
wholeName = False
myBestColour = wdYellow
myWarningColour = wdRed
myCitationColour = wdGray25

' If you want a beep warning for a multi-author citation...
addaBeepMulti = False

On Error Resume Next
isRefs = False
isText = False

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text
oldWild = Selection.Find.MatchWildcards
For Each v In ActiveDocument.Variables
  If v.Name = "textDocName" Then isRefs = True: textDoc = v
  If v.Name = "refsDocName" Then isText = True: refsDoc = v
Next v

' if both variables are set, delete them and tell the user
If isText = True And isRefs = True Then
  ActiveDocument.Variables("textDocName").Delete
  ActiveDocument.Variables("refsDocName").Delete
  MsgBox ("Please run the CiteFileLink macro.")
  Exit Sub
End If

' if variables not set, tell the user
If (isText = False And isRefs = False) Then
  MsgBox ("Please run the CiteFileLink macro.")
  Exit Sub
End If

'Remember existing highlight colour
oldColour = Options.DefaultHighlightColorIndex

' Refs doc procedure
If isRefs = True Then
  If Selection.Start = Selection.End Then
  ' if no word is selected, select the word at the cursor
    nameStart = Selection.Start
    Selection.MoveRight Unit:=wdCharacter, Count:=1
    Selection.MoveLeft Unit:=wdWord, Count:=1
    Selection.MoveRight Unit:=wdWord, Count:=1, Extend:=wdExtend
  End If
  myName = Selection
  ' remember whether the selection is highlighted
  thisColour = Selection.Range.HighlightColorIndex
  isHighlight = (thisColour = myWarningColour) Or thisColour > 255
  ' check the character after the word
  Selection.Start = Selection.End
  Selection.MoveEnd , 1
  aHyphen = False
  If Selection = "-" Then
  ' if it's a hyphen remember that and read the following word
    aHyphen = True
    Selection.MoveRight Unit:=wdWord, Count:=1, Extend:=wdExtend
    myName = myName + Selection
  ' now myName is, say ' Hamilton-Smythe'
  Selection.Start = nameStart
  Selection.End = nameStart
  End If
  ' if the name (beginning of the line) is highlighted,
  ' unhighlight all identical author names
  If isHighlight Then
    Selection.HomeKey Unit:=wdLine
    Selection.MoveLeft Unit:=wdWord, Count:=1
    With Selection.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = "^13" & Trim(myName) & "??"
      .Replacement.Text = ""
      .Replacement.Highlight = False
      .Wrap = wdFindContinue
      .Forward = True
      .MatchWildcards = True
      .Execute Replace:=wdReplaceAll
    End With
  End If
  ' go back to the text
  lookingFor = textDoc
  Application.Windows(textDoc).Activate
  If Err.Number = 5941 Then
    Err.Clear
    textDoc = Replace(textDoc, ".", " [Compatibility Mode].")
    Application.Windows(textDoc).Activate
    If Err.Number = 5941 Then GoTo ReportIt
  Else
    If Err.Number > 0 Then GoTo ReportIt
  End If
  If moveOnAfterOK = True Then
    With Selection.Find
      .ClearFormatting
      .Highlight = False
      .Replacement.ClearFormatting
      .Text = "[0-9]{4}[a-k\);:,]"
      .Replacement.Text = ""
      .Forward = True
      .Wrap = False
      .MatchWildcards = True
      .Execute
    End With
    Exit Sub
  End If
  GoTo Cleanup
End If

' Text doc procedure
' OR, if you're in the text, select the current word
Selection.MoveRight Unit:=wdCharacter, Count:=1
Selection.MoveLeft Unit:=wdWord, Count:=1
Selection.MoveRight Unit:=wdWord, Count:=1, Extend:=wdExtend
myName = Trim(Selection)
' remember where the name starts and ends
nameStart = Selection.Start
nameEnd = Selection.End
' if it says, say 'Brown's (2005) view...'
apostHere = InStr(myName, ChrW(8217))
' get rid of apostrophe
If apostHere > 0 Then
  myName = Left(myName, apostHere - 1)
  nameEnd = nameStart + apostHere - 1
End If
Selection.Start = Selection.End
' check for hyphenated name
Selection.MoveEnd , 1
If Selection = "-" Then
  Selection.MoveRight Unit:=wdWord, Count:=1, Extend:=wdExtend
  myName = myName + Trim(Selection)
  nameEnd = Selection.End
  apostHere = InStr(myName, ChrW(8217))
  ' again get rid of apostrophe
  If apostHere > 0 Then
    myName = Left(myName, apostHere - 1)
    nameEnd = nameStart + apostHere - 1
  End If
End If
' find the next date
Selection.Start = Selection.End
Selection.MoveLeft Unit:=wdWord, Count:=1
With Selection.Find
  .ClearFormatting
  .Text = "[0-9][0-9][0-9][0-9]"
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = True
  .Execute
End With
dateStart = Selection.Start

' check if it's '2005a', '2005b' etc
Selection.Start = Selection.End
Selection.MoveEnd , 1
mySuffix = Selection
If Asc(mySuffix) < 97 Or Asc(mySuffix) > 122 Then
  Selection.MoveEnd , -1
End If
Selection.Start = dateStart
myDate = Selection
dateEnd = Selection.End

' Record whole citation
Selection.Start = nameStart
wholeCitation = Selection
isMulti = False
If InStr(Selection, " and ") > 0 Then isMulti = True
If InStr(Selection, " et al") > 0 Then isMulti = True
Selection.End = Selection.Start


' go to the refs list
' Refs list after Text procedure
lookingFor = refsDoc
Application.Windows(refsDoc).Activate
If Err.Number = 5941 Then
  Err.Clear
  refsDoc = Replace(refsDoc, ".doc", " [Compatibility Mode].doc")
  Application.Windows(refsDoc).Activate
  If Err.Number = 5941 Then GoTo ReportIt
Else
  If Err.Number > 0 Then GoTo ReportIt
End If
If ActiveWindow.WindowState = 2 Then Application.WindowState = 1
Selection.MoveLeft Unit:=wdCharacter, Count:=1
textDoc = ActiveDocument.Variables("textDocName")
' find the name
If citeCheckBackwards = True Then
  crBit = ""
  cr13Bit = ""
Else
  crBit = "^p"
  cr13Bit = "^13"
End If
Selection.HomeKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Text = crBit & myName
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = False
  .Execute
End With

' if no such reference exists then
' try to find a name starting with the same
' letters, and then exit
If Selection.Find.Found = False Then
  For i = 1 To Len(myName) - 1
    Beep
    myTime = Timer: Do: Loop Until Timer > myTime + 0.05
    myName = Left(myName, Len(myName) - 1)
    With Selection.Find
      .ClearFormatting
      .Text = crBit & myName
      .Execute
    End With
    If Selection.Find.Found = True Then GoTo Cleanup
  Next
  GoTo Cleanup
End If

' Change highlight colour
Options.DefaultHighlightColorIndex = myBestColour

Do
' if you've found one, is it the right date?
  Selection.MoveStart , 1
  Selection.MoveDown Unit:=wdParagraph, Count:=1, Extend:=wdExtend
  wholeRef = Selection
  myStart = Selection.Start
  myEnd = Selection.End
  With Selection.Find
    .ClearFormatting
    .Text = myDate
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = True
    .Execute
  End With
  gotOne = Selection.Find.Found
  Selection.Find.MatchWildcards = False
  Selection.Start = Selection.End
  If Asc(Selection) > 96 And Asc(Selection) < 123 Then gotOne = False
  If gotOne = True Then
    dateEnd = Selection.End
  ' Check the current highlight colour a few characters to the right
    Selection.Start = Selection.End - 1
    hiColour = Selection.Range.HighlightColorIndex
    Selection.Start = myStart
    Selection.End = myStart
  ' Move selection to top of the screen
    Selection.MoveDown Unit:=wdScreen, Count:=2
    Selection.End = myStart - 1
    Selection.MoveRight Unit:=wdCharacter, Count:=1

    ' Ask if OK?
    Selection.End = dateEnd + 1
    Selection.Range.HighlightColorIndex = myBestColour
    If isMulti = True Then
      If addaBeepMulti = True Then Beep
      Selection.Range.HighlightColorIndex = myWarningColour
    End If
    Selection.End = Selection.Start
    myResponse = MsgBox("OK?", vbQuestion + vbYesNo)
    If myResponse = vbNo Then
      Selection.MoveDown Unit:=wdParagraph, Count:=1, Extend:=wdExtend
      Selection.Range.HighlightColorIndex = hiColour
      Selection.End = Selection.Start
      GoTo Cleanup
    Else
    ' Unhighlight reference
      Selection.MoveDown Unit:=wdParagraph, Count:=1, Extend:=wdExtend
      Selection.Range.HighlightColorIndex = wdNoHighlight
      Selection.End = Selection.Start
   
    ' Go back to text and grey all identical citations
      lookingFor = textDoc
      Application.Windows(textDoc).Activate
      If Err.Number = 5941 Then
        Err.Clear
        textDoc = Replace(textDoc, ".", " [Compatibility Mode].")
        Application.Windows(textDoc).Activate
        If Err.Number = 5941 Then GoTo ReportIt
      Else
        If Err.Number > 0 Then GoTo ReportIt
      End If
      Options.DefaultHighlightColorIndex = myCitationColour
      nowTrack = ActiveDocument.TrackRevisions
      ActiveDocument.TrackRevisions = False
      Selection.Start = Selection.End
      Set rng = ActiveDocument.Range
      With rng.Find
        .ClearFormatting
        .Replacement.ClearFormatting
        .Text = wholeCitation
        .Replacement.Text = "^&"
        .Replacement.Highlight = True
        .Wrap = wdFindContinue
        .Execute Replace:=wdReplaceAll
      End With
      ActiveDocument.TrackRevisions = nowTrack
      Options.DefaultHighlightColorIndex = oldColour
      If moveOnAfterOK = True Then
        With Selection.Find
          .ClearFormatting
          .Highlight = False
          .Replacement.ClearFormatting
          .Text = "[0-9]{4}[a-k\);:,]"
          .Replacement.Text = ""
          .Forward = True
          .Wrap = False
          .MatchWildcards = True
          .Execute
        End With
        Exit Sub
      End If
    End If
    GoTo Cleanup
  End If
  ' but if not the right date, look for next name
  Selection.Start = Selection.End
  Selection.MoveLeft Unit:=wdCharacter, Count:=1
  With Selection.Find
    .ClearFormatting
    .Text = crBit & myName
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = False
    .Execute
  End With
  nameStart = Selection.Start + 1
' keep looking until no such date can be found
Loop Until Selection.Find.Found = False

' if no such date for that author, highlight all
' like names, but first choose your favourite
' highlighting colour
Selection.HomeKey Unit:=wdStory
Beep
' find the first such name
Options.DefaultHighlightColorIndex = myWarningColour
'Set rng = ActiveDocument.Range
With Selection.Find
  .ClearFormatting
  If wholeName = True Then
    .Text = cr13Bit & myName & "[!a-z]"
  Else
    .Text = cr13Bit & myName
  End If
  .Replacement.Text = ""
  .Replacement.Highlight = True
  .Forward = True
  .MatchWildcards = True
  .Execute
End With
' highlight them all
Selection.Find.Execute Replace:=wdReplaceAll
Selection.MoveStart , 1
Selection.End = Selection.Start
For i = 1 To 3
  Beep
  myTime = Timer: Do: Loop Until Timer > myTime + 0.1
Next
' Tidy up
Options.DefaultHighlightColorIndex = oldColour

Cleanup:
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchWildcards = oldWild
End With
Exit Sub

ReportIt:
If Err.Number = 5941 Then
  If lookingFor = textDoc Then
    thisThing = "the text file"
  Else
    thisThing = "the references file"
  End If
  MsgBox ("Is " & thisThing & " loaded?" & vbCrLf & vbCrLf & _
       "If so, please try running CiteFileLink again.")
Else
  MsgBox Err.Description, vbExclamation, "Error from Word"
End If
End Sub





OT
Sub CitationLister()
' Version 15.10.13
' Create a list of all citations

' bannedWords = "January,February,March,April,June,July,August,"
bannedWords = "January,February,March,April,June,July,August,"
bannedWords = bannedWords & "September,October,November,December"

allPrefs = "van der de den da le la vahl "
prepreList = "van de ": ' for things like van der Waals or de la Rue

Dim myList As String
Dim myCite As String

myResponse = MsgBox("Is the reference list struck through?", vbQuestion _
      + vbYesNoCancel, "Citation Lister")
If myResponse <> vbYes Then Exit Sub

Set mainDoc = ActiveDocument
Selection.WholeStory
Selection.Copy
Selection.HomeKey Unit:=wdStory
gotFoots = (ActiveDocument.Footnotes.Count > 0)
gotEnds = (ActiveDocument.Endnotes.Count > 0)

' Create list document
Documents.Add
Set listDoc = ActiveDocument
Selection.Paste

' Pick up text of foot/endnotes
If gotFoots = True Then
  mainDoc.Activate
  ActiveDocument.StoryRanges(wdFootnotesStory).Copy
  listDoc.Activate
  Selection.EndKey Unit:=wdStory
  Selection.TypeText Text:=vbCrLf
  Selection.Paste
End If
If gotEnds = True Then
  mainDoc.Activate
  ActiveDocument.StoryRanges(wdEndnotesStory).Copy
  listDoc.Activate
  Selection.EndKey Unit:=wdStory
  Selection.TypeText Text:=vbCrLf
  Selection.Paste
End If

' Ignore struckthrough text
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = ""
  .Font.StrikeThrough = True
  .Wrap = wdFindContinue
  .Replacement.Text = "^p"
  .Forward = True
  .MatchWildcards = True
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute Replace:=wdReplaceAll
End With
Selection.WholeStory
Selection.Copy
Selection.PasteAndFormat (wdFormatPlainText)
Selection.WholeStory
Selection.Style = wdStyleNormal
Selection.ParagraphFormat.SpaceAfter = 0
Selection.Font.Reset
Selection.HomeKey Unit:=wdStory

' Go and find the first occurrence
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[!0-9][0-9]{4}[!0-9]"
  .Wrap = False
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = True
  .Execute
End With
Do While rng.Find.Found = True
' Note where the end of the found item is
  myEnd = rng.End
  rng.Select
  Selection.Collapse wdCollapseStart
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "[\( ^13^9][A-Z]"
    .Wrap = False
    .Replacement.Text = ""
    .Forward = False
    .MatchWildcards = True
    .Execute
  End With

  Selection.MoveStart wdCharacter, 1
  Selection.End = myEnd
  myShortCite = Selection
  lenShort = Selection.Words.Count
  firstWord = Trim(Selection.Words(1))
  If Selection.Find.Found = True Then secondWord = Trim(Selection.Words(2))
  Selection.Collapse wdCollapseStart
  With Selection.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "[\( ^13^9][A-Z]"
    .Wrap = False
    .Replacement.Text = ""
    .Forward = False
    .MatchWildcards = True
    .Execute
  End With
  Selection.MoveStart wdCharacter, 1
  Selection.End = myEnd
  myLongCite = Selection
  maxLen = 7
  lenLong = Selection.Words.Count
  If lenLong - lenShort = 1 Then
    Selection.Collapse wdCollapseStart
    With Selection.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = "[\( ^13^9][A-Z]"
      .Wrap = False
      .Replacement.Text = ""
      .Forward = False
      .MatchWildcards = True
      .Execute
    End With
    Selection.MoveStart wdCharacter, 1
    Selection.End = myEnd
    myLongCite = Selection
    maxLen = 8
  End If
  If InStr(myShortCite, ",") > 0 Then maxLen = maxLen + 1
 
  ' Check the two words before the first surname for 'van', etc,.
  Selection.MoveStart wdWord, -2
  preWord = Selection.Words(2)
  prepreWord = Selection.Words(1)
  Selection.MoveStart wdWord, 2

  myCite = myShortCite
  If InStr(myLongCite, " and ") > 0 Then myCite = myLongCite
  If InStr(myLongCite, " & ") > 0 Then myCite = myLongCite
  If InStr(myLongCite, Chr(13)) > 0 Then myCite = myShortCite
  numWords = Selection.Words.Count
  If numWords > maxLen Then myCite = myShortCite

  ' Check for van, der, den, de, etc
  If myCite = myShortCite Then
    If numWords > 4 Then
      preWord = Selection.Words(numWords - 4)
    Else
      preWord = "blah"
    End If
    If numWords > 5 Then
      prepreWord = Selection.Words(numWords - 5)
    Else
      prepreWord = "blah"
    End If
    If Len(prepreWord) > 1 And InStr(prepreList, LCase(prepreWord)) > 0 Then
      myCite = prepreWord & preWord & myCite
    Else
      If Len(preWord) > 1 And InStr(allPrefs, LCase(preWord)) > 0 Then
        myCite = preWord & myCite
      End If
    End If
  Else
    ' check long cite
    If InStr(prepreList, LCase(prepreWord)) > 0 Then
      myCite = prepreWord & preWord & myCite
    Else
      If InStr(allPrefs, LCase(preWord)) > 0 Then
        myCite = preWord & myCite
      End If
    End If
  End If
  rng.Start = Selection.Start
  rng.End = rng.Start
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "[!0-9,][0-9]{4}[!0-9]"
    .Wrap = False
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = True
    .Execute
  End With
  If rng.End - myEnd > 2 Then myCite = myShortCite
' Work out if this really is a citation
  isACite = False
' It's only a citation if it's fairly short
  If Len(myCite) - Len(Replace(myCite, " ", "")) < 8 Then isACite = True
  If InStr(bannedWords, firstWord) > 0 Then isACite = False
' Is the second word all lowercase (but not 'and' or 'et')?
  If LCase(secondWord) = secondWord And UCase(secondWord) <> secondWord _
       And secondWord <> "and" And secondWord <> "et" Then isACite = False
' Not wanted if it's all numbers
  If LCase(myCite) = UCase(myCite) Then isACite = False
' Not wanted if it's too short
  If Len(myCite) < 9 Then isACite = False
' No split lines
  If InStr(myCite, Chr(13)) > 0 Then isACite = False
  If InStr(myCite, Chr(9)) > 0 Then isACite = False
' OK then, add it to the list
  If isACite = True Then myList = myList & myCite & vbCrLf
' Go and find the next occurence (if there is one)
  rng.Start = myEnd
  rng.End = myEnd
  rng.Find.Execute
Loop

myList = Replace(myList, ";", "")
myList = Replace(myList, ":", "")
myList = Replace(myList, ".", "")
myList = Replace(myList, ")", "")
myList = Replace(myList, "(", "")
myList = Replace(myList, ",", "")
myList = Replace(myList, "=", "")
myList = Replace(myList, ChrW(8211), "")
myList = Replace(myList, ChrW(8212), "")
myList = Replace(myList, "'s", "")
myList = Replace(myList, ChrW(8217) & "s", "")
myList = Replace(myList, "&", "and")
myList = Replace(myList, " " & vbCr, vbCr)

Selection.WholeStory
Selection.Delete

Selection.Text = myList

' Replace runs of dates
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "([0-9]{4}) ([0-9]{4}) ([0-9]{4}) ([0-9]{4})"
  .Wrap = wdFindContinue
  .Replacement.Text = "\4"
  .Forward = True
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .Text = "([0-9]{4}) ([0-9]{4}) ([0-9]{4})"
  .Replacement.Text = "\3"
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .Text = "([0-9]{4}) ([0-9]{4})"
  .Replacement.Text = "\2"
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .Text = "([0-9]{4}) and ([0-9]{4})"
  .Replacement.Text = "\2"
  .Execute Replace:=wdReplaceAll
End With

Selection.WholeStory
Selection.Sort SortFieldType:=wdSortFieldAlphanumeric

' Remove duplicates
For j = ActiveDocument.Paragraphs.Count To 2 Step -1
  Set rng1 = ActiveDocument.Paragraphs(j).Range
  Set rng2 = ActiveDocument.Paragraphs(j - 1).Range
  If rng1 = rng2 Then rng1.Delete
  StatusBar = "Lines to go: " & Str(j)
Next j
Selection.WholeStory
Selection.Copy
ActiveDocument.Close SaveChanges:=False
Selection.EndKey Unit:=wdStory
listStart = Selection.Start
myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False
Selection.Paste
ActiveDocument.TrackRevisions = myTrack
Selection.End = listStart + 1
StatusBar = ""
With Selection.Find
  .Text = "References"
  .Replacement.Text = ""
End With
Beep
End Sub




OT
Sub CitationListChecker()
' Version 30.01.14
' Checks citations against references list

myColour = wdGray25
allPrefs = "van der de den da le la vahl "

myResponse = MsgBox("Is the reference list struck through?", vbQuestion _
      + vbYesNoCancel, "Citation List Checker")
If myResponse = vbCancel Then Exit Sub
myResponse = MsgBox("Is the cursor at the start of the reference list?", _
     vbQuestion + vbYesNoCancel, "Citation List Checker")
If myResponse = vbCancel Then Exit Sub

Selection.HomeKey Unit:=wdLine
Set rng = ActiveDocument.Content
rng.Start = Selection.Start
rng.Copy
Documents.Add
Selection.Paste
Selection.HomeKey Unit:=wdStory
numberCmnts = ActiveDocument.Comments.Count
If numberCmnts > 0 Then ActiveDocument.DeleteAllComments
Set rng = ActiveDocument.Content
rng.Revisions.AcceptAll
rng.HighlightColorIndex = myColour

Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = " ([0-9]{4})([ .,]) "
  .Wrap = wdFindContinue
  .Replacement.Text = " (\1)\2"
  .Forward = True
  .MatchWildcards = True
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = " ([0-9]{4}[a-f]) "
  .Wrap = wdFindContinue
  .Replacement.Text = " (\1) "
  .Forward = True
  .MatchWildcards = True
  .MatchWholeWord = False
  .MatchSoundsLike = False
  .Execute Replace:=wdReplaceAll
End With

' Find first unstruckthrough line
myResponse = vbNo
pCiteStart = 5
Do
  pCiteStart = pCiteStart + 1
  Set rng = ActiveDocument.Paragraphs(pCiteStart).Range
  If rng.Font.StrikeThrough = False And rng.Words.Count > 1 Then
    rng.Words(1).Select
    myResponse = MsgBox("Is the cursor now at the start of the citation list?", _
          vbQuestion + vbYesNoCancel, "Citation List Checker")
    If myResponse = vbCancel Then Exit Sub
  End If
Loop Until myResponse = vbYes

pCiteEnd = ActiveDocument.Paragraphs.Count
pRefsStart = 1
pRefsEnd = pCiteStart - 2

rng.Collapse wdCollapseStart
Set rng = ActiveDocument.Content
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "-"
  .Wrap = wdFindContinue
  .Replacement.Text = "zczc"
  .Forward = True
  .MatchWildcards = False
  .Execute Replace:=wdReplaceAll
End With
With rng.Find
  .Text = "'"
  .Replacement.Text = "pqpq"
  .Execute Replace:=wdReplaceAll
End With
With rng.Find
  .Text = ChrW(8217)
  .Replacement.Text = "pqpq"
  .Execute Replace:=wdReplaceAll
End With
With rng.Find
  .Text = "(eds.)"
  .Replacement.Text = "opopeds.clcl"
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .Text = "(ed.)"
  .Replacement.Text = "opopedclcl"
  .Execute Replace:=wdReplaceAll
End With

Do
  spcPos = InStr(allPrefs, " ")
  myPref = Left(allPrefs, spcPos)
  If allPrefs <> myPref Then allPrefs = Mid(allPrefs, spcPos + 1)
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "<" & myPref
    .Wrap = wdFindContinue
    .Replacement.Text = Trim(myPref) & "zxzx "
    .MatchWildcards = True
    .Execute Replace:=wdReplaceAll
  End With
  myPref = UCase(Left(myPref, 1)) & Mid(myPref, 2)
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "<" & myPref
    .Wrap = wdFindContinue
    .Replacement.Text = Trim(myPref) & "zxzx "
    .MatchWildcards = True
    .Execute Replace:=wdReplaceAll
  End With
Loop Until allPrefs = LCase(myPref)

With rng.Find
  .Text = "zxzx "
  .Replacement.Text = "zxzx"
  .Execute Replace:=wdReplaceAll
End With

myCount = pCiteEnd - pCiteStart
totCount = myCount

' Find the single author citations
For ct = pCiteStart To pCiteEnd
  Set rng = ActiveDocument.Paragraphs(ct).Range
  If rng.Words.Count = 3 Then
    aName = Trim(rng.Words(1))
      If aName = stopHere Then
      shkg = 0
      End If
    aDate = Trim(rng.Words(2))
    For rf = pRefsStart To pRefsEnd
      Set rng2 = ActiveDocument.Paragraphs(rf).Range
      If Trim(rng2.Words(1)) = aName Then
        brkpos = InStr(rng2, "(")
        If brkpos > 0 Then
          firstPart = Left(rng2, brkpos)
          thisDate = Left(Right(rng2, Len(rng2) - brkpos), 5)
          thisDate = Replace(thisDate, ")", "")
        Else
          thisDate = ""
        End If
        If thisDate = aDate And InStr(firstPart, " and ") = 0 And _
             InStr(firstPart, " & ") = 0 Then
          rng.HighlightColorIndex = False
          rng2.HighlightColorIndex = False
          myCount = myCount - 1
          rng.Select
          StatusBar = "(Single) To go: " & myCount
          Debug.Print "(Single) To go: " & myCount
        End If
      End If
    Next
  End If
Next ct


' Find the dual author citations
For ct = pCiteStart To pCiteEnd
  Set rng = ActiveDocument.Paragraphs(ct).Range
  If rng.HighlightColorIndex = myColour And rng.Words.Count = 5 Then
    aName1 = Trim(rng.Words(1))
      If aName1 = stopHere Then
      shkg = 0
      End If
    aName2 = Trim(rng.Words(3))
    aDate = rng.Words(4)
    For rf = pRefsStart To pRefsEnd
      Set rng2 = ActiveDocument.Paragraphs(rf).Range
      lhjldh = Trim(rng2.Words(1))
      If Trim(rng2.Words(1)) = aName1 Then
        brkpos = InStr(rng2, "(")
        If brkpos > 0 Then
          firstPart = Left(rng2, brkpos)
          thisDate = Left(Right(rng2, Len(rng2) - brkpos), 5)
          thisDate = Replace(thisDate, ")", "")
        Else
          thisDate = ""
        End If
        gotBothNames = (InStr(firstPart, " and " & aName2) > 0)
        If InStr(firstPart, " & " & aName2) > 0 Then gotBothNames = True
        If gotBothNames = True And thisDate = aDate Then
          rng.HighlightColorIndex = False
          rng2.HighlightColorIndex = False
          myCount = myCount - 1
          rng.Select
          StatusBar = "(Dual) To go: " & myCount
          Debug.Print "(Dual) To go: " & myCount
        End If
      End If
    Next
  End If
Next ct

' Find the et al citations
For ct = pCiteStart To pCiteEnd
  Set rng = ActiveDocument.Paragraphs(ct).Range
  If rng.HighlightColorIndex = myColour And rng.Words.Count = 5 _
       And InStr(rng, "et al") > 0 Then
    aName = Trim(rng.Words(1))
    aDate = rng.Words(4)
    For rf = pRefsStart To pRefsEnd
      Set rng2 = ActiveDocument.Paragraphs(rf).Range
      If Trim(rng2.Words(1)) = aName Then
        brkpos = InStr(rng2, "(")
        If brkpos > 0 Then
          firstPart = Left(rng2, brkpos)
          thisDate = Left(Right(rng2, Len(rng2) - brkpos), 5)
          thisDate = Replace(thisDate, ")", "")
        Else
          thisDate = ""
        End If
        If (Len(firstPart) - Len(Replace(firstPart, " ", "")) > 6 _
             Or InStr(firstPart, "et al") > 0) _
             And (InStr(firstPart, " & " & aName2) = 0) _
             And (InStr(firstPart, " and " & aName2) = 0) And _
             thisDate = aDate Then
          rng.HighlightColorIndex = False
          rng2.HighlightColorIndex = False
          myCount = myCount - 1
          rng.Select
          StatusBar = "(Et al's) To go: " & myCount
          Debug.Print "(Et al's) To go: " & myCount
        End If
      End If
    Next
  End If
Next ct

Set rng = ActiveDocument.Content
With rng.Find
  .Text = "zxzx"
  .Replacement.Text = " "
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .Text = "zczc"
  .Replacement.Text = "-"
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .Text = "pqpq"
  .Replacement.Text = ChrW(8217)
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .Text = "opop"
  .Replacement.Text = "("
  .Execute Replace:=wdReplaceAll
End With

With rng.Find
  .Text = "clcl"
  .Replacement.Text = ")"
  .Execute Replace:=wdReplaceAll
End With
ActiveDocument.Paragraphs(pCiteStart).Range.Select
Selection.Collapse wdCollapseStart
ActiveDocument.TrackRevisions = myTrack
MsgBox ("References checked: " & totCount - 1)
End Sub


OT
Sub MultifileCopier()
' Version 19.03.12
' Save a folder full of files as PDFs

toFolderName = "aaMainText"
' toFolderName = "WorkingText"
myPostFix = "_PB"

Dim allMyFiles(200) As String
Set rng = ActiveDocument.Content
myExtent = 250
If rng.End - rng.Start > myExtent Then rng.End = rng.Start + myExtent

If InStr(rng.Text, ".doc") = 0 And InStr(rng.Text, ".rtf") = 0 Then
' If not a file list then open a file in the relevant folder
  docCount = Documents.Count
  Dialogs(wdDialogFileOpen).Show
  If docCount = Documents.Count Then Exit Sub
  dirPath = Replace(ActiveDocument.Path, ":" & ActiveDocument.Name, "")
  ActiveDocument.Close
' Read the names of all the files in this directory
  myFile = Dir("")
  Documents.Add
  numFiles = 0
  Do While myFile <> ""
    If InStr(myFile, ".doc") > 0 Or InStr(myFile, ".rtf") > 0 Then
      Selection.TypeText myFile & vbCrLf
      numFiles = numFiles + 1
    End If
    myFile = Dir()
  Loop

' Now sort the file list (only actually needed for Macs)
  Selection.WholeStory
  Selection.Sort
  Selection.EndKey Unit:=wdStory
  Selection.TypeParagraph
  Selection.HomeKey Unit:=wdStory
  Selection.TypeText dirPath
' Go back until you hit myDelimiter
  Selection.MoveStartUntil cset:=":\", Count:=wdBackward
  dirName = Selection
  Selection.HomeKey Unit:=wdStory

  myResponse = MsgBox("Copy ALL the files in directory:" & dirName _
       & " ?", vbQuestion + vbYesNoCancel, "Multifile Copier")
  If myResponse <> vbYes Then Exit Sub
Else
  myResponse = MsgBox("Copy just the files listed here?", _
       vbQuestion + vbYesNoCancel, "Multifile Copier")
  If myResponse <> vbYes Then Exit Sub
End If

' Pick up the folder name and the filenames from the file list
numFiles = 0
myFolder = ""
For Each myPara In ActiveDocument.Paragraphs
  myPara.Range.Select
  Selection.MoveEnd , -1
  lineText = Selection
  If myFolder = "" Then
    myFolder = lineText
    Selection.Collapse wdCollapseEnd
    Selection.MoveStartUntil cset:=":\", Count:=wdBackward
    Selection.MoveStart , -1
    myDelimiter = Left(Selection, 1)
  Else
    thisFile = lineText
    If Len(thisFile) > 2 Then
      If Left(thisFile, 1) <> "|" Then
        numFiles = numFiles + 1
        allMyFiles(numFiles) = thisFile
      End If
    End If
  End If
Next myPara

' Check if new folder is ready
baseFolder = myFolder
Do
  baseFolder = Left(baseFolder, Len(baseFolder) - 1)
Loop Until Right(baseFolder, 1) = myDelimiter

Set FS = CreateObject("Scripting.FileSystemObject")
If FS.FolderExists(baseFolder & toFolderName) = False Then
  MsgBox "Please create folder: " & toFolderName
  Exit Sub
End If

' Load files and save copies
For i = 1 To numFiles
  thisName = allMyFiles(i)
  newName = toFolderName & myDelimiter & Replace(thisName, _
       ".", myPostFix & ".")
  thisFile = myFolder & myDelimiter & thisName
  Set myDoc = Application.Documents.Open(fileName:=thisFile, ReadOnly:=True)
  StatusBar = allMyFiles(i)
  myNewFile = baseFolder & newName
  ActiveDocument.SaveAs fileName:=myNewFile
  myDoc.Close SaveChanges:=wdDoNotSaveChanges
Next
Beep
End Sub





OT
Sub AddWordToStyleList()
' Version 26.05.12
' Add the selected text to the style list file

listName = "StyleSheet"
listName2 = "WordList"

withFormatting = True

myFont "Calibri"

If Selection.Start = Selection.End Then
  Selection.Words(1).Select
End If

myText = Selection
If withFormatting = True Then Selection.Copy

Set mainDoc = ActiveDocument
' Look for the list file
gotList = False
For i = 1 To Application.Windows.Count
  If InStr(Application.Windows(i).Document.Name, listName) > 0 Or _
       InStr(Application.Windows(i).Document.Name, listName2) > 0 Then
    Set listDoc = Application.Windows(i).Document
    gotList = True
  End If
Next

' Go to list and type word(s)
If gotList = True Then
  listDoc.Activate
  If withFormatting = True Then
    Selection.Paste
  Else
    Selection.TypeText Text:=myText
    Selection.Font.Name = myFont
  End If
  Selection.TypeText Text:=vbCrLf
  mainDoc.Activate
Else
  MsgBox "Sorry, can't find the style list."
  Exit Sub
End If
End Sub


OT
Sub MultiFilePDF()
' Version 19.03.12
' Save a folder full of files as PDFs

Dim allMyFiles(200) As String
Set rng = ActiveDocument.Content
myExtent = 250
If rng.End - rng.Start > myExtent Then rng.End = rng.Start + myExtent

If InStr(rng.Text, ".doc") = 0 And InStr(rng.Text, ".rtf") = 0 Then
' If not a file list then open a file in the relevant folder
  docCount = Documents.Count
  Dialogs(wdDialogFileOpen).Show
  If docCount = Documents.Count Then Exit Sub
  dirPath = Replace(ActiveDocument.Path, ":" & ActiveDocument.Name, "")
  ActiveDocument.Close
' Read the names of all the files in this directory
  myFile = Dir("")
  Documents.Add
  numFiles = 0
  Do While myFile <> ""
    If InStr(myFile, ".doc") > 0 Or InStr(myFile, ".rtf") > 0 Then
      Selection.TypeText myFile & vbCrLf
      numFiles = numFiles + 1
    End If
    myFile = Dir()
  Loop

' Now sort the file list (only actually needed for Macs)
  Selection.WholeStory
  Selection.Sort
  Selection.EndKey Unit:=wdStory
  Selection.TypeParagraph
  Selection.HomeKey Unit:=wdStory
  Selection.TypeText dirPath
' Go back until you hit myDelimiter
  Selection.MoveStartUntil cset:=":\", Count:=wdBackward
  dirName = Selection
  Selection.HomeKey Unit:=wdStory

  myResponse = MsgBox("Save PDFs of ALL the files in directory:" & dirName _
       & " ?", vbQuestion + vbYesNoCancel, "Multifile PDF Writer")
  If myResponse <> vbYes Then Exit Sub
Else
  myResponse = MsgBox("Save PDFs of just the files listed here?", _
       vbQuestion + vbYesNoCancel, "Multifile PDF Writer")
  If myResponse <> vbYes Then Exit Sub
End If

' Pick up the folder name and the filenames from the file list
numFiles = 0
myFolder = ""
For Each myPara In ActiveDocument.Paragraphs
  myPara.Range.Select
  Selection.MoveEnd , -1
  lineText = Selection
  If myFolder = "" Then
    myFolder = lineText
    Selection.Collapse wdCollapseEnd
    Selection.MoveStartUntil cset:=":\", Count:=wdBackward
    Selection.MoveStart , -1
    myDelimiter = Left(Selection, 1)
  Else
    thisFile = lineText
    If Len(thisFile) > 2 Then
      If Left(thisFile, 1) <> "|" Then
        numFiles = numFiles + 1
        allMyFiles(numFiles) = thisFile
      End If
    End If
  End If
Next myPara

For i = 1 To numFiles
  thisName = allMyFiles(i)
  myFileType = Mid(thisName, InStr(thisName, "."))
  justName = Replace(thisName, myFileType, "")
  thisFile = myFolder & myDelimiter & thisName
  Set myDoc = Application.Documents.Open(fileName:=thisFile, ReadOnly:=True)
  StatusBar = allMyFiles(i)
  ActiveDocument.ExportAsFixedFormat OutputFileName:=justName, _
      ExportFormat:=wdExportFormatPDF, OpenAfterExport:=False
  myDoc.Close SaveChanges:=wdDoNotSaveChanges
Next
MsgBox (numFiles & "  files saved as pdf")
End Sub


OT
Sub DoNowt()
' Version 01.01.01
' Do nothing at all!

End Sub


OT
Sub OvertypeBeep()
' Version 01.01.10
' Sound warning beep on overtype
Options.Overtype = Not Options.Overtype
Beep
If Options.Overtype = True Then
  Do
  Loop Until Timer > myTime + 0.1
  Beep
End If
End Sub


OT
Sub OvertypeBeep2()
' Version 01.01.10
' Sound warning beep on overtype + visual
Options.Overtype = Not Options.Overtype
Beep
myTime = Timer
If Options.Overtype = True Then
  StatusBar = String(100, "#")
  Do
  Loop Until Timer > myTime + 0.1
  Beep
End If
End Sub


OT
Sub ReverseList()
' Version 07.08.10
' Reverse the order of items in a list
oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

If Selection.End = Selection.Start Then
  myResponse = MsgBox("Reverse whole text?", vbQuestion + vbYesNo)
  If myResponse = vbNo Then Exit Sub
  wholeText = True
Else
' If working on selected text, put it in a
' separate, new document
  wholeText = False
  Set theText = ActiveDocument
  Selection.Cut
  Documents.Add
  Set tempDoc = ActiveDocument
  Selection.Paste
End If

'Add in dummy line numbers
i = 1000
For Each para In ActiveDocument.Paragraphs
  i = i + 1
  Set rng = para.Range
  rng.InsertBefore "zxc" & Trim(Str(i)) & "cxz"
Next

' Sort in reverse order
Selection.WholeStory
Selection.Sort SortOrder:=wdSortOrderDescending

' Remove dummy line numbers
Selection.HomeKey Unit:=wdStory
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = True
  .Text = "zxc[0-9]@cxz"
  .Replacement.Text = ""
  .Execute Replace:=wdReplaceAll
End With

' If working on selected text, put the sorted
' list back into the main text
If wholeText = False Then
  Selection.WholeStory
  Selection.Start = 2
  Selection.Cut
  tempDoc.Close SaveChanges:=False
  startHere = Selection.Start
  Selection.Paste
  Selection.Start = startHere
End If

' Restore original F&R
With Selection.Find
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchWildcards = False
End With
End Sub


OT
Sub PictureShow()
' Version 31.08.11
' Switch pictures on and off

ActiveWindow.View.ShowPicturePlaceHolders = _
     Not (ActiveWindow.View.ShowPicturePlaceHolders)
End Sub


OT
Sub SpecialCharList()
' Version 28.02.10
' Create a list of the unicode characters
Dim myFileSystem, myFileList, myFile, myFileType As String
Dim myDoc As Document

myChars = ""
mySymbols = ""
myResponse = MsgBox("Greek character collector" & vbCrLf & _
     "Multiple files?", vbQuestion + vbYesNo)
If myResponse = vbNo Then myFile = "": GoTo oneFile

myFolder = ActiveDocument.Path
ActiveDocument.Close SaveChanges:=False

Set myFileSystem = CreateObject("Scripting.FileSystemObject")
Set myFileList = myFileSystem.GetFolder(myFolder).Files

FilesTotal = 0
For Each myFile In myFileList
  myFileType = Right(myFile, 4)
  If (myFileType = ".doc" Or myFileType = "docx") And _
       Left(myFile, 1) <> "~" Then
    StatusBar = myFile
    Set myDoc = Application.Documents.Open(fileName:=myFile.Path, _
         ReadOnly:=True)
oneFile:
    StatusBar = "                           Counting ..."
    For Each myChr In ActiveDocument.Range.Characters
      If AscW(myChr) >= 255 Then
        If InStr(myChars, myChr) = 0 Then myChars = myChars & myChr & vbCrLf
      End If
      If myChr.Font.Name = "Symbol" Then
        If InStr(mySymbols, myChr) = 0 Then mySymbols = mySymbols & myChr & vbCrLf
      End If
    Next myChr
    If myResponse = vbNo Then GoTo theEnd
    myDoc.Close SaveChanges:=wdDoNotSaveChanges
  End If
Next myFile

theEnd:
Documents.Add
Selection.TypeText Text:=myChars & vbCrLf
Selection.WholeStory
Selection.Sort
Selection.EndKey Unit:=wdStory

Selection.TypeText Text:=vbCrLf & vbCrLf _
     & "Symbol fonts" & vbCrLf & vbCrLf
firstEnd = Selection.End
Selection.TypeText Text:=mySymbols & vbCrLf
Selection.Start = firstEnd
Selection.Sort
Selection.Font.Name = "Symbol"
Selection.HomeKey Unit:=wdStory
Selection.TypeText Text:="Unicode fonts" & vbCrLf & vbCrLf
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^13{1,}"
  .Replacement.Text = "^p"
  .MatchWildcards = True
  .Execute Replace:=wdReplaceAll
End With
StatusBar = ""
Selection.HomeKey Unit:=wdStory
End Sub


OT
Sub KeystrokeLister()
' Version 03.02.10
' Create a list of custom key allocations
Dim myKeys As String
Dim KeyCat As String
Dim Cmnd As String
Dim kb As KeyBinding

myKeys = "KeyString" & vbTab & "Category" _
   & vbTab & "Command" & vbTab & _
   "CommandParameter" & vbCrLf
Documents.Add
Selection.InsertAfter myKeys
Selection.Collapse wdCollapseEnd

For Each kb In KeyBindings
  Select Case kb.KeyCategory
     Case 0: KeyCat = "Disable"
     Case 1: KeyCat = "Command"
     Case 2: KeyCat = "Macro"
     Case 3: KeyCat = "Font"
     Case 4: KeyCat = "AutoText"
     Case 5: KeyCat = "Style"
     Case 6: KeyCat = "Symbol"
     Case 7: KeyCat = "Prefix"
  End Select
  Cmnd = Replace(kb.Command, "Normal.NewMacros.", "Nml.")
  myKeys = kb.KeyString & vbTab & KeyCat _
    & vbTab & Cmnd & vbTab _
    & kb.CommandParameter & vbCrLf
  Selection.InsertAfter myKeys
  Selection.Collapse wdCollapseEnd
Next kb
 
ActiveDocument.Paragraphs(1).Range.Font.Bold = True
ActiveDocument.Select
 
Set tbl = Selection.ConvertToTable(Separator:=wdSeparateByTabs)
tbl.Columns.AutoFit

' Now create a second copy of the list, but sorted by command
Selection.Copy
Selection.Sort ExcludeHeader:=True, FieldNumber:="Column 3"

Selection.Collapse wdCollapseStart
Selection.InsertBreak Type:=wdPageBreak
Selection.HomeKey Unit:=wdStory
Selection.Paste

End Sub


OT
Sub CustomKeys()
' Version 03.02.10
' Open the Customize Keyboard dialogue box
   Dialogs(wdDialogToolsCustomizeKeyboard).Show
End Sub



OT
Sub CustomKeys2()
' Version 03.02.10
' Open the Customize Keyboard dialogue box
   With Dialogs(wdDialogToolsCustomizeKeyboard)
     .Category = 2
     .Show
   End With
End Sub


OT
Sub TOCupdate()
' Version 30.07.10
' Update (and customise) the table of contents

ActiveDocument.TablesOfContents(1).Delete

' Position the ToC three lines down from the
' top of the document.
Selection.HomeKey Unit:=wdStory
Selection.MoveDown Unit:=wdLine, Count:=5

' This next command can be produced by recording
' the specific ToC creation that you want.
With ActiveDocument
  .TablesOfContents.Add Range:=Selection.Range, RightAlignPageNumbers:= _
    True, UseHeadingStyles:=True, UpperHeadingLevel:=1, _
    LowerHeadingLevel:=2, IncludePageNumbers:=False, AddedStyles:="", _
    UseHyperlinks:=True, HidePageNumbersInWeb:=True, UseOutlineLevels:= _
    True
  .TablesOfContents(1).TabLeader = wdTabLeaderDots
  .TablesOfContents.Format = wdIndexIndent
End With

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

' Now selectively delete any lines in the ToC
' that you don't want.
Selection.HomeKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = False
  .Text ""
  .Style = "Hyperlink"
  .Replacement.Text = ""
  .Execute Replace:=wdReplaceOne
End With
Selection.TypeBackspace

' This deletes some lines I don't want, from here...
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "Stylesheet"
  .Style = "Hyperlink"
  .Replacement.Text = ""
  .Execute
End With
myStart = Selection.Start

' ...to here
Selection.Start = Selection.End
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "Notes to publisher"
  .Style = "Hyperlink"
  .Replacement.Text = ""
  .Execute
End With
Selection.HomeKey Unit:=wdStory

' Extend the selection back to the first line
' that needs deleting.
Selection.Start = myStart
Selection.TypeBackspace
Selection.TypeBackspace

' Now highlight some of the lines
Selection.HomeKey Unit:=wdStory
With Selection.Find
  .Text = "Textual Analysis"
  .Style = "Hyperlink"
  .Replacement.Text = "^&"
  .Replacement.Highlight = True
  .Execute Replace:=wdReplaceOne
End With

Selection.HomeKey Unit:=wdLine
With Selection.Find
  .Text = "Pre-editing Tools"
  .Style = "Hyperlink"
  .Replacement.Text = "^&"
  .Replacement.Highlight = True
  .Execute Replace:=wdReplaceOne
End With

Selection.HomeKey Unit:=wdLine
With Selection.Find
  .Text = "Editing: Text Change"
  .Style = "Hyperlink"
  .Replacement.Text = "^&"
  .Replacement.Highlight = True
  .Execute Replace:=wdReplaceOne
End With

Selection.HomeKey Unit:=wdLine
With Selection.Find
  .Text = "Editing: Information"
  .Style = "Hyperlink"
  .Replacement.Text = "^&"
  .Replacement.Highlight = True
  .Execute Replace:=wdReplaceOne
End With

Selection.HomeKey Unit:=wdLine
With Selection.Find
  .Text = "Editing: Highlighting"
  .Style = "Hyperlink"
  .Replacement.Text = "^&"
  .Replacement.Highlight = True
  .Execute Replace:=wdReplaceOne
End With

Selection.HomeKey Unit:=wdLine
With Selection.Find
  .Text = "Editing: Navigation"
  .Style = "Hyperlink"
  .Replacement.Text = "^&"
  .Replacement.Highlight = True
  .Execute Replace:=wdReplaceOne
End With

Selection.HomeKey Unit:=wdLine
With Selection.Find
  .Text = "Other Tools"
  .Style = "Hyperlink"
  .Replacement.Text = "^&"
  .Replacement.Highlight = True
  .Execute Replace:=wdReplaceOne
End With

Selection.HomeKey Unit:=wdLine
With Selection.Find
  .Text = "Changes Log"
  .Style = "Hyperlink"
  .Replacement.Text = "^&"
  .Replacement.Highlight = True
  .Execute Replace:=wdReplaceOne
End With

Selection.HomeKey Unit:=wdStory

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = oldFind
  .Replacement.Text = oldReplace
End With
End Sub


OT
Sub AutoIndent()
' Version 01.01.10
' Automatically indent a macro listing

' Indent step size:
sizeStep = 2
' For indenting the wrapped-over long lines:
bigStep = 5
' Do you want the whole Sub to End Sub to be indented?
subIndent = False


' Remove all existing indents
Set rng = ActiveDocument.Range
For i = 1 To 2
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .MatchWildcards = True
    .Text = "^13 {1,}([! ])"
    .Wrap = wdFindContinue
    .Replacement.Text = "^p\1"
    .Execute Replace:=wdReplaceAll
  End With
Next i

' Add new indents
myIndent = 0
maxLine = ActiveDocument.Paragraphs.Count
For i = 1 To maxLine
  Set rng = ActiveDocument.Paragraphs(i).Range
  myLine = rng
  myLine = Left(myLine, Len(myLine) - 1)
  jumpNow = 0
  jumpNext = 0
  If myLine = "Do" Then jumpNext = 1
  If Left(myLine, 8) = "Do While" Then jumpNext = 1
  If subIndent = True And Left(myLine, 4) = "Sub " Then jumpNext = 1
  If Left(myLine, 8) = "Function" Then jumpNext = 1
  If Left(myLine, 5) = "While" Then jumpNext = 1
  If Left(myLine, 4) = "Else" Then jumpNext = 1: jumpNow = -1
  If Left(myLine, 4) = "With" Then jumpNext = 1
  If Right(myLine, 4) = "Then" Then jumpNext = 1
  If Left(myLine, 3) = "For" Then jumpNext = 1
  If Left(myLine, 11) = "Select Case" Then jumpNext = 1
  If Left(myLine, 4) = "End " Then jumpNow = -1
  If Left(myLine, 4) = "Wend" Then jumpNow = -1
  If subIndent = False And Left(myLine, 7) = "End Sub" Then jumpNow = 0
  If Left(myLine, 4) = "Loop" Then jumpNow = -1
  If Left(myLine, 4) = "Next" Then jumpNow = -1
  wasIndenting = indenting
  indenting = (Right(myLine, 1) = "_")
  extrabit = 0
  If wasIndenting = False And indenting = True Then extrabit = bigStep
  If wasIndenting = True And indenting = False Then extrabit = -bigStep
 
  myIndent = myIndent + sizeStep * jumpNow
  If myIndent > 0 Then rng.InsertBefore String(myIndent, " ")
  myIndent = myIndent + sizeStep * jumpNext + extrabit
  StatusBar = "Lines to go:        " & maxLine - i
  If myIndent < 0 Then
    rng.Select
    MsgBox ("Structure error?")
    Exit Sub
  End If
Next i
StatusBar = ""
Set rng = ActiveDocument.Range

' Empty the blank lines of spaces
For i = 1 To 8
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .MatchWildcards = False
    .Text = "  ^p"
    .Wrap = wdFindContinue
    .Replacement.Text = "^p"
    .Execute Replace:=wdReplaceAll
  End With
Next i

End Sub


OT
Sub WikiSwitch()
' Version 01.01.10
' Convert text to and from Wiki format
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "=" & "=^p"
  .Execute
End With

If rng.Find.Found = True Then
  Call WikiToStyles
Else
  Call WikiToText
End If

Selection.HomeKey Unit:=wdStory

End Sub


OT
Sub WikiToText()
' Version 01.01.10
' Convert from text to Wiki format
Dim Eq2, Eq3, Tk2, Tk3, pr, npr As String
Eq2 = "=" & "=": Eq3 = Eq2 + "="
Tk2 = "'" & "'": Tk3 = Tk2 + "'"
pr = "pre": npr = "</" & pr & ">"
pr = "<" & pr & ">"

' Convert Heading 2 to <=><=><=>Title<=><=><=>
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = True
  .Text = "(*)^13"
  .Style = "Heading 2"
  .Replacement.Text = Eq3 & "\1" & Eq3 & "^p"
  .Replacement.Style = wdStyleNormal
  .Execute Replace:=wdReplaceAll
End With

' Convert Heading 1 to <=><=>Title<=><=>
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = True
  .Text = "(*)^13"
  .Format = True
  .Style = "Heading 1"
  .Replacement.Text = Eq2 & "\1" & Eq2 & "^p"
  .Replacement.Style = wdStyleNormal
  .Execute Replace:=wdReplaceAll
End With

' Convert Bold to <'><'><'>word<'><'><'>
more = True
Do
  Set rng = ActiveDocument.Range
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = ""
    .Font.Bold = True
    .Execute
  End With
  more = rng.Find.Found
  If more = True Then
    rng.Font.Bold = False
    rng.InsertBefore Tk3
    rng.InsertAfter Tk3
  End If
Loop Until more = False

' Convert Italic to <'><'>word<'><'>
more = True
Do
  Set rng = ActiveDocument.Range
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = ""
    .Font.Italic = True
    .Execute
  End With
  more = rng.Find.Found
  If more = True Then
    rng.Font.Italic = False
    rng.InsertBefore Tk2
    rng.InsertAfter Tk2
  End If
Loop Until more = False

' Convert HTML sample to [pre]word[/pre]
gogo = True
Do
  Set rng = ActiveDocument.Range
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Style = "HTML Sample"
    .Execute
  End With
  myStart = rng.Start
  myEnd = rng.End
  gogo = rng.Find.Found

  If gogo = True Then
  Set rng = ActiveDocument.Range
  rng.Start = myEnd
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Style = "Default Paragraph Font"
    .Execute
  End With
  myEnd = rng.Start

  rng.Start = myStart
  rng.End = myEnd
  rng.Style = "Default Paragraph Font"

  rng.InsertBefore pr & vbCrLf
  rng.InsertAfter npr & vbCrLf
  End If

Loop Until gogo = False

' Remove blank lines above an npr
Set rng = ActiveDocument.Range
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .MatchWildcards = False
    .Text = "^p^p" & npr
    .Replacement.Text = "^p" & npr & "^p"
    .Execute Replace:=wdReplaceAll
  End With


' Add second blank lines after an npr before a heading
Set rng = ActiveDocument.Range
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .MatchWildcards = False
    .Text = npr & "^p^p"
    .Replacement.Text = npr & "^p^p^p=="
    .Execute Replace:=wdReplaceAll
  End With
End Sub


OT
Sub WikiToStyles()
' Version 01.01.10
' Convert from Wiki format to text
Dim Eq2, Eq3 As String
Eq2 = "=" & "=": Eq3 = Eq2 + "="
Tk2 = "'" & "'": Tk3 = Tk2 + "'"
pr = "pre": npr = "</" & pr & ">"
pr = "<" & pr & ">"

' Convert <=><=><=>Title<=><=><=> to Heading 2
Dim rng As Range
Set rng = ActiveDocument.Range
With rng.Find
   .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = True
  .Text = Eq3 & "(*)" & Eq3 & "^13"
  .Replacement.Text = "\1^p"
  .Replacement.Style = "Heading 2"
  .Execute Replace:=wdReplaceAll
End With

' Convert <=><=>Title<=><=> to Heading 1
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = True
  .Text = Eq2 & "(*)" & Eq2 & "^13"
  .Replacement.Text = "\1^p"
  .Replacement.Style = "Heading 1"
  .Execute Replace:=wdReplaceAll
End With

' Convert <'><'><'>word<'><'><'> to Bold
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = True
  .Text = Tk3 & "(*)" & Tk3
  .Replacement.Text = "\1"
  .Replacement.Font.Bold = True
  .Execute Replace:=wdReplaceAll
End With

' Convert <'><'>word<'><'> to Italic
Set rng = ActiveDocument.Range
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = True
  .Text = Tk2 & "(*)" & Tk2
  .Replacement.Text = "\1"
  .Replacement.Font.Italic = True
  .Execute Replace:=wdReplaceAll
End With

' Convert [pre]section[/pre] to HTML sample
Do
  Set rng = ActiveDocument.Range
  fileEnd = rng.End
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .MatchWildcards = False
    .Text = "^p" & pr & "^p"
    .Replacement.Text = "^p"
    .Execute Replace:=wdReplaceOne
  End With
  codeStart = rng.End

  If rng.End <> fileEnd Then
    Set rng = ActiveDocument.Range
    With rng.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .MatchWildcards = False
      .Text = "^p" & npr & "^p"
      .Replacement.Text = "^p"
      .Execute Replace:=wdReplaceOne
    End With
    codeEnd = rng.End
    rng.Start = codeStart
    rng.Style = "HTML Sample"
  End If
Loop Until rng.End = fileEnd
End Sub


OT
Sub IndexElide()
' Version 23.12.10
' Add elision to an index
For Each para In ActiveDocument.Paragraphs
Set rng = para.Range
thisNum = 0: firstNum = 0: topNum = 0: prevNum = 0
' Read thisNumber
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = True
  .Text = "[0-9]{1,}"
  .Replacement.Text = ""
  .Execute
End With
If rng.Find.Found = True Then
  thisText = rng
  thisNum = Val(thisText)
  thisNumStart = rng.Start
  thisNumEnd = rng.End
  rng.Start = rng.End
End If

If thisNum = 0 Then GoTo nextPara

gotJustOne:
' Got first number in a possible run
firstNum = thisNum
firstNEnd = thisNumEnd

onARun:
' Come here top look for the next number
topNum = thisNum: prevNum = thisNum
topNumStart = thisNumStart

' Read next number
' but first find where the line ends
Set rng2 = para.Range
lineEnd = rng2.End
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = True
  .Text = "[0-9]{1,}"
  .Replacement.Text = ""
  .Execute
End With
' If we've found a number and it's on the current line
If rng.Find.Found = True And rng.Start < lineEnd Then
  thisText = rng
  thisNum = Val(thisText)
  thisNumStart = rng.Start
  thisNumEnd = rng.End
  rng.Start = rng.End
End If

If thisNum = prevNum Then
' If no more numbers, and this is only
' a single number then go to next para
  If topNum = firstNum Then GoTo nextPara
Else
' If the run is continuing
  If thisNum = prevNum + 1 Then GoTo onARun
End If

' If we're at the beginning of a new run ...
If firstNum = topNum Then GoTo gotJustOne

' ...type the hyphen in the previous run
rng.Start = firstNEnd
rng.End = topNumStart
chopLength = topNumStart - firstNEnd - 1
rng.Select
Selection.Delete
Selection.TypeText Text:=Chr(150)
thisNumStart = thisNumStart - chopLength
thisNumEnd = thisNumEnd - chopLength
rng.Start = thisNumEnd
rng.End = thisNumEnd
prevNum = 0

If topNum = thisNum Then
' If the end of line has been reached
  GoTo nextPara
Else
' If not, then we've got a first number
' so go and find a second one.
  GoTo gotJustOne
End If

nextPara:
Next para
Selection.HomeKey Unit:=wdStory
End Sub


OT
Sub BasicIndexer()
' Version 27.12.10
' Basic indexing
textFile = "ReadyForIndexing"
listFile = "Keywords_Plus"
pageMarker = "Page Proof page "
searchDelimiter = ","
listDelimiter = ", "
repeatNumbers = True

Application.Windows(listFile).Activate
For Each para In ActiveDocument.Paragraphs
  Set rng = para.Range
  With rng.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .MatchWildcards = True
    .Text = "[" & searchDelimiter & "]"
    .Replacement.Text = ""
    .Execute
  End With
  rng.End = rng.Start
  rng.Start = para.Range.Start
 
  headWord = rng
  StatusBar = ">>>>>>>>>>>>   " & headWord

' Various dashes and apostrophes to "any character"
  headWord = Replace(headWord, "-", "^?")
  headWord = Replace(headWord, ChrW(8211), "^?")
  headWord = Replace(headWord, ChrW(8212), "^?")
  headWord = Replace(headWord, "'", "^?")
  headWord = Replace(headWord, ChrW(8217), "^?")
  If Len(headWord) > 3 Then
    Application.Windows(textFile).Activate
    Set rng2 = ActiveDocument.Range
    foundPages = ""
    Do
    previousPage = ""
    ' Find each occurrence of headword
      With rng2.Find
        .ClearFormatting
        .Replacement.ClearFormatting
        .MatchWildcards = False
        .MatchCase = False
        .MatchWholeWord = True
        .Text = headWord
        .Replacement.Highlight = True
        .Replacement.Text = ""
        .Execute Replace:=wdReplaceOne
      End With
      comeBackHere = rng2.End
      rng2.Start = rng2.End
      If rng2.Find.Found Then
      ' Find current page number
        With rng2.Find
          .ClearFormatting
          .Replacement.ClearFormatting
          .MatchWildcards = False
          .Text = pageMarker
          .Replacement.Text = ""
          .Execute
        End With
        If rng2.Find.Found = True Then
          rng2.Start = rng2.End
          rng2.MoveEnd wdCharacter, 6
          textBit = rng2
          pageNum = Left(textBit, InStr(textBit, " ") - 1)
          If repeatNumbers = True Then
            foundPages = foundPages & pageNum & listDelimiter
          Else
            If previousNumber <> pageNum Then
              foundPages = foundPages & pageNum & listDelimiter
              previousNumber = pageNum
            End If
          End If
          rng2.Start = comeBackHere
          rng2.End = comeBackHere
        Else
          comeBackHere = 0
        End If
      Else
        comeBackHere = 0
      End If
    Loop Until comeBackHere = 0
    Application.Windows(listFile).Activate
   
    rng.Start = para.Range.End - 1
    rng.InsertAfter Text:=": " & foundPages
    Application.Windows(textFile).Activate
  End If
Next para

' Remove trailing commas from list
Application.Windows(listFile).Activate
Selection.HomeKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = False
  .Text = ", ^p"
  .Replacement.Text = "^p"
  .Execute Replace:=wdReplaceAll
End With
Selection.HomeKey Unit:=wdStory
End Sub



OT
Sub BasicIndexer2()
' Version 22.10.12
' Basic indexing
repeatNumbers = False
listDelimiter = ", "
addaTab = True
myResponse = MsgBox("Is the cursor on the first line of the word list?", _
      vbQuestion + vbYesNo)
If myResponse = vbNo Then Exit Sub

paraStart = ActiveDocument.Range(0, Selection.Paragraphs(1).Range.End).Paragraphs.Count
Selection.HomeKey Unit:=wdLine
listStart = Selection.Start
For paraNum = paraStart To ActiveDocument.Paragraphs.Count
  Set rng = ActiveDocument.Paragraphs(paraNum).Range
  rng.Select
  Selection.Range.HighlightColorIndex = wdGray25
  Selection.MoveEnd , -1
  headWord = Selection

' Various dashes and apostrophes to "any character"
  headWord = Replace(headWord, "-", "^?")
  headWord = Replace(headWord, ChrW(8211), "^?")
  headWord = Replace(headWord, ChrW(8212), "^?")
  headWord = Replace(headWord, "'", "^?")
  headWord = Replace(headWord, ChrW(8217), "^?")
  If Len(headWord) > 3 Then
    Set rng = ActiveDocument.Content
    foundPages = ""
    Do
    previousPage = ""
    ' Find each occurrence of headword
      With rng.Find
        .ClearFormatting
        .Replacement.ClearFormatting
        .MatchWildcards = False
        .MatchCase = False
        .Highlight = False
        .Text = headWord
        .Replacement.Highlight = True
        .Execute Replace:=wdReplaceOne
      End With
      rng.Start = rng.End
      If rng.Find.Found Then
      ' Find current page number
        Application.Browser.Target = wdBrowsePage
        pageNum = Trim(Str(rng.Information(wdActiveEndPageNumber)))
        If repeatNumbers = True Then
          foundPages = foundPages & pageNum & listDelimiter
        Else
          If previousNumber <> pageNum Then
            foundPages = foundPages & pageNum & listDelimiter
            previousNumber = pageNum
          End If
        End If
      End If
    Loop Until rng.Find.Found = False
   
    If addaTab = True Then foundPages = vbTab & foundPages
    Selection.InsertAfter Text:=foundPages
  End If
Next paraNum

' Remove trailing commas from list
Set rng = ActiveDocument.Content
rng.Start = listStart
rng.HighlightColorIndex = 0
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .MatchWildcards = False
  .Text = ", ^p"
  .Replacement.Text = "^p"
  .Execute Replace:=wdReplaceAll
End With
rng.Select
Selection.Collapse wdCollapseEnd
End Sub



OT
Sub NumberParasAuto()
' Version 21.05.11
' Add hierarchical section numbering

addChapterNum = False
chapWord = ""
chapNum = 1

' Make sure that page breaks are in Normal style
Set rng = ActiveDocument.Content
With rng.Find
 .ClearFormatting
 .Replacement.ClearFormatting
 .Text = "^m"
 .Replacement.Text = ""
 .Forward = True
 .MatchWildcards = False
 .Execute Replace:=wdReplaceAll
 .Replacement.Style = ActiveDocument.Styles(wdStyleNormal)
End With

h = "Heading "
levelWas = 1
chapNum = chapNum - 1
myNum = Trim(Str(chapNum))
For Each para In ActiveDocument.Paragraphs
  para.Range.Select
  head = Selection.Style
  If head = "Heading 1" Then
    head = ""
    If Left(para.Range, Len(chapWord)) = chapWord Then
      levelWas = 1
      chapNum = chapNum + 1
      myNum = Trim(Str(chapNum))
      If addChapterNum = True Then para.Range.InsertBefore _
           Text:=myNum & vbTab
    End If
  End If
  If InStr(head, h) > 0 Then
    levelNow = Val(Replace(head, h, ""))
    If levelNow = levelWas Then
      endNum = Val(Right(myNum, 2))
      If endNum < 1 Then endNum = Val(Right(myNum, 1))
      newEndNum = endNum + 1
      myNum = Replace(myNum & "!", Trim(Str(endNum)) & "!", Trim(Str(newEndNum)))
    Else
      If levelNow > levelWas Then
        For i = 1 To levelNow - levelWas
          myNum = myNum + ".1"
        Next i
      Else
        For i = 1 To levelWas - levelNow
          myNumPlus = myNum & "!"
          temp = myNumPlus
          temp = Right(temp, Len(temp) - InStr(temp, ".") - 1)
          If Asc(temp) <> Asc(".") Then
            temp = Right(temp, Len(temp) - 1)
          End If
        Next i
        myNum = Replace(myNumPlus, temp, "")
        endNum = Val(Right(myNum, 2))
        If endNum < 1 Then endNum = Val(Right(myNum, 1))
        newEndNum = endNum + 1
        myNum = Replace(myNum & "!", Trim(Str(endNum)) & _
               "!", Trim(Str(newEndNum)))
      End If
    End If
    levelWas = levelNow
    para.Range.InsertBefore Text:=myNum & vbTab
  End If
Next para
End Sub


OT
Sub SuitToText()
' Version 21.07.11
' Convert playing card suit symbolks to text

For Each fld In ActiveDocument.Fields
  myText = fld.Code.Text
  codePos = InStr(myText, "SYMBOL ") + 7
  myCode = Mid(myText, codePos, 3)
  Select Case Val(myCode)
    Case 167: mySuit = "cx"
    Case 168: mySuit = "dx"
    Case 169: mySuit = "hx"
    Case 170: mySuit = "sx"
    Case Else: mySuit = "??????"
  End Select
  fld.Select
  Selection.TypeText Text:=mySuit
Next
End Sub

OT
Sub FetchThisMacro()
' Version 27.01.13
' Find and copy the current macro

stripOff = False

' Find the macro title
Selection.Paragraphs(1).Range.Select
Selection.MoveEnd wdCharacter, -1
myMacroName = Selection
Selection.Start = Selection.End

' Look down to find that macro name
Set rng = ActiveDocument.Content
rng.Start = Selection.End

With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = myMacroName
  .Wrap = False
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = False
  .Execute
End With
' Copy the macro
myMacroStart = rng.Start
rng.Start = rng.End
rng.End = ActiveDocument.Content.End
With rng.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "^pEnd S" & "ub"
  .Wrap = False
  .Forward = True
  .Replacement.Text = ""
  .MatchWildcards = False
  .Execute
End With
rng.Start = myMacroStart
rng.Copy

' Create a new file and paste in the macro
Documents.Add DocumentType:=wdNewBlankDocument
Selection.Paste

If stripOff = False Then Selection.HomeKey Unit:=wdStory: Exit Sub

' Strip off the Sub and End Sub lines
Selection.HomeKey Unit:=wdLine, Extend:=wdExtend
Selection.Delete
Selection.HomeKey Unit:=wdStory
Selection.MoveDown Unit:=wdLine, Count:=1, Extend:=wdExtend
Selection.Delete
Selection.EndKey Unit:=wdStory
Selection.HomeKey Unit:=wdStory, Extend:=wdExtend
End Sub



OT
Sub MarkFinalOnOff()
' Version 14.05.11
' Switch "Mark as final" facility on/off

ActiveDocument.Final = Not (ActiveDocument.Final)
End Sub



OT
Sub URLlinker()
' Version 06.05.12
' Find URLs in the text and link them

myTrack = ActiveDocument.TrackRevisions
ActiveDocument.TrackRevisions = False

' Extra characters at the ends of a URL
' that are not to be included
extraneousChars = ".,)(;[]" & ChrW(8211) & ChrW(8212)

Set rng = ActiveDocument.Range
rng.Start = Selection.Start

With rng.Find
  .Text = "[wh][wt][wt][p.]*."
  .Replacement.Text = ""
  .Wrap = wdFindContinue
  .Forward = True
  .MatchWildcards = True
  .Execute
End With
findLen = Len(rng)

Do While rng.Find.Found = True
  Set rng2 = ActiveDocument.Content
  rng2.End = rng.End
  With rng2.Find
    .ClearFormatting
    .Replacement.ClearFormatting
    .Text = "[^13 " & ChrW(8212) & "]"
    .Wrap = wdFindContinue
    .Replacement.Text = ""
    .Forward = False
    .MatchWildcards = True
    .Execute
  End With
  rng.Start = rng2.Start + 1
  With rng2.Find
    .Text = "[^13 ]"
    .Wrap = wdFindContinue
    .Forward = True
    .MatchWildcards = True
    .Replacement.Text = ""
    .Execute
  End With
  rng.End = rng2.Start
  rng.MoveEndWhile cset:=extraneousChars, Count:=wdBackward
  rng.MoveStartWhile cset:=extraneousChars, Count:=wdForward
  lenURL = Len(rng)
  If lenURL > 0 Then ActiveDocument.Hyperlinks.Add Anchor:=rng, Address:=rng, TextToDisplay:=rng
  rng.Start = rng.End + lenURL + findLen
  With rng.Find
    .Text = "[wh][wt][wt][p.]*."
    .Replacement.Text = ""
    .Wrap = False
    .Forward = True
    .MatchWildcards = True
    .Execute
  End With
  findLen = Len(rng)
 
  hereNow = rng.Start
  If hereNow = hereWas Then
    MsgBox "Sorry, I've got stuck!"
    Exit Sub
  End If
  hereWas = rng.Start
Loop
Beep
rng.Select
ActiveDocument.TrackRevisions = myTrack
End Sub


OT
Sub SaveWithPrompt()
' Version 01.04.13
' Prompt the user about saving the file

myCheckPrompt = False
checkLength = True
lenWords = 5000
timeLimitBeep = 1.1

myTime = Timer
If myCheckPrompt = True Then
  myResponse = MsgBox("Save?", vbQuestion + vbYesNo)
  If myResponse <> vbYes Then Exit Sub
End If
winState = ActiveDocument.ActiveWindow.WindowState
If checkLength = True Then
  If ActiveDocument.Words.Count > lenWords Then
    If winState = 1 Then
      ActiveDocument.ActiveWindow.WindowState = 0
    Else
      ActiveDocument.ActiveWindow.WindowState = 1
    End If
  End If
End If
ActiveDocument.Save
ActiveDocument.ActiveWindow.WindowState = winState
If Timer - myTime > timeLimitBeep Then Beep
End Sub




