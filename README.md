# README #

Paul Beverley publishes a set of Word macros that are very useful for the proofreading an copy editing community including my wife.

The macros are made available here: http://www.archivepub.co.uk/book.html

Unfortuntely we have run into problems when installing them and I often have to make some small changes when installing them on my wife's machine. 

Hopefully putting these under version control may help others.
