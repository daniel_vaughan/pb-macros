Attribute VB_Name = "PB_CiteCheck"
Sub CiteFileLink()
' Version 18.04.12
' Link text and references files for CiteCheck
Set textDoc = ActiveDocument

Application.GoBack
Set refsDoc = ActiveDocument

' We are now in the references
varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "textDocName" Then varExists = True
  If v.Name = "refsDocName" Then v.Delete
Next v
If varExists = False Then
  ActiveDocument.Variables.Add "textDocName", textDoc.Name
Else
  ActiveDocument.Variables("textDocName") = textDoc.Name
End If

' Now go to the text file
textDoc.Activate
varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "refsDocName" Then varExists = True
  If v.Name = "textDocName" Then v.Delete
Next v
If varExists = False Then
  ActiveDocument.Variables.Add "refsDocName", refsDoc.Name
Else
  ActiveDocument.Variables("refsDocName") = refsDoc.Name
End If

myResponse = MsgBox("Text = " & textDoc.Name & "    References = " & refsDoc.Name, _
     vbQuestion + vbYesNoCancel)
If myResponse = vbCancel Then Exit Sub
If myResponse = vbNo Then
  myResponse = MsgBox("Text = " & refsDoc.Name & "    References = " & textDoc.Name, _
       vbQuestion + vbYesNoCancel)
  If myResponse = vbCancel Then Exit Sub
  If myResponse = vbYes Then
    textDoc.Activate
    ' We are now in the references
    varExists = False
    For Each v In ActiveDocument.Variables
      If v.Name = "textDocName" Then varExists = True
      If v.Name = "refsDocName" Then v.Delete
    Next v
    If varExists = False Then
      ActiveDocument.Variables.Add "textDocName", refsDoc.Name
    Else
      ActiveDocument.Variables("textDocName") = refsDoc.Name
    End If
    refsDoc.Activate
    ' We are now in the text
    varExists = False
    For Each v In ActiveDocument.Variables
      If v.Name = "refsDocName" Then varExists = True
      If v.Name = "textDocName" Then v.Delete
    Next v
    If varExists = False Then
      ActiveDocument.Variables.Add "refsDocName", textDoc.Name
    Else
      ActiveDocument.Variables("refsDocName") = textDoc.Name
    End If
  End If
End If
' Prepare the Find to look for dates
With Selection.Find
  .ClearFormatting
  .Highlight = False
  .Replacement.ClearFormatting
  .Text = "[0-9]{4}[a-k\);:,]"
  .Replacement.Text = ""
  .Forward = True
  .Wrap = False
  .MatchWildcards = True
  .Execute
End With
End Sub




Sub CiteFileLink2()
' Version 25.06.12
' Link text and references files for CiteCheck

notTheseFiles = "zzSwitchList,zzFReditList,AllTextWord"

gottaRefs = False
gottaText = False
Do Until gottaRefs = True And gottaText = True
  For Each myWnd In Application.Windows
    myWnd.Document.Activate
    thisFile = Replace(myWnd.Document.Name, ".docx", "")
    thisFile = Replace(myWnd.Document.Name, ".doc", "")
    If InStr(notTheseFiles, thisFile) = 0 Then
      thisOne = InputBox("Text or Refs?", "CiteFileLink")
      If Len(thisOne) > 0 Then
        If Asc(LCase(thisOne)) = Asc("r") Then
          Set refsDoc = myWnd.Document
          gottaRefs = True
        End If
        If Asc(LCase(thisOne)) = Asc("t") Then
          Set textDoc = myWnd.Document
          gottaText = True
        End If
      End If
    End If
    If gottaRefs = True And gottaText = True Then Exit For
  Next myWnd
Loop

refsDoc.Activate
varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "textDocName" Then varExists = True
  If v.Name = "refsDocName" Then v.Delete
Next v
If varExists = False Then
  ActiveDocument.Variables.Add "textDocName", textDoc.Name
Else
  ActiveDocument.Variables("textDocName") = textDoc.Name
End If

textDoc.Activate
varExists = False
For Each v In ActiveDocument.Variables
  If v.Name = "refsDocName" Then varExists = True
  If v.Name = "textDocName" Then v.Delete
Next v
If varExists = False Then
  ActiveDocument.Variables.Add "refsDocName", refsDoc.Name
Else
  ActiveDocument.Variables("refsDocName") = refsDoc.Name
End If

' Prepare the Find to look for dates
With Selection.Find
  .ClearFormatting
  .Highlight = False
  .Replacement.ClearFormatting
  .Text = "[0-9]{4}[a-k\);:,]"
  .Replacement.Text = ""
  .Forward = True
  .Wrap = False
  .MatchWildcards = True
  .Execute
End With
Selection.HomeKey Unit:=wdStory
Beep
End Sub


Sub CiteFindName()
' Version 21.10.11
' Jump back from date to name

beepIFand = True

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = "[A-Z]"
  .Wrap = wdFindContinue
  .Replacement.Text = ""
  .Forward = False
  .MatchWildcards = True
  .Execute
End With

backHere = Selection.Start
Selection.MoveLeft Unit:=wdCharacter, Count:=3
Selection.Words(1).Select
If Selection = "and " Then
  Selection.MoveLeft Unit:=wdCharacter, Count:=3
  Selection.Words(1).Select
  Selection.End = Selection.Start
  If beepIFand = True Then Beep
Else
  Selection.Start = backHere
  Selection.Words(1).Select
  Selection.End = Selection.Start
End If

With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = oldFind
  .Forward = True
  .Highlight = False
  .Replacement.Text = oldReplace
  .MatchWildcards = True
  .Wrap = wdFindContinue
End With

End Sub


Sub CiteCheck()
' Version 29.11.11
' Check reference citations

moveOnAfterOK = True
citeCheckBackwards = False
wholeName = False
myBestColour = wdYellow
myWarningColour = wdRed
myCitationColour = wdGray25

' If you want a beep warning for a multi-author citation...
addaBeepMulti = False

On Error Resume Next
isRefs = False
isText = False

oldFind = Selection.Find.Text
oldReplace = Selection.Find.Replacement.Text
oldWild = Selection.Find.MatchWildcards
For Each v In ActiveDocument.Variables
  If v.Name = "textDocName" Then isRefs = True: textDoc = v
  If v.Name = "refsDocName" Then isText = True: refsDoc = v
Next v

' if both variables are set, delete them and tell the user
If isText = True And isRefs = True Then
  ActiveDocument.Variables("textDocName").Delete
  ActiveDocument.Variables("refsDocName").Delete
  MsgBox ("Please run the CiteFileLink macro.")
  Exit Sub
End If

' if variables not set, tell the user
If (isText = False And isRefs = False) Then
  MsgBox ("Please run the CiteFileLink macro.")
  Exit Sub
End If

'Remember existing highlight colour
oldColour = Options.DefaultHighlightColorIndex

' Refs doc procedure
If isRefs = True Then
  If Selection.Start = Selection.End Then
  ' if no word is selected, select the word at the cursor
    nameStart = Selection.Start
    Selection.MoveRight Unit:=wdCharacter, Count:=1
    Selection.MoveLeft Unit:=wdWord, Count:=1
    Selection.MoveRight Unit:=wdWord, Count:=1, Extend:=wdExtend
  End If
  myName = Selection
  ' remember whether the selection is highlighted
  thisColour = Selection.Range.HighlightColorIndex
  isHighlight = (thisColour = myWarningColour) Or thisColour > 255
  ' check the character after the word
  Selection.Start = Selection.End
  Selection.MoveEnd , 1
  aHyphen = False
  If Selection = "-" Then
  ' if it's a hyphen remember that and read the following word
    aHyphen = True
    Selection.MoveRight Unit:=wdWord, Count:=1, Extend:=wdExtend
    myName = myName + Selection
  ' now myName is, say ' Hamilton-Smythe'
  Selection.Start = nameStart
  Selection.End = nameStart
  End If
  ' if the name (beginning of the line) is highlighted,
  ' unhighlight all identical author names
  If isHighlight Then
    Selection.HomeKey Unit:=wdLine
    Selection.MoveLeft Unit:=wdWord, Count:=1
    With Selection.Find
      .ClearFormatting
      .Replacement.ClearFormatting
      .Text = "^13" & Trim(myName) & "??"
      .Replacement.Text = ""
      .Replacement.Highlight = False
      .Wrap = wdFindContinue
      .Forward = True
      .MatchWildcards = True
      .Execute Replace:=wdReplaceAll
    End With
  End If
  ' go back to the text
  lookingFor = textDoc
  Application.Windows(textDoc).Activate
  If Err.Number = 5941 Then
    Err.Clear
    textDoc = Replace(textDoc, ".", " [Compatibility Mode].")
    Application.Windows(textDoc).Activate
    If Err.Number = 5941 Then GoTo ReportIt
  Else
    If Err.Number > 0 Then GoTo ReportIt
  End If
  If moveOnAfterOK = True Then
    With Selection.Find
      .ClearFormatting
      .Highlight = False
      .Replacement.ClearFormatting
      .Text = "[0-9]{4}[a-k\);:,]"
      .Replacement.Text = ""
      .Forward = True
      .Wrap = False
      .MatchWildcards = True
      .Execute
    End With
    Exit Sub
  End If
  GoTo Cleanup
End If

' Text doc procedure
' OR, if you're in the text, select the current word
Selection.MoveRight Unit:=wdCharacter, Count:=1
Selection.MoveLeft Unit:=wdWord, Count:=1
Selection.MoveRight Unit:=wdWord, Count:=1, Extend:=wdExtend
myName = Trim(Selection)
' remember where the name starts and ends
nameStart = Selection.Start
nameEnd = Selection.End
' if it says, say 'Brown's (2005) view...'
apostHere = InStr(myName, ChrW(8217))
' get rid of apostrophe
If apostHere > 0 Then
  myName = Left(myName, apostHere - 1)
  nameEnd = nameStart + apostHere - 1
End If
Selection.Start = Selection.End
' check for hyphenated name
Selection.MoveEnd , 1
If Selection = "-" Then
  Selection.MoveRight Unit:=wdWord, Count:=1, Extend:=wdExtend
  myName = myName + Trim(Selection)
  nameEnd = Selection.End
  apostHere = InStr(myName, ChrW(8217))
  ' again get rid of apostrophe
  If apostHere > 0 Then
    myName = Left(myName, apostHere - 1)
    nameEnd = nameStart + apostHere - 1
  End If
End If
' find the next date
Selection.Start = Selection.End
Selection.MoveLeft Unit:=wdWord, Count:=1
With Selection.Find
  .ClearFormatting
  .Text = "[0-9][0-9][0-9][0-9]"
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = True
  .Execute
End With
dateStart = Selection.Start

' check if it's '2005a', '2005b' etc
Selection.Start = Selection.End
Selection.MoveEnd , 1
mySuffix = Selection
If Asc(mySuffix) < 97 Or Asc(mySuffix) > 122 Then
  Selection.MoveEnd , -1
End If
Selection.Start = dateStart
myDate = Selection
dateEnd = Selection.End

' Record whole citation
Selection.Start = nameStart
wholeCitation = Selection
isMulti = False
If InStr(Selection, " and ") > 0 Then isMulti = True
If InStr(Selection, " et al") > 0 Then isMulti = True
Selection.End = Selection.Start


' go to the refs list
' Refs list after Text procedure
lookingFor = refsDoc
Application.Windows(refsDoc).Activate
If Err.Number = 5941 Then
  Err.Clear
  refsDoc = Replace(refsDoc, ".doc", " [Compatibility Mode].doc")
  Application.Windows(refsDoc).Activate
  If Err.Number = 5941 Then GoTo ReportIt
Else
  If Err.Number > 0 Then GoTo ReportIt
End If
If ActiveWindow.WindowState = 2 Then Application.WindowState = 1
Selection.MoveLeft Unit:=wdCharacter, Count:=1
textDoc = ActiveDocument.Variables("textDocName")
' find the name
If citeCheckBackwards = True Then
  crBit = ""
  cr13Bit = ""
Else
  crBit = "^p"
  cr13Bit = "^13"
End If
Selection.HomeKey Unit:=wdStory
With Selection.Find
  .ClearFormatting
  .Text = crBit & myName
  .Replacement.Text = ""
  .Forward = True
  .MatchWildcards = False
  .Execute
End With

' if no such reference exists then
' try to find a name starting with the same
' letters, and then exit
If Selection.Find.Found = False Then
  For i = 1 To Len(myName) - 1
    Beep
    myTime = Timer: Do: Loop Until Timer > myTime + 0.05
    myName = Left(myName, Len(myName) - 1)
    With Selection.Find
      .ClearFormatting
      .Text = crBit & myName
      .Execute
    End With
    If Selection.Find.Found = True Then GoTo Cleanup
  Next
  GoTo Cleanup
End If

' Change highlight colour
Options.DefaultHighlightColorIndex = myBestColour

Do
' if you've found one, is it the right date?
  Selection.MoveStart , 1
  Selection.MoveDown Unit:=wdParagraph, Count:=1, Extend:=wdExtend
  wholeRef = Selection
  myStart = Selection.Start
  myEnd = Selection.End
  With Selection.Find
    .ClearFormatting
    .Text = myDate
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = True
    .Execute
  End With
  gotOne = Selection.Find.Found
  Selection.Find.MatchWildcards = False
  Selection.Start = Selection.End
  If Asc(Selection) > 96 And Asc(Selection) < 123 Then gotOne = False
  If gotOne = True Then
    dateEnd = Selection.End
  ' Check the current highlight colour a few characters to the right
    Selection.Start = Selection.End - 1
    hiColour = Selection.Range.HighlightColorIndex
    Selection.Start = myStart
    Selection.End = myStart
  ' Move selection to top of the screen
    Selection.MoveDown Unit:=wdScreen, Count:=2
    Selection.End = myStart - 1
    Selection.MoveRight Unit:=wdCharacter, Count:=1

    ' Ask if OK?
    Selection.End = dateEnd + 1
    Selection.Range.HighlightColorIndex = myBestColour
    If isMulti = True Then
      If addaBeepMulti = True Then Beep
      Selection.Range.HighlightColorIndex = myWarningColour
    End If
    Selection.End = Selection.Start
    myResponse = MsgBox("OK?", vbQuestion + vbYesNo)
    If myResponse = vbNo Then
      Selection.MoveDown Unit:=wdParagraph, Count:=1, Extend:=wdExtend
      Selection.Range.HighlightColorIndex = hiColour
      Selection.End = Selection.Start
      GoTo Cleanup
    Else
    ' Unhighlight reference
      Selection.MoveDown Unit:=wdParagraph, Count:=1, Extend:=wdExtend
      Selection.Range.HighlightColorIndex = wdNoHighlight
      Selection.End = Selection.Start
   
    ' Go back to text and grey all identical citations
      lookingFor = textDoc
      Application.Windows(textDoc).Activate
      If Err.Number = 5941 Then
        Err.Clear
        textDoc = Replace(textDoc, ".", " [Compatibility Mode].")
        Application.Windows(textDoc).Activate
        If Err.Number = 5941 Then GoTo ReportIt
      Else
        If Err.Number > 0 Then GoTo ReportIt
      End If
      Options.DefaultHighlightColorIndex = myCitationColour
      nowTrack = ActiveDocument.TrackRevisions
      ActiveDocument.TrackRevisions = False
      Selection.Start = Selection.End
      Set rng = ActiveDocument.Range
      With rng.Find
        .ClearFormatting
        .Replacement.ClearFormatting
        .Text = wholeCitation
        .Replacement.Text = "^&"
        .Replacement.Highlight = True
        .Wrap = wdFindContinue
        .Execute Replace:=wdReplaceAll
      End With
      ActiveDocument.TrackRevisions = nowTrack
      Options.DefaultHighlightColorIndex = oldColour
      If moveOnAfterOK = True Then
        With Selection.Find
          .ClearFormatting
          .Highlight = False
          .Replacement.ClearFormatting
          .Text = "[0-9]{4}[a-k\);:,]"
          .Replacement.Text = ""
          .Forward = True
          .Wrap = False
          .MatchWildcards = True
          .Execute
        End With
        Exit Sub
      End If
    End If
    GoTo Cleanup
  End If
  ' but if not the right date, look for next name
  Selection.Start = Selection.End
  Selection.MoveLeft Unit:=wdCharacter, Count:=1
  With Selection.Find
    .ClearFormatting
    .Text = crBit & myName
    .Replacement.Text = ""
    .Forward = True
    .MatchWildcards = False
    .Execute
  End With
  nameStart = Selection.Start + 1
' keep looking until no such date can be found
Loop Until Selection.Find.Found = False

' if no such date for that author, highlight all
' like names, but first choose your favourite
' highlighting colour
Selection.HomeKey Unit:=wdStory
Beep
' find the first such name
Options.DefaultHighlightColorIndex = myWarningColour
'Set rng = ActiveDocument.Range
With Selection.Find
  .ClearFormatting
  If wholeName = True Then
    .Text = cr13Bit & myName & "[!a-z]"
  Else
    .Text = cr13Bit & myName
  End If
  .Replacement.Text = ""
  .Replacement.Highlight = True
  .Forward = True
  .MatchWildcards = True
  .Execute
End With
' highlight them all
Selection.Find.Execute Replace:=wdReplaceAll
Selection.MoveStart , 1
Selection.End = Selection.Start
For i = 1 To 3
  Beep
  myTime = Timer: Do: Loop Until Timer > myTime + 0.2
Next
' Tidy up
Options.DefaultHighlightColorIndex = oldColour

Cleanup:
With Selection.Find
  .ClearFormatting
  .Replacement.ClearFormatting
  .Text = oldFind
  .Replacement.Text = oldReplace
  .MatchWildcards = oldWild
End With
Exit Sub

ReportIt:
If Err.Number = 5941 Then
  If lookingFor = textDoc Then
    thisThing = "the text file"
  Else
    thisThing = "the references file"
  End If
  MsgBox ("Is " & thisThing & " loaded?" & vbCrLf & vbCrLf & _
       "If so, please try running CiteFileLink again.")
Else
  MsgBox Err.Description, vbExclamation, "Error from Word"
End If
End Sub
